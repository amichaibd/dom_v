	.file	"power.c"
	.option nopic
	.globl	__mulsi3
	.section	.text.startup.main,"ax",@progbits
	.align	2
	.globl	main
	.type	main, @function
main:
	addi	sp,sp,-32
	sw	s1,20(sp)
	sw	s4,8(sp)
	sw	ra,28(sp)
	sw	s0,24(sp)
	sw	s2,16(sp)
	sw	s3,12(sp)
	li	s1,4096
	li	s4,1
.L2:
	lw	a4,-80(s1)
	beqz	a4,.L2
	sw	zero,-80(s1)
	lw	s3,-256(s1)
	lw	s2,-252(s1)
	mv	a0,s3
	ble	s2,s4,.L3
	li	s0,1
.L4:
	addi	s0,s0,1
	mv	a1,s3
	call	__mulsi3
	bne	s2,s0,.L4
.L3:
	sw	a0,-216(s1)
	sw	s4,-76(s1)
	j	.L2
	.size	main, .-main
	.ident	"GCC: (GNU MCU Eclipse RISC-V Embedded GCC, 64-bits) 7.2.0"
