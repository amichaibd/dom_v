//---------------------------------------------------------
// MEMORY
//    INSTRRAM     : ORIGIN = 0x0000, LENGTH = 0x0800,  #words=512
//    DATARAM      : ORIGIN = 0x0800, LENGTH = 0x0600,  #words=384
//    STACK        : ORIGIN = 0x0E00, LENGTH = 0x0100,  #words=64
//    MMIO_GENERAL : ORIGIN = 0x0F00, LENGTH = 0x0A0 ,  #words=40
//    MMIO_CSR     : ORIGIN = 0x0FA0, LENGTH = 0x020 ,  #words=8
//    MMIO_ER      : ORIGIN = 0x0FC0, LENGTH = 0x020 ,  #words=8
//    MMIO_DRCT_IN : ORIGIN = 0x0FE0, LENGTH = 0x010 ,  #words=4
//    MMIO_DRCT_OUT: ORIGIN = 0x0FF0, LENGTH = 0x010 ,  #words=4
//----------------------------------------------------------

#define MMIO_GENERAL  (*(volatile int (*)[40])(0x00000F00))//
#define MMIO_CSR      (*(volatile int (*)[ 8])(0x00000FA0))//
#define MMIO_DRCT_OUT (*(volatile int (*)[ 1])(0x00000FC0))//
#define MMIO_ER       (*(volatile int (*)[ 2])(0x00000FC4))//
#define MMIO_DRCT_IN  (*(volatile int (*)[ 1])(0x00000FCC))//

void swap(int *xp, int *yp) 
{ 
    int temp = *xp; 
    *xp = *yp; 
    *yp = temp; 
}

void bubbleSort(int arr[], int n) 
{ 
   int i, j; 
   int swapped; 
   for (i = 0; i < n-1; i++) { 
     swapped = 0; 
     for (j = 0; j < n-i-1; j++) { 
        if (arr[j] > arr[j+1]) { 
           swap(&arr[j], &arr[j+1]); 
           swapped = 1; 
        } 
     } 
     // IF no two elements were swapped by inner loop, then break 
     if (swapped == 0) 
        break; 
   } 
} 

int main() {
while (1){
   while(MMIO_CSR[4] == 0){     //CSR[start]
        //busy wait
   }
   MMIO_CSR[4] = 0;            //CSR[start] always reset by DOM_V
   //set # of numbers to sort.
   int n;
   n = MMIO_GENERAL[10];
   int arr[n];
   for(int i=0 ; i<n; i++) 
        arr[i] = MMIO_GENERAL[i];
   bubbleSort(arr, n); 
   for(int i=0 ; i<n; i++) 
        MMIO_GENERAL[i] = arr[i];
   MMIO_CSR[5] = 1; //CSR[Done]
}// while
return 0;
}// main
