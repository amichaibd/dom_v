	.file	"bub_sort.c"
	.option nopic
	.text
	.section	.text.swap,"ax",@progbits
	.align	2
	.globl	swap
	.type	swap, @function
swap:
	lw	a4,0(a1)
	lw	a5,0(a0)
	sw	a4,0(a0)
	sw	a5,0(a1)
	ret
	.size	swap, .-swap
	.section	.text.bubbleSort,"ax",@progbits
	.align	2
	.globl	bubbleSort
	.type	bubbleSort, @function
bubbleSort:
	addi	t4,a1,-1
	mv	a7,t4
	li	t3,0
	blez	t4,.L17
.L15:
	li	t1,0
	li	a3,0
.L4:
	slli	a5,a3,2
	addi	a4,a5,4
	add	a4,a0,a4
	add	a5,a0,a5
	lw	a2,0(a5)
	lw	a6,0(a4)
	ble	a2,a6,.L5
	sw	a6,0(a5)
	sw	a2,0(a4)
	addi	a3,a3,1
	li	t1,1
	bgt	a7,a3,.L4
.L7:
	addi	t3,t3,1
	sub	a7,a1,t3
	addi	a7,a7,-1
	bge	t3,t4,.L3
	bgtz	a7,.L15
.L3:
	ret
.L5:
	addi	a3,a3,1
	bgt	a7,a3,.L4
	bnez	t1,.L7
	ret
.L17:
	ret
	.size	bubbleSort, .-bubbleSort
	.section	.text.startup.main,"ax",@progbits
	.align	2
	.globl	main
	.type	main, @function
main:
	addi	sp,sp,-16
	sw	s0,12(sp)
	addi	s0,sp,16
	li	t4,4096
	li	t6,1
.L19:
	lw	a4,-80(t4)
	beqz	a4,.L19
	sw	zero,-80(t4)
	addi	a2,t4,-256
	lw	t1,40(a2)
	mv	t0,sp
	slli	a5,t1,2
	addi	a5,a5,15
	andi	a5,a5,-16
	sub	sp,sp,a5
	mv	a7,sp
	blez	t1,.L20
	mv	a3,a7
	li	a5,0
.L21:
	slli	a4,a5,2
	add	a4,a2,a4
	lw	a4,0(a4)
	addi	a3,a3,4
	addi	a5,a5,1
	sw	a4,-4(a3)
	bne	t1,a5,.L21
	addi	t5,t1,-1
	mv	a3,t5
	li	a1,0
	beqz	t5,.L30
.L43:
	li	t3,0
	li	a5,0
.L29:
	slli	a4,a5,2
	addi	a2,a4,4
	add	a2,a7,a2
	add	a4,a7,a4
	lw	a0,0(a4)
	lw	a6,0(a2)
	ble	a0,a6,.L23
	sw	a6,0(a4)
	sw	a0,0(a2)
	addi	a5,a5,1
	li	t3,1
	blt	a5,a3,.L29
.L25:
	addi	a1,a1,1
	sub	a3,t1,a1
	addi	a3,a3,-1
	bge	a1,t5,.L30
	bgtz	a3,.L43
.L30:
	li	a5,0
	addi	a2,t4,-256
.L28:
	lw	a3,0(a7)
	slli	a4,a5,2
	add	a4,a2,a4
	sw	a3,0(a4)
	addi	a5,a5,1
	addi	a7,a7,4
	bgt	t1,a5,.L28
.L20:
	sw	t6,-76(t4)
	mv	sp,t0
	j	.L19
.L23:
	addi	a5,a5,1
	blt	a5,a3,.L29
	bnez	t3,.L25
	j	.L30
	.size	main, .-main
	.ident	"GCC: (GNU MCU Eclipse RISC-V Embedded GCC, 64-bit) 8.2.0"
