# Compile C source
echo "Compiling Application $1"
$RISCV_GCC_BIN/$RISCV_XX-gcc   -O3 -march=rv32i -mabi=ilp32  \
-S    -Wextra -Wall  \
-Wno-unused-parameter -Wno-unused-variable -Wno-unused-function \
-fdata-sections -ffunction-sections -fdiagnostics-color=always \
-I./ ./$1.c -o $1.c.s

# Link
 $RISCV_GCC_BIN/$RISCV_XX-gcc  -O3  -march=rv32i -mabi=ilp32  \
  -Wextra -Wall \
-Wno-unused-parameter -Wno-unused-variable -Wno-unused-function \
-fdata-sections -ffunction-sections -fdiagnostics-color=always  \
-L$RISCV_PULP_SW_APPS_REF \
-T$RISCV_PULP_SW_APPS_REF/dom_link.riscv.ld -nostartfiles -Wl,--gc-sections \
-D__riscv__ \
$1.c.s \
$RISCV_PULP_SW_APPS_REF/crt0_dom.S  \
-o $1.elf

# dump object in text
$RISCV_GCC_BIN/$RISCV_XX-objdump  -g -d $1.elf > $1_elf.txt
 
# Convert 
$RISCV_GCC_BIN/$RISCV_XX-objcopy --srec-len 1 --output-target=srec $1.elf $1.s19
$RISCV_GCC_BIN/$RISCV_XX-objcopy --srec-len 1 --output-target=verilog  $1.elf inst_mem.v
$RISCV_PULP_SW_UTILS/s19toslm.py ./$1.s19

# mv memory to dom imput directory
cp inst_mem.v ../../verif/inputs/
