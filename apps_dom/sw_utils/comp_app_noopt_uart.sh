
# Compile C source

echo "Compiling Application $1"


$RISCV_GCC_BIN/$RISCV_XX-gcc -g -ggdb  -march=rv32im -mabi=ilp32  \
-S    -Wextra -Wall  \
-Wno-unused-parameter -Wno-unused-variable -Wno-unused-function \
-fdata-sections -ffunction-sections -fdiagnostics-color=always \
-I$RISCV_PULP_LIBS/sys_lib/inc \
-I$RISCV_PULP_LIBS/string_lib/inc \
-I$RISCV_PULP_LIBS/bench_lib/inc \
-I$RISCV_PULP_LIBS/bm_print_scan_uart \
-I./ ./$1.c -o $1.c.s

# Link


 $RISCV_GCC_BIN/$RISCV_XX-gcc -g -ggdb -march=rv32im -mabi=ilp32  \
  -Wextra -Wall \
-Wno-unused-parameter -Wno-unused-variable -Wno-unused-function \
-fdata-sections -ffunction-sections -fdiagnostics-color=always  \
-I$RISCV_PULP_LIBS/sys_lib/inc \
-I$RISCV_PULP_LIBS/bm_print_scan_uart \
-L$RISCV_PULP_SW_APPS_REF \
-T$RISCV_PULP_SW_APPS_REF/link.riscv.ld -nostartfiles -Wl,--gc-sections \
-D__riscv__ \
$1.c.s \
$RISCV_PULP_SW_APPS_REF/crt0.S  \
$RISCV_PULP_LIBS/bench_lib/bench.c.s \
$RISCV_PULP_LIBS/sys_lib/exceptions.c.s \
$RISCV_PULP_LIBS/sys_lib/gpio.c.s \
$RISCV_PULP_LIBS/sys_lib/i2c.c.s \
$RISCV_PULP_LIBS/sys_lib/int.c.s \
$RISCV_PULP_LIBS/sys_lib/spi.c.s \
$RISCV_PULP_LIBS/sys_lib/timer.c.s \
$RISCV_PULP_LIBS/sys_lib/uart.c.s \
$RISCV_PULP_LIBS/sys_lib/utils.c.s \
$RISCV_PULP_LIBS/bm_print_scan_uart/bm_print_scan_uart.c.s \
-o $1.elf

# dump object in text

$RISCV_GCC_BIN/$RISCV_XX-objdump  -g -d $1.elf > $1_elf.txt
 
# Convert 

$RISCV_GCC_BIN/$RISCV_XX-objcopy  --srec-len 1 --output-target=srec $1.elf $1.s19

$RISCV_PULP_SW_UTILS/s19toslm.py ./$1.s19

cp *.slm ../../sim/irun/slm_files/

# python $RISCV_PULP_SW_UTILS/slm2mif.py l2_stim.slm tcdm_bank0.slm
# cp *.mif $MY_PULP_ENV/fpga_project/


# $RISCV_PULP_SW_UTILS/s19to_loadh.py ./$1.s19  0x00000000 0x00100000


# mv instr_loadh.txt $1_instr_loadh.txt
# mv tcdm_bank0_loadh.txt $1_data_loadh.txt



 
 
