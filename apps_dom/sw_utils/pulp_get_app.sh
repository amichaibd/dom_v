

# gdb/eclipse command file generation

echo "file $MY_PULP_APPS/$1/$1.elf"       >  $RISCV_PULP_SW_UTILS/eclipse_gdb.cmd
echo "set tcp connect-timeout 3000"       >> $RISCV_PULP_SW_UTILS/eclipse_gdb.cmd
echo "set remotetimeout 3000"             >> $RISCV_PULP_SW_UTILS/eclipse_gdb.cmd
echo "target remote :$BRIDGE_DBG_PORT"    >> $RISCV_PULP_SW_UTILS/eclipse_gdb.cmd
echo "set "`echo $`"pc=0x80"              >> $RISCV_PULP_SW_UTILS/eclipse_gdb.cmd
echo "b main"                             >> $RISCV_PULP_SW_UTILS/eclipse_gdb.cmd

echo "set BRIDGE_DUT_PORT \"$BRIDGE_DUT_PORT\""  > set_bridge_var.do

echo "Getting Application $1 for Simulation"

\cp    $MY_PULP_APPS/$1/*.slm  slm_files/
\cp    $MY_PULP_APPS/$1/*.slm  slm_files/

