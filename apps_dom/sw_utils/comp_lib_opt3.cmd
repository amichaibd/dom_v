@echo off

set lib_name=%1

echo Compiling Library %lib_name%

SET RISCV_GCC_BIN=C:\Users\user\AppData\Roaming\"GNU MCU Eclipse"\"RISC-V Embedded GCC"\8.1.0-2-20181019-0952\bin
SET RISCV_PULP_LIBS=..\..\libs
SET RISCV_PULP_SW_APPS_REF=..\ref
SET RISCV_PULP_SW_UTILS=..\sw_utils

%RISCV_GCC_BIN%\riscv-none-embed-gcc   -O3 -march=rv32im  -mabi=ilp32  ^
-S    -Wextra -Wall  ^
-Wno-unused-parameter -Wno-unused-variable -Wno-unused-function ^
-fdata-sections -ffunction-sections -fdiagnostics-color=always ^
-Wimplicit-fallthrough=0 ^
-I%RISCV_PULP_LIBS%\sys_lib\inc ^
-I%RISCV_PULP_LIBS%\string_lib\inc ^
-I%RISCV_PULP_LIBS%\bench_lib\inc ^
-I%RISCV_PULP_LIBS%\iosim_lib\inc ^
-I%RISCV_PULP_LIBS%\bm_print_scan_uart ^
.\%lib_name%.c -o %lib_name%.c.s
