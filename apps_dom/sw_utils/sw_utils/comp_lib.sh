
# Compile C source

echo "Compiling lib $1"

$RISCV_GCC_BIN/$RISCV_XX-gcc -O3 -march=rv32im -mabi=ilp32 \
-S  -Wextra -Wall  \
-Wno-unused-parameter -Wno-unused-variable -Wno-unused-function \
-fdata-sections -ffunction-sections -fdiagnostics-color=always \
-Wimplicit-fallthrough=0 \
-I../sys_lib/inc \
-I../string_lib/inc \
-I../bench_lib/inc \
-I../bm_printf_lib \
-I../uart/inc \
-I../bm_print_scan_uart \
./$1.c -o $1.c.s
