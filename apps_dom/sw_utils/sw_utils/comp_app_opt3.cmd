@echo off

REM  Compile C source

set app_name=%1

echo Compiling Application %app_name%

SET RISCV_GCC_BIN=C:\Users\user\AppData\Roaming\"GNU MCU Eclipse"\"RISC-V Embedded GCC"\8.1.0-2-20181019-0952\bin
SET RISCV_PULP_LIBS=..\libs
SET RISCV_PULP_SW_APPS_REF=..\ref
SET RISCV_PULP_SW_UTILS=..\sw_utils

%RISCV_GCC_BIN%\riscv-none-embed-gcc   -O3 -march=rv32im  -mabi=ilp32  ^
-S    -Wextra -Wall  ^
-Wno-unused-parameter -Wno-unused-variable -Wno-unused-function ^
-fdata-sections -ffunction-sections -fdiagnostics-color=always ^
-I%RISCV_PULP_LIBS%\sys_lib\inc ^
-I%RISCV_PULP_LIBS%\string_lib\inc ^
-I%RISCV_PULP_LIBS%\bench_lib\inc ^
-I%RISCV_PULP_LIBS%\iosim_lib\inc ^
-I%RISCV_PULP_LIBS%\bm_print_scan_uart ^
.\%app_name%.c -o %app_name%.c.s

REM  Link 

%RISCV_GCC_BIN%\riscv-none-embed-gcc  -O3 -march=rv32im -mabi=ilp32  ^
-Wextra -Wall ^
-Wno-unused-parameter -Wno-unused-variable -Wno-unused-function ^
-fdata-sections -ffunction-sections -fdiagnostics-color=always  ^
-I%RISCV_PULP_LIBS%\sys_lib\inc ^
-L%RISCV_PULP_SW_APPS_REF% ^
-T%RISCV_PULP_SW_APPS_REF%\link.riscv.ld -nostartfiles -Wl,--gc-sections ^
-D__riscv__ ^
%app_name%.c.s ^
%RISCV_PULP_SW_APPS_REF%\crt0.S ^
..\libs\bench_lib\bench.c.s ^
..\libs\sys_lib\exceptions.c.s ^
..\libs\sys_lib\gpio.c.s ^
..\libs\sys_lib\i2c.c.s ^
..\libs\sys_lib\int.c.s ^
..\libs\sys_lib\spi.c.s ^
..\libs\sys_lib\timer.c.s ^
..\libs\sys_lib\uart.c.s ^
..\libs\sys_lib\utils.c.s ^
..\libs\bm_print_scan_uart\bm_print_scan_uart.c.s ^
-o %app_name%.elf
 
REM  dump object in text


%RISCV_GCC_BIN%\riscv-none-embed-objdump  -g -d %app_name%.elf > %app_name%.elf_read
 
REM  Convert 

%RISCV_GCC_BIN%\riscv-none-embed-objcopy  --srec-len 1 --output-target=srec %app_name%.elf %app_name%.s19

python %RISCV_PULP_SW_UTILS%\s19toslm.py .\%app_name%.s19
 
copy *.slm          ..\..\sim\modelsim\slm_files\    > comp_log.txt
copy spi_stim.txt   ..\..\sim\modelsim\slm_files\    >> comp_log.txt
 
 
