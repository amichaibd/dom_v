	.file	"prime_factors.c"
	.option nopic
	.text
	.globl	__modsi3
	.globl	__umodsi3
	.section	.text.startup.main,"ax",@progbits
	.align	2
	.globl	main
	.type	main, @function
main:
	addi	sp,sp,-48
	sw	s0,40(sp)
	li	s0,4096
	sw	s4,24(sp)
	sw	s5,20(sp)
	sw	s6,16(sp)
	sw	ra,44(sp)
	sw	s1,36(sp)
	sw	s2,32(sp)
	sw	s3,28(sp)
	sw	s7,12(sp)
	sw	s8,8(sp)
	sw	s9,4(sp)
	addi	s4,s0,-256
	li	s6,5
	li	s5,1
.L2:
	lw	a4,-80(s0)
	beqz	a4,.L2
	sw	zero,-80(s0)
	lw	s7,0(s4)
	ble	s7,s6,.L10
	srai	s9,s7,1
	li	s3,0
	li	s2,2
.L8:
	mv	a1,s2
	mv	a0,s7
	call	__modsi3
	beqz	a0,.L22
.L4:
	addi	s2,s2,1
	bgt	s9,s2,.L8
	addi	a5,s3,10
.L3:
	slli	a5,a5,2
	add	a5,s4,a5
	sw	zero,0(a5)
	sw	s5,-76(s0)
	j	.L2
.L10:
	li	a5,10
	j	.L3
.L22:
	srai	s8,s2,1
	beq	s8,s5,.L5
	andi	a5,s2,1
	beqz	a5,.L4
	addi	s8,s8,1
	li	s1,2
	j	.L6
.L7:
	call	__modsi3
	beqz	a0,.L4
.L6:
	addi	s1,s1,1
	mv	a0,s2
	mv	a1,s1
	bne	s8,s1,.L7
.L5:
	addi	a5,s3,10
	slli	a5,a5,2
	add	a5,s4,a5
	sw	s2,0(a5)
	addi	s3,s3,1
	j	.L4
	.size	main, .-main
	.ident	"GCC: (GNU MCU Eclipse RISC-V Embedded GCC, 64-bit) 8.2.0"
