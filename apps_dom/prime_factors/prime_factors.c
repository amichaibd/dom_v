//---------------------------------------------------------
// MEMORY
//    INSTRRAM     : ORIGIN = 0x0000, LENGTH = 0x0800,  #words=512
//    DATARAM      : ORIGIN = 0x0800, LENGTH = 0x0600,  #words=384
//    STACK        : ORIGIN = 0x0E00, LENGTH = 0x0100,  #words=64
//    MMIO_GENERAL : ORIGIN = 0x0F00, LENGTH = 0x0A0 ,  #words=40
//    MMIO_CSR     : ORIGIN = 0x0FA0, LENGTH = 0x020 ,  #words=8
//    MMIO_ER      : ORIGIN = 0x0FC0, LENGTH = 0x020 ,  #words=8
//    MMIO_DRCT_IN : ORIGIN = 0x0FE0, LENGTH = 0x010 ,  #words=4
//    MMIO_DRCT_OUT: ORIGIN = 0x0FF0, LENGTH = 0x010 ,  #words=4
//----------------------------------------------------------
#define MMIO_GENERAL  (*(volatile int (*)[40])(0x00000F00))//
#define MMIO_CSR      (*(volatile int (*)[ 8])(0x00000FA0))//
#define MMIO_DRCT_OUT (*(volatile int (*)[ 1])(0x00000FC0))//
#define MMIO_ER       (*(volatile int (*)[ 2])(0x00000FC4))//
#define MMIO_DRCT_IN  (*(volatile int (*)[ 1])(0x00000FCC))//

int main() {
 
int i, j, num;
int isPrime;
int offset;

while (1){
   isPrime = 0;
   offset = 0;
   while(MMIO_CSR[4] == 0){     //CSR[start]
        //busy wait
    }
    MMIO_CSR[4] = 0;            //CSR[start] always reset by DOM_V
    num = MMIO_GENERAL[0];      //input a number from pulpenix
    
    for(i=2; i<num/2; i++){      //find its prime factors
        if(num%i==0) {
            isPrime = 1;
            for(j=2; j<=i/2; j++) {
                if(i%j==0) {
                    isPrime =0;
                    break;
                }
            }//for(j=2; j<i/2; l++)
            if(isPrime==1){
                MMIO_GENERAL[10+offset] = i; //results
                offset++;
            }
        }//if(num%i==0)
    }//for(i=2; i<=num; i++)
    MMIO_GENERAL[10+offset] = 0; //make sure last mmio is 0 to indicate no more primes.
    MMIO_CSR[5] = 1; //CSR[Done]
}// while
return 0;
}// main
