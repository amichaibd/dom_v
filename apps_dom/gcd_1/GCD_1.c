//---------------------------------------------------------
// MEMORY
//    INSTRRAM     : ORIGIN = 0x0000, LENGTH = 0x0800,  #words=512
//    DATARAM      : ORIGIN = 0x0800, LENGTH = 0x0600,  #words=384
//    STACK        : ORIGIN = 0x0E00, LENGTH = 0x0100,  #words=64
//    MMIO_GENERAL : ORIGIN = 0x0F00, LENGTH = 0x0A0 ,  #words=40
//    MMIO_CSR     : ORIGIN = 0x0FA0, LENGTH = 0x020 ,  #words=8
//    MMIO_ER      : ORIGIN = 0x0FC0, LENGTH = 0x020 ,  #words=8
//    MMIO_DRCT_IN : ORIGIN = 0x0FE0, LENGTH = 0x010 ,  #words=4
//    MMIO_DRCT_OUT: ORIGIN = 0x0FF0, LENGTH = 0x010 ,  #words=4
//----------------------------------------------------------

#define MMIO_GENERAL  (*(volatile int (*)[40])(0x00000F00))//
#define MMIO_CSR      (*(volatile int (*)[ 8])(0x00000FA0))//
#define MMIO_DRCT_OUT (*(volatile int (*)[ 4])(0x00000FC0))//
#define MMIO_ER       (*(volatile int (*)[ 8])(0x00000FD0))//
#define MMIO_DRCT_IN  (*(volatile int (*)[ 4])(0x00000FF0))//
int main() {
 
int a, b;
while (1){
   while(MMIO_CSR[4] == 0){ //CSR[start]
        //busy wait
    }
    MMIO_CSR[4] = 0; //CSR[start] always be reset by DOM_V
    a = MMIO_GENERAL[0];
    b = MMIO_GENERAL[1];
    while(a!=b) {
        if(a > b)
            a = a-b;
        else
            b = b-a;
    }
    MMIO_GENERAL[10] = a; //results
    MMIO_CSR[5] = 1; //CSR[Done]
}
return 0;
}
