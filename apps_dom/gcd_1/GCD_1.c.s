	.file	"GCD_1.c"
	.option nopic
	.section	.text.startup.main,"ax",@progbits
	.align	2
	.globl	main
	.type	main, @function
main:
	li	a3,4096
	li	a2,1
.L2:
	lw	a4,-80(a3)
	beqz	a4,.L2
	sw	zero,-80(a3)
	lw	a5,-256(a3)
	lw	a4,-252(a3)
	beq	a5,a4,.L4
.L3:
	bge	a4,a5,.L5
	sub	a5,a5,a4
	bne	a4,a5,.L3
.L4:
	sw	a5,-216(a3)
	sw	a2,-76(a3)
	j	.L2
.L5:
	sub	a4,a4,a5
	bne	a4,a5,.L3
	sw	a5,-216(a3)
	sw	a2,-76(a3)
	j	.L2
	.size	main, .-main
	.ident	"GCC: (GNU MCU Eclipse RISC-V Embedded GCC, 64-bits) 7.2.0"
