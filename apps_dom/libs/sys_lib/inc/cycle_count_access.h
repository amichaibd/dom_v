

//  TIME STAMPIMG MACROS

static int start_cycle_count , end_cycle_count ;

//inline int get_cycle_count() {
 int get_cycle_count() {
   int cycle_count;
   csrr(0x780,cycle_count);
   return cycle_count ;
}; 

#define ENABLE_CYCLE_COUNT  csrw(0x7A0,1) ; // Enable Cycle counter CSR 

#define RESET_CYCLE_COUNT   csrw(0x780,0) ; // Reset  Cycle counter CSR 

#define GET_CYCLE_COUNT  get_cycle_count() ; // Return cycle count CSR value
