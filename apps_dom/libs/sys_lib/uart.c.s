	.file	"uart.c"
	.option nopic
	.section	.text.uart_set_cfg_default,"ax",@progbits
	.align	2
	.globl	uart_set_cfg_default
	.type	uart_set_cfg_default, @function
uart_set_cfg_default:
	li	a3,437284864
	lw	a4,4(a3)
	li	a5,437256192
	or	a4,a4,2
	sw	a4,4(a3)
	li	a4,131
	sw	a4,12(a5)
	sw	zero,4(a5)
	li	a4,85
	sw	a4,0(a5)
	li	a4,167
	sw	a4,8(a5)
	li	a4,3
	sw	a4,12(a5)
	lw	a4,4(a5)
	and	a4,a4,240
	or	a4,a4,2
	sw	a4,4(a5)
	ret
	.size	uart_set_cfg_default, .-uart_set_cfg_default
	.section	.text.uart_set_cfg,"ax",@progbits
	.align	2
	.globl	uart_set_cfg
	.type	uart_set_cfg, @function
uart_set_cfg:
	li	a3,437284864
	lw	a4,4(a3)
	li	a5,437256192
	or	a4,a4,2
	sw	a4,4(a3)
	li	a4,131
	sw	a4,12(a5)
	srl	a4,a1,8
	sw	a4,4(a5)
	and	a1,a1,0xff
	sw	a1,0(a5)
	li	a4,167
	sw	a4,8(a5)
	li	a4,3
	sw	a4,12(a5)
	lw	a4,4(a5)
	and	a4,a4,240
	or	a4,a4,2
	sw	a4,4(a5)
	ret
	.size	uart_set_cfg, .-uart_set_cfg
	.section	.text.uart_send,"ax",@progbits
	.align	2
	.globl	uart_send
	.type	uart_send, @function
uart_send:
	li	a4,437256192
.L5:
	beqz	a1,.L14
	add	a3,a0,64
.L6:
	lw	a5,20(a4)
	and	a5,a5,32
	beqz	a5,.L6
	add	a0,a0,1
	lbu	a5,-1(a0)
	add	a1,a1,-1
	sw	a5,0(a4)
	beq	a0,a3,.L5
	bnez	a1,.L6
	ret
.L14:
	ret
	.size	uart_send, .-uart_send
	.section	.text.uart_getchar,"ax",@progbits
	.align	2
	.globl	uart_getchar
	.type	uart_getchar, @function
uart_getchar:
	li	a4,437256192
.L16:
	lw	a5,20(a4)
	and	a5,a5,1
	beqz	a5,.L16
	lw	a0,0(a4)
	and	a0,a0,0xff
	ret
	.size	uart_getchar, .-uart_getchar
	.section	.text.uart_sendchar,"ax",@progbits
	.align	2
	.globl	uart_sendchar
	.type	uart_sendchar, @function
uart_sendchar:
	li	a4,437256192
.L20:
	lw	a5,20(a4)
	and	a5,a5,32
	beqz	a5,.L20
	sw	a0,0(a4)
	ret
	.size	uart_sendchar, .-uart_sendchar
	.section	.text.uart_wait_tx_done,"ax",@progbits
	.align	2
	.globl	uart_wait_tx_done
	.type	uart_wait_tx_done, @function
uart_wait_tx_done:
	li	a4,437256192
.L24:
	lw	a5,20(a4)
	and	a5,a5,64
	beqz	a5,.L24
	ret
	.size	uart_wait_tx_done, .-uart_wait_tx_done
	.section	.text.uart_send_gdb_ready_msg,"ax",@progbits
	.align	2
	.globl	uart_send_gdb_ready_msg
	.type	uart_send_gdb_ready_msg, @function
uart_send_gdb_ready_msg:
	lui	a2,%hi(.LC0)
	li	a3,0
	li	a1,8
	addi	a2,a2,%lo(.LC0)
	li	a4,437256192
	li	a6,64
	li	a7,1
.L29:
	lw	a5,20(a4)
	and	a5,a5,32
	beqz	a5,.L29
	add	a2,a2,1
	lbu	a0,-1(a2)
	add	a3,a3,1
	add	a5,a1,-1
	sw	a0,0(a4)
	beq	a3,a6,.L30
	beqz	a5,.L32
	mv	a1,a5
	j	.L29
.L32:
	li	a5,437321728
	li	a4,65536
	sw	a4,0(a5)
	ret
.L30:
	beq	a1,a7,.L32
	li	a3,0
	mv	a1,a5
	j	.L29
	.size	uart_send_gdb_ready_msg, .-uart_send_gdb_ready_msg
	.section	.rodata.uart_send_gdb_ready_msg.str1.4,"aMS",@progbits,1
	.align	2
.LC0:
	.string	"gdb-rdy"
	.ident	"GCC: (GNU) 7.1.1 20170509"
