	.file	"qprintf.c"
	.option nopic
	.section	.text.qprinti.constprop.1,"ax",@progbits
	.align	2
	.type	qprinti.constprop.1, @function
qprinti.constprop.1:
	add	sp,sp,-80
	sw	s1,68(sp)
	sw	s3,60(sp)
	sw	ra,76(sp)
	sw	s0,72(sp)
	sw	s2,64(sp)
	sw	s4,56(sp)
	sw	s5,52(sp)
	mv	s3,a0
	mv	s1,a3
	beqz	a0,.L73
	mv	s2,a2
	beqz	a2,.L12
	li	a3,10
	beq	a1,a3,.L74
.L12:
	sb	zero,47(sp)
	li	a3,16
	li	s2,0
	beq	a1,a3,.L75
.L14:
	lui	a1,%hi(.LANCHOR0)
	li	a2,429498368
	add	s3,sp,47
	addi	a1,a1,%lo(.LANCHOR0)
	add	a2,a2,-1639
.L19:
	mul	a3,a0,a2
	srl	a6,a0,1
	srl	a5,a0,2
	add	a5,a5,a6
	srl	a7,a5,4
	add	a5,a5,a7
	srl	a7,a5,8
	srl	t1,a0,3
	add	a5,a5,a7
	srl	a7,a5,16
	add	a3,a3,a6
	add	a3,a3,t1
	srl	a3,a3,28
	add	a5,a5,a7
	srl	a5,a5,3
	add	a3,a1,a3
	lbu	s0,0(a3)
	sll	a3,a5,2
	add	a3,a3,a5
	add	a0,a0,6
	sll	a3,a3,1
	add	s0,s0,48
	sub	a0,a0,a3
	add	s3,s3,-1
	and	s0,s0,0xff
	srl	a0,a0,4
	sb	s0,0(s3)
	add	a0,a0,a5
	bnez	a0,.L19
	beqz	s2,.L44
	beqz	s1,.L21
	and	s2,a4,2
	mv	a2,s2
	beqz	s2,.L22
	li	a0,45
	sw	a4,12(sp)
	call	uart_sendchar
	lw	a4,12(sp)
	lbu	s0,0(s3)
	add	s1,s1,-1
	mv	s4,s3
	li	s2,1
.L20:
	blez	s1,.L46
	beqz	s0,.L47
	and	a2,a4,2
.L23:
	mv	a5,s4
	li	s3,0
.L27:
	add	a5,a5,1
	lbu	a3,0(a5)
	add	s3,s3,1
	bnez	a3,.L27
	blt	s3,s1,.L28
	li	s5,32
	bnez	a2,.L76
.L30:
	li	s1,0
	li	s3,0
.L37:
	mv	a0,s0
	add	s4,s4,1
	call	uart_sendchar
	lbu	s0,0(s4)
	add	s3,s3,1
	bnez	s0,.L37
.L36:
	blez	s1,.L38
	mv	s0,s1
.L39:
	add	s0,s0,-1
	mv	a0,s5
	call	uart_sendchar
	bnez	s0,.L39
	add	s3,s3,s1
.L38:
	add	s2,s3,s2
.L1:
	lw	ra,76(sp)
	lw	s0,72(sp)
	mv	a0,s2
	lw	s1,68(sp)
	lw	s2,64(sp)
	lw	s3,60(sp)
	lw	s4,56(sp)
	lw	s5,52(sp)
	add	sp,sp,80
	jr	ra
.L75:
	add	s4,sp,47
	li	a2,9
	add	a5,a5,-58
.L40:
	and	a3,a0,15
	add	s0,a5,a3
	add	s0,s0,48
	and	s0,s0,0xff
	add	s4,s4,-1
	bgt	a3,a2,.L70
	add	s0,a3,48
.L70:
	sb	s0,0(s4)
	srl	a0,a0,4
	bnez	a0,.L40
	li	s2,0
	and	a2,a4,2
	bgtz	s1,.L23
.L46:
	li	s5,32
.L71:
	li	s3,0
	bnez	s0,.L37
	j	.L38
.L73:
	li	a5,48
	sh	a5,16(sp)
	blez	a3,.L41
	add	s0,sp,16
	li	a2,1
	mv	a5,s0
	sub	a2,a2,s0
.L4:
	add	a1,a5,a2
	add	a5,a5,1
	lbu	a3,0(a5)
	bnez	a3,.L4
	bgt	s1,a1,.L5
	and	s1,a4,2
	beqz	s1,.L77
	li	s1,0
	li	s4,48
.L3:
	li	a0,48
	j	.L8
.L43:
	mv	s3,s2
.L8:
	add	s0,s0,1
	call	uart_sendchar
	lbu	a0,0(s0)
	add	s2,s3,1
	bnez	a0,.L43
	blez	s1,.L1
	mv	s0,s1
.L11:
	add	s0,s0,-1
	mv	a0,s4
	call	uart_sendchar
	bnez	s0,.L11
	add	s2,s3,s1
	add	s2,s2,1
	j	.L1
.L21:
	li	a5,45
	add	s4,s3,-1
	sb	a5,-1(s3)
.L24:
	li	s0,45
	li	s2,0
	li	s5,32
	li	s3,0
	j	.L37
.L74:
	bltz	a0,.L78
	sb	zero,47(sp)
	li	s2,0
	j	.L14
.L76:
	li	s5,48
	j	.L30
.L41:
	li	s4,32
	add	s0,sp,16
	j	.L3
.L28:
	sub	s3,s1,s3
.L26:
	li	s5,32
	beqz	a2,.L32
	li	s5,48
.L32:
	and	a4,a4,1
	mv	s1,s3
	bnez	a4,.L33
	blez	s3,.L71
.L34:
	add	s1,s1,-1
	mv	a0,s5
	call	uart_sendchar
	bnez	s1,.L34
	bnez	s0,.L37
	j	.L38
.L33:
	li	s3,0
	bnez	s0,.L37
	j	.L36
.L5:
	and	a5,a4,2
	sub	s2,s1,a1
	li	s4,48
	beqz	a5,.L79
.L7:
	and	a4,a4,1
	mv	s1,s2
	bnez	a4,.L3
.L9:
	add	s1,s1,-1
	mv	a0,s4
	call	uart_sendchar
	bnez	s1,.L9
	mv	s3,s2
	j	.L3
.L22:
	li	a5,45
	sb	a5,-1(s3)
	add	s4,s3,-1
	li	s0,45
	bgtz	s1,.L23
	j	.L24
.L79:
	li	s4,32
	j	.L7
.L77:
	li	s3,0
	li	s4,32
	j	.L3
.L47:
	mv	s3,s1
	and	a2,a4,2
	j	.L26
.L44:
	mv	s4,s3
	j	.L20
.L78:
	sub	a0,zero,a0
	sb	zero,47(sp)
	j	.L14
	.size	qprinti.constprop.1, .-qprinti.constprop.1
	.section	.text.putchar,"ax",@progbits
	.align	2
	.globl	putchar
	.type	putchar, @function
putchar:
	add	sp,sp,-16
	sw	s0,8(sp)
	mv	s0,a0
	and	a0,a0,0xff
	sw	ra,12(sp)
	call	uart_sendchar
	mv	a0,s0
	lw	ra,12(sp)
	lw	s0,8(sp)
	add	sp,sp,16
	jr	ra
	.size	putchar, .-putchar
	.section	.text.printf,"ax",@progbits
	.align	2
	.globl	printf
	.type	printf, @function
printf:
	add	sp,sp,-176
	sw	s9,100(sp)
	sw	s10,96(sp)
	sw	ra,140(sp)
	sw	s0,136(sp)
	sw	s1,132(sp)
	sw	s2,128(sp)
	sw	s3,124(sp)
	sw	s4,120(sp)
	sw	s5,116(sp)
	sw	s6,112(sp)
	sw	s7,108(sp)
	sw	s8,104(sp)
	sw	s11,92(sp)
	sw	a1,148(sp)
	sw	a2,152(sp)
	sw	a3,156(sp)
	sw	a4,160(sp)
	sw	a5,164(sp)
	sw	a6,168(sp)
	sw	a7,172(sp)
	mv	s9,a0
	lbu	a0,0(a0)
	add	s10,sp,148
	sw	s10,44(sp)
	beqz	a0,.L186
	li	s3,1
	add	a2,sp,48
	add	s5,sp,40
	sub	a3,s3,s5
	sub	a5,s3,a2
	sw	a2,12(sp)
	li	s1,0
	li	s2,37
	li	s4,45
	sw	a3,16(sp)
	sw	a5,20(sp)
	j	.L184
.L84:
	call	uart_sendchar
	mv	a5,s7
	add	s1,s1,1
	mv	s7,s9
	mv	s9,a5
.L109:
	lbu	a0,1(s7)
	beqz	a0,.L82
.L184:
	add	s7,s9,1
	bne	a0,s2,.L84
	lbu	a0,1(s9)
	beqz	a0,.L82
	bne	a0,s2,.L85
	add	a5,s9,2
	mv	s9,s7
	mv	s7,a5
	j	.L84
.L85:
	li	a4,0
	bne	a0,s4,.L86
	lbu	a0,2(s9)
	add	s7,s9,2
	li	a4,1
.L86:
	li	a5,48
	bne	a0,a5,.L188
.L88:
	add	s7,s7,1
	lbu	a0,0(s7)
	or	a4,a4,2
	beq	a0,a5,.L88
	li	a7,2
.L87:
	add	a5,a0,-48
	and	a2,a5,0xff
	li	a3,9
	li	s8,0
	bgtu	a2,a3,.L89
	li	a1,9
.L90:
	add	s7,s7,1
	sll	a3,s8,2
	lbu	a0,0(s7)
	add	a3,a3,s8
	sll	a3,a3,1
	add	s8,a5,a3
	add	a5,a0,-48
	and	a3,a5,0xff
	bleu	a3,a1,.L90
.L89:
	li	a5,115
	add	s9,s7,1
	beq	a0,a5,.L281
	li	a5,100
	beq	a0,a5,.L282
	li	a5,117
	beq	a0,a5,.L283
	li	a5,120
	beq	a0,a5,.L284
	li	a5,88
	beq	a0,a5,.L285
	li	a5,99
	bne	a0,a5,.L109
	lbu	a5,0(s10)
	sb	zero,41(sp)
	add	s10,s10,4
	sb	a5,40(sp)
	beqz	s8,.L168
	beqz	a5,.L214
	lw	a2,16(sp)
	mv	a3,s5
.L170:
	add	s11,a3,a2
	add	a3,a3,1
	lbu	a1,0(a3)
	bnez	a1,.L170
	bgt	s8,s11,.L169
	and	s8,a4,1
	bnez	a7,.L171
	bnez	s8,.L286
	li	s11,0
	li	s0,32
.L172:
	beqz	a5,.L280
.L178:
	mv	s6,s5
.L180:
	mv	a0,a5
	add	s6,s6,1
	call	uart_sendchar
	lbu	a5,0(s6)
	add	s11,s11,1
	bnez	a5,.L180
.L179:
	blez	s8,.L280
	mv	s6,s8
.L182:
	add	s6,s6,-1
	mv	a0,s0
	call	uart_sendchar
	bnez	s6,.L182
	add	a3,s8,s11
	add	s1,s1,a3
	j	.L109
.L186:
	li	s1,0
.L82:
	lw	ra,140(sp)
	lw	s0,136(sp)
	mv	a0,s1
	lw	s2,128(sp)
	lw	s1,132(sp)
	lw	s3,124(sp)
	lw	s4,120(sp)
	lw	s5,116(sp)
	lw	s6,112(sp)
	lw	s7,108(sp)
	lw	s8,104(sp)
	lw	s9,100(sp)
	lw	s10,96(sp)
	lw	s11,92(sp)
	add	sp,sp,176
	jr	ra
.L281:
	lw	s6,0(s10)
	add	s10,s10,4
	beqz	s6,.L92
	lbu	a0,0(s6)
	beqz	s8,.L93
	beqz	a0,.L190
.L185:
	mv	a3,s6
	sub	t3,s3,s6
.L95:
	add	a0,a3,t3
	add	a3,a3,1
	lbu	a1,0(a3)
	bnez	a1,.L95
	bgt	s8,a0,.L94
	and	s0,a4,1
	bnez	a7,.L96
	bnez	s0,.L287
	li	s8,0
	li	s11,32
.L97:
	lbu	a0,0(s6)
	bnez	a0,.L105
.L279:
	add	s1,s1,s8
	j	.L109
.L188:
	li	a7,0
	j	.L87
.L93:
	li	s0,0
	li	s11,32
	beqz	a0,.L109
.L105:
	add	s6,s6,1
	call	uart_sendchar
	lbu	a0,0(s6)
	add	s8,s8,1
	bnez	a0,.L105
.L104:
	blez	s0,.L279
	mv	s6,s0
.L107:
	add	s6,s6,-1
	mv	a0,s11
	call	uart_sendchar
	bnez	s6,.L107
	add	a3,s0,s8
	add	s1,s1,a3
	j	.L109
.L282:
	lw	a0,0(s10)
	li	a5,97
	mv	a3,s8
	li	a2,1
	li	a1,10
	call	qprinti.constprop.1
	add	s10,s10,4
	add	s1,s1,a0
	j	.L109
.L283:
	lw	a0,0(s10)
	li	a5,97
	mv	a3,s8
	li	a2,0
	li	a1,10
	call	qprinti.constprop.1
	add	s10,s10,4
	add	s1,s1,a0
	j	.L109
.L284:
	lw	a1,0(s10)
	add	s10,s10,4
	sw	s10,28(sp)
	beqz	a1,.L288
	sb	zero,79(sp)
	add	s11,sp,79
	li	t3,9
.L126:
	and	a0,a1,15
	add	s10,a0,87
	add	s11,s11,-1
	bgt	a0,t3,.L276
	add	s10,a0,48
.L276:
	sb	s10,0(s11)
	srl	a1,a1,4
	bnez	a1,.L126
	mv	a1,s11
	li	a0,0
	beqz	s8,.L289
.L130:
	add	a1,a1,1
	lbu	t3,0(a1)
	add	a0,a0,1
	bnez	t3,.L130
	bgt	s8,a0,.L131
	and	s0,a4,1
	bnez	a7,.L132
	li	s8,0
	beqz	s0,.L199
	li	a3,32
	sw	a3,24(sp)
.L134:
	mv	s0,s8
	li	s8,0
	j	.L138
.L203:
	mv	s8,a4
.L138:
	mv	a0,s10
	add	s11,s11,1
	call	uart_sendchar
	lbu	s10,0(s11)
	add	a4,s8,1
	bnez	s10,.L203
	mv	s6,s0
	blez	s0,.L290
.L139:
	lw	a0,24(sp)
	add	s6,s6,-1
	call	uart_sendchar
	bnez	s6,.L139
	add	a3,s0,s8
	add	a5,a3,1
	add	s1,s1,a5
	lw	s10,28(sp)
	j	.L109
.L190:
	li	a0,0
.L94:
	sub	s8,s8,a0
	li	s11,32
	beqz	a7,.L100
	li	s11,48
.L100:
	and	s0,a4,1
	bnez	s0,.L101
	mv	s0,s8
	blez	s8,.L291
.L102:
	add	s0,s0,-1
	mv	a0,s11
	call	uart_sendchar
	bnez	s0,.L102
	j	.L97
.L285:
	lw	a1,0(s10)
	add	s10,s10,4
	sw	s10,24(sp)
	beqz	a1,.L292
	sb	zero,79(sp)
	add	s10,sp,79
	li	t3,9
.L154:
	and	a0,a1,15
	add	a3,a0,55
	add	s10,s10,-1
	bgt	a0,t3,.L277
	add	a3,a0,48
.L277:
	sb	a3,0(s10)
	srl	a1,a1,4
	bnez	a1,.L154
	mv	a1,s10
	li	a0,0
	li	s0,0
	li	s11,32
	beqz	s8,.L166
.L158:
	add	a1,a1,1
	lbu	t3,0(a1)
	add	a0,a0,1
	bnez	t3,.L158
	bgt	s8,a0,.L159
	and	s0,a4,1
	bnez	a7,.L160
	li	s8,0
	li	s11,32
	beqz	s0,.L166
.L162:
	mv	s0,s8
	li	s8,0
	j	.L166
.L212:
	mv	s8,a4
.L166:
	mv	a0,a3
	call	uart_sendchar
	add	s10,s10,1
	lbu	a3,0(s10)
	add	a4,s8,1
	bnez	a3,.L212
	mv	s6,s0
	blez	s0,.L293
.L167:
	add	s6,s6,-1
	mv	a0,s11
	call	uart_sendchar
	bnez	s6,.L167
	add	a3,s0,s8
	add	a5,a3,1
	add	s1,s1,a5
	lw	s10,24(sp)
	j	.L109
.L92:
	lui	a3,%hi(.LC0)
	addi	s6,a3,%lo(.LC0)
	bnez	s8,.L185
	li	s0,0
	li	s11,32
	li	a0,40
	j	.L105
.L96:
	bnez	s0,.L294
	li	s8,0
	li	s11,48
	j	.L97
.L101:
	lbu	a0,0(s6)
	mv	s0,s8
	li	s8,0
	bnez	a0,.L105
	j	.L104
.L289:
	li	a5,32
	li	s0,0
	sw	a5,24(sp)
	j	.L138
.L214:
	li	s11,0
.L169:
	sub	s11,s8,s11
	li	s0,32
	beqz	a7,.L175
	li	s0,48
.L175:
	and	a4,a4,1
	mv	s8,s11
	bnez	a4,.L176
	blez	s11,.L295
.L177:
	mv	a0,s0
	sw	a5,8(sp)
	add	s8,s8,-1
	call	uart_sendchar
	lw	a5,8(sp)
	bnez	s8,.L177
	bnez	a5,.L178
	j	.L280
.L278:
	li	s11,0
.L280:
	add	s1,s1,s11
	j	.L109
.L288:
	li	a5,48
	sh	a5,48(sp)
	beqz	s8,.L196
	lw	a2,20(sp)
	add	a5,sp,48
.L115:
	add	a1,a5,a2
	add	a5,a5,1
	lbu	a3,0(a5)
	bnez	a3,.L115
	bgt	s8,a1,.L116
	beqz	a7,.L296
	li	s11,48
	li	s8,0
	li	a7,0
.L114:
	add	s0,sp,48
	li	a0,48
	j	.L122
.L198:
	mv	s8,a5
.L122:
	sw	a7,8(sp)
	add	s0,s0,1
	call	uart_sendchar
	lbu	a0,0(s0)
	add	a5,s8,1
	lw	a7,8(sp)
	bnez	a0,.L198
	beqz	a7,.L123
	mv	s0,a7
.L124:
	mv	a0,s11
	sw	a7,8(sp)
	add	s0,s0,-1
	call	uart_sendchar
	lw	a7,8(sp)
	bnez	s0,.L124
	add	a3,s8,a7
	add	a5,a3,1
	add	s1,s1,a5
	lw	s10,28(sp)
	j	.L109
.L168:
	li	s11,0
	li	s0,32
	bnez	a5,.L178
	j	.L109
.L292:
	li	a5,48
	sh	a5,48(sp)
	beqz	s8,.L205
	lw	a2,20(sp)
	add	a5,sp,48
.L143:
	add	a1,a5,a2
	add	a5,a5,1
	lbu	a3,0(a5)
	bnez	a3,.L143
	bgt	s8,a1,.L144
	beqz	a7,.L297
	li	s11,48
	li	s8,0
	li	a7,0
.L142:
	add	s0,sp,48
	li	a0,48
	j	.L150
.L207:
	mv	s8,a5
.L150:
	sw	a7,8(sp)
	add	s0,s0,1
	call	uart_sendchar
	lbu	a0,0(s0)
	add	a5,s8,1
	lw	a7,8(sp)
	bnez	a0,.L207
	beqz	a7,.L151
	mv	s0,a7
.L152:
	mv	a0,s11
	sw	a7,8(sp)
	add	s0,s0,-1
	call	uart_sendchar
	lw	a7,8(sp)
	bnez	s0,.L152
	add	a3,s8,a7
	add	a5,a3,1
	add	s1,s1,a5
	lw	s10,24(sp)
	j	.L109
.L294:
	lbu	a0,0(s6)
	beqz	a0,.L275
	li	s11,48
	li	s0,0
	li	s8,0
	j	.L105
.L287:
	lbu	a0,0(s6)
	beqz	a0,.L275
	li	s0,0
	li	s8,0
	li	s11,32
	j	.L105
.L131:
	li	a2,32
	sw	a2,24(sp)
	sub	s8,s8,a0
	beqz	a7,.L136
	li	a3,48
	sw	a3,24(sp)
.L136:
	and	s0,a4,1
	bnez	s0,.L134
	mv	s0,s8
	blez	s8,.L202
.L137:
	lw	a0,24(sp)
	add	s0,s0,-1
	call	uart_sendchar
	bnez	s0,.L137
	j	.L138
.L159:
	sub	s8,s8,a0
	li	s11,32
	beqz	a7,.L164
	li	s11,48
.L164:
	and	s0,a4,1
	bnez	s0,.L162
	mv	s0,s8
	blez	s8,.L211
.L165:
	mv	a0,s11
	sw	a3,8(sp)
	add	s0,s0,-1
	call	uart_sendchar
	lw	a3,8(sp)
	bnez	s0,.L165
	j	.L166
.L171:
	bnez	s8,.L298
	li	s11,0
	li	s0,48
	bnez	a5,.L178
	j	.L280
.L132:
	beqz	s0,.L200
	li	a5,48
	sw	a5,24(sp)
	li	s0,0
	li	s8,0
	j	.L138
.L160:
	beqz	s0,.L209
	li	s11,48
	li	s0,0
	li	s8,0
	j	.L166
.L196:
	li	a7,0
	li	s11,32
	j	.L114
.L176:
	li	s11,0
	bnez	a5,.L178
	j	.L179
.L205:
	li	a7,0
	li	s11,32
	j	.L142
.L116:
	sub	s8,s8,a1
	li	s11,48
	bnez	a7,.L120
	li	s11,32
.L120:
	and	s0,a4,1
	mv	a7,s8
	beqz	s0,.L121
	li	s8,0
	j	.L114
.L121:
	add	a7,a7,-1
	mv	a0,s11
	sw	a7,8(sp)
	call	uart_sendchar
	lw	a7,8(sp)
	bnez	a7,.L121
	j	.L114
.L144:
	sub	s8,s8,a1
	li	s11,48
	bnez	a7,.L148
	li	s11,32
.L148:
	and	s0,a4,1
	mv	a7,s8
	beqz	s0,.L149
	li	s8,0
	j	.L142
.L149:
	add	a7,a7,-1
	mv	a0,s11
	sw	a7,8(sp)
	call	uart_sendchar
	lw	a7,8(sp)
	bnez	a7,.L149
	j	.L142
.L275:
	li	s8,0
	add	s1,s1,s8
	j	.L109
.L298:
	beqz	a5,.L278
	li	s0,48
	li	s8,0
	li	s11,0
	j	.L178
.L286:
	beqz	a5,.L278
	li	s8,0
	li	s11,0
	li	s0,32
	j	.L178
.L199:
	li	a2,32
	sw	a2,24(sp)
	j	.L138
.L200:
	li	a3,48
	li	s8,0
	sw	a3,24(sp)
	j	.L138
.L209:
	li	s8,0
	li	s11,48
	j	.L166
.L297:
	li	s8,0
	li	s11,32
	j	.L142
.L296:
	li	s8,0
	li	s11,32
	j	.L114
.L293:
	add	s1,s1,a4
	lw	s10,24(sp)
	j	.L109
.L290:
	add	s1,s1,a4
	lw	s10,28(sp)
	j	.L109
.L123:
	add	s1,s1,a5
	lw	s10,28(sp)
	j	.L109
.L151:
	add	s1,s1,a5
	lw	s10,24(sp)
	j	.L109
.L295:
	li	s11,0
	j	.L172
.L211:
	li	s8,0
	j	.L166
.L291:
	li	s8,0
	j	.L97
.L202:
	li	s8,0
	j	.L138
	.size	printf, .-printf
	.section	.text.puts,"ax",@progbits
	.align	2
	.globl	puts
	.type	puts, @function
puts:
	add	sp,sp,-16
	sw	s0,8(sp)
	sw	ra,12(sp)
	sw	s1,4(sp)
	mv	s0,a0
	lbu	a0,0(a0)
	beqz	a0,.L302
	add	s0,s0,1
	li	s1,0
.L301:
	add	s0,s0,1
	call	uart_sendchar
	lbu	a0,-1(s0)
	add	s1,s1,1
	bnez	a0,.L301
.L300:
	li	a0,10
	call	uart_sendchar
	lw	ra,12(sp)
	lw	s0,8(sp)
	mv	a0,s1
	lw	s1,4(sp)
	add	sp,sp,16
	jr	ra
.L302:
	li	s1,0
	j	.L300
	.size	puts, .-puts
	.section	.text.strcmp,"ax",@progbits
	.align	2
	.globl	strcmp
	.type	strcmp, @function
strcmp:
	lbu	a5,0(a1)
	lbu	a4,0(a0)
	beqz	a5,.L309
	bne	a4,a5,.L306
.L313:
	add	a1,a1,1
	lbu	a5,0(a1)
	add	a3,a0,1
	lbu	a4,1(a0)
	beqz	a5,.L306
	mv	a0,a3
	beq	a4,a5,.L313
.L306:
	sub	a0,a4,a5
	ret
.L309:
	li	a5,0
	j	.L306
	.size	strcmp, .-strcmp
	.section	.text.strcpy,"ax",@progbits
	.align	2
	.globl	strcpy
	.type	strcpy, @function
strcpy:
	lbu	a5,0(a1)
	beqz	a5,.L315
	mv	a4,a0
.L316:
	sb	a5,0(a4)
	add	a1,a1,1
	lbu	a5,0(a1)
	add	a4,a4,1
	bnez	a5,.L316
.L315:
	ret
	.size	strcpy, .-strcpy
	.section	.text.strlen,"ax",@progbits
	.align	2
	.globl	strlen
	.type	strlen, @function
strlen:
	lbu	a4,0(a0)
	mv	a3,a0
	add	a5,a0,1
	beqz	a4,.L324
.L323:
	sub	a0,a5,a3
	add	a5,a5,1
	lbu	a4,-1(a5)
	bnez	a4,.L323
	ret
.L324:
	li	a0,0
	ret
	.size	strlen, .-strlen
	.globl	remu10_table
	.section	.data.remu10_table,"aw",@progbits
	.align	2
	.set	.LANCHOR0,. + 0
	.type	remu10_table, @object
	.size	remu10_table, 16
remu10_table:
	.byte	0
	.byte	1
	.byte	2
	.byte	2
	.byte	3
	.byte	3
	.byte	4
	.byte	5
	.byte	5
	.byte	6
	.byte	7
	.byte	7
	.byte	8
	.byte	8
	.byte	9
	.byte	0
	.section	.rodata.printf.str1.4,"aMS",@progbits,1
	.align	2
.LC0:
	.string	"(null)"
	.ident	"GCC: (GNU) 7.1.1 20170509"
