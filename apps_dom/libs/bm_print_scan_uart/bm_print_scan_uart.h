
int  bm_printf(const char *fmt, ...);

int bm_fopen(char * file_name, char type) ;

int bm_fopen_r(char * file_name) ;

int bm_fopen_w(char * file_name) ;

int  bm_fclose(int file_idx) ;

void pyshell_reset() ;

char bm_gets (char * str_buf) ;

char bm_fgets (int file_id, char * str_buf) ;

char bm_get_line (char * str_buf) ;

char load_memh(char * cmd_str) ;

unsigned int load_run_app(char * cmd_str) ;

int  bm_access_file(int file_id);

int bm_fprintf(int file_id , const char *fmt, ...);

unsigned int hex_str_to_uint (char * s) ;

unsigned int dec_str_to_uint (char * s) ;
   
int scan_str (char * str_in, int start_position, char * str_out) ;

char bm_quit_app() ;

// Added to prevent CLIB printf included in code needed for calls as Illigal instructions etc.
int printf(const char *fmt, ...) ;
