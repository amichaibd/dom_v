	.file	"pulp_prime_factor.c"
	.option nopic
	.text
	.section	.text.write_gpp_reg,"ax",@progbits
	.align	2
	.globl	write_gpp_reg
	.type	write_gpp_reg, @function
write_gpp_reg:
	li	a5,437280768
	add	a0,a0,a5
	sw	a1,0(a0)
	ret
	.size	write_gpp_reg, .-write_gpp_reg
	.section	.text.read_gpp_reg,"ax",@progbits
	.align	2
	.globl	read_gpp_reg
	.type	read_gpp_reg, @function
read_gpp_reg:
	li	a5,437280768
	add	a0,a0,a5
	lw	a0,0(a0)
	ret
	.size	read_gpp_reg, .-read_gpp_reg
	.section	.text.get_factors,"ax",@progbits
	.align	2
	.globl	get_factors
	.type	get_factors, @function
get_factors:
	addi	sp,sp,-16
	sw	s0,8(sp)
	mv	a1,a0
	mv	s0,a0
	lui	a0,%hi(.LC0)
	addi	a0,a0,%lo(.LC0)
	sw	ra,12(sp)
	sw	s1,4(sp)
	sw	s2,0(sp)
	call	bm_printf
	li	a5,437284864
	sw	s0,-256(a5)
	li	a4,1
	sw	a4,-80(a5)
.L5:
	lw	a4,-76(a5)
	beqz	a4,.L5
	sw	zero,-76(a5)
	lw	a2,-216(a5)
	bnez	a2,.L13
	lw	s0,8(sp)
	lw	ra,12(sp)
	lw	s1,4(sp)
	lw	s2,0(sp)
	lui	a0,%hi(.LC2)
	addi	a0,a0,%lo(.LC2)
	addi	sp,sp,16
	tail	bm_printf
.L13:
	li	s1,437284864
	li	s0,0
	lui	s2,%hi(.LC1)
	addi	s1,s1,-212
.L7:
	srai	a1,s0,2
	addi	a0,s2,%lo(.LC1)
	call	bm_printf
	add	a5,s0,s1
	lw	a2,0(a5)
	addi	s0,s0,4
	bnez	a2,.L7
	lw	ra,12(sp)
	lw	s0,8(sp)
	lw	s1,4(sp)
	lw	s2,0(sp)
	addi	sp,sp,16
	jr	ra
	.size	get_factors, .-get_factors
	.section	.text.startup.main,"ax",@progbits
	.align	2
	.globl	main
	.type	main, @function
main:
	lui	a0,%hi(.LC3)
	addi	sp,sp,-16
	addi	a0,a0,%lo(.LC3)
	sw	ra,12(sp)
	sw	s0,8(sp)
	call	bm_printf
	li	a5,437284864
	li	a4,1
	sw	a4,-92(a5)
	sw	zero,-92(a5)
	sw	zero,-80(a5)
	sw	zero,-84(a5)
	lui	s0,%hi(.LC4)
	sw	a4,-96(a5)
	addi	a0,s0,%lo(.LC4)
	call	bm_printf
	li	a0,999424
	addi	a0,a0,433
	call	get_factors
	addi	a0,s0,%lo(.LC4)
	call	bm_printf
	li	a0,233472
	addi	a0,a0,1095
	call	get_factors
	call	bm_quit_app
	lw	ra,12(sp)
	lw	s0,8(sp)
	li	a0,0
	addi	sp,sp,16
	jr	ra
	.size	main, .-main
	.section	.rodata.get_factors.str1.4,"aMS",@progbits,1
	.align	2
.LC0:
	.string	"\nfactors of %d: \n"
	.zero	2
.LC1:
	.string	"[%d]:%d \n"
	.zero	2
.LC2:
	.string	"===is a prime===\n\n"
	.section	.rodata.main.str1.4,"aMS",@progbits,1
	.align	2
.LC3:
	.string	"\n\n***testing Factor Accelerator***\n"
.LC4:
	.string	"\n--------------\n"
	.ident	"GCC: (GNU MCU Eclipse RISC-V Embedded GCC, 64-bit) 8.2.0"
