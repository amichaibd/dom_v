	.file	"bm_print_scan_uart.c"
	.option nopic
	.text
	.section	.text.number,"ax",@progbits
	.align	2
	.type	number, @function
number:
	addi	sp,sp,-160
	sw	s1,148(sp)
	sw	s4,136(sp)
	sw	ra,156(sp)
	sw	s0,152(sp)
	sw	s2,144(sp)
	sw	s3,140(sp)
	sw	s5,132(sp)
	sw	s6,128(sp)
	sw	s7,124(sp)
	sw	s8,120(sp)
	sw	s9,116(sp)
	sw	s10,112(sp)
	sw	s11,108(sp)
	andi	a6,a5,64
	mv	s1,a0
	mv	s4,a2
	beqz	a6,.L28
	lui	a7,%hi(.LC1)
	addi	a7,a7,%lo(.LC1)
.L2:
	andi	s10,a5,16
	bnez	s10,.L48
	andi	a2,a5,1
	andi	t3,a5,17
	li	s8,48
	beqz	a2,.L46
	andi	a2,a5,2
	andi	s11,a5,32
	beqz	a2,.L29
.L56:
	bltz	a1,.L49
	andi	a2,a5,4
	bnez	a2,.L50
	andi	a5,a5,8
	li	t1,0
	beqz	a5,.L4
	addi	a3,a3,-1
	li	t1,32
.L4:
	beqz	s11,.L9
.L6:
	li	a5,16
	beq	s4,a5,.L51
	addi	a5,s4,-8
	seqz	a5,a5
	sub	a3,a3,a5
.L9:
	bnez	a1,.L7
	li	a5,48
	sb	a5,28(sp)
	li	s2,0
	li	s9,1
	li	s3,1
	addi	s0,sp,28
.L11:
	mv	s5,s9
	bge	s9,a4,.L13
	mv	s5,a4
.L13:
	sub	s6,a3,s5
	addi	s7,s6,-1
	bnez	t3,.L14
	blez	s6,.L52
	mv	a2,s6
	mv	a0,s1
	li	a1,32
	sw	t1,12(sp)
	call	memset
	lw	t1,12(sp)
	add	s1,s1,s6
	li	s7,-2
	li	s6,-1
.L14:
	beqz	t1,.L16
	sb	t1,0(s1)
	addi	s1,s1,1
.L16:
	beqz	s11,.L17
	li	a5,8
	beq	s4,a5,.L53
	li	a5,16
	beq	s4,a5,.L54
.L17:
	bnez	s10,.L19
	blez	s6,.L55
	not	s4,s7
	srai	s4,s4,31
	and	s4,s7,s4
	addi	s10,s4,1
	addi	s6,s7,-1
	mv	a0,s1
	mv	a2,s10
	mv	a1,s8
	sub	s6,s6,s4
	call	memset
	add	s1,s1,s10
	addi	s7,s6,-1
.L19:
	bge	s9,s5,.L22
	sub	s5,s5,s9
	mv	a0,s1
	mv	a2,s5
	li	a1,48
	call	memset
	add	s1,s1,s5
.L22:
	add	a2,s0,s2
	mv	a4,s1
	li	a0,1
.L24:
	addi	a4,a4,1
	lbu	a1,0(a2)
	sub	a5,a0,a4
	add	a5,a5,s2
	sb	a1,-1(a4)
	add	a5,s1,a5
	addi	a2,a2,-1
	bgtz	a5,.L24
	add	s1,s1,s3
	blez	s6,.L1
	not	a5,s7
	srai	a5,a5,31
	and	s0,s7,a5
	addi	s0,s0,1
	mv	a0,s1
	mv	a2,s0
	li	a1,32
	call	memset
	add	s1,s1,s0
.L1:
	lw	ra,156(sp)
	lw	s0,152(sp)
	mv	a0,s1
	lw	s2,144(sp)
	lw	s1,148(sp)
	lw	s3,140(sp)
	lw	s4,136(sp)
	lw	s5,132(sp)
	lw	s6,128(sp)
	lw	s7,124(sp)
	lw	s8,120(sp)
	lw	s9,116(sp)
	lw	s10,112(sp)
	lw	s11,108(sp)
	addi	sp,sp,160
	jr	ra
.L48:
	andi	a5,a5,-2
	mv	t3,s10
.L46:
	andi	a2,a5,2
	li	s8,32
	andi	s11,a5,32
	beqz	a2,.L29
	j	.L56
.L28:
	lui	a7,%hi(.LC0)
	addi	a7,a7,%lo(.LC0)
	j	.L2
.L29:
	li	t1,0
	j	.L4
.L49:
	sub	a1,zero,a1
	addi	a3,a3,-1
	li	t1,45
	bnez	s11,.L6
.L7:
	li	s2,0
	addi	s0,sp,28
	j	.L12
.L32:
	mv	s2,s3
.L12:
	remu	a5,a1,s4
	addi	s3,s2,1
	add	a6,s0,s3
	mv	a0,a1
	mv	s9,s3
	add	a5,a7,a5
	lbu	a5,0(a5)
	divu	a1,a1,s4
	sb	a5,-1(a6)
	bgeu	a0,s4,.L32
	j	.L11
.L50:
	addi	a3,a3,-1
	li	t1,43
	j	.L4
.L54:
	li	a5,48
	sb	a5,0(s1)
	li	a5,120
	sb	a5,1(s1)
	addi	s1,s1,2
	j	.L17
.L51:
	addi	a3,a3,-2
	j	.L9
.L53:
	li	a5,48
	sb	a5,0(s1)
	addi	s1,s1,1
	j	.L17
.L55:
	mv	s6,s7
	addi	s7,s7,-1
	j	.L19
.L52:
	addi	a5,s6,-2
	mv	s6,s7
	mv	s7,a5
	j	.L14
	.size	number, .-number
	.section	.text.ee_vsprintf,"ax",@progbits
	.align	2
	.type	ee_vsprintf, @function
ee_vsprintf:
	addi	sp,sp,-112
	sw	s2,96(sp)
	sw	ra,108(sp)
	sw	s0,104(sp)
	sw	s1,100(sp)
	sw	s3,92(sp)
	sw	s4,88(sp)
	sw	s5,84(sp)
	sw	s6,80(sp)
	sw	s7,76(sp)
	sw	s8,72(sp)
	sw	s9,68(sp)
	sw	s10,64(sp)
	sw	s11,60(sp)
	lbu	a5,0(a1)
	mv	s2,a0
	beqz	a5,.L148
	lui	s7,%hi(.L63)
	lui	s4,%hi(.L146)
	lui	s3,%hi(.LC0)
	mv	s0,a1
	mv	s8,a2
	mv	s6,a0
	addi	s7,s7,%lo(.L63)
	addi	s4,s4,%lo(.L146)
	addi	s3,s3,%lo(.LC0)
	lui	s5,%hi(.LC2)
.L144:
	li	a4,37
	beq	a5,a4,.L149
	sb	a5,0(s6)
	lbu	a5,1(s0)
	addi	s6,s6,1
	addi	s0,s0,1
	bnez	a5,.L144
.L141:
	sub	a0,s6,s2
.L58:
	sb	zero,0(s6)
	lw	ra,108(sp)
	lw	s0,104(sp)
	lw	s1,100(sp)
	lw	s2,96(sp)
	lw	s3,92(sp)
	lw	s4,88(sp)
	lw	s5,84(sp)
	lw	s6,80(sp)
	lw	s7,76(sp)
	lw	s8,72(sp)
	lw	s9,68(sp)
	lw	s10,64(sp)
	lw	s11,60(sp)
	addi	sp,sp,112
	jr	ra
.L149:
	lbu	a1,1(s0)
	li	a3,16
	li	a5,0
	addi	a4,a1,-32
	andi	a4,a4,0xff
	addi	s1,s0,1
	bgtu	a4,a3,.L61
.L210:
	slli	a4,a4,2
	add	a4,a4,s7
	lw	a4,0(a4)
	jr	a4
	.section	.rodata.ee_vsprintf,"a",@progbits
	.align	2
	.align	2
.L63:
	.word	.L67
	.word	.L61
	.word	.L61
	.word	.L66
	.word	.L61
	.word	.L61
	.word	.L61
	.word	.L61
	.word	.L61
	.word	.L61
	.word	.L61
	.word	.L65
	.word	.L61
	.word	.L64
	.word	.L61
	.word	.L61
	.word	.L62
	.section	.text.ee_vsprintf
.L62:
	ori	a5,a5,1
	mv	s0,s1
.L215:
	lbu	a1,1(s0)
	addi	s1,s0,1
	addi	a4,a1,-32
	andi	a4,a4,0xff
	bleu	a4,a3,.L210
.L61:
	addi	a4,a1,-48
	andi	a4,a4,0xff
	li	a3,9
	bleu	a4,a3,.L211
	li	a4,42
	li	a3,-1
	beq	a1,a4,.L212
.L71:
	li	a2,46
	li	a4,-1
	beq	a1,a2,.L213
.L72:
	andi	a2,a1,223
	li	a0,76
	beq	a2,a0,.L214
	addi	a2,a1,-65
	andi	a2,a2,0xff
	li	a0,55
	bgtu	a2,a0,.L163
	slli	a2,a2,2
	add	a2,a2,s4
	lw	a2,0(a2)
	jr	a2
	.section	.rodata.ee_vsprintf
	.align	2
	.align	2
.L146:
	.word	.L106
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L164
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L147
	.word	.L163
	.word	.L165
	.word	.L137
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L137
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L163
	.word	.L166
	.word	.L167
	.word	.L163
	.word	.L163
	.word	.L168
	.word	.L163
	.word	.L169
	.word	.L163
	.word	.L163
	.word	.L170
	.section	.text.ee_vsprintf
.L64:
	ori	a5,a5,16
	mv	s0,s1
	j	.L215
.L65:
	ori	a5,a5,4
	mv	s0,s1
	j	.L215
.L66:
	ori	a5,a5,32
	mv	s0,s1
	j	.L215
.L67:
	ori	a5,a5,8
	mv	s0,s1
	j	.L215
.L214:
	lbu	a6,1(s1)
	li	a0,55
	addi	s9,s1,1
	addi	a2,a6,-65
	andi	a2,a2,0xff
	bgtu	a2,a0,.L77
	lui	a0,%hi(.L79)
	slli	a2,a2,2
	addi	a0,a0,%lo(.L79)
	add	a2,a2,a0
	lw	a2,0(a2)
	jr	a2
	.section	.rodata.ee_vsprintf
	.align	2
	.align	2
.L79:
	.word	.L88
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L87
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L86
	.word	.L77
	.word	.L85
	.word	.L84
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L84
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L77
	.word	.L83
	.word	.L82
	.word	.L77
	.word	.L77
	.word	.L81
	.word	.L77
	.word	.L207
	.word	.L77
	.word	.L77
	.word	.L206
	.section	.text.ee_vsprintf
.L213:
	lbu	a1,1(s1)
	li	a2,9
	addi	a0,s1,1
	addi	a4,a1,-48
	andi	a4,a4,0xff
	bleu	a4,a2,.L216
	li	a4,42
	beq	a1,a4,.L217
	mv	s1,a0
	li	a4,0
	j	.L72
.L211:
	li	a3,0
	li	a2,9
.L70:
	slli	a4,a3,2
	add	a3,a4,a3
	addi	s1,s1,1
	slli	a3,a3,1
	add	a3,a3,a1
	lbu	a1,0(s1)
	addi	a3,a3,-48
	addi	a4,a1,-48
	andi	a4,a4,0xff
	bleu	a4,a2,.L70
	j	.L71
.L212:
	lw	a3,0(s8)
	addi	s1,s0,2
	lbu	a1,2(s0)
	addi	s8,s8,4
	bgez	a3,.L71
	sub	a3,zero,a3
	ori	a5,a5,16
	j	.L71
.L164:
	mv	s9,s1
.L87:
	ori	a5,a5,64
.L206:
	li	a2,16
.L80:
	addi	a0,s8,4
.L142:
	lw	a1,0(s8)
	mv	s8,a0
.L143:
	mv	a0,s6
	call	number
	lbu	a5,1(s9)
	mv	s6,a0
	addi	s0,s9,1
	bnez	a5,.L144
	j	.L141
.L169:
	mv	s9,s1
.L207:
	li	a2,10
	j	.L80
.L168:
	mv	s9,s1
.L81:
	lw	s0,0(s8)
	addi	s8,s8,4
	beqz	s0,.L155
	lbu	a2,0(s0)
	beqz	a2,.L94
.L93:
	beqz	a4,.L94
	mv	a2,s0
	j	.L96
.L218:
	sub	a1,a2,a4
	beq	a1,s0,.L95
.L96:
	addi	a2,a2,1
	lbu	a1,0(a2)
	bnez	a1,.L218
.L95:
	andi	a5,a5,16
	sub	s1,a2,s0
	beqz	a5,.L145
.L97:
	blez	s1,.L98
	addi	a4,s6,4
	addi	a2,s0,4
	sltu	a4,s0,a4
	sltu	a2,s6,a2
	addi	a5,s1,-1
	xori	a4,a4,1
	xori	a2,a2,1
	sltiu	a5,a5,9
	or	a4,a4,a2
	xori	a5,a5,1
	and	a5,a5,a4
	beqz	a5,.L99
	or	a5,s6,s0
	andi	a5,a5,3
	bnez	a5,.L99
	andi	a1,s1,-4
	mv	a5,s0
	mv	a4,s6
	add	a1,a1,s0
.L100:
	lw	a2,0(a5)
	addi	a4,a4,4
	addi	a5,a5,4
	sw	a2,-4(a4)
	bne	a1,a5,.L100
	andi	a5,s1,-4
	add	a2,s6,a5
	add	s0,s0,a5
	beq	a5,s1,.L103
	lbu	a1,0(s0)
	addi	a4,a5,1
	sb	a1,0(a2)
	bge	a4,s1,.L103
	lbu	a4,1(s0)
	addi	a5,a5,2
	sb	a4,1(a2)
	ble	s1,a5,.L103
	lbu	a5,2(s0)
	sb	a5,2(a2)
.L103:
	add	s6,s6,s1
.L98:
	addi	s0,s9,1
	ble	a3,s1,.L209
	sub	s1,a3,s1
	mv	a0,s6
	mv	a2,s1
	li	a1,32
	call	memset
	add	s6,s6,s1
.L209:
	lbu	a5,1(s9)
	bnez	a5,.L144
	j	.L141
.L167:
	mv	s9,s1
.L82:
	li	a2,-1
	beq	a3,a2,.L219
.L105:
	lw	a1,0(s8)
	mv	a0,s6
	li	a2,16
	call	number
	lbu	a5,1(s9)
	addi	s8,s8,4
	mv	s6,a0
	addi	s0,s9,1
	bnez	a5,.L144
	j	.L141
.L166:
	mv	s9,s1
.L83:
	li	a2,8
	j	.L80
.L165:
	mv	s9,s1
.L85:
	andi	a5,a5,16
	addi	s1,a3,-1
	beqz	a5,.L220
.L89:
	lw	a5,0(s8)
	addi	a4,s6,1
	addi	s8,s8,4
	sb	a5,0(s6)
	addi	s0,s9,1
	blez	s1,.L208
	mv	a2,s1
	li	a1,32
	mv	a0,a4
	call	memset
	lbu	a5,1(s9)
	add	s6,a0,s1
	bnez	a5,.L144
	j	.L141
.L163:
	mv	s9,s1
	mv	a6,a1
.L77:
	li	a5,37
	addi	a4,s6,1
	beq	a6,a5,.L140
	sb	a5,0(s6)
	lbu	a6,0(s9)
	bnez	a6,.L221
	mv	s6,a4
	j	.L141
.L216:
	li	a4,0
	li	a6,9
.L74:
	slli	a2,a4,2
	add	a4,a2,a4
	addi	a0,a0,1
	slli	a4,a4,1
	add	a4,a4,a1
	lbu	a1,0(a0)
	addi	a4,a4,-48
	addi	a2,a1,-48
	andi	a2,a2,0xff
	bleu	a2,a6,.L74
	mv	s1,a0
	j	.L72
.L217:
	lw	a4,0(s8)
	lbu	a1,2(s1)
	addi	s8,s8,4
	not	a2,a4
	srai	a2,a2,31
	and	a4,a4,a2
	addi	s1,s1,2
	j	.L72
.L88:
	li	a2,108
	lw	a4,0(s8)
	ori	a5,a5,64
	addi	s8,s8,4
	bne	a1,a2,.L158
	lui	a2,%hi(.LC1)
	addi	a2,a2,%lo(.LC1)
.L108:
	lbu	a6,0(a4)
	lbu	a0,1(a4)
	lbu	t3,2(a4)
	lbu	t1,3(a4)
	lbu	a7,4(a4)
	lbu	a1,5(a4)
	srli	t2,a6,4
	srli	t0,a0,4
	srli	t6,t3,4
	srli	t5,t1,4
	srli	t4,a7,4
	andi	a6,a6,15
	andi	a0,a0,15
	andi	t3,t3,15
	andi	t1,t1,15
	andi	a7,a7,15
	srli	a4,a1,4
	add	a6,a2,a6
	add	a0,a2,a0
	add	t2,a2,t2
	add	t0,a2,t0
	add	t6,a2,t6
	add	t3,a2,t3
	add	t5,a2,t5
	add	t1,a2,t1
	add	t4,a2,t4
	add	a7,a2,a7
	lbu	s0,0(a6)
	lbu	s9,0(a0)
	lbu	t2,0(t2)
	lbu	t0,0(t0)
	lbu	t6,0(t6)
	lbu	t3,0(t3)
	lbu	t5,0(t5)
	lbu	t1,0(t1)
	lbu	t4,0(t4)
	lbu	a6,0(a7)
	add	a0,a2,a4
	lbu	a0,0(a0)
	li	a4,58
	andi	a1,a1,15
	sb	s0,25(sp)
	sb	a4,26(sp)
	sb	a4,29(sp)
	sb	a4,32(sp)
	sb	a4,35(sp)
	sb	a4,38(sp)
	sb	t2,24(sp)
	sb	t0,27(sp)
	sb	s9,28(sp)
	sb	t6,30(sp)
	sb	t3,31(sp)
	sb	t5,33(sp)
	sb	t1,34(sp)
	sb	t4,36(sp)
	sb	a6,37(sp)
	add	a4,a2,a1
	sb	a0,39(sp)
	lbu	a4,0(a4)
	andi	a5,a5,16
	addi	s0,a3,-1
	sb	a4,40(sp)
	bnez	a5,.L109
	li	a5,17
	ble	a3,a5,.L222
	addi	s0,a3,-17
	mv	a2,s0
	li	a1,32
	mv	a0,s6
	sw	a3,12(sp)
	call	memset
	li	a2,17
	addi	a1,sp,24
	add	a0,s6,s0
	call	memcpy
	lw	a3,12(sp)
	add	a4,s6,a3
.L111:
	lbu	a5,2(s1)
	addi	s0,s1,2
	mv	s6,a4
	bnez	a5,.L144
	j	.L141
.L86:
	li	a2,108
	lw	a4,0(s8)
	addi	s8,s8,4
	bne	a1,a2,.L158
	mv	a2,s3
	j	.L108
.L84:
	li	a6,108
	ori	a5,a5,2
	addi	a0,s8,4
	li	a2,10
	beq	a1,a6,.L142
	j	.L138
.L170:
	mv	s9,s1
	li	a2,16
	j	.L80
.L147:
	lw	a4,0(s8)
	addi	s8,s8,4
.L107:
	lbu	a2,0(a4)
	beqz	a2,.L113
	li	a1,99
	ble	a2,a1,.L223
	li	a0,100
	rem	t1,a2,a0
	li	a7,10
	li	a1,4
	li	s0,3
	li	a6,2
	div	a2,a2,a0
	div	a0,t1,a7
	add	a2,s3,a2
	lbu	a2,0(a2)
	sb	a2,24(sp)
	add	a2,s3,a0
	lbu	a0,0(a2)
	rem	a2,t1,a7
	sb	a0,25(sp)
.L117:
	add	a2,s3,a2
	lbu	a0,0(a2)
	addi	a2,sp,48
	add	a2,a2,a6
	sb	a0,-24(a2)
	addi	a0,sp,48
	lbu	a2,1(a4)
	add	a0,a0,s0
	li	a6,46
	sb	a6,-24(a0)
	beqz	a2,.L118
.L229:
	li	a6,99
	ble	a2,a6,.L224
	li	a6,100
	li	a7,10
	addi	t3,sp,48
	add	t3,t3,a1
	addi	a1,s0,3
	rem	t1,a2,a6
	div	a2,a2,a6
	div	a6,t1,a7
	add	a2,s3,a2
	lbu	a2,0(a2)
	sb	a2,-24(t3)
	add	a2,s3,a6
	lbu	a6,0(a2)
	rem	a2,t1,a7
	sb	a6,-22(a0)
.L122:
	add	a2,s3,a2
	lbu	a0,0(a2)
	addi	a2,sp,48
	add	a2,a2,a1
	addi	s0,a1,1
	sb	a0,-24(a2)
	addi	a1,sp,48
	lbu	a2,2(a4)
	add	a0,a1,s0
	li	a1,46
	sb	a1,-24(a0)
	addi	a1,s0,1
	beqz	a2,.L123
.L230:
	li	a6,99
	ble	a2,a6,.L225
	li	a6,100
	li	a7,10
	addi	t3,sp,48
	add	t3,t3,a1
	addi	a1,s0,3
	rem	t1,a2,a6
	div	a2,a2,a6
	div	a6,t1,a7
	add	a2,s3,a2
	lbu	a2,0(a2)
	sb	a2,-24(t3)
	add	a2,s3,a6
	lbu	a6,0(a2)
	rem	a2,t1,a7
	sb	a6,-22(a0)
.L127:
	add	a2,s3,a2
	lbu	a0,0(a2)
	addi	a2,sp,48
	add	a2,a2,a1
	addi	s0,a1,1
	sb	a0,-24(a2)
	lbu	a4,3(a4)
	addi	a2,sp,48
	add	a1,a2,s0
	li	a2,46
	sb	a2,-24(a1)
	addi	a2,s0,1
	beqz	a4,.L128
.L231:
	li	a0,99
	ble	a4,a0,.L226
	li	a0,100
	li	a6,10
	addi	t1,sp,48
	add	t1,t1,a2
	addi	a2,s0,3
	rem	a7,a4,a0
	div	a4,a4,a0
	div	a0,a7,a6
	add	a4,s3,a4
	lbu	a4,0(a4)
	sb	a4,-24(t1)
	add	a4,s3,a0
	lbu	a0,0(a4)
	rem	a4,a7,a6
	sb	a0,-22(a1)
.L132:
	add	a4,s3,a4
	lbu	a1,0(a4)
	addi	a4,sp,48
	add	a4,a4,a2
	addi	s0,a2,1
	sb	a1,-24(a4)
.L131:
	andi	a5,a5,16
	addi	s9,a3,-1
	bnez	a5,.L133
	ble	a3,s0,.L227
	sub	s10,a3,s0
	mv	a0,s6
	mv	a2,s10
	li	a1,32
	sw	a3,12(sp)
	call	memset
	lw	a3,12(sp)
	add	s6,s6,s10
	sub	a3,s0,a3
	add	a3,a3,s9
	addi	s9,a3,-1
.L133:
	mv	a0,s6
	mv	a2,s0
	addi	a1,sp,24
	sw	a3,12(sp)
	call	memcpy
	lw	a3,12(sp)
	add	a4,s6,s0
	mv	s6,a4
	ble	a3,s0,.L135
	li	a2,32
	li	a3,1
.L136:
	addi	s6,s6,1
	sub	a5,a3,s6
	add	a5,a5,s9
	sb	a2,-1(s6)
	add	a5,a4,a5
	blt	s0,a5,.L136
.L135:
	lbu	a5,1(s1)
	addi	s0,s1,1
	bnez	a5,.L144
	j	.L141
.L106:
	lw	a4,0(s8)
	ori	a5,a5,64
	addi	s8,s8,4
	j	.L107
.L137:
	ori	a5,a5,2
	addi	a0,s8,4
	mv	s9,s1
.L138:
	lw	a1,0(s8)
	li	a2,10
	mv	s8,a0
	j	.L143
.L221:
	addi	a5,s6,2
	mv	s6,a4
	mv	a4,a5
.L140:
	sb	a6,0(s6)
	addi	s0,s9,1
.L208:
	lbu	a5,1(s9)
	mv	s6,a4
	bnez	a5,.L144
	j	.L141
.L219:
	ori	a5,a5,1
	li	a3,8
	j	.L105
.L155:
	addi	s0,s5,%lo(.LC2)
	j	.L93
.L220:
	blez	s1,.L228
	mv	a2,s1
	mv	a0,s6
	li	a1,32
	add	s6,s6,s1
	call	memset
	li	s1,-1
	j	.L89
.L145:
	addi	s11,a3,-1
	ble	a3,s1,.L156
	sub	s10,a3,s1
	mv	a0,s6
	mv	a2,s10
	li	a1,32
	sw	a3,12(sp)
	call	memset
	lw	a3,12(sp)
	add	s6,s6,s10
	sub	a3,s1,a3
	add	a3,a3,s11
	j	.L97
.L113:
	li	a2,48
	sb	a2,24(sp)
	li	s0,1
	addi	a0,sp,48
	lbu	a2,1(a4)
	add	a0,a0,s0
	li	a6,46
	sb	a6,-24(a0)
	li	a1,2
	bnez	a2,.L229
.L118:
	addi	a2,sp,48
	add	a2,a2,a1
	li	a1,48
	sb	a1,-24(a2)
	addi	s0,s0,2
	addi	a1,sp,48
	lbu	a2,2(a4)
	add	a0,a1,s0
	li	a1,46
	sb	a1,-24(a0)
	addi	a1,s0,1
	bnez	a2,.L230
.L123:
	addi	a2,sp,48
	add	a2,a2,a1
	li	a1,48
	sb	a1,-24(a2)
	addi	s0,s0,2
	addi	a2,sp,48
	lbu	a4,3(a4)
	add	a1,a2,s0
	li	a2,46
	sb	a2,-24(a1)
	addi	a2,s0,1
	bnez	a4,.L231
.L128:
	addi	a4,sp,48
	add	a4,a4,a2
	li	a2,48
	addi	s0,s0,2
	sb	a2,-24(a4)
	j	.L131
.L222:
	addi	a5,a3,-2
	mv	a3,s0
	mv	s0,a5
.L109:
	li	a2,17
	addi	a1,sp,24
	mv	a0,s6
	sw	a3,12(sp)
	call	memcpy
	lw	a3,12(sp)
	li	s9,17
	addi	a4,s6,17
	ble	a3,s9,.L111
	li	a1,32
	li	a2,18
	li	a3,17
.L112:
	addi	a4,a4,1
	sub	a5,a2,a4
	add	a5,s6,a5
	sb	a1,-1(a4)
	add	a5,a5,s0
	bgt	a5,a3,.L112
	j	.L111
.L148:
	mv	s6,a0
	li	a0,0
	j	.L58
.L158:
	mv	s1,s9
	j	.L107
.L226:
	li	a1,9
	ble	a4,a1,.L132
	li	a0,10
	div	a1,a4,a0
	addi	a6,sp,48
	add	a6,a6,a2
	addi	a2,s0,2
	add	a1,s3,a1
	lbu	a1,0(a1)
	rem	a4,a4,a0
	sb	a1,-24(a6)
	j	.L132
.L223:
	li	a1,9
	ble	a2,a1,.L159
	li	a7,10
	div	a0,a2,a7
	li	a1,3
	li	s0,2
	li	a6,1
	add	a0,s3,a0
	lbu	a0,0(a0)
	rem	a2,a2,a7
	sb	a0,24(sp)
	j	.L117
.L225:
	li	a0,9
	ble	a2,a0,.L127
	li	a6,10
	div	a0,a2,a6
	addi	a7,sp,48
	add	a7,a7,a1
	addi	a1,s0,2
	add	a0,s3,a0
	lbu	a0,0(a0)
	rem	a2,a2,a6
	sb	a0,-24(a7)
	j	.L127
.L224:
	li	a0,9
	ble	a2,a0,.L122
	li	a6,10
	div	a0,a2,a6
	addi	a7,sp,48
	add	a7,a7,a1
	addi	a1,s0,2
	add	a0,s3,a0
	lbu	a0,0(a0)
	rem	a2,a2,a6
	sb	a0,-24(a7)
	j	.L122
.L159:
	li	a1,2
	li	s0,1
	li	a6,0
	j	.L117
.L94:
	andi	s1,a5,16
	beqz	s1,.L145
	li	s1,0
	j	.L98
.L228:
	lw	a5,0(s8)
	addi	a4,s6,1
	addi	s8,s8,4
	sb	a5,0(s6)
	addi	s0,s9,1
	j	.L208
.L156:
	mv	a3,s11
	j	.L97
.L99:
	add	a2,s0,s1
	mv	a5,s6
.L102:
	addi	s0,s0,1
	lbu	a4,-1(s0)
	addi	a5,a5,1
	sb	a4,-1(a5)
	bne	s0,a2,.L102
	j	.L103
.L227:
	addi	a5,a3,-2
	mv	a3,s9
	mv	s9,a5
	j	.L133
	.size	ee_vsprintf, .-ee_vsprintf
	.section	.text.uart_sendbuf,"ax",@progbits
	.align	2
	.globl	uart_sendbuf
	.type	uart_sendbuf, @function
uart_sendbuf:
	addi	sp,sp,-16
	sw	s0,8(sp)
	sw	s1,4(sp)
	sw	ra,12(sp)
	mv	s1,a0
	lbu	a0,0(a0)
	li	s0,0
	beqz	a0,.L232
.L234:
	call	uart_sendchar
	addi	s0,s0,1
	add	a5,s1,s0
	lbu	a0,0(a5)
	bnez	a0,.L234
.L232:
	mv	a0,s0
	lw	ra,12(sp)
	lw	s0,8(sp)
	lw	s1,4(sp)
	addi	sp,sp,16
	jr	ra
	.size	uart_sendbuf, .-uart_sendbuf
	.section	.text.bm_printf,"ax",@progbits
	.align	2
	.globl	bm_printf
	.type	bm_printf, @function
bm_printf:
	addi	sp,sp,-1088
	addi	t1,sp,1060
	sw	a1,1060(sp)
	sw	a2,1064(sp)
	mv	a1,a0
	mv	a2,t1
	addi	a0,sp,16
	sw	ra,1052(sp)
	sw	s0,1048(sp)
	sw	s1,1044(sp)
	sw	a3,1068(sp)
	sw	a4,1072(sp)
	sw	a5,1076(sp)
	sw	a6,1080(sp)
	sw	a7,1084(sp)
	sw	t1,12(sp)
	call	ee_vsprintf
	lbu	a0,16(sp)
	beqz	a0,.L241
	addi	s0,sp,16
	li	s1,1
	sub	s1,s1,s0
.L240:
	call	uart_sendchar
	add	a5,s0,s1
	addi	s0,s0,1
	lbu	a0,0(s0)
	bnez	a0,.L240
	lw	ra,1052(sp)
	lw	s0,1048(sp)
	lw	s1,1044(sp)
	mv	a0,a5
	addi	sp,sp,1088
	jr	ra
.L241:
	lw	ra,1052(sp)
	lw	s0,1048(sp)
	li	a5,0
	lw	s1,1044(sp)
	mv	a0,a5
	addi	sp,sp,1088
	jr	ra
	.size	bm_printf, .-bm_printf
	.section	.text.printf,"ax",@progbits
	.align	2
	.globl	printf
	.type	printf, @function
printf:
	addi	sp,sp,-1088
	addi	t1,sp,1060
	sw	a1,1060(sp)
	sw	a2,1064(sp)
	mv	a1,a0
	mv	a2,t1
	addi	a0,sp,16
	sw	ra,1052(sp)
	sw	s0,1048(sp)
	sw	s1,1044(sp)
	sw	a3,1068(sp)
	sw	a4,1072(sp)
	sw	a5,1076(sp)
	sw	a6,1080(sp)
	sw	a7,1084(sp)
	sw	t1,12(sp)
	call	ee_vsprintf
	lbu	a0,16(sp)
	beqz	a0,.L247
	addi	s0,sp,16
	li	s1,1
	sub	s1,s1,s0
.L246:
	call	uart_sendchar
	add	a5,s0,s1
	addi	s0,s0,1
	lbu	a0,0(s0)
	bnez	a0,.L246
	lw	ra,1052(sp)
	lw	s0,1048(sp)
	lw	s1,1044(sp)
	mv	a0,a5
	addi	sp,sp,1088
	jr	ra
.L247:
	lw	ra,1052(sp)
	lw	s0,1048(sp)
	li	a5,0
	lw	s1,1044(sp)
	mv	a0,a5
	addi	sp,sp,1088
	jr	ra
	.size	printf, .-printf
	.section	.text.bm_isspace,"ax",@progbits
	.align	2
	.globl	bm_isspace
	.type	bm_isspace, @function
bm_isspace:
	addi	a5,a0,-9
	andi	a5,a5,0xff
	li	a4,4
	bleu	a5,a4,.L252
	addi	a0,a0,-32
	seqz	a0,a0
	ret
.L252:
	li	a0,1
	ret
	.size	bm_isspace, .-bm_isspace
	.section	.text.bm_gets,"ax",@progbits
	.align	2
	.globl	bm_gets
	.type	bm_gets, @function
bm_gets:
	addi	sp,sp,-32
	sw	s0,24(sp)
	sw	s1,20(sp)
	sw	s2,16(sp)
	sw	s3,12(sp)
	sw	s4,8(sp)
	sw	s5,4(sp)
	sw	s6,0(sp)
	sw	ra,28(sp)
	mv	s5,a0
	sb	zero,0(a0)
	li	s0,0
	li	s1,0
	mv	s2,a0
	li	s3,255
	li	s4,4
	li	s6,32
.L254:
	call	uart_getchar
	addi	a5,a0,-9
	andi	a5,a5,0xff
	beq	a0,s3,.L264
.L258:
	bleu	a5,s4,.L255
	beq	a0,s6,.L255
	sb	a0,0(s2)
	call	uart_getchar
	addi	s0,s0,1
	addi	a5,a0,-9
	li	s1,1
	add	s2,s5,s0
	andi	a5,a5,0xff
	bne	a0,s3,.L258
.L264:
	li	a5,-1
	sb	a5,0(s2)
	li	s1,0
.L262:
	lw	ra,28(sp)
	lw	s0,24(sp)
	mv	a0,s1
	lw	s2,16(sp)
	lw	s1,20(sp)
	lw	s3,12(sp)
	lw	s4,8(sp)
	lw	s5,4(sp)
	lw	s6,0(sp)
	addi	sp,sp,32
	jr	ra
.L255:
	beqz	s1,.L254
	sb	zero,0(s2)
	j	.L262
	.size	bm_gets, .-bm_gets
	.section	.text.hex_str_to_uint,"ax",@progbits
	.align	2
	.globl	hex_str_to_uint
	.type	hex_str_to_uint, @function
hex_str_to_uint:
	lbu	a4,0(a0)
	beqz	a4,.L271
	addi	a2,a0,1
	li	a6,9
	li	a0,0
	li	a7,5
.L270:
	addi	a5,a4,-48
	addi	a3,a4,-65
	andi	a5,a5,0xff
	andi	a3,a3,0xff
	bleu	a5,a6,.L267
	addi	a1,a4,-97
	addi	a5,a4,-55
	andi	a1,a1,0xff
	addi	a4,a4,-87
	andi	a5,a5,0xff
	bleu	a3,a7,.L267
	bgtu	a1,a7,.L269
	andi	a5,a4,0xff
.L267:
	addi	a2,a2,1
	lbu	a4,-1(a2)
	slli	a0,a0,4
	add	a0,a5,a0
	bnez	a4,.L270
	ret
.L269:
	lui	a0,%hi(.LC3)
	addi	sp,sp,-16
	addi	a0,a0,%lo(.LC3)
	sw	ra,12(sp)
	call	bm_printf
	lw	ra,12(sp)
	li	a0,0
	addi	sp,sp,16
	jr	ra
.L271:
	li	a0,0
	ret
	.size	hex_str_to_uint, .-hex_str_to_uint
	.section	.text.dec_str_to_uint,"ax",@progbits
	.align	2
	.globl	dec_str_to_uint
	.type	dec_str_to_uint, @function
dec_str_to_uint:
	lbu	a4,0(a0)
	beqz	a4,.L285
	addi	a4,a4,-48
	andi	a4,a4,0xff
	li	a5,9
	bgtu	a4,a5,.L279
	addi	a3,a0,1
	li	a1,9
	li	a0,0
	j	.L281
.L280:
	addi	a3,a3,1
	bgtu	a4,a1,.L279
.L281:
	lbu	a2,0(a3)
	slli	a5,a0,2
	add	a0,a5,a0
	slli	a0,a0,1
	addi	a5,a2,-48
	add	a0,a4,a0
	andi	a4,a5,0xff
	bnez	a2,.L280
	ret
.L279:
	lui	a0,%hi(.LC4)
	addi	sp,sp,-16
	addi	a0,a0,%lo(.LC4)
	sw	ra,12(sp)
	call	bm_printf
	lw	ra,12(sp)
	li	a0,0
	addi	sp,sp,16
	jr	ra
.L285:
	li	a0,0
	ret
	.size	dec_str_to_uint, .-dec_str_to_uint
	.section	.text.bm_get_line,"ax",@progbits
	.align	2
	.globl	bm_get_line
	.type	bm_get_line, @function
bm_get_line:
	addi	sp,sp,-32
	sw	s0,24(sp)
	sw	s1,20(sp)
	sw	s2,16(sp)
	sw	s3,12(sp)
	sw	ra,28(sp)
	sb	zero,0(a0)
	mv	s0,a0
	li	s1,255
	li	s2,10
	li	s3,13
	j	.L287
.L291:
	beq	a0,s2,.L288
	beq	a0,s3,.L288
	sb	a0,0(s0)
	addi	s0,s0,1
.L287:
	call	uart_getchar
	bne	a0,s1,.L291
	sb	zero,0(s0)
	lw	ra,28(sp)
	lw	s0,24(sp)
	lw	s1,20(sp)
	lw	s2,16(sp)
	lw	s3,12(sp)
	li	a0,0
	addi	sp,sp,32
	jr	ra
.L288:
	sb	zero,0(s0)
	lw	ra,28(sp)
	lw	s0,24(sp)
	lw	s1,20(sp)
	lw	s2,16(sp)
	lw	s3,12(sp)
	li	a0,1
	addi	sp,sp,32
	jr	ra
	.size	bm_get_line, .-bm_get_line
	.section	.text.scan_str,"ax",@progbits
	.align	2
	.globl	scan_str
	.type	scan_str, @function
scan_str:
	li	a5,32
.L294:
	addi	a1,a1,1
	add	a6,a0,a1
	lbu	a4,-1(a6)
	mv	a7,a1
	beq	a4,a5,.L294
	li	a5,0
	li	t1,32
.L296:
	addi	a5,a5,1
	add	a3,a2,a5
	sb	a4,-1(a3)
	add	a1,a6,a5
	beqz	a4,.L300
	lbu	a4,-1(a1)
	add	a0,a7,a5
	bne	a4,t1,.L296
	sb	zero,0(a3)
	ret
.L300:
	li	a0,0
	ret
	.size	scan_str, .-scan_str
	.section	.text.pyshell_reset,"ax",@progbits
	.align	2
	.globl	pyshell_reset
	.type	pyshell_reset, @function
pyshell_reset:
	lui	a0,%hi(.LC5)
	addi	sp,sp,-16
	addi	a0,a0,%lo(.LC5)
	sw	ra,12(sp)
	call	bm_printf
	lw	ra,12(sp)
	addi	sp,sp,16
	tail	uart_getchar
	.size	pyshell_reset, .-pyshell_reset
	.section	.text.bm_fopen,"ax",@progbits
	.align	2
	.globl	bm_fopen
	.type	bm_fopen, @function
bm_fopen:
	mv	a2,a1
	mv	a1,a0
	lui	a0,%hi(.LC6)
	addi	sp,sp,-16
	addi	a0,a0,%lo(.LC6)
	sw	ra,12(sp)
	call	bm_printf
	call	uart_getchar
	lw	ra,12(sp)
	addi	sp,sp,16
	jr	ra
	.size	bm_fopen, .-bm_fopen
	.section	.text.bm_fopen_r,"ax",@progbits
	.align	2
	.globl	bm_fopen_r
	.type	bm_fopen_r, @function
bm_fopen_r:
	mv	a1,a0
	lui	a0,%hi(.LC6)
	addi	sp,sp,-16
	li	a2,114
	addi	a0,a0,%lo(.LC6)
	sw	ra,12(sp)
	call	bm_printf
	call	uart_getchar
	lw	ra,12(sp)
	addi	sp,sp,16
	jr	ra
	.size	bm_fopen_r, .-bm_fopen_r
	.section	.text.bm_fopen_w,"ax",@progbits
	.align	2
	.globl	bm_fopen_w
	.type	bm_fopen_w, @function
bm_fopen_w:
	mv	a1,a0
	lui	a0,%hi(.LC6)
	addi	sp,sp,-16
	li	a2,119
	addi	a0,a0,%lo(.LC6)
	sw	ra,12(sp)
	call	bm_printf
	call	uart_getchar
	lw	ra,12(sp)
	addi	sp,sp,16
	jr	ra
	.size	bm_fopen_w, .-bm_fopen_w
	.section	.text.bm_fclose,"ax",@progbits
	.align	2
	.globl	bm_fclose
	.type	bm_fclose, @function
bm_fclose:
	mv	a1,a0
	lui	a0,%hi(.LC7)
	addi	sp,sp,-16
	addi	a0,a0,%lo(.LC7)
	sw	ra,12(sp)
	call	bm_printf
	call	uart_getchar
	lw	ra,12(sp)
	li	a0,1
	addi	sp,sp,16
	jr	ra
	.size	bm_fclose, .-bm_fclose
	.section	.text.bm_access_file,"ax",@progbits
	.align	2
	.globl	bm_access_file
	.type	bm_access_file, @function
bm_access_file:
	mv	a1,a0
	lui	a0,%hi(.LC8)
	addi	sp,sp,-16
	addi	a0,a0,%lo(.LC8)
	sw	ra,12(sp)
	call	bm_printf
	call	uart_getchar
	lw	ra,12(sp)
	li	a0,1
	addi	sp,sp,16
	jr	ra
	.size	bm_access_file, .-bm_access_file
	.section	.text.load_memh_trans,"ax",@progbits
	.align	2
	.globl	load_memh_trans
	.type	load_memh_trans, @function
load_memh_trans:
	addi	sp,sp,-96
	sw	s2,80(sp)
	mv	s2,a0
	lui	a0,%hi(.LC9)
	sw	s5,68(sp)
	addi	a0,a0,%lo(.LC9)
	mv	s5,a1
	mv	a1,s2
	sw	s0,88(sp)
	sw	s1,84(sp)
	sw	s3,76(sp)
	sw	s4,72(sp)
	sw	ra,92(sp)
	sw	s6,64(sp)
	sw	s7,60(sp)
	sw	s8,56(sp)
	lui	s3,%hi(.LC10)
	call	bm_printf
	li	s1,4
	li	s0,32
	lui	s4,%hi(.LC3)
.L314:
	mv	a1,s2
	addi	a0,s3,%lo(.LC10)
	call	bm_printf
	li	s6,0
	sb	zero,8(sp)
	li	s7,0
	li	s8,255
.L315:
	call	uart_getchar
	addi	a5,a0,-9
	addi	a4,sp,8
	andi	a5,a5,0xff
	add	a4,a4,s6
	beq	a0,s8,.L348
.L319:
	bleu	a5,s1,.L316
	beq	a0,s0,.L316
	sb	a0,0(a4)
	call	uart_getchar
	addi	s6,s6,1
	addi	a5,a0,-9
	addi	a4,sp,8
	li	s7,1
	andi	a5,a5,0xff
	add	a4,a4,s6
	bne	a0,s8,.L319
.L348:
	li	a5,-1
	sb	a5,0(a4)
	call	uart_getchar
	lbu	a5,8(sp)
	li	a4,64
	beq	a5,a4,.L349
.L320:
	li	a4,255
	bne	a5,a4,.L314
	lw	s0,88(sp)
	lw	ra,92(sp)
	lw	s1,84(sp)
	lw	s3,76(sp)
	lw	s4,72(sp)
	lw	s5,68(sp)
	lw	s6,64(sp)
	lw	s7,60(sp)
	lw	s8,56(sp)
	mv	a1,s2
	lw	s2,80(sp)
	lui	a0,%hi(.LC11)
	addi	a0,a0,%lo(.LC11)
	addi	sp,sp,96
	tail	bm_printf
.L316:
	beqz	s7,.L315
	sb	zero,0(a4)
	call	uart_getchar
	lbu	a5,8(sp)
	li	a4,64
	bne	a5,a4,.L320
.L349:
	mv	a1,s2
	addi	a0,s3,%lo(.LC10)
	call	bm_printf
	li	s6,0
	sb	zero,28(sp)
	li	s7,0
	li	s8,255
.L321:
	call	uart_getchar
	addi	a5,a0,-9
	addi	a4,sp,28
	andi	a5,a5,0xff
	add	a4,a4,s6
	beq	a0,s8,.L350
.L325:
	bleu	a5,s1,.L322
	beq	a0,s0,.L322
	sb	a0,0(a4)
	call	uart_getchar
	addi	s6,s6,1
	addi	a5,a0,-9
	addi	a4,sp,28
	li	s7,1
	andi	a5,a5,0xff
	add	a4,a4,s6
	bne	a0,s8,.L325
.L350:
	li	a5,-1
	sb	a5,0(a4)
.L324:
	call	uart_getchar
	lbu	a4,9(sp)
	beqz	a4,.L341
	addi	a1,sp,10
	li	s6,0
	li	a6,9
	li	a0,5
.L336:
	addi	a5,a4,-48
	addi	a3,a4,-65
	andi	a5,a5,0xff
	andi	a3,a3,0xff
	bleu	a5,a6,.L327
	addi	a2,a4,-97
	addi	a5,a4,-55
	andi	a2,a2,0xff
	addi	a4,a4,-87
	andi	a5,a5,0xff
	bleu	a3,a0,.L327
	bgtu	a2,a0,.L329
	andi	a5,a4,0xff
.L327:
	addi	a1,a1,1
	lbu	a4,-1(a1)
	slli	s6,s6,4
	add	s6,a5,s6
	bnez	a4,.L336
.L326:
	lbu	a4,28(sp)
	beqz	a4,.L342
	addi	a0,sp,29
	li	a2,0
	li	a7,9
	li	a6,5
.L335:
	addi	a5,a4,-48
	addi	a3,a4,-65
	andi	a5,a5,0xff
	andi	a3,a3,0xff
	bleu	a5,a7,.L331
	addi	a1,a4,-97
	addi	a5,a4,-55
	andi	a1,a1,0xff
	addi	a4,a4,-87
	andi	a5,a5,0xff
	bleu	a3,a6,.L331
	bgtu	a1,a6,.L333
	andi	a5,a4,0xff
.L331:
	addi	a0,a0,1
	lbu	a4,-1(a0)
	slli	a2,a2,4
	add	a2,a5,a2
	bnez	a4,.L335
.L330:
	add	s6,s5,s6
	sw	a2,0(s6)
	j	.L314
.L322:
	beqz	s7,.L321
	sb	zero,0(a4)
	j	.L324
.L333:
	addi	a0,s4,%lo(.LC3)
	call	bm_printf
	li	a2,0
	add	s6,s5,s6
	sw	a2,0(s6)
	j	.L314
.L329:
	addi	a0,s4,%lo(.LC3)
	call	bm_printf
	li	s6,0
	j	.L326
.L341:
	li	s6,0
	j	.L326
.L342:
	li	a2,0
	j	.L330
	.size	load_memh_trans, .-load_memh_trans
	.section	.text.load_memh,"ax",@progbits
	.align	2
	.globl	load_memh
	.type	load_memh, @function
load_memh:
	addi	sp,sp,-128
	sw	ra,124(sp)
	sw	s0,120(sp)
	sw	s1,116(sp)
	li	a2,0
	li	a5,32
.L352:
	addi	a2,a2,1
	add	a6,a0,a2
	lbu	a4,-1(a6)
	beq	a4,a5,.L352
	li	a5,0
	li	a7,32
.L354:
	addi	a5,a5,1
	addi	a1,sp,8
	add	a1,a1,a5
	sb	a4,-1(a1)
	add	a3,a6,a5
	beqz	a4,.L383
	lbu	a4,-1(a3)
	add	a3,a2,a5
	bne	a4,a7,.L354
	sb	zero,0(a1)
.L355:
	li	a5,32
.L359:
	addi	a3,a3,1
	add	a6,a0,a3
	lbu	a4,-1(a6)
	beq	a4,a5,.L359
	li	a5,0
	li	a7,32
.L358:
	addi	a5,a5,1
	addi	a1,sp,32
	add	a1,a1,a5
	sb	a4,-1(a1)
	add	a2,a6,a5
	beqz	a4,.L384
	lbu	a4,-1(a2)
	add	a2,a3,a5
	bne	a4,a7,.L358
	sb	zero,0(a1)
.L360:
	li	a4,32
.L364:
	addi	a2,a2,1
	add	a1,a0,a2
	lbu	a5,-1(a1)
	beq	a5,a4,.L364
	li	a4,0
	li	a0,32
.L363:
	addi	a4,a4,1
	addi	a3,sp,12
	add	a3,a3,a4
	sb	a5,-1(a3)
	add	a2,a1,a4
	beqz	a5,.L365
	lbu	a5,-1(a2)
	bne	a5,a0,.L363
	sb	zero,0(a3)
.L365:
	lbu	a4,12(sp)
	beqz	a4,.L374
	addi	a0,sp,13
	li	a2,0
	li	a7,9
	li	a6,5
.L371:
	addi	a5,a4,-48
	addi	a3,a4,-65
	andi	a5,a5,0xff
	andi	a3,a3,0xff
	bleu	a5,a7,.L368
	addi	a1,a4,-97
	addi	a5,a4,-55
	andi	a1,a1,0xff
	addi	a4,a4,-87
	andi	a5,a5,0xff
	bleu	a3,a6,.L368
	bgtu	a1,a6,.L370
	andi	a5,a4,0xff
.L368:
	addi	a0,a0,1
	lbu	a4,-1(a0)
	slli	a2,a2,4
	add	a2,a5,a2
	bnez	a4,.L371
	mv	s1,a2
	j	.L367
.L370:
	lui	a0,%hi(.LC3)
	addi	a0,a0,%lo(.LC3)
	call	bm_printf
	li	s1,0
	li	a2,0
.L367:
	lui	a0,%hi(.LC12)
	addi	a1,sp,32
	addi	a0,a0,%lo(.LC12)
	call	bm_printf
	lui	a0,%hi(.LC6)
	li	a2,114
	addi	a1,sp,32
	addi	a0,a0,%lo(.LC6)
	call	bm_printf
	call	uart_getchar
	mv	a1,s1
	mv	s0,a0
	call	load_memh_trans
	lui	a0,%hi(.LC7)
	mv	a1,s0
	addi	a0,a0,%lo(.LC7)
	call	bm_printf
	call	uart_getchar
	lw	ra,124(sp)
	lw	s0,120(sp)
	lw	s1,116(sp)
	li	a0,1
	addi	sp,sp,128
	jr	ra
.L384:
	li	a2,0
	j	.L360
.L383:
	li	a3,0
	j	.L355
.L374:
	li	s1,0
	li	a2,0
	j	.L367
	.size	load_memh, .-load_memh
	.section	.text.load_run_app,"ax",@progbits
	.align	2
	.globl	load_run_app
	.type	load_run_app, @function
load_run_app:
	addi	sp,sp,-160
	sw	ra,156(sp)
	sw	s0,152(sp)
	sw	s1,148(sp)
	sw	s2,144(sp)
	sw	s3,140(sp)
	li	a3,0
	li	a5,32
.L386:
	addi	a3,a3,1
	add	a6,a0,a3
	lbu	a4,-1(a6)
	beq	a4,a5,.L386
	li	a5,0
	li	a7,32
.L388:
	addi	a5,a5,1
	addi	a1,sp,4
	add	a1,a1,a5
	sb	a4,-1(a1)
	add	a2,a6,a5
	beqz	a4,.L401
	lbu	a4,-1(a2)
	add	a2,a3,a5
	bne	a4,a7,.L388
	sb	zero,0(a1)
.L389:
	li	a4,32
.L393:
	addi	a2,a2,1
	add	a1,a0,a2
	lbu	a5,-1(a1)
	beq	a5,a4,.L393
	li	a4,0
	li	a0,32
.L392:
	addi	a4,a4,1
	addi	a3,sp,8
	add	a3,a3,a4
	sb	a5,-1(a3)
	add	a2,a1,a4
	beqz	a5,.L394
	lbu	a5,-1(a2)
	bne	a5,a0,.L392
	sb	zero,0(a3)
.L394:
	lui	a0,%hi(.LC13)
	addi	a1,sp,8
	addi	a0,a0,%lo(.LC13)
	call	bm_printf
	call	uart_getchar
	li	a1,0
	mv	s0,a0
	call	load_memh_trans
	mv	a1,s0
	lui	s0,%hi(.LC7)
	addi	a0,s0,%lo(.LC7)
	call	bm_printf
	call	uart_getchar
	lui	a0,%hi(.LC14)
	addi	a1,sp,8
	addi	a0,a0,%lo(.LC14)
	call	bm_printf
	call	uart_getchar
	li	a1,0
	mv	s1,a0
	call	load_memh_trans
	mv	a1,s1
	addi	a0,s0,%lo(.LC7)
	call	bm_printf
	call	uart_getchar
	li	s2,437256192
	lui	a0,%hi(.LC15)
	addi	a1,sp,8
	lw	s3,28(s2)
	addi	a0,a0,%lo(.LC15)
	sw	zero,28(s2)
	call	bm_printf
	call	uart_getchar
	mv	s0,a0
	call	uart_getchar
	mv	s1,a0
	call	uart_getchar
	slli	a0,a0,16
	slli	s1,s1,8
	add	s1,s1,a0
	andi	s3,s3,255
	call	uart_getchar
	add	s0,s1,s0
	sw	s3,28(s2)
	slli	a0,a0,24
	add	s0,a0,s0
	call	uart_getchar
	lui	a0,%hi(.LC16)
	mv	a1,s0
	addi	a0,a0,%lo(.LC16)
	call	bm_printf
	jalr	s0
	mv	a0,s0
	lw	ra,156(sp)
	lw	s0,152(sp)
	lw	s1,148(sp)
	lw	s2,144(sp)
	lw	s3,140(sp)
	addi	sp,sp,160
	jr	ra
.L401:
	li	a2,0
	j	.L389
	.size	load_run_app, .-load_run_app
	.section	.text.bm_fprintf,"ax",@progbits
	.align	2
	.globl	bm_fprintf
	.type	bm_fprintf, @function
bm_fprintf:
	addi	sp,sp,-1104
	sw	s3,1052(sp)
	lui	s3,%hi(.LC8)
	sw	s0,1064(sp)
	mv	s0,a1
	mv	a1,a0
	addi	a0,s3,%lo(.LC8)
	sw	ra,1068(sp)
	sw	a2,1080(sp)
	sw	a3,1084(sp)
	sw	a4,1088(sp)
	sw	a5,1092(sp)
	sw	a6,1096(sp)
	sw	a7,1100(sp)
	sw	s1,1060(sp)
	sw	s2,1056(sp)
	call	bm_printf
	call	uart_getchar
	addi	a5,sp,1080
	mv	a2,a5
	mv	a1,s0
	addi	a0,sp,16
	sw	a5,12(sp)
	call	ee_vsprintf
	lbu	a0,16(sp)
	beqz	a0,.L405
	addi	s0,sp,16
	li	s1,1
	sub	s1,s1,s0
.L404:
	add	s2,s0,s1
	call	uart_sendchar
	addi	s0,s0,1
	lbu	a0,0(s0)
	bnez	a0,.L404
.L403:
	li	a1,-1
	addi	a0,s3,%lo(.LC8)
	call	bm_printf
	call	uart_getchar
	lw	ra,1068(sp)
	lw	s0,1064(sp)
	mv	a0,s2
	lw	s1,1060(sp)
	lw	s2,1056(sp)
	lw	s3,1052(sp)
	addi	sp,sp,1104
	jr	ra
.L405:
	li	s2,0
	j	.L403
	.size	bm_fprintf, .-bm_fprintf
	.section	.text.bm_fgets,"ax",@progbits
	.align	2
	.globl	bm_fgets
	.type	bm_fgets, @function
bm_fgets:
	addi	sp,sp,-32
	sw	s1,20(sp)
	mv	s1,a1
	mv	a1,a0
	lui	a0,%hi(.LC10)
	addi	a0,a0,%lo(.LC10)
	sw	s0,24(sp)
	sw	s2,16(sp)
	sw	s3,12(sp)
	sw	s4,8(sp)
	sw	s5,4(sp)
	sw	s6,0(sp)
	sw	ra,28(sp)
	li	s2,0
	call	bm_printf
	li	s0,0
	sb	zero,0(s1)
	mv	s3,s1
	li	s6,255
	li	s5,4
	li	s4,32
.L409:
	call	uart_getchar
	addi	a5,a0,-9
	andi	a5,a5,0xff
	beq	a0,s6,.L419
.L413:
	bleu	a5,s5,.L410
	beq	a0,s4,.L410
	sb	a0,0(s3)
	call	uart_getchar
	addi	s2,s2,1
	addi	a5,a0,-9
	li	s0,1
	add	s3,s1,s2
	andi	a5,a5,0xff
	bne	a0,s6,.L413
.L419:
	li	a5,-1
	sb	a5,0(s3)
	li	s0,0
.L412:
	call	uart_getchar
	mv	a0,s0
	lw	ra,28(sp)
	lw	s0,24(sp)
	lw	s1,20(sp)
	lw	s2,16(sp)
	lw	s3,12(sp)
	lw	s4,8(sp)
	lw	s5,4(sp)
	lw	s6,0(sp)
	addi	sp,sp,32
	jr	ra
.L410:
	beqz	s0,.L409
	sb	zero,0(s3)
	j	.L412
	.size	bm_fgets, .-bm_fgets
	.section	.text.bm_quit_app,"ax",@progbits
	.align	2
	.globl	bm_quit_app
	.type	bm_quit_app, @function
bm_quit_app:
	lui	a0,%hi(.LC17)
	addi	sp,sp,-16
	addi	a0,a0,%lo(.LC17)
	sw	ra,12(sp)
	call	bm_printf
	lw	ra,12(sp)
	addi	sp,sp,16
	tail	uart_getchar
	.size	bm_quit_app, .-bm_quit_app
	.section	.rodata.bm_access_file.str1.4,"aMS",@progbits,1
	.align	2
.LC8:
	.string	"$pyshell accessFile(%d)\n"
	.section	.rodata.bm_fclose.str1.4,"aMS",@progbits,1
	.align	2
.LC7:
	.string	"$pyshell closeFile(%d)\n"
	.section	.rodata.bm_fopen.str1.4,"aMS",@progbits,1
	.align	2
.LC6:
	.string	"$pyshell openFile(\"%s\",'%c')\n"
	.section	.rodata.bm_quit_app.str1.4,"aMS",@progbits,1
	.align	2
.LC17:
	.string	"$pyshell quitApp()\n"
	.section	.rodata.dec_str_to_uint.str1.4,"aMS",@progbits,1
	.align	2
.LC4:
	.string	"\nERROR dec_str_to_uint got a non decimal digit char.\n"
	.section	.rodata.ee_vsprintf.str1.4,"aMS",@progbits,1
	.align	2
.LC2:
	.string	"<NULL>"
	.section	.rodata.hex_str_to_uint.str1.4,"aMS",@progbits,1
	.align	2
.LC3:
	.string	"\nERROR reading into hex a non hex char.\n"
	.section	.rodata.load_memh.str1.4,"aMS",@progbits,1
	.align	2
.LC12:
	.string	"Loading file %s to base address , added address offset = %x (hex)\n"
	.section	.rodata.load_memh_trans.str1.4,"aMS",@progbits,1
	.align	2
.LC9:
	.string	"Starting load File %d\n"
	.zero	1
.LC10:
	.string	"$pyshell fgets(%d)\n"
.LC11:
	.string	"Memory File %d Loaded\n"
	.section	.rodata.load_run_app.str1.4,"aMS",@progbits,1
	.align	2
.LC13:
	.string	"$pyshell openFile(\"%s_instr_loadh.txt\",'r')\n"
	.zero	3
.LC14:
	.string	"$pyshell openFile(\"%s_data_loadh.txt\",'r')\n"
.LC15:
	.string	"$pyshell get_appMainAddr(\"%s\")\n"
.LC16:
	.string	"C Message: app_main_addr = %X\n"
	.section	.rodata.number.str1.4,"aMS",@progbits,1
	.align	2
.LC0:
	.string	"0123456789abcdefghijklmnopqrstuvwxyz"
	.zero	3
.LC1:
	.string	"0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ"
	.section	.rodata.pyshell_reset.str1.4,"aMS",@progbits,1
	.align	2
.LC5:
	.string	"$pyshell reset()\n"
	.ident	"GCC: (GNU MCU Eclipse RISC-V Embedded GCC, 64-bit) 8.1.0"
