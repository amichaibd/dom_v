cd $MY_PULP_APPS/libs
source source ../../../misc/pulpenix_setup.sh
cd sys_lib
../../sw_utils/comp_lib_with_src_dir.sh   exceptions
../../sw_utils/comp_lib_with_src_dir.sh   gpio
../../sw_utils/comp_lib_with_src_dir.sh   i2c
../../sw_utils/comp_lib_with_src_dir.sh   int
../../sw_utils/comp_lib_with_src_dir.sh   spi
../../sw_utils/comp_lib_with_src_dir.sh   timer
../../sw_utils/comp_lib_with_src_dir.sh   uart
../../sw_utils/comp_lib_with_src_dir.sh   utils
cd ../bench_lib
../../sw_utils/comp_lib_with_src_dir.sh   bench
cd ../string_lib
../../sw_utils/comp_lib_with_src_dir.sh   qprintf
cd ../bm_print_scan_uart  
../../sw_utils/comp_lib.sh                bm_print_scan_uart
cd ..


