#include <stdio.h>
#include <stdlib.h>
#include <bm_print_scan_uart.h>
#include <uart.h>
//--------------------------------------------------------------------------------------------
typedef enum {FALSE,TRUE} boolean;  // Define boolean type For more readable code
// GPP Registers access functions
#define GPP_BASE_ADDR 0x1A106000  // This is the base address of the registers.
                                  // Each register index is an increment of 4 in address (ass addresses are in bytes)
                                  // Notice Illegal address will return "deadbeef"
//--------------------------------------------------------------------------------------------
void write_gpp_reg (unsigned int gpp_reg_idx, unsigned int gpp_reg_val) { 
  volatile unsigned int * gpp_reg_addr ; // "volatile" guarantee that compiler will not optimize out the physical access.
  gpp_reg_addr =  (volatile unsigned int *)(GPP_BASE_ADDR+(gpp_reg_idx)) ;  
  *gpp_reg_addr  = gpp_reg_val ;
}
//--------------------------------------------------------------------------------------------
unsigned int read_gpp_reg (unsigned int gpp_reg_idx) { 
  volatile unsigned int * gpp_reg_addr ;
  gpp_reg_addr =  (volatile unsigned int *)(GPP_BASE_ADDR+(gpp_reg_idx)) ;
  return *gpp_reg_addr ;
}
//--------------------------------------------------------------------------------------------


void func(int arr[], int n) {
  
}
//============================================================================================
//---------------------MEMORY------------------------
//                start   size     # of words
// Inst_mem       0x000  0x800     512
// Data_mem       0x800  0x600     384
// Stack          0xE00  0x100     64 
// MMIO_general   0xF00  0xA0      40 
// MMIO_CSR       0xFA0  0x20      8  -
// MMIO_drct_out  0xFC0  0x04      1  
// MMIO_ER[pc]    0xFC4  0x04      2  
// MMIO_ER[reg]   0xFC8  0x04      1  
// MMIO_drct_in   0xFCC  0x04      1  
//-----------------usfull addresses----------------------
// CSR[0]       = 0xFA0:  EnPC
// CSR[1]       = 0xFA4:  rstPC
// CSR[2]       = 0xFA8:  reg_rd_ptr
// CSR[3]       = 0xFAC:  gear
// CSR[4]       = 0xFB0:  Start       -> after SOC sets this bit dom should reset it.
// CSR[5]       = 0xFB4:  Done        -> After DOM sets this SOC should reset it.
// MMIO_ER[pc]  = 0xFC4   current PC 
// MMIO_ER[reg] = 0xFC8   register (acording to CSR[2]) 
//---------------------------------------------------
int main() {  // The Main program
  bm_printf("\n***TESTING func***\n") ;   // bare-metal output to terminal
//rst DOM and config the CSR
  write_gpp_reg (0xFA4 , 1) ; // write to CSR[rstPC]
  write_gpp_reg (0xFA4 , 0) ; // write to CSR[rstPC]
  write_gpp_reg (0xFAC , 0) ; // write to CSR[gear] - set the sample cycle in DOM
  write_gpp_reg (0xFA0 , 1) ; // write to CSR[EnPC] - start running instructions

  func(arr, n); 
  
  
  bm_printf("pc=%X //end - back to busy wait\n",pc ); 
  
  bm_quit_app() ; // uart message simulation/emulation in simulation/FPGA environment.
  return 0;
}
