#include <stdio.h>
#include <stdlib.h>
#include <bm_print_scan_uart.h>
#include <uart.h>

//--------------------------------------------------------------------------------------------

typedef enum {FALSE,TRUE} boolean;  // Define boolean type For more readable code

//============================================================================================

// GPP Registers access functions

#define GPP_BASE_ADDR 0x1A106000  // This is the base address of the registers.
                                  // Each register index is an increment of 4 in address (ass addresses are in bytes)
                                  // Notice Illegal address will return "deadbeef"

//--------------------------------------------------------------------------------------------

void write_gpp_reg (unsigned int gpp_reg_idx, unsigned int gpp_reg_val) { 

  volatile unsigned int * gpp_reg_addr ; // "volatile" guarantee that compiler will not optimize out the physical access.
  gpp_reg_addr =  (volatile unsigned int *)(GPP_BASE_ADDR+(gpp_reg_idx*4)) ;  
  *gpp_reg_addr  = gpp_reg_val ;
}

//--------------------------------------------------------------------------------------------

unsigned int read_gpp_reg (unsigned int gpp_reg_idx) { 

  volatile unsigned int * gpp_reg_addr ;
  gpp_reg_addr =  (volatile unsigned int *)(GPP_BASE_ADDR+(gpp_reg_idx*4)) ;
  return *gpp_reg_addr ;
}

//--------------------------------------------------------------------------------------------

// Get value from file

unsigned int get_val_from_file (int file_idx) {

  char op_str[10] ;  // operand string read from file (up to 10 decimal digits)

  bm_fgets (file_idx,op_str) ;         // Get the next string from the file (String are separated by white characters)
  return dec_str_to_uint (op_str) ;  
}

//============================================================================================

// GCD accelerators service functions

//--------------------------------------------------------------------------------------------

void issue_gcd_xop (int op_a , int op_b ) { // Issue the GCD acceleration operation

  write_gpp_reg (0,op_a) ; // Provide 'a' in gpp reg 0 as required by GCD
  write_gpp_reg (1,op_b) ; // Provide 'b' in gpp reg 1 as required by GCD
                           // Notice that writing to reg #1 is also the accelerator trigger to start the calculation,
}

//--------------------------------------------------------------------------------------------

char is_gcd_done () {   // Check if GCD is done

  char gcd_done = (read_gpp_reg(2) ==1) ; // Check value of GCD register 2 , indicating done.
  return gcd_done ;
}

//--------------------------------------------------------------------------------------------

char get_gcd_result() {   // Get the GCD result at 

 return read_gpp_reg (3) ;  // Check value of GCD register 3 , holding GCD results when done.
}

//============================================================================================

void test_get_gcd(int op_a ,int op_b) {
    
  issue_gcd_xop(op_a,op_b) ; // Issue the GCD operation.
  

  // Polling for GCD to be done 
  
  while (!is_gcd_done()) {   // Loop Till GCD is done.
      
    bm_printf("\n\n C program message: Polling till accelerator is done \n\n") ;
    // NOTICE the Program can do here or in a bigger lop other things in parallel while waiting for the result
    // For example above bm_print (which is long over uart) is taking place while the GCD is working in parallel.
    // Also this can be handled by an interrupt, out of scope for this example.    
  }
 
  // Getting the result
  int gcd_result = get_gcd_result() ; 
 
 bm_printf("\n\n C program message: Test result: GCD ( %d, %d ) = %d\n\n",op_a, op_b, gcd_result) ; 
    // Notice you may print with %d decimals, but be aware it takes many cycles as it involved devision.

}

//============================================================================================

int main() {  // The Main program


  unsigned int  gpp_test_val ;
  int gpp_reg_idx ;
  volatile unsigned int * gpp_reg_addr ;

  bm_printf("\n\n*** HELLO WORLD , TESTING GPP GCD ACCELERATOR ***\n\n") ;   // bare-metal output to terminal

  // Checking Some Examples
  
  test_get_gcd (   24   ,    28 ) ;
  test_get_gcd ( 2356   ,  9951 ) ;
  test_get_gcd ( 1563   ,   102 ) ;

   // Finish 
  bm_quit_app() ; // uart message simulation/emulation in simulation/FPGA environment.
 
  return 0;
}
