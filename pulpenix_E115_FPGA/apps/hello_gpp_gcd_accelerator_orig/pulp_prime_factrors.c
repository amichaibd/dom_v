#include <stdio.h>
#include <stdlib.h>
#include <bm_print_scan_uart.h>
#include <uart.h>
//--------------------------------------------------------------------------------------------
typedef enum {FALSE,TRUE} boolean;  // Define boolean type For more readable code
// GPP Registers access functions
#define GPP_BASE_ADDR 0x1A106000  // This is the base address of the registers.
                                  // Each register index is an increment of 4 in address (ass addresses are in bytes)
                                  // Notice Illegal address will return "deadbeef"
//--------------------------------------------------------------------------------------------
void write_gpp_reg (unsigned int gpp_reg_idx, unsigned int gpp_reg_val) { 
  volatile unsigned int * gpp_reg_addr ; // "volatile" guarantee that compiler will not optimize out the physical access.
  gpp_reg_addr =  (volatile unsigned int *)(GPP_BASE_ADDR+(gpp_reg_idx)) ;  
  *gpp_reg_addr  = gpp_reg_val ;
}
//--------------------------------------------------------------------------------------------
unsigned int read_gpp_reg (unsigned int gpp_reg_idx) { 
  volatile unsigned int * gpp_reg_addr ;
  gpp_reg_addr =  (volatile unsigned int *)(GPP_BASE_ADDR+(gpp_reg_idx)) ;
  return *gpp_reg_addr ;
}

void test_get_gcd(int op_a ,int op_b) {
  write_gpp_reg (0xF00,op_a) ; // Provide 'a' in gpp reg 0 as required by GCD
  write_gpp_reg (0xF04,op_b) ; // Provide 'b' in gpp reg 1 as required by GCD
  write_gpp_reg (0xFB0 , 1) ;  // write to CSR[start]
  // Polling for GCD to be done 
  while (read_gpp_reg(0xFB4) ==0) {   // Loop Till GCD is done.
    bm_printf("C program message: Polling till accelerator is done \n") ;
  }
  write_gpp_reg (0xFB4 , 0) ; // write to CSR[Done]
  // Getting the result
  int gcd_result = read_gpp_reg(0xF28) ; //MMIO_generatl 
  bm_printf("C program message: Test result: GCD ( %d, %d ) = %d\n",op_a, op_b, gcd_result) ; 
}
//============================================================================================
//---------------------MEMORY------------------------
//                start   size    end    # of words
//  Inst memory   0x000  0x800  0x7FF    512
//  Data memory   0x800  0x600  0xDFF    384
//  Stack         0xE00  0x100  0xEFF    64 
//  MMIO_general  0xF00  0xA0   0xF9F    40 
//  MMIO_CSR      0xFA0  0x20   0xFBF    8  -
//  MMIO_drct_out 0xFC0  0x10   0xFCF    4  
//  MMIO_ER       0xFD0  0x20   0xFEF    8  
//  MMIO_drct_in  0xFF0  0x10   0xFFF    4  
//---------------------------------------------------
// CSR[0] = 0xFA0:  EnPC
// CSR[1] = 0xFA4:  rstPC
// CSR[2] = 0xFA8:  reg_rd_ptr
// CSR[3] = 0xFAC:  gear
// CSR[4] = 0xFB0:  Start       -> after SOC sets this bit dom should reset it.
// CSR[5] = 0xFB4:  Done        -> After DOM sets this SOC should reset it.
//---------------------------------------------------
int main() {  // The Main program
  bm_printf("\n\n*** HELLO WORLD  Amichai Aviad, TESTING GPP GCD ACCELERATOR ***\n") ;   // bare-metal output to terminal
  ////load dom GCD code
  //load_inst_to_dom(<instruction_file_pointer>);
  write_gpp_reg (0xFA0 , 1) ; // write to CSR[PC_en] (start executing dom code - (will enter the busy wait)
  // Checking Some Examples
  test_get_gcd (   344   ,  152 ) ;
//  test_get_gcd ( 2356   ,  9951 ) ;
//  test_get_gcd ( 1563   ,   102 ) ;
   // Finish 
  bm_quit_app() ; // uart message simulation/emulation in simulation/FPGA environment.
  return 0;
}
