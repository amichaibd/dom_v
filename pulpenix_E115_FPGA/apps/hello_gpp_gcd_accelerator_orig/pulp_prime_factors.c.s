	.file	"pulp_prime_factors.c"
	.option nopic
	.text
.Ltext0:
	.cfi_sections	.debug_frame
	.section	.text.write_gpp_reg,"ax",@progbits
	.align	2
	.globl	write_gpp_reg
	.type	write_gpp_reg, @function
write_gpp_reg:
.LFB1:
	.file 1 "./pulp_prime_factors.c"
	.loc 1 12 0
	.cfi_startproc
	addi	sp,sp,-48
	.cfi_def_cfa_offset 48
	sw	s0,44(sp)
	.cfi_offset 8, -4
	addi	s0,sp,48
	.cfi_def_cfa 8, 0
	sw	a0,-36(s0)
	sw	a1,-40(s0)
	.loc 1 14 0
	lw	a4,-36(s0)
	li	a5,437280768
	add	a5,a4,a5
	sw	a5,-20(s0)
	.loc 1 15 0
	lw	a5,-20(s0)
	lw	a4,-40(s0)
	sw	a4,0(a5)
	.loc 1 16 0
	nop
	lw	s0,44(sp)
	.cfi_restore 8
	addi	sp,sp,48
	.cfi_def_cfa_register 2
	jr	ra
	.cfi_endproc
.LFE1:
	.size	write_gpp_reg, .-write_gpp_reg
	.section	.text.read_gpp_reg,"ax",@progbits
	.align	2
	.globl	read_gpp_reg
	.type	read_gpp_reg, @function
read_gpp_reg:
.LFB2:
	.loc 1 18 0
	.cfi_startproc
	addi	sp,sp,-48
	.cfi_def_cfa_offset 48
	sw	s0,44(sp)
	.cfi_offset 8, -4
	addi	s0,sp,48
	.cfi_def_cfa 8, 0
	sw	a0,-36(s0)
	.loc 1 20 0
	lw	a4,-36(s0)
	li	a5,437280768
	add	a5,a4,a5
	sw	a5,-20(s0)
	.loc 1 21 0
	lw	a5,-20(s0)
	lw	a5,0(a5)
	.loc 1 22 0
	mv	a0,a5
	lw	s0,44(sp)
	.cfi_restore 8
	addi	sp,sp,48
	.cfi_def_cfa_register 2
	jr	ra
	.cfi_endproc
.LFE2:
	.size	read_gpp_reg, .-read_gpp_reg
	.section	.rodata
	.align	2
.LC0:
	.string	"C program message: Polling till accelerator is done \n"
	.align	2
.LC1:
	.string	"C program message: Factor of %d:\n"
	.align	2
.LC2:
	.string	"Factor[%d]: is %d \n"
	.section	.text.get_factors,"ax",@progbits
	.align	2
	.globl	get_factors
	.type	get_factors, @function
get_factors:
.LFB3:
	.loc 1 24 0
	.cfi_startproc
	addi	sp,sp,-48
	.cfi_def_cfa_offset 48
	sw	ra,44(sp)
	sw	s0,40(sp)
	.cfi_offset 1, -4
	.cfi_offset 8, -8
	addi	s0,sp,48
	.cfi_def_cfa 8, 0
	sw	a0,-36(s0)
	.loc 1 25 0
	lw	a5,-36(s0)
	mv	a1,a5
	li	a5,4096
	addi	a0,a5,-256
	call	write_gpp_reg
	.loc 1 26 0
	li	a1,1
	li	a5,4096
	addi	a0,a5,-80
	call	write_gpp_reg
	.loc 1 28 0
	j	.L5
.L6:
	.loc 1 29 0
	lui	a5,%hi(.LC0)
	addi	a0,a5,%lo(.LC0)
	call	bm_printf
.L5:
	.loc 1 28 0
	li	a5,4096
	addi	a0,a5,-76
	call	read_gpp_reg
	mv	a5,a0
	beqz	a5,.L6
	.loc 1 31 0
	li	a1,0
	li	a5,4096
	addi	a0,a5,-76
	call	write_gpp_reg
	.loc 1 33 0
	li	a5,1
	sw	a5,-20(s0)
	.loc 1 34 0
	sw	zero,-24(s0)
	.loc 1 35 0
	lw	a1,-36(s0)
	lui	a5,%hi(.LC1)
	addi	a0,a5,%lo(.LC1)
	call	bm_printf
	.loc 1 36 0
	j	.L7
.L9:
	.loc 1 37 0
	lw	a4,-24(s0)
	li	a5,4096
	addi	a5,a5,-216
	add	a5,a4,a5
	mv	a0,a5
	call	read_gpp_reg
	mv	a5,a0
	sw	a5,-20(s0)
	.loc 1 38 0
	lw	a5,-20(s0)
	beqz	a5,.L8
	.loc 1 39 0
	lw	a5,-24(s0)
	srai	a4,a5,31
	andi	a4,a4,3
	add	a5,a4,a5
	srai	a5,a5,2
	lw	a2,-20(s0)
	mv	a1,a5
	lui	a5,%hi(.LC2)
	addi	a0,a5,%lo(.LC2)
	call	bm_printf
.L8:
	.loc 1 41 0
	lw	a5,-24(s0)
	addi	a5,a5,4
	sw	a5,-24(s0)
.L7:
	.loc 1 36 0
	lw	a5,-20(s0)
	bnez	a5,.L9
	.loc 1 43 0
	nop
	lw	ra,44(sp)
	.cfi_restore 1
	lw	s0,40(sp)
	.cfi_restore 8
	addi	sp,sp,48
	.cfi_def_cfa_register 2
	jr	ra
	.cfi_endproc
.LFE3:
	.size	get_factors, .-get_factors
	.section	.rodata
	.align	2
.LC3:
	.string	"\n\n*** HELLO WORLD  Amichai Aviad, TESTING factor finder ACCELERATOR ***\n"
	.section	.text.main,"ax",@progbits
	.align	2
	.globl	main
	.type	main, @function
main:
.LFB4:
	.loc 1 63 0
	.cfi_startproc
	addi	sp,sp,-16
	.cfi_def_cfa_offset 16
	sw	ra,12(sp)
	sw	s0,8(sp)
	.cfi_offset 1, -4
	.cfi_offset 8, -8
	addi	s0,sp,16
	.cfi_def_cfa 8, 0
	.loc 1 64 0
	lui	a5,%hi(.LC3)
	addi	a0,a5,%lo(.LC3)
	call	bm_printf
	.loc 1 67 0
	li	a1,1
	li	a5,4096
	addi	a0,a5,-96
	call	write_gpp_reg
	.loc 1 70 0
	li	a0,30
	call	get_factors
	.loc 1 72 0
	call	bm_quit_app
	.loc 1 73 0
	li	a5,0
	.loc 1 74 0
	mv	a0,a5
	lw	ra,12(sp)
	.cfi_restore 1
	lw	s0,8(sp)
	.cfi_restore 8
	addi	sp,sp,16
	.cfi_def_cfa_register 2
	jr	ra
	.cfi_endproc
.LFE4:
	.size	main, .-main
	.text
.Letext0:
	.file 2 "/project/generic/users/udik/ws/pulp/toolchain/gnu-mcu-eclipse/7.2.0-2-20180111-2230/riscv-none-embed/include/sys/lock.h"
	.file 3 "/project/generic/users/udik/ws/pulp/toolchain/gnu-mcu-eclipse/7.2.0-2-20180111-2230/riscv-none-embed/include/sys/_types.h"
	.file 4 "/project/generic/users/udik/ws/pulp/toolchain/gnu-mcu-eclipse/7.2.0-2-20180111-2230/lib/gcc/riscv-none-embed/7.2.0/include/stddef.h"
	.file 5 "/project/generic/users/udik/ws/pulp/toolchain/gnu-mcu-eclipse/7.2.0-2-20180111-2230/riscv-none-embed/include/sys/reent.h"
	.file 6 "/project/generic/users/udik/ws/pulp/toolchain/gnu-mcu-eclipse/7.2.0-2-20180111-2230/riscv-none-embed/include/stdlib.h"
	.section	.debug_info,"",@progbits
.Ldebug_info0:
	.4byte	0x96e
	.2byte	0x4
	.4byte	.Ldebug_abbrev0
	.byte	0x4
	.byte	0x1
	.4byte	.LASF127
	.byte	0xc
	.4byte	.LASF128
	.4byte	.LASF129
	.4byte	.Ldebug_ranges0+0
	.4byte	0
	.4byte	.Ldebug_line0
	.byte	0x2
	.byte	0x1
	.byte	0x6
	.4byte	.LASF0
	.byte	0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF1
	.byte	0x2
	.byte	0x2
	.byte	0x5
	.4byte	.LASF2
	.byte	0x2
	.byte	0x2
	.byte	0x7
	.4byte	.LASF3
	.byte	0x2
	.byte	0x4
	.byte	0x5
	.4byte	.LASF4
	.byte	0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF5
	.byte	0x2
	.byte	0x8
	.byte	0x5
	.4byte	.LASF6
	.byte	0x2
	.byte	0x8
	.byte	0x7
	.4byte	.LASF7
	.byte	0x3
	.byte	0x4
	.byte	0x5
	.string	"int"
	.byte	0x2
	.byte	0x4
	.byte	0x7
	.4byte	.LASF8
	.byte	0x4
	.4byte	0x64
	.byte	0x2
	.byte	0x10
	.byte	0x4
	.4byte	.LASF9
	.byte	0x5
	.4byte	.LASF10
	.byte	0x2
	.byte	0x7
	.4byte	0x5d
	.byte	0x5
	.4byte	.LASF11
	.byte	0x3
	.byte	0x2c
	.4byte	0x41
	.byte	0x5
	.4byte	.LASF12
	.byte	0x3
	.byte	0x72
	.4byte	0x41
	.byte	0x5
	.4byte	.LASF13
	.byte	0x3
	.byte	0x91
	.4byte	0x5d
	.byte	0x6
	.4byte	.LASF14
	.byte	0x4
	.2byte	0x165
	.4byte	0x64
	.byte	0x7
	.byte	0x4
	.byte	0x3
	.byte	0xa6
	.4byte	0xce
	.byte	0x8
	.4byte	.LASF15
	.byte	0x3
	.byte	0xa8
	.4byte	0xa3
	.byte	0x8
	.4byte	.LASF16
	.byte	0x3
	.byte	0xa9
	.4byte	0xce
	.byte	0
	.byte	0x9
	.4byte	0x2c
	.4byte	0xde
	.byte	0xa
	.4byte	0x64
	.byte	0x3
	.byte	0
	.byte	0xb
	.byte	0x8
	.byte	0x3
	.byte	0xa3
	.4byte	0xff
	.byte	0xc
	.4byte	.LASF17
	.byte	0x3
	.byte	0xa5
	.4byte	0x5d
	.byte	0
	.byte	0xc
	.4byte	.LASF18
	.byte	0x3
	.byte	0xaa
	.4byte	0xaf
	.byte	0x4
	.byte	0
	.byte	0x5
	.4byte	.LASF19
	.byte	0x3
	.byte	0xab
	.4byte	0xde
	.byte	0x5
	.4byte	.LASF20
	.byte	0x3
	.byte	0xaf
	.4byte	0x77
	.byte	0xd
	.byte	0x4
	.byte	0x5
	.4byte	.LASF21
	.byte	0x5
	.byte	0x16
	.4byte	0x48
	.byte	0xe
	.4byte	.LASF26
	.byte	0x18
	.byte	0x5
	.byte	0x2f
	.4byte	0x175
	.byte	0xc
	.4byte	.LASF22
	.byte	0x5
	.byte	0x31
	.4byte	0x175
	.byte	0
	.byte	0xf
	.string	"_k"
	.byte	0x5
	.byte	0x32
	.4byte	0x5d
	.byte	0x4
	.byte	0xc
	.4byte	.LASF23
	.byte	0x5
	.byte	0x32
	.4byte	0x5d
	.byte	0x8
	.byte	0xc
	.4byte	.LASF24
	.byte	0x5
	.byte	0x32
	.4byte	0x5d
	.byte	0xc
	.byte	0xc
	.4byte	.LASF25
	.byte	0x5
	.byte	0x32
	.4byte	0x5d
	.byte	0x10
	.byte	0xf
	.string	"_x"
	.byte	0x5
	.byte	0x33
	.4byte	0x17b
	.byte	0x14
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x122
	.byte	0x9
	.4byte	0x117
	.4byte	0x18b
	.byte	0xa
	.4byte	0x64
	.byte	0
	.byte	0
	.byte	0xe
	.4byte	.LASF27
	.byte	0x24
	.byte	0x5
	.byte	0x37
	.4byte	0x204
	.byte	0xc
	.4byte	.LASF28
	.byte	0x5
	.byte	0x39
	.4byte	0x5d
	.byte	0
	.byte	0xc
	.4byte	.LASF29
	.byte	0x5
	.byte	0x3a
	.4byte	0x5d
	.byte	0x4
	.byte	0xc
	.4byte	.LASF30
	.byte	0x5
	.byte	0x3b
	.4byte	0x5d
	.byte	0x8
	.byte	0xc
	.4byte	.LASF31
	.byte	0x5
	.byte	0x3c
	.4byte	0x5d
	.byte	0xc
	.byte	0xc
	.4byte	.LASF32
	.byte	0x5
	.byte	0x3d
	.4byte	0x5d
	.byte	0x10
	.byte	0xc
	.4byte	.LASF33
	.byte	0x5
	.byte	0x3e
	.4byte	0x5d
	.byte	0x14
	.byte	0xc
	.4byte	.LASF34
	.byte	0x5
	.byte	0x3f
	.4byte	0x5d
	.byte	0x18
	.byte	0xc
	.4byte	.LASF35
	.byte	0x5
	.byte	0x40
	.4byte	0x5d
	.byte	0x1c
	.byte	0xc
	.4byte	.LASF36
	.byte	0x5
	.byte	0x41
	.4byte	0x5d
	.byte	0x20
	.byte	0
	.byte	0x11
	.4byte	.LASF37
	.2byte	0x108
	.byte	0x5
	.byte	0x4a
	.4byte	0x244
	.byte	0xc
	.4byte	.LASF38
	.byte	0x5
	.byte	0x4b
	.4byte	0x244
	.byte	0
	.byte	0xc
	.4byte	.LASF39
	.byte	0x5
	.byte	0x4c
	.4byte	0x244
	.byte	0x80
	.byte	0x12
	.4byte	.LASF40
	.byte	0x5
	.byte	0x4e
	.4byte	0x117
	.2byte	0x100
	.byte	0x12
	.4byte	.LASF41
	.byte	0x5
	.byte	0x51
	.4byte	0x117
	.2byte	0x104
	.byte	0
	.byte	0x9
	.4byte	0x115
	.4byte	0x254
	.byte	0xa
	.4byte	0x64
	.byte	0x1f
	.byte	0
	.byte	0x11
	.4byte	.LASF42
	.2byte	0x190
	.byte	0x5
	.byte	0x5d
	.4byte	0x292
	.byte	0xc
	.4byte	.LASF22
	.byte	0x5
	.byte	0x5e
	.4byte	0x292
	.byte	0
	.byte	0xc
	.4byte	.LASF43
	.byte	0x5
	.byte	0x5f
	.4byte	0x5d
	.byte	0x4
	.byte	0xc
	.4byte	.LASF44
	.byte	0x5
	.byte	0x61
	.4byte	0x298
	.byte	0x8
	.byte	0xc
	.4byte	.LASF37
	.byte	0x5
	.byte	0x62
	.4byte	0x204
	.byte	0x88
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x254
	.byte	0x9
	.4byte	0x2a8
	.4byte	0x2a8
	.byte	0xa
	.4byte	0x64
	.byte	0x1f
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x2ae
	.byte	0x13
	.byte	0xe
	.4byte	.LASF45
	.byte	0x8
	.byte	0x5
	.byte	0x75
	.4byte	0x2d4
	.byte	0xc
	.4byte	.LASF46
	.byte	0x5
	.byte	0x76
	.4byte	0x2d4
	.byte	0
	.byte	0xc
	.4byte	.LASF47
	.byte	0x5
	.byte	0x77
	.4byte	0x5d
	.byte	0x4
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x2c
	.byte	0xe
	.4byte	.LASF48
	.byte	0x68
	.byte	0x5
	.byte	0xb5
	.4byte	0x404
	.byte	0xf
	.string	"_p"
	.byte	0x5
	.byte	0xb6
	.4byte	0x2d4
	.byte	0
	.byte	0xf
	.string	"_r"
	.byte	0x5
	.byte	0xb7
	.4byte	0x5d
	.byte	0x4
	.byte	0xf
	.string	"_w"
	.byte	0x5
	.byte	0xb8
	.4byte	0x5d
	.byte	0x8
	.byte	0xc
	.4byte	.LASF49
	.byte	0x5
	.byte	0xb9
	.4byte	0x33
	.byte	0xc
	.byte	0xc
	.4byte	.LASF50
	.byte	0x5
	.byte	0xba
	.4byte	0x33
	.byte	0xe
	.byte	0xf
	.string	"_bf"
	.byte	0x5
	.byte	0xbb
	.4byte	0x2af
	.byte	0x10
	.byte	0xc
	.4byte	.LASF51
	.byte	0x5
	.byte	0xbc
	.4byte	0x5d
	.byte	0x18
	.byte	0xc
	.4byte	.LASF52
	.byte	0x5
	.byte	0xc3
	.4byte	0x115
	.byte	0x1c
	.byte	0xc
	.4byte	.LASF53
	.byte	0x5
	.byte	0xc5
	.4byte	0x571
	.byte	0x20
	.byte	0xc
	.4byte	.LASF54
	.byte	0x5
	.byte	0xc7
	.4byte	0x59b
	.byte	0x24
	.byte	0xc
	.4byte	.LASF55
	.byte	0x5
	.byte	0xca
	.4byte	0x5bf
	.byte	0x28
	.byte	0xc
	.4byte	.LASF56
	.byte	0x5
	.byte	0xcb
	.4byte	0x5d9
	.byte	0x2c
	.byte	0xf
	.string	"_ub"
	.byte	0x5
	.byte	0xce
	.4byte	0x2af
	.byte	0x30
	.byte	0xf
	.string	"_up"
	.byte	0x5
	.byte	0xcf
	.4byte	0x2d4
	.byte	0x38
	.byte	0xf
	.string	"_ur"
	.byte	0x5
	.byte	0xd0
	.4byte	0x5d
	.byte	0x3c
	.byte	0xc
	.4byte	.LASF57
	.byte	0x5
	.byte	0xd3
	.4byte	0x5df
	.byte	0x40
	.byte	0xc
	.4byte	.LASF58
	.byte	0x5
	.byte	0xd4
	.4byte	0x5ef
	.byte	0x43
	.byte	0xf
	.string	"_lb"
	.byte	0x5
	.byte	0xd7
	.4byte	0x2af
	.byte	0x44
	.byte	0xc
	.4byte	.LASF59
	.byte	0x5
	.byte	0xda
	.4byte	0x5d
	.byte	0x4c
	.byte	0xc
	.4byte	.LASF60
	.byte	0x5
	.byte	0xdb
	.4byte	0x82
	.byte	0x50
	.byte	0xc
	.4byte	.LASF61
	.byte	0x5
	.byte	0xde
	.4byte	0x422
	.byte	0x54
	.byte	0xc
	.4byte	.LASF62
	.byte	0x5
	.byte	0xe2
	.4byte	0x10a
	.byte	0x58
	.byte	0xc
	.4byte	.LASF63
	.byte	0x5
	.byte	0xe4
	.4byte	0xff
	.byte	0x5c
	.byte	0xc
	.4byte	.LASF64
	.byte	0x5
	.byte	0xe5
	.4byte	0x5d
	.byte	0x64
	.byte	0
	.byte	0x14
	.4byte	0x98
	.4byte	0x422
	.byte	0x15
	.4byte	0x422
	.byte	0x15
	.4byte	0x115
	.byte	0x15
	.4byte	0x55f
	.byte	0x15
	.4byte	0x5d
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x42d
	.byte	0x16
	.4byte	0x422
	.byte	0x17
	.4byte	.LASF65
	.2byte	0x428
	.byte	0x5
	.2byte	0x239
	.4byte	0x55f
	.byte	0x18
	.4byte	.LASF66
	.byte	0x5
	.2byte	0x23b
	.4byte	0x5d
	.byte	0
	.byte	0x18
	.4byte	.LASF67
	.byte	0x5
	.2byte	0x240
	.4byte	0x646
	.byte	0x4
	.byte	0x18
	.4byte	.LASF68
	.byte	0x5
	.2byte	0x240
	.4byte	0x646
	.byte	0x8
	.byte	0x18
	.4byte	.LASF69
	.byte	0x5
	.2byte	0x240
	.4byte	0x646
	.byte	0xc
	.byte	0x18
	.4byte	.LASF70
	.byte	0x5
	.2byte	0x242
	.4byte	0x5d
	.byte	0x10
	.byte	0x18
	.4byte	.LASF71
	.byte	0x5
	.2byte	0x243
	.4byte	0x828
	.byte	0x14
	.byte	0x18
	.4byte	.LASF72
	.byte	0x5
	.2byte	0x246
	.4byte	0x5d
	.byte	0x30
	.byte	0x18
	.4byte	.LASF73
	.byte	0x5
	.2byte	0x247
	.4byte	0x83d
	.byte	0x34
	.byte	0x18
	.4byte	.LASF74
	.byte	0x5
	.2byte	0x249
	.4byte	0x5d
	.byte	0x38
	.byte	0x18
	.4byte	.LASF75
	.byte	0x5
	.2byte	0x24b
	.4byte	0x84e
	.byte	0x3c
	.byte	0x18
	.4byte	.LASF76
	.byte	0x5
	.2byte	0x24e
	.4byte	0x175
	.byte	0x40
	.byte	0x18
	.4byte	.LASF77
	.byte	0x5
	.2byte	0x24f
	.4byte	0x5d
	.byte	0x44
	.byte	0x18
	.4byte	.LASF78
	.byte	0x5
	.2byte	0x250
	.4byte	0x175
	.byte	0x48
	.byte	0x18
	.4byte	.LASF79
	.byte	0x5
	.2byte	0x251
	.4byte	0x854
	.byte	0x4c
	.byte	0x18
	.4byte	.LASF80
	.byte	0x5
	.2byte	0x254
	.4byte	0x5d
	.byte	0x50
	.byte	0x18
	.4byte	.LASF81
	.byte	0x5
	.2byte	0x255
	.4byte	0x55f
	.byte	0x54
	.byte	0x18
	.4byte	.LASF82
	.byte	0x5
	.2byte	0x278
	.4byte	0x806
	.byte	0x58
	.byte	0x19
	.4byte	.LASF42
	.byte	0x5
	.2byte	0x27c
	.4byte	0x292
	.2byte	0x148
	.byte	0x19
	.4byte	.LASF83
	.byte	0x5
	.2byte	0x27d
	.4byte	0x254
	.2byte	0x14c
	.byte	0x19
	.4byte	.LASF84
	.byte	0x5
	.2byte	0x281
	.4byte	0x865
	.2byte	0x2dc
	.byte	0x19
	.4byte	.LASF85
	.byte	0x5
	.2byte	0x286
	.4byte	0x60b
	.2byte	0x2e0
	.byte	0x19
	.4byte	.LASF86
	.byte	0x5
	.2byte	0x287
	.4byte	0x871
	.2byte	0x2ec
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x565
	.byte	0x2
	.byte	0x1
	.byte	0x8
	.4byte	.LASF87
	.byte	0x16
	.4byte	0x565
	.byte	0x10
	.byte	0x4
	.4byte	0x404
	.byte	0x14
	.4byte	0x98
	.4byte	0x595
	.byte	0x15
	.4byte	0x422
	.byte	0x15
	.4byte	0x115
	.byte	0x15
	.4byte	0x595
	.byte	0x15
	.4byte	0x5d
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x56c
	.byte	0x10
	.byte	0x4
	.4byte	0x577
	.byte	0x14
	.4byte	0x8d
	.4byte	0x5bf
	.byte	0x15
	.4byte	0x422
	.byte	0x15
	.4byte	0x115
	.byte	0x15
	.4byte	0x8d
	.byte	0x15
	.4byte	0x5d
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x5a1
	.byte	0x14
	.4byte	0x5d
	.4byte	0x5d9
	.byte	0x15
	.4byte	0x422
	.byte	0x15
	.4byte	0x115
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x5c5
	.byte	0x9
	.4byte	0x2c
	.4byte	0x5ef
	.byte	0xa
	.4byte	0x64
	.byte	0x2
	.byte	0
	.byte	0x9
	.4byte	0x2c
	.4byte	0x5ff
	.byte	0xa
	.4byte	0x64
	.byte	0
	.byte	0
	.byte	0x6
	.4byte	.LASF88
	.byte	0x5
	.2byte	0x11f
	.4byte	0x2da
	.byte	0x1a
	.4byte	.LASF89
	.byte	0xc
	.byte	0x5
	.2byte	0x123
	.4byte	0x640
	.byte	0x18
	.4byte	.LASF22
	.byte	0x5
	.2byte	0x125
	.4byte	0x640
	.byte	0
	.byte	0x18
	.4byte	.LASF90
	.byte	0x5
	.2byte	0x126
	.4byte	0x5d
	.byte	0x4
	.byte	0x18
	.4byte	.LASF91
	.byte	0x5
	.2byte	0x127
	.4byte	0x646
	.byte	0x8
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x60b
	.byte	0x10
	.byte	0x4
	.4byte	0x5ff
	.byte	0x1a
	.4byte	.LASF92
	.byte	0xe
	.byte	0x5
	.2byte	0x13f
	.4byte	0x681
	.byte	0x18
	.4byte	.LASF93
	.byte	0x5
	.2byte	0x140
	.4byte	0x681
	.byte	0
	.byte	0x18
	.4byte	.LASF94
	.byte	0x5
	.2byte	0x141
	.4byte	0x681
	.byte	0x6
	.byte	0x18
	.4byte	.LASF95
	.byte	0x5
	.2byte	0x142
	.4byte	0x3a
	.byte	0xc
	.byte	0
	.byte	0x9
	.4byte	0x3a
	.4byte	0x691
	.byte	0xa
	.4byte	0x64
	.byte	0x2
	.byte	0
	.byte	0x1b
	.byte	0xd0
	.byte	0x5
	.2byte	0x259
	.4byte	0x792
	.byte	0x18
	.4byte	.LASF96
	.byte	0x5
	.2byte	0x25b
	.4byte	0x64
	.byte	0
	.byte	0x18
	.4byte	.LASF97
	.byte	0x5
	.2byte	0x25c
	.4byte	0x55f
	.byte	0x4
	.byte	0x18
	.4byte	.LASF98
	.byte	0x5
	.2byte	0x25d
	.4byte	0x792
	.byte	0x8
	.byte	0x18
	.4byte	.LASF99
	.byte	0x5
	.2byte	0x25e
	.4byte	0x18b
	.byte	0x24
	.byte	0x18
	.4byte	.LASF100
	.byte	0x5
	.2byte	0x25f
	.4byte	0x5d
	.byte	0x48
	.byte	0x18
	.4byte	.LASF101
	.byte	0x5
	.2byte	0x260
	.4byte	0x56
	.byte	0x50
	.byte	0x18
	.4byte	.LASF102
	.byte	0x5
	.2byte	0x261
	.4byte	0x64c
	.byte	0x58
	.byte	0x18
	.4byte	.LASF103
	.byte	0x5
	.2byte	0x262
	.4byte	0xff
	.byte	0x68
	.byte	0x18
	.4byte	.LASF104
	.byte	0x5
	.2byte	0x263
	.4byte	0xff
	.byte	0x70
	.byte	0x18
	.4byte	.LASF105
	.byte	0x5
	.2byte	0x264
	.4byte	0xff
	.byte	0x78
	.byte	0x18
	.4byte	.LASF106
	.byte	0x5
	.2byte	0x265
	.4byte	0x7a2
	.byte	0x80
	.byte	0x18
	.4byte	.LASF107
	.byte	0x5
	.2byte	0x266
	.4byte	0x7b2
	.byte	0x88
	.byte	0x18
	.4byte	.LASF108
	.byte	0x5
	.2byte	0x267
	.4byte	0x5d
	.byte	0xa0
	.byte	0x18
	.4byte	.LASF109
	.byte	0x5
	.2byte	0x268
	.4byte	0xff
	.byte	0xa4
	.byte	0x18
	.4byte	.LASF110
	.byte	0x5
	.2byte	0x269
	.4byte	0xff
	.byte	0xac
	.byte	0x18
	.4byte	.LASF111
	.byte	0x5
	.2byte	0x26a
	.4byte	0xff
	.byte	0xb4
	.byte	0x18
	.4byte	.LASF112
	.byte	0x5
	.2byte	0x26b
	.4byte	0xff
	.byte	0xbc
	.byte	0x18
	.4byte	.LASF113
	.byte	0x5
	.2byte	0x26c
	.4byte	0xff
	.byte	0xc4
	.byte	0x18
	.4byte	.LASF114
	.byte	0x5
	.2byte	0x26d
	.4byte	0x5d
	.byte	0xcc
	.byte	0
	.byte	0x9
	.4byte	0x565
	.4byte	0x7a2
	.byte	0xa
	.4byte	0x64
	.byte	0x19
	.byte	0
	.byte	0x9
	.4byte	0x565
	.4byte	0x7b2
	.byte	0xa
	.4byte	0x64
	.byte	0x7
	.byte	0
	.byte	0x9
	.4byte	0x565
	.4byte	0x7c2
	.byte	0xa
	.4byte	0x64
	.byte	0x17
	.byte	0
	.byte	0x1b
	.byte	0xf0
	.byte	0x5
	.2byte	0x272
	.4byte	0x7e6
	.byte	0x18
	.4byte	.LASF115
	.byte	0x5
	.2byte	0x275
	.4byte	0x7e6
	.byte	0
	.byte	0x18
	.4byte	.LASF116
	.byte	0x5
	.2byte	0x276
	.4byte	0x7f6
	.byte	0x78
	.byte	0
	.byte	0x9
	.4byte	0x2d4
	.4byte	0x7f6
	.byte	0xa
	.4byte	0x64
	.byte	0x1d
	.byte	0
	.byte	0x9
	.4byte	0x64
	.4byte	0x806
	.byte	0xa
	.4byte	0x64
	.byte	0x1d
	.byte	0
	.byte	0x1c
	.byte	0xf0
	.byte	0x5
	.2byte	0x257
	.4byte	0x828
	.byte	0x1d
	.4byte	.LASF65
	.byte	0x5
	.2byte	0x26e
	.4byte	0x691
	.byte	0x1d
	.4byte	.LASF117
	.byte	0x5
	.2byte	0x277
	.4byte	0x7c2
	.byte	0
	.byte	0x9
	.4byte	0x565
	.4byte	0x838
	.byte	0xa
	.4byte	0x64
	.byte	0x18
	.byte	0
	.byte	0x1e
	.4byte	.LASF130
	.byte	0x10
	.byte	0x4
	.4byte	0x838
	.byte	0x1f
	.4byte	0x84e
	.byte	0x15
	.4byte	0x422
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x843
	.byte	0x10
	.byte	0x4
	.4byte	0x175
	.byte	0x1f
	.4byte	0x865
	.byte	0x15
	.4byte	0x5d
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x86b
	.byte	0x10
	.byte	0x4
	.4byte	0x85a
	.byte	0x9
	.4byte	0x5ff
	.4byte	0x881
	.byte	0xa
	.4byte	0x64
	.byte	0x2
	.byte	0
	.byte	0x20
	.4byte	.LASF118
	.byte	0x5
	.2byte	0x2fe
	.4byte	0x422
	.byte	0x20
	.4byte	.LASF119
	.byte	0x5
	.2byte	0x2ff
	.4byte	0x428
	.byte	0x21
	.4byte	.LASF120
	.byte	0x6
	.byte	0x63
	.4byte	0x55f
	.byte	0x22
	.4byte	.LASF131
	.byte	0x1
	.byte	0x3f
	.4byte	0x5d
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.byte	0x1
	.byte	0x9c
	.byte	0x23
	.4byte	.LASF132
	.byte	0x1
	.byte	0x18
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.byte	0x1
	.byte	0x9c
	.4byte	0x8f9
	.byte	0x24
	.4byte	.LASF123
	.byte	0x1
	.byte	0x18
	.4byte	0x5d
	.byte	0x2
	.byte	0x91
	.byte	0x5c
	.byte	0x25
	.4byte	.LASF121
	.byte	0x1
	.byte	0x21
	.4byte	0x5d
	.byte	0x2
	.byte	0x91
	.byte	0x6c
	.byte	0x25
	.4byte	.LASF122
	.byte	0x1
	.byte	0x22
	.4byte	0x5d
	.byte	0x2
	.byte	0x91
	.byte	0x68
	.byte	0
	.byte	0x26
	.4byte	.LASF133
	.byte	0x1
	.byte	0x12
	.4byte	0x64
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.byte	0x1
	.byte	0x9c
	.4byte	0x92f
	.byte	0x24
	.4byte	.LASF124
	.byte	0x1
	.byte	0x12
	.4byte	0x64
	.byte	0x2
	.byte	0x91
	.byte	0x5c
	.byte	0x25
	.4byte	.LASF125
	.byte	0x1
	.byte	0x13
	.4byte	0x92f
	.byte	0x2
	.byte	0x91
	.byte	0x6c
	.byte	0
	.byte	0x10
	.byte	0x4
	.4byte	0x6b
	.byte	0x27
	.4byte	.LASF134
	.byte	0x1
	.byte	0xc
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.byte	0x1
	.byte	0x9c
	.byte	0x24
	.4byte	.LASF124
	.byte	0x1
	.byte	0xc
	.4byte	0x64
	.byte	0x2
	.byte	0x91
	.byte	0x5c
	.byte	0x24
	.4byte	.LASF126
	.byte	0x1
	.byte	0xc
	.4byte	0x64
	.byte	0x2
	.byte	0x91
	.byte	0x58
	.byte	0x25
	.4byte	.LASF125
	.byte	0x1
	.byte	0xd
	.4byte	0x92f
	.byte	0x2
	.byte	0x91
	.byte	0x6c
	.byte	0
	.byte	0
	.section	.debug_abbrev,"",@progbits
.Ldebug_abbrev0:
	.byte	0x1
	.byte	0x11
	.byte	0x1
	.byte	0x25
	.byte	0xe
	.byte	0x13
	.byte	0xb
	.byte	0x3
	.byte	0xe
	.byte	0x1b
	.byte	0xe
	.byte	0x55
	.byte	0x17
	.byte	0x11
	.byte	0x1
	.byte	0x10
	.byte	0x17
	.byte	0
	.byte	0
	.byte	0x2
	.byte	0x24
	.byte	0
	.byte	0xb
	.byte	0xb
	.byte	0x3e
	.byte	0xb
	.byte	0x3
	.byte	0xe
	.byte	0
	.byte	0
	.byte	0x3
	.byte	0x24
	.byte	0
	.byte	0xb
	.byte	0xb
	.byte	0x3e
	.byte	0xb
	.byte	0x3
	.byte	0x8
	.byte	0
	.byte	0
	.byte	0x4
	.byte	0x35
	.byte	0
	.byte	0x49
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x5
	.byte	0x16
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x49
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x6
	.byte	0x16
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0x5
	.byte	0x49
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x7
	.byte	0x17
	.byte	0x1
	.byte	0xb
	.byte	0xb
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x8
	.byte	0xd
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x49
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x9
	.byte	0x1
	.byte	0x1
	.byte	0x49
	.byte	0x13
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0xa
	.byte	0x21
	.byte	0
	.byte	0x49
	.byte	0x13
	.byte	0x2f
	.byte	0xb
	.byte	0
	.byte	0
	.byte	0xb
	.byte	0x13
	.byte	0x1
	.byte	0xb
	.byte	0xb
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0xc
	.byte	0xd
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x49
	.byte	0x13
	.byte	0x38
	.byte	0xb
	.byte	0
	.byte	0
	.byte	0xd
	.byte	0xf
	.byte	0
	.byte	0xb
	.byte	0xb
	.byte	0
	.byte	0
	.byte	0xe
	.byte	0x13
	.byte	0x1
	.byte	0x3
	.byte	0xe
	.byte	0xb
	.byte	0xb
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0xf
	.byte	0xd
	.byte	0
	.byte	0x3
	.byte	0x8
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x49
	.byte	0x13
	.byte	0x38
	.byte	0xb
	.byte	0
	.byte	0
	.byte	0x10
	.byte	0xf
	.byte	0
	.byte	0xb
	.byte	0xb
	.byte	0x49
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x11
	.byte	0x13
	.byte	0x1
	.byte	0x3
	.byte	0xe
	.byte	0xb
	.byte	0x5
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x12
	.byte	0xd
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x49
	.byte	0x13
	.byte	0x38
	.byte	0x5
	.byte	0
	.byte	0
	.byte	0x13
	.byte	0x15
	.byte	0
	.byte	0x27
	.byte	0x19
	.byte	0
	.byte	0
	.byte	0x14
	.byte	0x15
	.byte	0x1
	.byte	0x27
	.byte	0x19
	.byte	0x49
	.byte	0x13
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x15
	.byte	0x5
	.byte	0
	.byte	0x49
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x16
	.byte	0x26
	.byte	0
	.byte	0x49
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x17
	.byte	0x13
	.byte	0x1
	.byte	0x3
	.byte	0xe
	.byte	0xb
	.byte	0x5
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0x5
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x18
	.byte	0xd
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0x5
	.byte	0x49
	.byte	0x13
	.byte	0x38
	.byte	0xb
	.byte	0
	.byte	0
	.byte	0x19
	.byte	0xd
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0x5
	.byte	0x49
	.byte	0x13
	.byte	0x38
	.byte	0x5
	.byte	0
	.byte	0
	.byte	0x1a
	.byte	0x13
	.byte	0x1
	.byte	0x3
	.byte	0xe
	.byte	0xb
	.byte	0xb
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0x5
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x1b
	.byte	0x13
	.byte	0x1
	.byte	0xb
	.byte	0xb
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0x5
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x1c
	.byte	0x17
	.byte	0x1
	.byte	0xb
	.byte	0xb
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0x5
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x1d
	.byte	0xd
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0x5
	.byte	0x49
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x1e
	.byte	0x13
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3c
	.byte	0x19
	.byte	0
	.byte	0
	.byte	0x1f
	.byte	0x15
	.byte	0x1
	.byte	0x27
	.byte	0x19
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x20
	.byte	0x34
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0x5
	.byte	0x49
	.byte	0x13
	.byte	0x3f
	.byte	0x19
	.byte	0x3c
	.byte	0x19
	.byte	0
	.byte	0
	.byte	0x21
	.byte	0x34
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x49
	.byte	0x13
	.byte	0x3f
	.byte	0x19
	.byte	0x3c
	.byte	0x19
	.byte	0
	.byte	0
	.byte	0x22
	.byte	0x2e
	.byte	0
	.byte	0x3f
	.byte	0x19
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x49
	.byte	0x13
	.byte	0x11
	.byte	0x1
	.byte	0x12
	.byte	0x6
	.byte	0x40
	.byte	0x18
	.byte	0x96,0x42
	.byte	0x19
	.byte	0
	.byte	0
	.byte	0x23
	.byte	0x2e
	.byte	0x1
	.byte	0x3f
	.byte	0x19
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x27
	.byte	0x19
	.byte	0x11
	.byte	0x1
	.byte	0x12
	.byte	0x6
	.byte	0x40
	.byte	0x18
	.byte	0x96,0x42
	.byte	0x19
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x24
	.byte	0x5
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x49
	.byte	0x13
	.byte	0x2
	.byte	0x18
	.byte	0
	.byte	0
	.byte	0x25
	.byte	0x34
	.byte	0
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x49
	.byte	0x13
	.byte	0x2
	.byte	0x18
	.byte	0
	.byte	0
	.byte	0x26
	.byte	0x2e
	.byte	0x1
	.byte	0x3f
	.byte	0x19
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x27
	.byte	0x19
	.byte	0x49
	.byte	0x13
	.byte	0x11
	.byte	0x1
	.byte	0x12
	.byte	0x6
	.byte	0x40
	.byte	0x18
	.byte	0x97,0x42
	.byte	0x19
	.byte	0x1
	.byte	0x13
	.byte	0
	.byte	0
	.byte	0x27
	.byte	0x2e
	.byte	0x1
	.byte	0x3f
	.byte	0x19
	.byte	0x3
	.byte	0xe
	.byte	0x3a
	.byte	0xb
	.byte	0x3b
	.byte	0xb
	.byte	0x27
	.byte	0x19
	.byte	0x11
	.byte	0x1
	.byte	0x12
	.byte	0x6
	.byte	0x40
	.byte	0x18
	.byte	0x97,0x42
	.byte	0x19
	.byte	0
	.byte	0
	.byte	0
	.section	.debug_aranges,"",@progbits
	.4byte	0x34
	.2byte	0x2
	.4byte	.Ldebug_info0
	.byte	0x4
	.byte	0
	.2byte	0
	.2byte	0
	.4byte	.LFB1
	.4byte	.LFE1-.LFB1
	.4byte	.LFB2
	.4byte	.LFE2-.LFB2
	.4byte	.LFB3
	.4byte	.LFE3-.LFB3
	.4byte	.LFB4
	.4byte	.LFE4-.LFB4
	.4byte	0
	.4byte	0
	.section	.debug_ranges,"",@progbits
.Ldebug_ranges0:
	.4byte	.LFB1
	.4byte	.LFE1
	.4byte	.LFB2
	.4byte	.LFE2
	.4byte	.LFB3
	.4byte	.LFE3
	.4byte	.LFB4
	.4byte	.LFE4
	.4byte	0
	.4byte	0
	.section	.debug_line,"",@progbits
.Ldebug_line0:
	.section	.debug_str,"MS",@progbits,1
.LASF39:
	.string	"_dso_handle"
.LASF47:
	.string	"_size"
.LASF134:
	.string	"write_gpp_reg"
.LASF92:
	.string	"_rand48"
.LASF71:
	.string	"_emergency"
.LASF63:
	.string	"_mbstate"
.LASF61:
	.string	"_data"
.LASF112:
	.string	"_wcrtomb_state"
.LASF113:
	.string	"_wcsrtombs_state"
.LASF7:
	.string	"long long unsigned int"
.LASF51:
	.string	"_lbfsize"
.LASF130:
	.string	"__locale_t"
.LASF110:
	.string	"_mbrtowc_state"
.LASF121:
	.string	"factor"
.LASF127:
	.string	"GNU C11 7.2.0 -march=rv32im -mabi=ilp32 -g -ggdb -fdata-sections -ffunction-sections"
.LASF105:
	.string	"_wctomb_state"
.LASF28:
	.string	"__tm_sec"
.LASF6:
	.string	"long long int"
.LASF0:
	.string	"signed char"
.LASF57:
	.string	"_ubuf"
.LASF46:
	.string	"_base"
.LASF30:
	.string	"__tm_hour"
.LASF86:
	.string	"__sf"
.LASF37:
	.string	"_on_exit_args"
.LASF52:
	.string	"_cookie"
.LASF85:
	.string	"__sglue"
.LASF4:
	.string	"long int"
.LASF49:
	.string	"_flags"
.LASF41:
	.string	"_is_cxa"
.LASF67:
	.string	"_stdin"
.LASF59:
	.string	"_blksize"
.LASF128:
	.string	"./pulp_prime_factors.c"
.LASF81:
	.string	"_cvtbuf"
.LASF60:
	.string	"_offset"
.LASF111:
	.string	"_mbsrtowcs_state"
.LASF109:
	.string	"_mbrlen_state"
.LASF38:
	.string	"_fnargs"
.LASF44:
	.string	"_fns"
.LASF24:
	.string	"_sign"
.LASF20:
	.string	"_flock_t"
.LASF69:
	.string	"_stderr"
.LASF26:
	.string	"_Bigint"
.LASF100:
	.string	"_gamma_signgam"
.LASF53:
	.string	"_read"
.LASF77:
	.string	"_result_k"
.LASF27:
	.string	"__tm"
.LASF8:
	.string	"unsigned int"
.LASF16:
	.string	"__wchb"
.LASF68:
	.string	"_stdout"
.LASF80:
	.string	"_cvtlen"
.LASF5:
	.string	"long unsigned int"
.LASF50:
	.string	"_file"
.LASF90:
	.string	"_niobs"
.LASF3:
	.string	"short unsigned int"
.LASF83:
	.string	"_atexit0"
.LASF107:
	.string	"_signal_buf"
.LASF98:
	.string	"_asctime_buf"
.LASF76:
	.string	"_result"
.LASF15:
	.string	"__wch"
.LASF14:
	.string	"wint_t"
.LASF62:
	.string	"_lock"
.LASF64:
	.string	"_flags2"
.LASF54:
	.string	"_write"
.LASF33:
	.string	"__tm_year"
.LASF116:
	.string	"_nmalloc"
.LASF9:
	.string	"long double"
.LASF115:
	.string	"_nextf"
.LASF125:
	.string	"gpp_reg_addr"
.LASF32:
	.string	"__tm_mon"
.LASF42:
	.string	"_atexit"
.LASF120:
	.string	"suboptarg"
.LASF74:
	.string	"__sdidinit"
.LASF11:
	.string	"_off_t"
.LASF124:
	.string	"gpp_reg_idx"
.LASF79:
	.string	"_freelist"
.LASF10:
	.string	"_LOCK_RECURSIVE_T"
.LASF1:
	.string	"unsigned char"
.LASF82:
	.string	"_new"
.LASF114:
	.string	"_h_errno"
.LASF2:
	.string	"short int"
.LASF35:
	.string	"__tm_yday"
.LASF45:
	.string	"__sbuf"
.LASF91:
	.string	"_iobs"
.LASF88:
	.string	"__FILE"
.LASF19:
	.string	"_mbstate_t"
.LASF48:
	.string	"__sFILE"
.LASF129:
	.string	"/project/generic/users/bendavag/ws/final_project/pulpenix_E115_FPGA/apps/hello_gpp_gcd_accelerator"
.LASF101:
	.string	"_rand_next"
.LASF123:
	.string	"op_a"
.LASF103:
	.string	"_mblen_state"
.LASF70:
	.string	"_inc"
.LASF43:
	.string	"_ind"
.LASF73:
	.string	"_locale"
.LASF75:
	.string	"__cleanup"
.LASF72:
	.string	"_unspecified_locale_info"
.LASF23:
	.string	"_maxwds"
.LASF65:
	.string	"_reent"
.LASF93:
	.string	"_seed"
.LASF17:
	.string	"__count"
.LASF18:
	.string	"__value"
.LASF55:
	.string	"_seek"
.LASF118:
	.string	"_impure_ptr"
.LASF12:
	.string	"_fpos_t"
.LASF66:
	.string	"_errno"
.LASF87:
	.string	"char"
.LASF29:
	.string	"__tm_min"
.LASF132:
	.string	"get_factors"
.LASF94:
	.string	"_mult"
.LASF22:
	.string	"_next"
.LASF97:
	.string	"_strtok_last"
.LASF122:
	.string	"offset"
.LASF126:
	.string	"gpp_reg_val"
.LASF40:
	.string	"_fntypes"
.LASF95:
	.string	"_add"
.LASF21:
	.string	"__ULong"
.LASF108:
	.string	"_getdate_err"
.LASF119:
	.string	"_global_impure_ptr"
.LASF96:
	.string	"_unused_rand"
.LASF25:
	.string	"_wds"
.LASF34:
	.string	"__tm_wday"
.LASF89:
	.string	"_glue"
.LASF13:
	.string	"_ssize_t"
.LASF106:
	.string	"_l64a_buf"
.LASF84:
	.string	"_sig_func"
.LASF133:
	.string	"read_gpp_reg"
.LASF58:
	.string	"_nbuf"
.LASF117:
	.string	"_unused"
.LASF36:
	.string	"__tm_isdst"
.LASF99:
	.string	"_localtime_buf"
.LASF56:
	.string	"_close"
.LASF102:
	.string	"_r48"
.LASF104:
	.string	"_mbtowc_state"
.LASF78:
	.string	"_p5s"
.LASF131:
	.string	"main"
.LASF31:
	.string	"__tm_mday"
	.ident	"GCC: (GNU MCU Eclipse RISC-V Embedded GCC, 64-bits) 7.2.0"
