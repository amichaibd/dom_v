	.file	"pulp_bub_sort.c"
	.option nopic
	.text
	.section	.text.write_gpp_reg,"ax",@progbits
	.align	2
	.globl	write_gpp_reg
	.type	write_gpp_reg, @function
write_gpp_reg:
	li	a5,437280768
	add	a0,a0,a5
	sw	a1,0(a0)
	ret
	.size	write_gpp_reg, .-write_gpp_reg
	.section	.text.read_gpp_reg,"ax",@progbits
	.align	2
	.globl	read_gpp_reg
	.type	read_gpp_reg, @function
read_gpp_reg:
	li	a5,437280768
	add	a0,a0,a5
	lw	a0,0(a0)
	ret
	.size	read_gpp_reg, .-read_gpp_reg
	.section	.text.bubbleSort,"ax",@progbits
	.align	2
	.globl	bubbleSort
	.type	bubbleSort, @function
bubbleSort:
	addi	sp,sp,-32
	sw	s4,8(sp)
	mv	s4,a0
	lui	a0,%hi(.LC1)
	sw	s3,12(sp)
	addi	a0,a0,%lo(.LC1)
	addi	s3,a1,-1
	sw	s2,16(sp)
	sw	ra,28(sp)
	sw	s0,24(sp)
	sw	s1,20(sp)
	sw	s5,4(sp)
	mv	s2,a1
	call	bm_printf
	blez	s3,.L5
	addi	a5,s4,-4
	slli	s5,s2,2
	mv	s0,s4
	add	s5,a5,s5
	lui	s1,%hi(.LC2)
.L6:
	lw	a1,0(s0)
	addi	a0,s1,%lo(.LC2)
	addi	s0,s0,4
	call	bm_printf
	bne	s5,s0,.L6
.L5:
	slli	s1,s2,2
	addi	s1,s1,-4
	add	a5,s4,s1
	lw	a1,0(a5)
	lui	a0,%hi(.LC3)
	addi	a0,a0,%lo(.LC3)
	call	bm_printf
	li	a2,437284864
	sw	s2,-216(a2)
	blez	s2,.L7
	addi	a0,s1,4
	addi	a2,a2,-256
	mv	a5,s4
	add	a0,s4,a0
	sub	a2,a2,s4
.L8:
	lw	a3,0(a5)
	add	a4,a2,a5
	addi	a5,a5,4
	sw	a3,0(a4)
	bne	a0,a5,.L8
.L7:
	li	a5,437284864
	li	a4,1
	sw	a4,-80(a5)
	lw	a5,-76(a5)
	bnez	a5,.L9
	lui	s4,%hi(.LC4)
	li	s0,437284864
.L10:
	lw	a1,-60(s0)
	addi	a0,s4,%lo(.LC4)
	call	bm_printf
	lw	a5,-76(s0)
	beqz	a5,.L10
.L9:
	li	s0,437284864
	lui	a0,%hi(.LC5)
	sw	zero,-76(s0)
	addi	a0,a0,%lo(.LC5)
	call	bm_printf
	blez	s3,.L11
	addi	s0,s0,-256
	add	s3,s1,s0
	lui	s1,%hi(.LC2)
.L12:
	lw	a1,0(s0)
	addi	a0,s1,%lo(.LC2)
	addi	s0,s0,4
	call	bm_printf
	bne	s0,s3,.L12
.L11:
	addi	s2,s2,959
	li	a5,437280768
	slli	s2,s2,2
	add	s2,s2,a5
	lw	a1,0(s2)
	lui	a0,%hi(.LC6)
	addi	a0,a0,%lo(.LC6)
	call	bm_printf
	lw	s0,24(sp)
	lw	ra,28(sp)
	lw	s1,20(sp)
	lw	s2,16(sp)
	lw	s3,12(sp)
	lw	s4,8(sp)
	lw	s5,4(sp)
	lui	a0,%hi(.LC7)
	addi	a0,a0,%lo(.LC7)
	addi	sp,sp,32
	tail	bm_printf
	.size	bubbleSort, .-bubbleSort
	.section	.text.startup.main,"ax",@progbits
	.align	2
	.globl	main
	.type	main, @function
main:
	lui	a0,%hi(.LC8)
	addi	sp,sp,-48
	addi	a0,a0,%lo(.LC8)
	sw	ra,44(sp)
	call	bm_printf
	li	a4,437284864
	li	a3,1
	lui	a5,%hi(.LANCHOR0)
	sw	a3,-92(a4)
	addi	a5,a5,%lo(.LANCHOR0)
	sw	zero,-92(a4)
	lw	t4,0(a5)
	lw	t3,4(a5)
	lw	t1,8(a5)
	lw	a7,12(a5)
	lw	a6,16(a5)
	lw	a2,20(a5)
	lw	a5,24(a5)
	sw	zero,-84(a4)
	sw	a3,-96(a4)
	li	a1,7
	addi	a0,sp,4
	sw	t4,4(sp)
	sw	t3,8(sp)
	sw	t1,12(sp)
	sw	a7,16(sp)
	sw	a6,20(sp)
	sw	a2,24(sp)
	sw	a5,28(sp)
	call	bubbleSort
	lui	a0,%hi(.LC7)
	addi	a0,a0,%lo(.LC7)
	call	bm_printf
	call	bm_quit_app
	lw	ra,44(sp)
	li	a0,0
	addi	sp,sp,48
	jr	ra
	.size	main, .-main
	.section	.rodata
	.align	2
	.set	.LANCHOR0,. + 0
.LC0:
	.word	64
	.word	34
	.word	25
	.word	12
	.word	22
	.word	11
	.word	90
	.section	.rodata.bubbleSort.str1.4,"aMS",@progbits,1
	.align	2
.LC1:
	.string	"\n==Start Buble-Sort %X # ==\n"
	.zero	3
.LC2:
	.string	"[%X],"
	.zero	2
.LC3:
	.string	"[%X]\n--------------\n"
	.zero	3
.LC4:
	.string	"pc=%X        //C message: Polling\n"
	.zero	1
.LC5:
	.string	"\n==Results Bubble-Sort == \n"
.LC6:
	.string	"[%X]\n--------------"
.LC7:
	.string	"\n***finish***\n"
	.section	.rodata.main.str1.4,"aMS",@progbits,1
	.align	2
.LC8:
	.string	"\n***TESTING Bubble-Sort***\n"
	.ident	"GCC: (GNU MCU Eclipse RISC-V Embedded GCC, 64-bit) 8.2.0"
