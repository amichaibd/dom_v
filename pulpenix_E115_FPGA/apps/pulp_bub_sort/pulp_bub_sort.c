#include <stdio.h>
#include <stdlib.h>
#include <bm_print_scan_uart.h>
#include <uart.h>
//--------------------------------------------------------------------------------------------
typedef enum {FALSE,TRUE} boolean;  // Define boolean type For more readable code
// GPP Registers access functions
#define GPP_BASE_ADDR 0x1A106000  // This is the base address of the registers.
                                  // Each register index is an increment of 4 in address (ass addresses are in bytes)
                                  // Notice Illegal address will return "deadbeef"
//--------------------------------------------------------------------------------------------
void write_gpp_reg (unsigned int gpp_reg_idx, unsigned int gpp_reg_val) { 
  volatile unsigned int * gpp_reg_addr ; // "volatile" guarantee that compiler will not optimize out the physical access.
  gpp_reg_addr =  (volatile unsigned int *)(GPP_BASE_ADDR+(gpp_reg_idx)) ;  
  *gpp_reg_addr  = gpp_reg_val ;
}
//--------------------------------------------------------------------------------------------
unsigned int read_gpp_reg (unsigned int gpp_reg_idx) { 
  volatile unsigned int * gpp_reg_addr ;
  gpp_reg_addr =  (volatile unsigned int *)(GPP_BASE_ADDR+(gpp_reg_idx)) ;
  return *gpp_reg_addr ;
}
//--------------------------------------------------------------------------------------------
void bubbleSort(int arr[], int n) {
    int reg[32];
    bm_printf("\n==Start Buble-Sort %X # ==\n",n) ; 
    //bm_printf("pc = [%X]\n",read_gpp_reg(0xFC4) ); 
    for(int i=0 ; i<n-1; i++) {
      bm_printf("[%X],",arr[i]);
    }
    bm_printf("[%X]\n--------------\n",arr[n-1]); 

        write_gpp_reg (0xF28,n) ;    // write the array to memory
    for(int i=0 ; i<n; i++) {
        write_gpp_reg (0xF00+i*4,arr[i]) ;    // write the array to memory
        }
    write_gpp_reg (0xFB0,1) ;    // write to CSR[start]
    // Polling check if done 
    while (read_gpp_reg(0xFB4)==0) {                  //wait until CSR[Done]==1
        //write_gpp_reg (0xFA0 , 1) ; // write to CSR[EnPC] - pause running instructions
        bm_printf("pc=%X        //C message: Polling\n",read_gpp_reg(0xFC4) ); //MMIO_ER[pc]
    }  
    write_gpp_reg (0xFB4 , 0) ;                       // reset CSR[Done]
    bm_printf("\n==Results Bubble-Sort == \n") ; 
    for(int i=0 ; i<n-1; i++){
        bm_printf("[%X],",read_gpp_reg(0xF00+i*4));
    }
    bm_printf("[%X]\n--------------",read_gpp_reg(0xF00+n*4-4)); 
    bm_printf("\n***finish***\n") ;   // bare-metal output to terminal
}
//============================================================================================
int main() {  // The Main program
  bm_printf("\n***TESTING Bubble-Sort***\n") ;   // bare-metal output to terminal
//rst DOM and config the CSR
  write_gpp_reg (0xFA4 , 1) ; // write to CSR[rstPC]
  write_gpp_reg (0xFA4 , 0) ; // write to CSR[rstPC]
  write_gpp_reg (0xFAC , 0) ; // write to CSR[gear] - set the sample cycle in DOM
  write_gpp_reg (0xFA0 , 1) ; // write to CSR[EnPC] - start running instructions
  
  int arr[] = {64, 34, 25, 12, 22, 11, 90}; 
  int n = sizeof(arr)/sizeof(arr[0]);  
  bubbleSort(arr, n); 
  
  bm_printf("\n***finish***\n") ;   // bare-metal output to terminal
  //// Finish 
  bm_quit_app() ; // uart message simulation/emulation in simulation/FPGA environment.
  return 0;
}
