
`include "tb_mem_pkg_fpga_sim.sv"

module tb;

  timeunit      1ns;
  timeprecision 1ps;

  logic         s_clk   = 1'b0;
  logic         s_rst_n = 1'b0;
  
  logic         altera_clk25mhz;
  logic         brd_rst;

  logic         uart_tx;
  logic         uart_rx;

  uart_bus
  #(
    .BAUDRATE(115200),
    .PARITY_EN(0)
  )
  uart
  (
    .rx         ( uart_tx ),
    .tx         ( uart_rx ),
    .rx_en      ( 1'b1    )
  );
    
  
  pulpenix pulpenix_i (

    .altera_clk25mhz ( altera_clk25mhz ), // input  
    .brd_rst         ( brd_rst         ), // input  
    .uart_tx         ( uart_tx         ), // output 
    .uart_rx         ( uart_rx         ), // input  
    .led             (                 )  // output 
  );
    
//--------------------------------------------------------------------


  initial
  
  begin  
`ifndef PULP_FPGA_EMUL
    mem_preload();  
`endif
    brd_rst = 1'b0;
    #500ns;
    brd_rst = 1'b1;
    #500ns;
  end
  
endmodule
