// Clocked "synthesizable" uart test bench ,mostly to enable verilotor

`include "pulpenix_defines.v"
`include "smart_uart_defines.sv"

`define VERILOG_STDIN  32'h8000_0000 // Verilog pre-opened
`define VERILOG_STDOUT 32'h8000_0002 // Verilog pre-opened

module uart_bus_clocked
  #(
    parameter BAUDRATE = `DEFAULT_BAUDRATE,
    parameter PARITY_EN = `DEFAULT_PARITY_EN,
    parameter NUM_SELECTABLE_UARTS = 1   
    )
  (
    input rst_n,
    input clk,
  
    input  [NUM_SELECTABLE_UARTS-1:0] uart_rx,
    output [NUM_SELECTABLE_UARTS-1:0] uart_tx,

    input  logic rx_en
  );
  timeunit      1ns;
  timeprecision 1ps;



  logic  rx,tx ; 
  logic [$clog2(NUM_SELECTABLE_UARTS):0] selected_uart = 0 ;  

  assign rx = (NUM_SELECTABLE_UARTS==1) ? uart_rx[0] : uart_rx[selected_uart] ;
  
  generate
    genvar u;
    for (u=0;u<NUM_SELECTABLE_UARTS;u++) begin : seluart
      assign uart_tx[u]  = ((NUM_SELECTABLE_UARTS==1)||(selected_uart==u)) ? tx : 1 ; 
    end
  endgenerate
   


`ifndef SIM_UART_SPEED_FACTOR
  localparam NS_PER_BIT = 1000000000/BAUDRATE;  // = 8680.5556 for BAUDRATE=115200 @40ns cycle (25 MHz)
`else    
  localparam NS_PER_BIT = (1000000000/BAUDRATE)/`SIM_UART_SPEED_FACTOR ;  
`endif

  localparam CLK_PERIOD_NS = `DEFAULT_CLK_PERIOD_NS ;
  localparam CLK_DIV_CNTR = (NS_PER_BIT/CLK_PERIOD_NS)-1 ; // =  217.01 for BAUDRATE=115200 @40ns cycle (25 MHz)
 
    
  logic [7:0]       character,ms_char,ls_char;
  logic [256*8-1:0] stringa;
  logic             parity;
  integer           charnum;
  logic [31:0]      dpi_ret_val ;  // dpi tasks return values
  
  
  logic [8*8-1:0]  ready_string      = {"Ready> "} ;
  logic [8*8-1:0]  gdb_ready_string  = {"gdb-rdy"} ;  // String from Core , indicating it is ready for gdb debug commumication
  logic [14*8-1:0] pyshell_quit_str  = {"$pyshell quit"} ; //  String from Core , indicating pyshell to quit and simulation to stop
  logic [14*8-1:0]  stringa_head = "" ;
  logic prompt_active = 0 ; 
  logic su_cmd_rsp_active = 0 ;
  logic gdb_ready = 0 ;  
  logic su_uart_gateway_msg_on = 0 ;

  int su_rsp_byte_cnt = 0 ;  // response to smart uart terminal 
  int dpi_rsp_byte_cnt = 0 ; // response to mem_dpi.svh
  
  initial
  begin
  
    $display("\n\nUART TB INFO: BOAUDRATE = %-d , CLK_DIV_CNTR =%-d" , BAUDRATE ,CLK_DIV_CNTR) ;
`ifdef SIM_UART_SPEED_FACTOR
    $display("Notice SIM_UART_SPEED_FACTOR=%d, Simulation pseudo Baudrate = %d",`SIM_UART_SPEED_FACTOR,`SIM_UART_SPEED_FACTOR*BAUDRATE) ;
`endif
    
    tx   = 1'b1;
  end

// Capture RX characters


    reg [7:0] rx_data ;
    reg rx_valid ;
    
    uart_rx uart_rx_i
    (
        .clk_i              ( clk              ),
        .rstn_i             ( rst_n            ),
        .rx_i               ( rx               ),
        .cfg_en_i           ( 1'b1             ),
        .cfg_div_i          ( CLK_DIV_CNTR     ),
        .cfg_parity_en_i    ( PARITY_EN        ),
        .cfg_bits_i         ( 2'b11            ),
        .busy_o             (                  ),
        .err_o              (                  ),
        .err_clr_i          ( 1'b1             ),
        .rx_data_o          ( rx_data          ),
        .rx_valid_o         ( rx_valid         ),
        .rx_ready_i         ( 1'b1             )
    );

  always @(posedge clk) begin
   if (rx_en && rx_valid) begin 
  
      character = rx_data ;
      
      if (character==`SU_CMD_RSP) su_cmd_rsp_active = 1 ;

      if (su_cmd_rsp_active && (su_rsp_byte_cnt>0)) begin
         if (su_rsp_byte_cnt<5) begin   // skip SU_CMD_RSP byte
           if (character[7:4]<=9) ms_char = "0" + character[7:4]  ;
           else ms_char = "a" +(character[7:4] - 10) ;
           $write("%c", ms_char);
           if (character[3:0]<=9) ls_char = "0" + character[3:0]  ;
           else ls_char = "a" +(character[3:0] - 10) ;                            
           $write("%c", ls_char);
         end 
         su_rsp_byte_cnt =  su_rsp_byte_cnt-1 ;
         if (su_rsp_byte_cnt==0) su_cmd_rsp_active = 0 ;
         
      end  else
      
      if (su_cmd_rsp_active && (dpi_rsp_byte_cnt>0)) begin 
         if (dpi_rsp_byte_cnt<5) dpi_ret_val[(dpi_rsp_byte_cnt-1)*8 +: 8] = character ; // skip SU_CMD_RSP byte
         dpi_rsp_byte_cnt =  dpi_rsp_byte_cnt-1 ; 
         if (dpi_rsp_byte_cnt==0) su_cmd_rsp_active = 0 ; 
         
      end else begin  // print apb uart
      
           $write("%c", character); // ascii charterer from core via apb
           
           if (su_uart_gateway_msg_on && (character==8'h0A)) begin
              su_uart_gateway_msg_on = 0 ;      
              send_char(8'h0A) ; // to return control to SW menu via apb            
           end
                             
           stringa[(255-charnum)*8 +: 8] = character;
           
           stringa_head[(12-charnum)*8 +: 8] = character;
              
           if (stringa_head==ready_string) begin
             prompt_active = 1 ; 
             stringa_head = "";
           end
           
           if (stringa_head==gdb_ready_string) begin // Indicate core is ready for debug
             gdb_ready = 1 ; 
             stringa_head = "";
             $write("\n");
           end
                    
           if (stringa_head==pyshell_quit_str) begin // stop simulation on pyshell quit
             $write("$pyshell quit() printed by SW , stopping simulation\n");
             $stop() ;
           end
                      
           if (character == 8'h0A || charnum == 254) // line feed or max. chars reached
           begin
               if (character == 8'h0A)
                 stringa[(255-charnum)*8 +: 8] = 8'h0; // null terminate string, replace line feed
               else
                 stringa[(255-charnum-1)*8 +: 8] = 8'h0; // null terminate string
               
               charnum = 0;
               stringa = "";
           end else charnum = charnum + 1;           
      end // print apb uart
      
    end  // rx_en && rx_valid
    
    else  
    begin
     if (!rx_en) begin
      charnum = 0;
      stringa = "";
      //#10;
     end 
    end
  end // always 
  
  // Send Terminal input to DUT 
  logic [7:0] c ;  
  logic su_msg_on = 0 ;
  logic su_cmd_identified = 0 ; 
  logic ui_cmd_identified = 0 ;   
  logic [31:0] su_arg_word ;
  integer tb_su_cmd_str_idx = 0 ;
  logic [(2*8)-1:0] tb_su_cmd_str ;
  integer nibble_idx = 0 ;
  integer byte_idx = 0 ;
  logic dpi_msg_on = 0 ; 
  logic prompt_msg_on = 0 ;    

  
  always @(prompt_active) begin               
     su_msg_on = 0 ;
     su_uart_gateway_msg_on = 0 ;
    su_cmd_identified = 0 ; 
     ui_cmd_identified = 0 ;     
     tb_su_cmd_str_idx = 0 ;
     tb_su_cmd_str = ""  ;
     if (dpi_msg_on) wait (!dpi_msg_on) ; // Avoid collision on tx 
     prompt_msg_on = 1 ;
     while (prompt_active) begin
     
           c = $fgetc(`VERILOG_STDIN) ;  
           
           if (c=="^") begin
              su_msg_on = 1 ;
              continue ;
           end 
           else if (c=="#") su_uart_gateway_msg_on = 1 ;
           
                                               
           if (!su_msg_on)  send_char(c); // send to apb slave for core to read
           else begin // su_msg_on
               if ((!su_cmd_identified)&&(!ui_cmd_identified)) begin 
                 tb_su_cmd_str[(1-tb_su_cmd_str_idx)*8 +: 8] = c;
                 tb_su_cmd_str_idx++ ;
                 //if (tb_su_cmd_str=="ww") begin
                   if (c=="w") begin
                    tb_su_cmd_str = "" ;
                    su_cmd_identified = 1 ;
                    send_char(`SU_CMD_WR_WORD); 
                 end
                 //else if (tb_su_cmd_str=="rw") begin
                    if (c=="r") begin
                    tb_su_cmd_str = "" ;
                    su_cmd_identified = 1 ; 
                    su_rsp_byte_cnt = 5 ;       // SU_CMD_RSP byte + data word          
                    send_char(`SU_CMD_RD_WORD); 
                 end 
                 else if (tb_su_cmd_str=="ui") begin  // Multi-Uart support, uart selection
                    tb_su_cmd_str = "" ;
                    ui_cmd_identified = 1 ; 
                    selected_uart = 0 ;
                 end 

               end else if (ui_cmd_identified) begin 
                  if ((c>="0")&&(c<="9")) begin
                     selected_uart = (selected_uart*10) + (c - "0") ;  
                     //$display ("Selected test-bench UART port index = %-d",selected_uart);
                  end
               end else if ((c>="0")&&(c<="9")) begin 
                  su_arg_word[(7-nibble_idx)*4+:4] = (c - "0") ;
                  nibble_idx++;              
               end else if ((c>="a")&&(c<="f")) begin 
                  su_arg_word[(7-nibble_idx)*4+:4] = 10+(c - "a") ; 
                  nibble_idx++;                  
               end else if ((nibble_idx > 0) && ((c==" ")||(c==8'h0A))) begin
                  su_arg_word = su_arg_word >> ((8-nibble_idx)*4) ;
                  for (byte_idx=3;byte_idx>=0;byte_idx--) send_char(su_arg_word[(byte_idx*8) +: 8]);
                  nibble_idx = 0 ;
                  byte_idx = 0 ;                  
               end  
               if (c==8'h0A) begin
                 if (ui_cmd_identified) $display ("Selected test-bench UART port index = %-d",selected_uart);  
                 send_char(c); // return control to prompt
               end  
           end // su_msg_on
                                              
           prompt_active = (c!=8'h0A) ; // Return control on line feed
                      
           if (!prompt_active) prompt_msg_on = 0 ;
     end // while
   end  // always    

  


  task send_char(input logic [7:0] c);
    int i;

    // start bit
    tx = 1'b0;

    for (i = 0; i < 8; i++) begin
      #(NS_PER_BIT);
      tx = c[i];
    end

    // stop bit
    #(NS_PER_BIT);
    tx = 1'b1;
    #(NS_PER_BIT);
  endtask

 // Master Read/Write over uart, mostly to suppurt mem_dpi.svh debugger interface

  task uart_read_nword;
    input   [31:0] addr;
    input int      n;
    inout [31:0] data_out[];
    logic [31:0] word_addr ;
    if (prompt_msg_on) wait (!prompt_msg_on) ; // Avoid collision on tx    
    dpi_msg_on = 1 ;
    word_addr = addr ;
    for (int i=0;i<n;i++) begin
       word_addr = addr + 4*i ;
       send_char(`SU_CMD_RD_WORD);
       dpi_rsp_byte_cnt = 5 ;  // SU_CMD_RSP byte + data word
       for (byte_idx=3;byte_idx>=0;byte_idx--) send_char(word_addr[(byte_idx*8) +: 8]);   
       wait (su_cmd_rsp_active==1) ;
       wait (dpi_rsp_byte_cnt==0) ;  
       data_out[i][31:0] = uart.dpi_ret_val[31:0] ; 
     end  
     dpi_msg_on = 0 ;
  endtask
  
 
  task uart_read_word;
    input   [31:0] addr;
    output  [31:0] data;
    logic   [31:0] tmp[1];
    begin
      uart_read_nword(addr, 1, tmp);
      data = tmp[0];
    end
  endtask

  task uart_read_halfword;
    input   [31:0] addr;
    output  [15:0] data;

    logic   [31:0] temp;
    begin
      uart_read_word({addr[31:2], 2'b00}, temp);

      case (addr[1])
        1'b0: data[15:0] = temp[15: 0];
        1'b1: data[15:0] = temp[31:16];
      endcase
    end
  endtask

  task uart_read_byte;
    input   [31:0] addr;
    output  [ 7:0] data;

    logic   [31:0] temp;
    begin
      uart_read_word({addr[31:2], 2'b00}, temp);

      case (addr[1:0])
        2'b00: data[7:0] = temp[ 7: 0];
        2'b01: data[7:0] = temp[15: 8];
        2'b10: data[7:0] = temp[23:16];
        2'b11: data[7:0] = temp[31:24];
      endcase
    end
  endtask

  task uart_write_word;
    input   [31:0] addr;
    input   [31:0] data;
    int b ;
    begin
       send_char(`SU_CMD_WR_WORD);
       for (b=3;b>=0;b--) send_char(addr[(b*8) +: 8]);    
       for (b=3;b>=0;b--) send_char(data[(b*8) +: 8]);       
    end
  endtask
 
  task uart_write_halfword;
    input   [31:0] addr;
    input   [15:0] data;
    logic   [31:0] temp;
    begin
      uart_read_word({addr[31:2], 2'b00}, temp);

      case (addr[1])
        1'b0: temp[15: 0] = data[15:0];
        1'b1: temp[31:16] = data[15:0];
      endcase

      uart_write_word({addr[31:2], 2'b00}, temp);
    end
  endtask

  task uart_write_byte;
    input   [31:0] addr;
    input   [ 7:0] data;

    logic   [31:0] temp;
    begin
      uart_read_word({addr[31:2], 2'b00}, temp);

      case (addr[1:0])
        2'b00: temp[ 7: 0] = data[7:0];
        2'b01: temp[15: 8] = data[7:0];
        2'b10: temp[23:16] = data[7:0];
        2'b11: temp[31:24] = data[7:0];
      endcase

      uart_write_word({addr[31:2], 2'b00}, temp);
    end
  endtask

endmodule

