//////////////////////////////////////////////////////////////////
//                                                              //
//  Top-level module instantiating the entire Amber 2 system.   //
//                                                              //
//  This file is part of the Amber project                      //
//  http://www.opencores.org/project,amber                      //
//                                                              //
//  Description                                                 //
//  This is the highest level synthesizable module in the       //
//  project. The ports in this module represent pins on the     //
//  FPGA.                                                       //
//                                                              //
//  Author(s):                                                  //
//      - Conor Santifort, csantifort.amber@gmail.com           //
//                                                              //
//////////////////////////////////////////////////////////////////
//                                                              //
// Copyright (C) 2010 Authors and OPENCORES.ORG                 //
//                                                              //
// This source file may be used and distributed without         //
// restriction provided that this copyright statement is not    //
// removed from the file and that any derivative work contains  //
// the original copyright notice and the associated disclaimer. //
//                                                              //
// This source file is free software; you can redistribute it   //
// and/or modify it under the terms of the GNU Lesser General   //
// Public License as published by the Free Software Foundation; //
// either version 2.1 of the License, or (at your option) any   //
// later version.                                               //
//                                                              //
// This source is distributed in the hope that it will be       //
// useful, but WITHOUT ANY WARRANTY; without even the implied   //
// warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR      //
// PURPOSE.  See the GNU Lesser General Public License for more //
// details.                                                     //
//                                                              //
// You should have received a copy of the GNU Lesser General    //
// Public License along with this source; if not, download it   //
// from http://www.opencores.org/lgpl.shtml                     //
//                                                              //
//////////////////////////////////////////////////////////////////

module altera (
input                       sys_rst,
input                       sys_clk,
//input wire [2:0]			brd_id,
//inout wire [2:0]			io_flash_control,
//output wire [2:0]			reset_from_master,
input						altera_clk25mhz,
//Common UART 
output wire                 o_uart_rx,
input  wire                 i_uart_tx,
// Flash Interface
//inout	wire [3:0]  		io_qspi_dat ,
input   wire                pad_spif_di ,
output  wire                pad_spim_do_spis_do ,
output	wire	    		o_qspi_cs_n ,
output	wire 				o_qspi_sck	,
// SPI
//output	wire				o_m_spi_clk_out,
//input	wire				i_spi_din,
//output	wire				o_spi_dout,		
//output	wire				o_m_spi_cs,		
// SPI Slave
//input	wire				i_s_spi_clk_in,	
//output	wire				o_spi_slave_dout,
//input	wire				i_s_spi_cs_in,
// M<odem ADC connections
inout	wire [13:0]			QAM_data_out_re	,
inout	wire [13:0]			QAM_data_out_im	,
inout	wire				clk_real_out ,
inout	wire				clk_imag_out ,
inout	wire				wrt_real_out ,
inout	wire				wrt_imag_out ,
//leds
output  [3:0]       		led
);

//wire i_qspi_dat ;
//wire o_qspi_dat ;
//wire [1:0] o_qspi_mod ;
//assign io_qspi_dat = (!o_qspi_mod[1]) ? ({2'b11,1'bz,o_qspi_dat}) : //serial
//                     ((o_qspi_mod[0]) ? (4'bzzzz) : ({3'b000,o_qspi_dat})) ; //quad

  pulpenix pulpenix_i1 (
    .clk_msystem            ( altera_clk25mhz  ), // input 
    .rst_msystem_n          ( sys_rst          ), // input  
    .clk_mdm                ( altera_clk25mhz  ), // input 
    .rst_mdm_n              ( sys_rst          ), // input  
    .pad_test_mode          ( 1'b0             ), // for DFT 
    .pad_scan_enable        ( 1'b0             ), // for DFT 
    .pad_master_slave       ( 1'b1             ), //1=master 0=slave
    
    .pad_uart_tx            ( o_uart_rx        ), // output 
    .pad_uart_rx            ( i_uart_tx        ), // input 
    .o_qspi_mod 			( o_qspi_mod       ),
    .led                    ( led              ), // output 
     //.fetch_enable_i  (fetch_enable      ),
                                                 
    .pad_spim_cs1           (                  ),
    .pad_spim_cs2           (                  ),
    .pad_spim_cs3           (                  ),
    .pad_spim_cs4           (                  ),
    .pad_spim_din1          ( 1'b0             ),  
    .pad_spim_din2_spis_clk ( 1'b0             ),  
    .pad_spim_din3_spis_di  ( 1'b0             ),  
    .pad_spim_din4          ( 1'b0             ),  
    .pad_spim_clk           ( o_qspi_sck       ),
    .pad_spim_do_spis_do    ( pad_spim_do_spis_do       ),
    .pad_spif_cs            ( o_qspi_cs_n      ),
    .pad_spif_di            ( pad_spif_di      ),
    .pad_spis_cs            ( 1'b1             ),
    
    .QAM_data_out_re        ( QAM_data_out_re  ),
    .QAM_data_out_im        ( QAM_data_out_im  ),
    .clk_real_out           ( clk_real_out     ),
    .clk_imag_out           ( clk_imag_out     ),
    .wrt_real_out           ( wrt_real_out     ),
    .wrt_imag_out           ( wrt_imag_out     ),

    .o_rx_agc_grf           (                  ),          

    .o_clk_msystem_rd       (                  ),
    .o_clk_msystem_wr       (                  ),
    .o_a_msystem2regf       (                  ),
    .o_d_msystem2regf       (                  ),
    .i_d_regf2msystem       (                  ),
    .i_rf_regs2msystem      (                  ) 
    
    );


endmodule
