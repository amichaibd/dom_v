
// Test and template for PMM

module tb_pmm
#(
    parameter APB_ADDR_WIDTH = 20  // 1M Byte address space
)
(
    input  logic                      HCLK,
    input  logic                      HRESETn,
    input  logic [APB_ADDR_WIDTH-1:0] PADDR,
    input  logic               [31:0] PWDATA,
    input  logic                      PWRITE,
    input  logic                      PSEL,
    input  logic                      PENABLE,
    output logic               [31:0] PRDATA,
    output logic                      PREADY,
    output logic                      PSLVERR

);

 localparam NUM_MEM_WORDS = (2**APB_ADDR_WIDTH)/4 ;

 reg [31:0] mem [NUM_MEM_WORDS-1:0];
 reg [31:0] data_out ;
 reg ready ;

 wire apb_setup_wr = PSEL &&  PWRITE  ;    
 wire apb_setup_rd = PSEL && !PWRITE  ;    
 
 wire apb_access = PSEL && PENABLE ;
 
 wire [APB_ADDR_WIDTH-3:0] mem_addr = PADDR[APB_ADDR_WIDTH-1:2] ;
 

// WRITE 
 always @(posedge HCLK, negedge HRESETn)
       if (apb_setup_wr && !ready)         // write only once 
         mem[mem_addr] <= PWDATA  ;

 // SYNCHRONOUS READ
 always @(posedge HCLK, negedge HRESETn) 
   if (apb_setup_rd && !ready) data_out <= mem[mem_addr] ;    // read only once
 
 assign PRDATA = data_out ;
   
 
 // READY HANDLING
  always @(posedge HCLK, negedge HRESETn) 
     if(~HRESETn) 
       ready <= 0 ;
     else 
       ready <= PSEL ;
       
  assign PREADY = ready && PENABLE ;

  // ERROR HANDLING
  assign PSLVERR = 1'b0; // not supporting transfer failure
    
 //=====================================================
 
 // TB Messages
 
 reg [APB_ADDR_WIDTH-1:0] addr_s ;
 reg [31:0] wr_data_s ;
 reg write_s ;

 always @(posedge HCLK) begin
 
       addr_s <= PADDR ;
       wr_data_s <= PWDATA ;
       write_s <= PWRITE;
     
       if (PREADY) begin
        if (write_s)
          $display($time," PMM TB: Writing %x to addr %x (pmm[%05X])",wr_data_s,{12'h1a2,addr_s},addr_s[APB_ADDR_WIDTH-1:2]) ;
        else
          $display($time," PMM TB: Reading %x from addr %x (pmm[%05X])",PRDATA,{12'h1a2,addr_s},addr_s[APB_ADDR_WIDTH-1:2]) ;
       end 
 end

 

endmodule

