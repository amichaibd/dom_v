
`define CLK_PERIOD 40.00

`timescale 1 ns / 1 ns

module ALTPLL1 #(parameter  CLK_PERIOD = 0) (

	input	  areset,
	input	  inclk0,
	output	  c0,
	output	  c1,
	output	  c2,
	output	  c3,
	output	  locked );
    
  reg clk ;

  initial
  begin
    #(CLK_PERIOD/2);
    clk = 1'b1;
    forever clk = #(CLK_PERIOD/2) ~clk;
  end  

  assign c0 = clk ;
       
endmodule    
