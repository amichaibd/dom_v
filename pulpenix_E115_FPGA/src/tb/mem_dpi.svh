typedef struct {
  byte we;
  int  addr;
  int  size;
} mem_packet_t;

import "DPI-C"         function void mem_init(input int port);
import "DPI-C"         function int  mem_poll(output mem_packet_t packet, output byte buffer[1024]);
import "DPI-C"         function int  mem_push(input mem_packet_t packet, input byte buffer[1024]);

integer dpi_trace_file ;
logic dpi_trace_enable = 0 ;

typedef enum {DPER_UART,DPER_SPI} dbg_periph_t ;
dbg_periph_t dbg_periph = DPER_UART ;

task mem_dpi;


  input int port;
  byte buffer[1024];
  mem_packet_t packet;
  int i;
  int j;
  int local_addr;
  int local_size;
  logic [31:0] rdata_temp;
  logic [31:0] rdata_temp_arr[];
   
  begin
  

    $display($time," Message from mem_dpi.svh: Entered task mem_dpi , port = %d\n",port) ;
        
    if (dpi_trace_enable) dpi_trace_file = $fopen("dpi_trace_log.txt","w");
    
    wait (uart.gdb_ready) ;  // Wait till core indicates it is ready to initiate debug communication. (gdb-rdy message arrived over UART from Core)
    // for (i = 0; i < 100; i++) @(posedge s_clk); // wait for additional 100+ cycles to let core enter debug mode before we start polling (not sure why needed) 
    
    @(posedge s_clk); //  align with clock edge as uart.gdb_ready is asynchronous.

    $display($time," Message from mem_dpi.svh: Initializing mem_dpi\n") ;   
    
    mem_init(port);
 
    $display($time," Message from mem_dpi.svh: START NOW debug_bridge task and resume run\n") ;
    $stop() ; // To allow automatic bridge invocation from simulation script run_gdb.tcl        
    for (i = 0; i < 100; i++) @(posedge s_clk); // wait for additional 100+ cycles to let bridge code invoke before we start polling (not sure why needed) 
    $display($time," Message from mem_dpi.svh: AUTO STARTED DEBUG BRIDGE PROCESS , ECLIPSE/GDB should be up to proceed");    


    while(1) begin
      
      //for (i = 0; i < 100; i++) @(posedge s_clk); // wait some cycles between polls
      @(posedge s_clk); // CHANGED BY UDI TO CHECK EVERY CYCLE
      //$display($time," TMP_DBG mem_dpi.svh Polling");

      if (mem_poll(packet, buffer) == 0) begin

      
        // DEBUG BY UDI
        // $write("TMP_DBG: mem_poll Got a valid packet , packet.addr=%h,packet.size=%d,packet.we=%d ,buffer=",packet.addr,packet.size,packet.we) ;
        // for (i = 0; i < packet.size; i++) begin
        //    $write("%h",buffer[i]);
        //    if (i!=packet.size) $write(",");
        // end
        // $write("\n");
        // END OF DEBUG BY UDI
             
        // we got a valid packet
        // let's perform the SPI transactions and send back the result
        local_addr = packet.addr;
        local_size = packet.size;
        i = 0;

        if (packet.we) begin
          // write

          // first align addresses to words
          if (local_addr[0]) begin
            dpi_write_byte(use_qspi, local_addr, buffer[i]);
            i          += 1;
            local_addr += 1;
            local_size -= 1;
          end

          if (local_addr[1] && local_size >= 2) begin
            dpi_write_halfword(use_qspi, local_addr, {buffer[i+1][7:0], buffer[i][7:0]});
            i          += 2;
            local_addr += 2;
            local_size -= 2;
          end

          // now main loop, always aligned
          // TODO: this can be replaced by one single burst
          while(local_size >= 4) begin
            dpi_write_word(use_qspi, local_addr, {buffer[i+3][7:0], buffer[i+2][7:0], buffer[i+1][7:0], buffer[i][7:0]});
            i          += 4;
            local_addr += 4;
            local_size -= 4;
          end

          // now take care of the last max 3 bytes
          if (local_size >= 2) begin
            dpi_write_halfword(use_qspi, local_addr, {buffer[i+1][7:0], buffer[i][7:0]});
            i          += 2;
            local_addr += 2;
            local_size -= 2;
          end

          if (local_size >= 1) begin
            dpi_write_byte(use_qspi, local_addr, buffer[i]);
            i          += 1;
            local_addr += 1;
            local_size -= 1;
          end
        end else begin
          // read

          // first align addresses to words
          if (local_addr[0]) begin
            dpi_read_byte(use_qspi, local_addr, rdata_temp[7:0]);
            buffer[i] = rdata_temp[7:0];
            i          += 1;
            local_addr += 1;
            local_size -= 1;
          end

          if (local_addr[1] && local_size >= 2) begin
            dpi_read_halfword(use_qspi, local_addr, rdata_temp[15:0]);
            buffer[i]   = rdata_temp[ 7:0];
            buffer[i+1] = rdata_temp[15:8];
            i          += 2;
            local_addr += 2;
            local_size -= 2;
          end

          // now main loop, always aligned
          // Done in one single burst
          rdata_temp_arr = new[local_size/4];
          dpi_read_nword(use_qspi, local_addr, local_size/4, rdata_temp_arr);
          for (j = 0; j < local_size/4; j++) begin
            buffer[i]   = rdata_temp_arr[j][ 7:0];
            buffer[i+1] = rdata_temp_arr[j][15:8];
            buffer[i+2] = rdata_temp_arr[j][23:16];
            buffer[i+3] = rdata_temp_arr[j][31:24];
            i += 4;
          end
          local_addr += (local_size/4) * 4;
          local_size -= (local_size/4) * 4;
          rdata_temp_arr.delete();

          // now take care of the last max 3 bytes
          if (local_size >= 2) begin
            dpi_read_halfword(use_qspi, local_addr, rdata_temp[15:0]);
            buffer[i]   = rdata_temp[ 7:0];
            buffer[i+1] = rdata_temp[15:8];
            i          += 2;
            local_addr += 2;
            local_size -= 2;
          end

          if (local_size >= 1) begin
            dpi_read_byte(use_qspi, local_addr, rdata_temp[7:0]);
            buffer[i] = rdata_temp[7:0];
            i          += 1;
            local_addr += 1;
            local_size -= 1;
          end
        end

        if (mem_push(packet, buffer) != 0)
          $display("mem_push has failed");
      end
    end
  end
endtask

//--------------------------------------------------------------------


  
// spi/uart interface 

  task dpi_read_nword;
    input          use_qspi;
    input   [31:0] addr;
    input int      n;
    inout   [31:0] data[];

    logic [31:0] uart_data[] ;  // TMP 

    // if (dbg_periph==DPER_UART)  uart.uart_read_nword(addr,n,data);     // TMP DBG
    /* else */   spi_read_nword(use_qspi,addr,n,data); 
    
    for (int i=0;i<n;i++) if (dpi_trace_enable) $fwrite(dpi_trace_file,"%t dpi_read_nword  addr=%h , data=%h\n",$time,addr+4*i,data[i]) ;

    
  endtask


  task dpi_read_halfword;
    input          use_qspi;  
    input   [31:0] addr;
    output  [15:0] data;
    
    // if (dbg_periph==DPER_UART)  uart.uart_read_halfword(addr,data);    
    /* else */   spi_read_halfword(use_qspi,addr,data); 
    
    if (dpi_trace_enable) $fwrite(dpi_trace_file,"%t dpi_read_halfword  addr=%h , data=%h\n",$time,addr,data) ;
    
  endtask

  task dpi_read_byte;
    input          use_qspi;  
    input   [31:0] addr;
    output  [ 7:0] data;
    
    // if (dbg_periph==DPER_UART)  uart.uart_read_byte(addr,data);    
    /* else */   spi_read_byte(use_qspi,addr,data); 
    
    if (dpi_trace_enable) $fwrite(dpi_trace_file,"%t dpi_read_byte  addr=%h , data=%h\n",$time,addr,data) ;
    
  endtask
 
  task dpi_write_word;
    input          use_qspi;  
    input   [31:0] addr;
    input   [31:0] data;
    
    // if (dbg_periph==DPER_UART)  uart.uart_write_word(addr,data);    
    /* else */   spi_write_word(use_qspi,addr,data);
    
    if (dpi_trace_enable) $fwrite(dpi_trace_file,"%t dpi_write_word  addr=%h , data=%h\n",$time,addr,data) ;
   
  endtask
  
  task dpi_write_halfword;
    input          use_qspi;  
    input   [31:0] addr;
    input   [15:0] data;
    
    // if (dbg_periph==DPER_UART)  uart.uart_write_halfword(addr,data);    
    /* else */   spi_write_halfword(use_qspi,addr,data);
    
    if (dpi_trace_enable) $fwrite(dpi_trace_file,"%t dpi_write_half_word  addr=%h , data=%h\n",$time,addr,data) ;
        
  endtask

  task dpi_write_byte;
    input          use_qspi;  
    input   [31:0] addr;
    input   [ 7:0] data;

    // if (dbg_periph==DPER_UART)  uart.uart_write_byte(addr,data);        
    /* else */   spi_write_byte(use_qspi,addr,data);
    
    if (dpi_trace_enable) $fwrite(dpi_trace_file,"%t dpi_write_byte addr=%h , data=%h\n",$time,addr,data) ;
     
  endtask

