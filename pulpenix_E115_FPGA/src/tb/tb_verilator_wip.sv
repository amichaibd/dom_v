`include "tb_mem_pkg.sv"
`include "pulpenix_defines.v"

module tb;

  timeunit      1ns;
  timeprecision 1ps;
  
  parameter  SPI = "SINGLE"; // SPI = "QUAD";
  
   
  parameter NUM_SELECTABLE_UARTS = 2  ;

  logic [NUM_SELECTABLE_UARTS-1:0] uart_rx   ;
  logic [NUM_SELECTABLE_UARTS-1:0] uart_tx   ;

  string        memload;
  logic         use_qspi;

  logic fetch_enable ;  
  
//=====================================================
  //SPI Signals                  
  wire  spim_cs1_1              ;
  wire  spim_cs2_1              ;
  wire  spim_cs3_1              ;
  wire  spim_cs4_1              ;
  wire  spim_din1_spis_cs_1     ;
  wire  spim_din2_spis_clk_1    ;
  wire  spim_din3_spis_di_1     ;
  wire  spim_din4_1             ;
  wire  spim_clk_1              ;
  wire  spim_do_spis_do_1       ;
  wire  spif_cs_1               ;
  wire  spif_di_1               ;
  wire  pad_spi_master_slave_1  ;
  
  wire  spim_cs1_2              ;
  wire  spim_cs2_2              ;
  wire  spim_cs3_2              ;
  wire  spim_cs4_2              ;
  wire  spim_din1_spis_cs_2     ;
  wire  spim_din2_spis_clk_2    ;
  wire  spim_din3_spis_di_2     ;
  wire  spim_din4_2             ;
  wire  spim_clk_2              ;
  wire  spim_do_spis_do_2       ;
  wire  spif_cs_2               ;
  wire  spif_di_2               ;
  wire  pad_spi_master_slave_2  ;
  
  
 
// ======================================
// Serial Flash Cypresss model
// ======================================
wire [3:0]	  io_qspi_dat ;
wire					o_qspi_cs_n ;
wire					o_qspi_sck  ;
wire          vcc = 1'b1  ;



`ifdef NOFLASH
assign spif_di_1 = 1'b0 ;
`else
s25fl064p s25fl064p_i1 (
		.SCK      		(spim_clk_1		    ),
		.SI       		(spim_do_spis_do_1  ),
		.CSNeg    		(spif_cs_1          ),
		.HOLDNeg  		(vcc          		),
		.WPNeg    		(vcc          		),
		.SO		  		(spif_di_1    		)
	);
`endif

            
// =======================================================
 
  //*****************************************************************
  //                Clocks and Resets Module
  //*****************************************************************

  // Board like clock and reset
 
 
  reg brd_rst ;
  
  initial begin
    brd_rst = 1'b0;
    #2000ns;
    brd_rst = 1'b1;
  end
  
  reg altera_clk25mhz ;
  initial altera_clk25mhz = 0 ;
  always #(`DEFAULT_CLK_PERIOD_NS/2) altera_clk25mhz = !altera_clk25mhz ;
  
  wire clk_slow         ; 
  wire clk_midl         ;
  wire clk_fast         ;
  wire clk_ultra        ;
  wire pll_locked       ;

  ALTPLL1 #(.CLK_PERIOD(`DEFAULT_CLK_PERIOD_NS)) u_clocks_resets (
      .areset    (!brd_rst           ),
      .inclk0    (altera_clk25mhz    ),
      .c0        (clk_slow           ),
      .c1        (clk_midl           ),
      .c2        (clk_fast           ),
      .c3        (clk_ultra          ),
      .locked    (pll_locked         )
    );
  
//------------------------------------------------------------------------------



wire test_mode      = 1'b0 ;
wire lock_detect    = 1'b1 ; 
wire system_mem_rdy ;
wire sel_clock      = 1'b0 ;  
wire spi_slave_mode = 1'b1 ;  
wire pad_spin_clk_1 = 1'b0 ;
wire pad_spin_do_1         ;
wire pad_spin_di_1  = 1'b0 ;
wire pad_spin_cs_1  = 1'b1 ;
wire pad_spin_clk_2 = 1'b0 ;
wire pad_spin_do_2         ;
wire pad_spin_di_2  = 1'b0 ;
wire pad_spin_cs_2  = 1'b1 ;
wire pad_spis_cs_1  = 1'b1 ;

wire clk_native      = 1'b0  ;           
wire spi_native_dout         ;
wire spi_native_din  = 1'b0  ;
wire spi_native_cs   = 1'b0  ;

wire [1:0]  o_qspi_mod ;


pulpenix  #() pulpenix_i1 (

.pad_test_mode             (    test_mode                  )   , //  input                
.pad_scan_enable           (    1'b0                       )   , //  input                
.pad_master_slave          (    1'b1                       )   , //  input                
.pad_uart_tx               (    uart_tx[0]                 )   , //  output               
.pad_uart_rx               (    uart_rx[0]                 )   , //  input                
.pad_spim_cs1              (    spim_cs1_1                 )   , //  output               
.pad_spim_cs2              (    spim_cs2_1                 )   , //  output               
.pad_spim_cs3              (    spim_cs3_1                 )   , //  output               
.pad_spim_cs4              (    spim_cs4_1                 )   , //  output               
.pad_spim_din1             (    spim_din1_1                )   , //  input                
.pad_spim_din2_spis_clk    (    spim_din2_spis_clk_1       )   , //  input                
.pad_spim_din3_spis_di     (    spim_din3_spis_di_1        )   , //  input                
.pad_spim_din4             (    spim_din4_1                )   , //  input                
.pad_spim_clk              (    spim_clk_1                 )   , //  output               
.pad_spim_do_spis_do       (    spim_do_spis_do_1          )   , //  output               
.pad_spif_cs               (    spif_cs_1                  )   , //  output               
.pad_spif_di               (    spif_di_1                  )   , //  input                
.pad_spis_cs               (    pad_spis_cs_1              )   , //  input                
.pad_rst_n                 (    brd_rst                    )   , //  input                
.o_qspi_mod                (    o_qspi_mod                 )   , //  output [1:0]         
.clk_slow                  (    clk_slow                   )   , //  input                
.clk_midl                  (    clk_midl                   )   , //  input                
.clk_fast                  (    clk_fast                   )   , //  input                
.spi_nativ_n               (    1'b1                       )     //  input                
                                                                           
);


//================================================================================

 

// External UART Testbench

 uart_bus_clocked
  #(
    .BAUDRATE(`DEFAULT_BAUDRATE),
    .PARITY_EN(`DEFAULT_PARITY_EN),
    .NUM_SELECTABLE_UARTS(NUM_SELECTABLE_UARTS)
  )
  uart
  (
    .clk(clk_slow),
    .rst_n(brd_rst),
  
    .uart_rx         ( uart_tx ),
    .uart_tx         ( uart_rx ),
    .rx_en           ( 1'b1    )
  );
//================================================================================
   

  assign fetch_enable =   pulpenix_i1.msystem_i.system_rdy ;
   
  initial begin  
    
    memload = "PRELOAD";
    
    use_qspi = SPI == "QUAD" ? 1'b1 : 1'b0;

   
    $display("Using MEMLOAD method: %s", memload);
    
    if (memload == "PRELOAD")
    begin
      // preload memories
      mem_preload();
    end
    

    if (use_qspi)
      spi_enable_qpi();

   `ifdef MEM_DPI   // System memory socket for debug interface
         wait (fetch_enable) ;   
         $display("Message from tb.sv: MEM_DPI mode for debug interface ; MEM_DPI_PORT=%d \n",`MEM_DPI_PORT) ;
         mem_dpi(`MEM_DPI_PORT);
   `endif   
           
 end
//---------------------------------------------------------------
 
// ECLIPSE/GDB Socket interface 
 
 logic spi_sdo3 ;
 logic spi_sdo2 ;
 logic spi_sdo1 ;
 logic spi_sdo0 ;
 assign spim_din3_spis_di_1 = spi_sdo0 ;
 logic spi_sck ;
 assign spim_din2_spis_clk_1 = spi_sck ; 
 logic spi_csn ;
 assign spim_din1_spis_cs_1 = spi_csn ;
 logic spi_sdi3 ;
 logic spi_sdi2 ;
 logic spi_sdi1 ;
 logic spi_sdi0 ;
 assign spi_sdi0 = spim_do_spis_do_1 ;
 
`define EXIT_SUCCESS  0
`define EXIT_FAIL     1
`define EXIT_ERROR   -1

logic [1:0]   padmode_spi_master;
logic [31:0]  recv_data;

`include "tb_spi_pkg.sv"

`ifdef MEM_DPI  
`include "mem_dpi.svh"   
`endif
    
endmodule
