module clocks_resets (
  //INPUTS
  input  wire                pad_rst_n       ,// Reset from crystal
  input  wire                clk_slow        ,// 40MHz from outside
  input  wire                clk_midl        ,// 20Mhz-80Mhz  from synthesizer for modem, ADC & DAC 
  input  wire                clk_fast        ,//100Mhz~400Mhz from synthesizer for msystem
  input  wire                test_mode       ,//for DFT 
  input  wire                lock_detect     ,//fast clock ready
  input  wire                msystem_clk_sel ,//Select clock source: 0 slow, 1 fast
  input  wire                sw_rst_slow_regs2rf0_n,//sw reset slow    active low
  input  wire                sw_rst_fast_n   ,//sw reset fast    active low
  input  wire                sw_rst_midl_n   ,//sw reset midl    active low
  input  wire                sw_rst_mdm_n    ,//sw reset modem   active low
  input  wire                sw_rst_msystem_n,//sw reset msystem active low
  input  wire          [1:0] modem_clk_sel   ,//Select clock modem 
  //                         00: clk_slow, 01:clk_midl, 10:div of clk_fast 11: disable clk modem
  input  wire          [3:0] modem_clk_tot   ,//Total clk_fast cycles for clk_mdm 
  input  wire          [3:0] modem_clk_low   ,//clk_fast cycles for clk_mdm Low
  input  wire                en_clk_fast     ,//en_clk_fast option from local reg1 
  input  wire                en_clk_midl   ,//en_clk_midl option from local reg1 
  //OUTPUTS                                  
  output wire                rst_slow_local_n,//sync to clock slow
  output wire                rst_slow_regs2rf0_n,//sync to clock slow for bank0 NOT for local
  output wire                clk_mdm         ,//Clock to Modem
  output wire                rst_mdm_n       ,//Reset to Modem synch to clk_mdm
  output wire                clk_msystem     ,//Clock to msystem
  output wire                rst_msystem_n   ,//Reset to msystem synch to clk_msystem
  output wire                system_mem_rdy
);
//Mask clk_midl and clk_fast
//cg requires clock so we use AND and OR for 
m_and clk_midl_and_i    (.A(clk_midl), .B(en_clk_midl), .Y(clk_midl_masked_p) );
m_mx2 clk_midl_masked_i (.A(clk_midl_masked_p), .B(clk_slow), .S0(test_mode),  .Y(clk_midl_masked)) ;
m_and clk_fast_and_i    (.A(clk_fast), .B(en_clk_fast), .Y(clk_fast_masked_p) );
m_mx2 clk_fast_masked_i (.A(clk_fast_masked_p), .B(clk_slow), .S0(test_mode),  .Y(clk_fast_masked)) ;


localparam SYSTEM_MEM_RDY_TC = 16'hffff; // counts of clock slow
//Glitch protection delays one path and compare with straight path
 //inverters on one path of reset in or to remove glitches (duplicated by 2)
localparam GF_INVERTERS = 10*2 ;//Number must be even
wire [GF_INVERTERS-1:0] reset_glitch_shift ;
m_inv   m_inv1   (  .A(pad_rst_n),      .Y(reset_glitch_shift[0]));

genvar iii;
generate 
  for (iii=1;iii<GF_INVERTERS;iii=iii+1) begin: m_block_inv_reset
    m_inv  m_inv      (.A(reset_glitch_shift[iii-1]),    .Y(reset_glitch_shift[iii]));
  end
endgenerate

wire reset_n_gf ;//Glitch Free reset
m_or m_or_reset  (
  .A(pad_rst_n),  .B(reset_glitch_shift[GF_INVERTERS-1]),  .Y(reset_n_gf)) ;

m_reset_sw  m_reset_slow (
  .clk        (clk_slow       ),   
  .rst_n      (reset_n_gf     ), 
  .rst_sw_n   (1'b1           ), 
  .test_mode  (test_mode      ), 
  .rst_sync_n (reset_n_gf_sync)
);

reg reset_n_gf_sync_s, reset_slow_filtered_n;
reg [2:0] glitch_cnt;
//Add a counter to mask glitches longer than few clocks
always @(posedge clk_slow ) begin
    if (reset_n_gf_sync_s != reset_n_gf_sync)
      //reset changed - start counting
      glitch_cnt   <= 3'd0;
    else if (glitch_cnt < 3'd3)
      glitch_cnt   <= glitch_cnt + 1'd1;
    else //now get the reset 
      reset_slow_filtered_n <= reset_n_gf_sync_s;
    //sample prev reset
    reset_n_gf_sync_s <= reset_n_gf_sync;
end

//Reset shift registers, sample the async reset to 30 bit shift register
//Last 27 flip flops of shift register in generate for loop
//First 3 registers of shift register filter out short spurs on reset less than 1 clock cycle
wire [29:0] reset_n_s ;
wire  rst_sync_slow ;
m_inv     m_inv_rst_reset_s ( .A(reset_slow_filtered_n), .Y(rst_sync_slow));
m_dff_ar  m_dff_reset_s0 (.CK(clk_slow), .RD(rst_sync_slow), .D(reset_slow_filtered_n), .Q(reset_n_s[0]));
m_dff_ar  m_dff_reset_s1 (.CK(clk_slow), .RD(rst_sync_slow), .D(reset_n_s[0]),          .Q(reset_n_s[1]));
m_dff_ar  m_dff_reset_s2 (.CK(clk_slow), .RD(rst_sync_slow), .D(reset_n_s[1]),          .Q(reset_n_s[2]));


genvar ii;
generate 
   for (ii=3;ii<30;ii=ii+1) begin  : m_block_reset_n_s 
     m_dff_ar m_dff_reset_s (.CK(clk_slow),   .RD(rst_sync_slow),  .D(reset_n_s[ii-1]), .Q(reset_n_s[ii]));
   end
endgenerate
m_mx2 m_mx2_i  (.A(reset_n_s[29]), .B(pad_rst_n), .S0(test_mode),  .Y(rst_slow_local_n)) ;

m_reset_sw m_reset_sw_slow_regs2rf0(
  .clk        (clk_slow              ),   
  .rst_n      (rst_slow_local_n      ), 
  .rst_sw_n   (sw_rst_slow_regs2rf0_n), 
  .test_mode  (test_mode             ), 
  .rst_sync_n (rst_slow_regs2rf0_n   )
);


//2do:m_reset_sync m_reset_sync_i1 (.clk(clk_msystem),   .rst_n(reset_n_s_clk_slow), .test_mode(test_mode), .rst_sync_n(reset_n_sync_clk_out));

//The output reset signal, if test_mode output is same as input else last FF of shift register
//m_mx2 m_mx2_reset_n_out (.A(reset_n_s[29]), .B(pad_rst_n), .S0(test_mode),  .Y(reset_slow_filtered_n)) ;
//2do:m_mx2 m_mx2_reset_n_out (.A(reset_n_sync_clk_out), .B(pad_rst_n), .S0(test_mode),  .Y(rst_msystem_n)) ;

wire        rst_fast_n ;//sync to clock fast
m_reset_sw m_reset_sw_fast  (
  .clk        (clk_fast_masked ),   
  .rst_n      (rst_slow_local_n), 
  .rst_sw_n   (sw_rst_fast_n   ), 
  .test_mode  (test_mode       ), 
  .rst_sync_n (rst_fast_n      )
);
wire rst_fast;
m_inv     m_inv_rst_fast ( .A(rst_fast_n), .Y(rst_fast));


//Sample lock detect signal for 6 synchronizer steps
//Final step is and of all last 5 stages to avoid glitches of lock detect rising for less than 5 slow clocks
wire  lock_detect_s1, lock_detect_s2, lock_detect_s3, lock_detect_s4, lock_detect_s5, lock_detect_s6, lock_detect_sync ; 
wire lock_detect_s56, lock_detect_s34, lock_detect_s234 ; 
m_dff m_dff_lock_detect_s1 (.CK(clk_slow), .D(lock_detect   ), .Q(lock_detect_s1));
m_dff m_dff_lock_detect_s2 (.CK(clk_slow), .D(lock_detect_s1), .Q(lock_detect_s2));
m_dff m_dff_lock_detect_s3 (.CK(clk_slow), .D(lock_detect_s2), .Q(lock_detect_s3));
m_dff m_dff_lock_detect_s4 (.CK(clk_slow), .D(lock_detect_s3), .Q(lock_detect_s4));
m_dff m_dff_lock_detect_s5 (.CK(clk_slow), .D(lock_detect_s4), .Q(lock_detect_s5));
m_dff m_dff_lock_detect_s6 (.CK(clk_slow), .D(lock_detect_s5), .Q(lock_detect_s6));
m_and m_and_lock_detect1   (.A(lock_detect_s6),   .B(lock_detect_s5),  .Y(lock_detect_s56));
m_and m_and_lock_detect2   (.A(lock_detect_s4),   .B(lock_detect_s3),  .Y(lock_detect_s34));
m_and m_and_lock_detect3   (.A(lock_detect_s2),   .B(lock_detect_s34), .Y(lock_detect_s234));
m_and m_and_lock_detect4   (.A(lock_detect_s234), .B(lock_detect_s56), .Y(lock_detect_sync));


//Sample (msystem_clk_sel && lock_detect_sync) for 7 synchronizer steps
wire  msystem_clk_sel_s0, msystem_clk_sel_s1, msystem_clk_sel_s2, msystem_clk_sel_s3, msystem_clk_sel_s4, msystem_clk_sel_s5, msystem_clk_sel_s11, msystem_clk_sel_s21    ; 
m_and m_and_msystem_clk_sel4   (.A(msystem_clk_sel), .B(lock_detect_sync), .Y(msystem_clk_sel_s0));
m_dff m_dff_msystem_clk_sel_s1 (.CK(clk_slow),   .D(msystem_clk_sel_s0 ), .Q(msystem_clk_sel_s1));
m_dff m_dff_msystem_clk_sel_s2 (.CK(clk_slow),   .D(msystem_clk_sel_s1 ), .Q(msystem_clk_sel_s2));
m_dff m_dff_msystem_clk_sel_s3 (.CK(clk_slow),   .D(msystem_clk_sel_s2 ), .Q(msystem_clk_sel_s3));
m_dff m_dff_msystem_clk_sel_s4 (.CK(clk_slow),   .D(msystem_clk_sel_s3 ), .Q(msystem_clk_sel_s4));
m_dff m_dff_msystem_clk_sel_s5 (.CK(clk_slow),   .D(msystem_clk_sel_s4 ), .Q(msystem_clk_sel_s5));
m_dff m_dff_msystem_clk_sel_s6 (.CK(clk_slow),   .D(msystem_clk_sel    ), .Q(msystem_clk_sel_s11));
m_dff m_dff_msystem_clk_sel_s7 (.CK(clk_slow),   .D(msystem_clk_sel_s11), .Q(msystem_clk_sel_s21));

//Block clocks for 64 cycles after reset and when changing source
//In reset, minimal reset out will be for 30 clocks, 20 clocks after reset start clock will stop for at least 64 clocks
reg         msystem_clock_en ;
wire [ 7:0] sync_counter ;
wire rst_sync_slow_dft_n;
m_mx2 m_mx2_i1 (.A(reset_slow_filtered_n), .B(pad_rst_n), .S0(test_mode),  .Y(rst_sync_slow_dft_n)) ;
always @(posedge clk_slow or negedge rst_sync_slow_dft_n)
  if      (!rst_sync_slow_dft_n)                     msystem_clock_en <= 1'b1;
  else if (!reset_n_s[19] && reset_n_s[18])          msystem_clock_en <= 1'b0;//reset goes back up stop clock
  else if (msystem_clk_sel_s3 != msystem_clk_sel_s2) msystem_clock_en <= 1'b0;//change of clock source stop clock
  else if (msystem_clk_sel_s21 && !lock_detect_sync) msystem_clock_en <= 1'b0;//fast clock and waiting for lock,
  else if (!sw_rst_msystem_n)                        msystem_clock_en <= 1'b0;//SW reset
  else if (sync_counter[7:0]==8'h20)                 msystem_clock_en <= 1'b1;//we can start clock again
  
  
// MODEM clock and reset
////////////////////////////////
// clock divider
//00: clk_slow, 01:clk_midl, 10:div of clk_fast 11: disable clk modem
wire mdm_clk_sel_slow        = (modem_clk_sel == 2'b00) || test_mode;
wire mdm_clk_sel_midl        =  modem_clk_sel == 2'b01;
wire mdm_clk_sel_div         =  modem_clk_sel == 2'b10;
wire mdm_clk_sel_dis         =  modem_clk_sel == 2'b11;
wire mdm_clk_sel_midl_or_div =  mdm_clk_sel_div || mdm_clk_sel_midl;
wire clk_msystem_int;
reg modem_clk_div;//modem_clk_from_clk_msystem
reg [3:0] modem_clk_cnt;
//start div once sync_counter cycle before avoid glitch of few clocks on start of mdm_clk_sel_div 
//start div once sync_counter cycle before avoid glitch of few clocks on start of mdm_clk_sel_div 
//mdm_clk_sel_div && (mdm_sync_counter[7:0]==8'h21));
//(!mdm_clock_en) || 
//start modem_clk_div before mdm_clock_en rise to let gf get into correct state
reg         mdm_clock_en ;
wire        mdm_clock_en_fast ;
wire [ 7:0] mdm_sync_counter ;
wire [ 3:0] modem_clk_tot_fast;//Total clk_fast cycles for clk_mdm 
wire [ 3:0] modem_clk_low_fast;//clk_fast cycles for clk_mdm Low
wire         mdm_clk_sel_div_fast;
wire         mdm_clk_sel_midl_fast;
m_sync_2ff_rst sync_i0 (.rst(rst_fast), .clk(clk_fast_masked), .in(modem_clk_tot[0]), .out(modem_clk_tot_fast[0]));
m_sync_2ff_rst sync_i1 (.rst(rst_fast), .clk(clk_fast_masked), .in(modem_clk_tot[1]), .out(modem_clk_tot_fast[1]));
m_sync_2ff_rst sync_i2 (.rst(rst_fast), .clk(clk_fast_masked), .in(modem_clk_tot[2]), .out(modem_clk_tot_fast[2]));
m_sync_2ff_rst sync_i3 (.rst(rst_fast), .clk(clk_fast_masked), .in(modem_clk_tot[3]), .out(modem_clk_tot_fast[3]));
m_sync_2ff_rst sync_i4 (.rst(rst_fast), .clk(clk_fast_masked), .in(modem_clk_low[0]), .out(modem_clk_low_fast[0]));
m_sync_2ff_rst sync_i5 (.rst(rst_fast), .clk(clk_fast_masked), .in(modem_clk_low[1]), .out(modem_clk_low_fast[1]));
m_sync_2ff_rst sync_i6 (.rst(rst_fast), .clk(clk_fast_masked), .in(modem_clk_low[2]), .out(modem_clk_low_fast[2]));
m_sync_2ff_rst sync_i7 (.rst(rst_fast), .clk(clk_fast_masked), .in(modem_clk_low[3]), .out(modem_clk_low_fast[3]));
m_sync_2ff_rst sync_i8 (.rst(rst_fast), .clk(clk_fast_masked), .in(mdm_clk_sel_div),  .out(mdm_clk_sel_div_fast));
m_sync_2ff_rst sync_i9 (.rst(rst_fast), .clk(clk_fast_masked), .in(mdm_clk_sel_midl), .out(mdm_clk_sel_midl_fast));
m_sync_2ff_rst sync_ia (.rst(rst_fast), .clk(clk_fast_masked), .in(mdm_clock_en),     .out(mdm_clock_en_fast));


always @(posedge clk_fast_masked or negedge rst_fast_n)
  if (!rst_fast_n) begin
    modem_clk_div <= 1'b0;
    modem_clk_cnt <= 4'd0  ;
  end  
  else if (!mdm_clk_sel_div_fast)  begin
      modem_clk_div <= 1'b0;
      modem_clk_cnt <= 4'd0;
  end  
  else if ( modem_clk_cnt >= modem_clk_tot_fast) begin
      modem_clk_div <= !modem_clk_div;
      modem_clk_cnt <= 4'd0;
  end  
  else begin
    if (modem_clk_cnt >= modem_clk_low_fast)
      modem_clk_div <= 1'b1;
    modem_clk_cnt <= modem_clk_cnt + 1;
  end



//same lock_detect_sync for midl 
//Sample (mdm_clk_sel && lock_detect_sync) for  synchronizer steps
wire mdm_clk_sel_midl_s0, mdm_clk_sel_midl_s1, mdm_clk_sel_midl_s2, mdm_clk_sel_midl_s3, mdm_clk_sel_midl_s4, mdm_clk_sel_midl_s5, mdm_clk_sel_midl_s11, mdm_clk_sel_midl_s21    ; 
m_and m_and_mdm_clk_sel4 (.A(mdm_clk_sel_midl), .B(lock_detect_sync), .Y(mdm_clk_sel_midl_s0));
m_dff m_dff_mdm_clk_sel_midl_s1 (.CK(clk_slow), .D(mdm_clk_sel_midl_s0 ), .Q(mdm_clk_sel_midl_s1));
m_dff m_dff_mdm_clk_sel_midl_s2 (.CK(clk_slow), .D(mdm_clk_sel_midl_s1 ), .Q(mdm_clk_sel_midl_s2));
m_dff m_dff_mdm_clk_sel_midl_s3 (.CK(clk_slow), .D(mdm_clk_sel_midl_s2 ), .Q(mdm_clk_sel_midl_s3));
m_dff m_dff_mdm_clk_sel_midl_s4 (.CK(clk_slow), .D(mdm_clk_sel_midl_s3 ), .Q(mdm_clk_sel_midl_s4));
m_dff m_dff_mdm_clk_sel_midl_s5 (.CK(clk_slow), .D(mdm_clk_sel_midl_s4 ), .Q(mdm_clk_sel_midl_s5));
m_dff m_dff_mdm_clk_sel_midl_s6 (.CK(clk_slow), .D(mdm_clk_sel_midl    ), .Q(mdm_clk_sel_midl_s11));
m_dff m_dff_mdm_clk_sel_midl_s7 (.CK(clk_slow), .D(mdm_clk_sel_midl_s11), .Q(mdm_clk_sel_midl_s21));


//detect change of clock source
wire mdm_clk_sel_div_s1, mdm_clk_sel_div_s2, mdm_clk_sel_div_s3; 
m_dff m_dff_mdm_clk_sel_div_s1 (.CK(clk_slow), .D(mdm_clk_sel_div ),    .Q(mdm_clk_sel_div_s1));
m_dff m_dff_mdm_clk_sel_div_s2 (.CK(clk_slow), .D(mdm_clk_sel_div_s1 ), .Q(mdm_clk_sel_div_s2));
m_dff m_dff_mdm_clk_sel_div_s3 (.CK(clk_slow), .D(mdm_clk_sel_div_s2 ), .Q(mdm_clk_sel_div_s3));


//Block clocks for 64 cycles after reset and when changing source
//In reset, minimal reset out will be for 30 clocks, 20 clocks after reset start clock will stop for at least 64 clocks
always @(posedge clk_slow or negedge rst_sync_slow_dft_n)
  if      (!rst_sync_slow_dft_n)                       mdm_clock_en <= 1'b1;
  else if (!reset_n_s[19] && reset_n_s[18])            mdm_clock_en <= 1'b0;//reset goes back up stop clock
  else if (mdm_clk_sel_midl_s3 != mdm_clk_sel_midl_s2) mdm_clock_en <= 1'b0;//change of clock source stop clock
  else if (mdm_clk_sel_div_s3  != mdm_clk_sel_div_s2)  mdm_clock_en <= 1'b0;//change of clock source stop clock
  else if (mdm_clk_sel_dis)                            mdm_clock_en <= 1'b0;//to keep disable till div or mid are synced 
  else if (mdm_clk_sel_midl_s21 && !lock_detect_sync)  mdm_clock_en <= 1'b0;//midl clock and waiting for lock,
  else if (!sw_rst_mdm_n)                              mdm_clock_en <= 1'b0;//SW reset
  else if (!sw_rst_midl_n)                             mdm_clock_en <= 1'b0;//SW reset
  else if (mdm_sync_counter[7:0]==8'h20)               mdm_clock_en <= 1'b1;//we can start clock again




//when we want to use clk_slow when clk_midl is X or Z
//we have to mask clk_midl by !mdm_clk_sel_slow
wire clk_mid_or_div;
//MUX was enough since we can alway stop the clock by mdm_clk_sel_dis
// but for sake of sync of reset and controls we'll have 2 gf_clk_mux
wire rst_midl_n;
m_reset_sw m_reset_sw_midl(
  .clk        (clk_midl_masked ),   
  .rst_n      (rst_slow_local_n), 
  .rst_sw_n   (sw_rst_midl_n   ), 
  .test_mode  (test_mode       ), 
  .rst_sync_n (rst_midl_n      )
);

m_gf_clk_mux  m_gf_clk_mux_div_mid (
  .rst_sync0_n      (rst_fast_n           ),
  .rst_sync1_n      (rst_midl_n           ),
  .clk0             (modem_clk_div        ),
  .clk1             (clk_midl_masked      ),
  .sel_clk1         (mdm_clk_sel_midl_fast), //sync to midl which is clk1 is internal
  .clocks_enbale    (mdm_clock_en_fast    ), //sync to midl which is clk1 is internal
  .test_mode        (test_mode            ),
  .clk_out          (clk_mid_or_div       )
);

m_gf_clk_mux  m_gf_clk_mux_div_mid_slow (
  .rst_sync0_n      (rst_slow_local_n   ),
  .rst_sync1_n      (rst_fast_n         ),
  .clk0             (clk_slow           ),
  .clk1             (clk_mid_or_div     ),
  .sel_clk1         (mdm_clk_sel_midl_or_div),
  .clocks_enbale    (mdm_clock_en       ),
  .test_mode        (test_mode          ),// selects clk0
  .clk_out          (clk_mdm            )
);

//No need to sync to clk_mdm since the clock stops around rise of reset
m_and rst_mdm_i (
  .A(sw_rst_mdm_n),      .B(rst_slow_local_n),  .Y(rst_mdm_n)) ;
//No need to sync to clk_msystem since the clock stops around rise of reset
m_and rst_msystem_i (
  .A(sw_rst_msystem_n),  .B(rst_slow_local_n),  .Y(rst_msystem_n)) ;

  
// MSYSTEM clock and reset
////////////////////////////////
m_gf_clk_mux  m_gf_clk_mux_i (
  .rst_sync0_n      (rst_slow_local_n  ),
  .rst_sync1_n      (rst_fast_n        ),
  .clk0             (clk_slow          ),
  .clk1             (clk_fast_masked   ),
  .sel_clk1         (msystem_clk_sel_s5),
  .clocks_enbale    (msystem_clock_en  ),
  .test_mode        (test_mode         ),// selects clk0
  .clk_out          (clk_msystem_int   )
);


m_buf m_buf_clk_msystem (.A(clk_msystem_int), .Y(clk_msystem)) ;


// system_mem_rdy
////////////////////
reg  [19:0] sync_rst_vect ;
wire sync_rst = sync_rst_vect[0] ;
always @( posedge clk_slow  or posedge rst_slow_local_n)
  if (rst_slow_local_n) sync_rst_vect <= {20{1'b1}} ;
  else sync_rst_vect <= {1'b0,sync_rst_vect[19:1]} ;

reg  [15:0] system_mem_rdy_cnt ;
assign system_mem_rdy = system_mem_rdy_cnt==SYSTEM_MEM_RDY_TC ;    
//always @( negedge clk_slow  or negedge rst_slow_local_n)
always @( posedge clk_slow  or negedge rst_slow_local_n)
  if (!rst_slow_local_n)  system_mem_rdy_cnt <=#1 'd0 ;
  else if (!sync_rst)       system_mem_rdy_cnt <=#1 'd0 ;
  else if (!system_mem_rdy) system_mem_rdy_cnt <=#1 system_mem_rdy_cnt + 1'b1 ; 

  
//Standard gray counter
gray_count i_gray_count (
  .clk        (clk_slow         ), 
  .enable     (!msystem_clock_en ), 
  .reset      (msystem_clock_en),// synchronous reset
  .gray_count (sync_counter     )
);

//Standard gray counter
gray_count i_gray_count_1 (
  .clk        (clk_slow        ), 
  .enable     (!mdm_clock_en   ), 
  .reset      ( mdm_clock_en   ),// synchronous reset
  .gray_count (mdm_sync_counter)
);

endmodule

//Gate clocks => an enable for fast and for slow clock, when changing clocks there will be period with no enable
//for at least 64 clock, one will go down and 64 clock after the other will go up.
//Note: Fast clock will only change after lock detect as well

