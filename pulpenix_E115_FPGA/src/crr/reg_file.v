//////////////////////////////////////////////////////////////////
// Register File 
// Connects to either msystem or SPI nativ according to spi_nativ_n
// An address selects the register available to msystem or SPI nativ
//Read/Write is according to msystem/SPI address
// Width of registers is 16 bits
// Register File Structure:
//,-------------------------------------------------------------------,
// In Regs   : IREGS number of registers
//             Sampling rf2regs signals from the RF
//             Sample signal is clk_msystem_rd or clk_spim_din2_spisn 
//             Lowest RFSIG registers are also output
//               on rf_regs2msystem bus to msystem.
//|-------------------------------------------------------------------|
// Out Regs  : OREGS*2 number of registers
//             Writen by msystem or SPI nativ
//             Divided to 2 banks by a Local register 
//             Seleced bank is output on regs2rf bus for the RF
//|-------------------------------------------------------------------|
// Local Regs: LREGS registers, Writen and read by msystem or SPI nativ
//             Upper Register is hard wire to VERSION 
//'-------------------------------------------------------------------'
//---------------------------------------------------------------------
//////////////////////////////////////////////////////////////////

module reg_file #(
  parameter IREGS  =  4,// number of registers Sampling rf2regs bus from the RF 
  parameter OREGS  = 61,// number of registers in one bank which is output on regs2rf bus for the RF
  parameter LREGS  =  4,// Local registers, rd/wr by msystem/SPI. Bit 0 is bank select.
  parameter RFSIG  =  4,// number of IREGS registers which are output on rf_regs2msystem bus to msystem
  parameter W_ADDR =  8 // Width of Address Bus
  )(
  //modes
  input  wire				         test_mode         ,   
  input  wire				         clk_slow          ,   
  input  wire				         spi_nativ_n       ,//SPI slave select 0=native 1=host_based  
  input  wire                rst_slow_local_n  ,//sync to clock slow
  input  wire                rst_slow_regs2rf0_n,//sync to clock slow for bank0 NOT for local
  output wire                msystem_clk_sel   ,//Select clock msystem source: 0 slow, 1 fast
  output wire                sw_rst_slow_regs2rf0_n ,//sw reset slow    active low
  output wire                sw_rst_fast_n     ,//sw reset fast    active low
  output wire                sw_rst_midl_n     ,//sw reset midl    active low
  output wire                sw_rst_mdm_n      ,//sw reset modem   active low
  output wire                sw_rst_msystem_n  ,//sw reset msystem active low
  output wire          [1:0] modem_clk_sel     ,//Select clock modem 
  //                                // 00: clk_slow, 01:clk_midl, 10:div of clk_fast 11: disable clk modem
  output wire          [3:0] modem_clk_tot     ,//Total clk_fast cycles for clk_mdm 
  output wire          [3:0] modem_clk_low     ,//clk_fast cycles for clk_mdm Low
  output wire                en_clk_fast       ,//en_clk_fast option from local reg1 
  output wire                en_clk_midl     ,//en_clk_midl option from local reg1 
  output wire                sel_clk_spisn_pos ,//select clk_spisn to be positive. def=neg
  // Buses of registers                      
  input  wire [IREGS*16-1:0] rf2regs           ,// inputs  from RF. Sampled into In Regs and Read by msystem/SPI
  output wire [RFSIG*16-1:0] rf_regs2msystem   ,// Signals from In Regs to msystem
  output wire [OREGS*16-1:0] regs2rf           ,// outputs for  RF. Writen by msystem/SPI
  //SPI Native 2 Regf                            
  input  wire                clk_spim_din2_spisn      ,// clock to read rf2regs
  input  wire                clk_native_wr     ,// clock to write regs2rf
  input  wire [  W_ADDR-1:0] a_native2regf     ,// address to select reg
  input  wire [      16-1:0] d_native2regf     ,// data in for regs2rf
  output wire [      16-1:0] d_regf2native     ,// data out from reg_file 2 native SPI
  //msystem Regf IF                                
  input  wire                clk_msystem_rd    ,// clock for rf2regs
  input  wire                clk_msystem_wr    ,// clock for regs2rf
  input  wire [  W_ADDR-1:0] a_msystem2regf    ,// address to select reg
  input  wire [      16-1:0] d_msystem2regf    ,// data in for regs2rf
  output wire [      16-1:0] d_regf2msystem     // data out from reg_file 2 msystem
);                                           

localparam VERSION = 16'd1; 
localparam LAST_USED_BIT_LREG = 18; 
                   
// Local Control registers
///////////////////////////////////////   
//sync to clock slow
reg [LREGS*16-1:0] regs_local;// Local Register File. Upper register is wired to VERSION
wire [LAST_USED_BIT_LREG:0] regs_local_sync_slow;
genvar iii;
generate 
  for (iii=0;iii<LAST_USED_BIT_LREG+1;iii=iii+1) begin: m_sync_local2slow
    m_sync_2ff  m_sync_2ff (.clk(clk_slow), .in(regs_local[iii]), .out(regs_local_sync_slow[iii]));
  end
endgenerate

//Local reg 0
wire   regs2rf_en       =  regs_local_sync_slow[0]   ;//Selects regs2rf from which register bank. 0:low, 1: hi
assign msystem_clk_sel  =  regs_local_sync_slow[1]   ;//Select clock source: 0 slow, 1 fast
assign modem_clk_sel    =  regs_local_sync_slow[3:2] ;//Select clock modem
                    // 00: clk_slow, 01:clk_midl, 10:div of clk_fast 11: disable clk modem
                    
assign modem_clk_tot    =  regs_local_sync_slow[ 7:4];//Total clk_fast cycles for clk_mdm

assign modem_clk_low    =  regs_local_sync_slow[10:8];//clk_fast cycles for clk_mdm Low 
//if modem_clk_low is equal or greater to modem_clk_tot than clk_fast cycles will be modem_clk_tot*2
// counting from 0, i.e. 1 means 2 cycles

assign sw_rst_slow_regs2rf0_n = !regs_local_sync_slow[11]  ;//sw reset slow not local active low
assign sw_rst_fast_n          = !regs_local_sync_slow[12]  ;//sw reset slow    active low
assign sw_rst_midl_n          = !regs_local_sync_slow[13]  ;//sw reset midl    active low
assign sw_rst_mdm_n           = !regs_local_sync_slow[14]  ;//sw reset modem   active low
assign sw_rst_msystem_n       = !regs_local_sync_slow[15]  ;//sw reset msystem active low
//Local reg 1
assign en_clk_fast            =  regs_local_sync_slow[16]  ;//en_clk_fast on reset 
assign en_clk_midl            =  regs_local_sync_slow[17]  ;//en_clk_midl on reset 
assign sel_clk_spisn_pos      = regs_local_sync_slow[LAST_USED_BIT_LREG]  ;//en_clk_midl on reset 

//Access MUX between SPI and msystem
///////////////////////////////////////
wire               clk_regf_rd = test_mode ? clk_slow : spi_nativ_n ? clk_msystem_rd : clk_spim_din2_spisn   ;
wire               clk_regf_wr = test_mode ? clk_slow : spi_nativ_n ? clk_msystem_wr : clk_native_wr;
wire [W_ADDR-1:0]  addr_regf   =                        spi_nativ_n ? a_msystem2regf : a_native2regf;
wire [    16-1:0]  di_regf     =                        spi_nativ_n ? d_msystem2regf : d_native2regf;
//wire clk_native_int;
//m_mx2 m_mx2_clk (.A(clk_spim_din2_spisn), .B(clk_slow), .S0(test_mode),  .Y(clk_native_int)) ;

//Write Local registers
always @(posedge clk_regf_wr or negedge rst_slow_local_n) 
	if (!rst_slow_local_n) 
    regs_local <=#1 {VERSION, {(LREGS-1)*16{1'b0}}};
  else if ( (LREGS-1) > addr_regf ) 
    regs_local[16*addr_regf +: 16] <=#1 di_regf;

//Write Output registers
wire [OREGS*16-1:0] outreg_rst_val= {
  {1{1'b1}},{1{1'b0}},{1{1'b1}},{1{1'b1}},{1{1'b1}},{4{1'b0}},{1{1'b1}},{4{1'b0}},{1{1'b0}},{1{1'b0}},{3{1'b0}},
  {1{1'b1}},{2{1'b0}},{2{1'b0}},{2{1'b0}},{1{1'b1}},{3{1'b0}},{2{1'b0}},{1{1'b0}},{1{1'b1}},{4{1'b0}},{4{1'b0}},
  {4{1'b0}},{1{1'b0}},{1{1'b0}},{3{1'b0}},{3{1'b0}},{3{1'b0}},{3{1'b0}},{4{1'b0}},{6{1'b1}},{5{1'b1}},{5{1'b1}},
  {1{1'b0}},{5{1'b0}},{1{1'b0}},{3{1'b0}},{1{1'b1}},{1{1'b1}},{1{1'b0}},{1{1'b0}},{1{1'b0}},{1{1'b0}},{24{1'b0}},
  {8{1'b1}},{8{1'b0}},{1{1'b0}},{3{1'b0}},{4{1'b0}},{1{1'b1}},{5{1'b0}},{5{1'b0}},{1{1'b1}},{1{1'b0}},{1{1'b1}},
  {2{1'b0}},{1{1'b0}},{4{1'b0}},{1{1'b1}},{1{1'b0}},{1{1'b0}},{4{1'b0}},{4{1'b0}},{1{1'b0}},{9{1'b0}},{3{1'b0}},
  {1{1'b0}},{1{1'b0}},{1{1'b1}},{8{1'b0}},{7{1'b0}},{1{1'b0}},{4{1'b0}},{4{1'b0}},{4{1'b0}},{4{1'b0}},{3{1'b0}},
  {4{1'b0}},{4{1'b0}},{4{1'b0}},{1{1'b0}},{4{1'b0}},{10{1'b0}},{1{1'b1}},{1{1'b0}},{1{1'b1}},{1{1'b1}},{1{1'b1}},
  {4{1'b0}},{1{1'b1}},{4{1'b0}},{1{1'b0}},{1{1'b0}},{2{1'b0}},{3{1'b0}},{1{1'b1}},{2{1'b0}},{2{1'b0}},{1{1'b1}},
  {3{1'b0}},{2{1'b0}},{2{1'b0}},{2{1'b0}},{1{1'b0}},{2{1'b0}},{1{1'b1}},{3{1'b0}},{2{1'b0}},{1{1'b0}},{4{1'b0}},
  {1{1'b0}},{1{1'b1}},{4{1'b0}},{4{1'b0}},{3{1'b0}},{3{1'b0}},{4{1'b0}},{1{1'b0}},{3{1'b0}},{3{1'b0}},{4{1'b1}},
  {1{1'b0}},{2{1'b1}},{5{1'b0}},{3{1'b1}},{1{1'b0}},{1{1'b0}},{1{1'b0}},{3{1'b0}},{1{1'b0}},{1{1'b0}},{24{1'b0}},
  {3{1'b0}},{1{1'b1}},{1{1'b1}},{1{1'b0}},{8{1'b1}},{8{1'b0}},{1{1'b0}},{1{1'b1}},{4{1'b0}},{5{1'b0}},{5{1'b0}},
  {1{1'b1}},{1{1'b0}},{1{1'b1}},{1{1'b1}},{2{1'b0}},{1{1'b0}},{2{1'b0}},{2{1'b0}},{2{1'b0}},{3{1'b0}},{1{1'b1}},
  {1{1'b0}},{2{1'b0}},{2{1'b0}},{2{1'b0}},{2{1'b0}},{1{1'b1}},{3{1'b0}},{2{1'b0}},{1{1'b0}},{1{1'b0}},{4{1'b0}},
  {1{1'b0}},{4{1'b0}},{4{1'b0}},{1{1'b0}},{9{1'b0}},{7{1'b0}},{3{1'b0}},{1{1'b1}},{8{1'b0}},{4{1'b0}},{4{1'b0}},
  {4{1'b0}},{4{1'b0}},{4{1'b0}},{1{1'b0}},{3{1'b0}},{4{1'b0}},{4{1'b0}},{4{1'b0}},{1{1'b0}},{1{1'b0}},{1{1'b1}},
  {4{1'b0}},{4{1'b0}},{1{1'b1}},{4{1'b0}},{1{1'b1}},{11{1'b0}},{4{1'b0}},{16{1'b0}},{16{1'b0}},{1{1'b0}},{1{1'b1}},
  {11{1'b0}},{1{1'b0}},{1{1'b0}},{1{1'b1}},{4{1'b0}},{1{1'b1}},{11{1'b0}},{1{1'b0}},{1{1'b1}},{4{1'b0}},{4{1'b0}},
  {1{1'b1}},{4{1'b0}},{1{1'b0}},{1{1'b1}},{1{1'b1}},{1{1'b1}},{1{1'b1}},{4{1'b0}},{4{1'b0}},{4{1'b0}},{1{1'b1}},
  {4{1'b0}},{11{1'b0}},{1{1'b1}},{1{1'b1}},{1{1'b1}},{1{1'b1}},{4{1'b0}},{4{1'b0}},{4{1'b0}},{16{1'b0}},{16{1'b0}},
  {11{1'b0}},{1{1'b1}},{4{1'b0}},{1{1'b1}},{1{1'b0}},{1{1'b1}},{1{1'b1}},{1{1'b1}},{11{1'b0}},{4{1'b0}},{4{1'b0}},
  {4{1'b0}},{4{1'b0}},{1{1'b1}},{1{1'b1}},{1{1'b1}},{1{1'b1}},{4{1'b0}},{4{1'b0}},{4{1'b0}},{1{1'b0}},{11{1'b0}},
  {4{1'b0}},{1{1'b1}},{11{1'b0}},{4{1'b0}},{1{1'b1}},{1{1'b1}},{4{1'b0}},{4{1'b0}},{4{1'b0}},{1{1'b1}},{1{1'b1}},
  {4{1'b0}},{11{1'b0}},{1{1'b1}},{4{1'b0}},{1{1'b0}},{4{1'b0}},{4{1'b0}},{1{1'b1}},{1{1'b1}},{1{1'b1}},{4{1'b0}},
  {11{1'b0}},{1{1'b0}},{4{1'b0}},{4{1'b0}},{4{1'b0}},{1{1'b1}},{1{1'b1}},{1{1'b0}},{1{1'b0}},{7{1'b0}},{7{1'b0}},
  {1{1'b1}},{1{1'b0}},{16{1'b0}},{16{1'b0}},{7{1'b0}},{7{1'b0}},{1{1'b0}},{1{1'b0}},{5{1'b0}},{4{1'b0}},{5{1'b0}},
  {1{1'b1}},{1{1'b0}}
};
reg [(1*OREGS+LREGS)*16-1:         LREGS *16] regs2rf_0;
reg [(2*OREGS+LREGS)*16-1:(1*OREGS+LREGS)*16] regs2rf_1;
always @(posedge clk_regf_wr or negedge rst_slow_regs2rf0_n) 
	if (!rst_slow_regs2rf0_n) 
    regs2rf_0 <=#1 outreg_rst_val;
	else if ( ((1*OREGS+LREGS) > addr_regf) && (addr_regf >= LREGS) )
    regs2rf_0[16*addr_regf +: 16] <=#1 di_regf;
    
//`ifdef 2BANKS
//always @(posedge clk_regf_wr) 
// if       ( ((2*OREGS+LREGS) > addr_regf) && (addr_regf >= (1*OREGS+LREGS)) )
//    regs2rf_1[16*addr_regf +: 16] <=#1 di_regf;
//`else
always @(posedge clk_regf_wr) 
 if (!regs2rf_en) regs2rf_1 <= regs2rf_0;
//`endif


// Sample Input registers
reg		[(IREGS+2*OREGS+LREGS)*16-1:(2*OREGS+LREGS)*16] rf2regs_s;
always @(posedge clk_regf_rd) 
    rf2regs_s <=#1 rf2regs;

//Signals from RF rf2regs to msystem
assign rf_regs2msystem  = rf2regs_s [(RFSIG+2*OREGS+LREGS)*16-1:(2*OREGS+LREGS)*16];

//Signals to RF from regs2rf
//`ifdef 2BANKS
//assign regs2rf =  regs2rf_sel
//                         ? regs2rf_1
//                         : regs2rf_0;
//`else
assign regs2rf =  regs2rf_1;
//`endif

//Read Data bus 2 SPI Native
assign d_regf2native = (a_native2regf < LREGS)
                          ?     regs_local[a_native2regf*16 +: 16 ]
                            : a_native2regf < (1*OREGS+LREGS)
                               ? regs2rf_0[a_native2regf*16 +: 16 ] 
//`ifdef 2BANKS
//                            : a_native2regf < (2*OREGS+LREGS)
//                               ? regs2rf_1[a_native2regf*16 +: 16 ] 
//`endif
                               : rf2regs_s [a_native2regf*16 +: 16 ];
//Read Data bus 2 msystem
assign d_regf2msystem = (a_msystem2regf < LREGS)
                          ?     regs_local[a_msystem2regf*16 +: 16 ]
                            : a_msystem2regf < (1*OREGS+LREGS)
                               ? regs2rf_0[a_msystem2regf*16 +: 16 ] 
//`ifdef 2BANKS
//                            : a_msystem2regf < (2*OREGS+LREGS)
//                               ? regs2rf_1[a_msystem2regf*16 +: 16 ] 
//`endif
                               : rf2regs_s [a_msystem2regf*16 +: 16 ];
endmodule







 