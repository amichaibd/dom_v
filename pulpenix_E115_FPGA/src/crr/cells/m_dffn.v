//`timescale 1ps/1ps
module m_dffn (
   input  CKN
  ,input  D
  ,output Q
);
`ifdef PROCESS_GF45
  DFFNQ_X3M_A12TR m_dffn_i(
     .D (D)
    ,.CKN(CKN)
    ,.Q (Q)
   );
`else
   reg Q_next;
   always @(negedge CKN) Q_next <= D;
   assign Q = Q_next ;
`endif

endmodule


  
