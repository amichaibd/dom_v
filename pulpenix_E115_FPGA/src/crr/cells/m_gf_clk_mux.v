// Description  : Glitch Free Clock Mux
module m_gf_clk_mux(
   input	wire rst_sync0_n   // Synced to clk0
  ,input	wire rst_sync1_n   // Synced to clk1
  ,input	wire clk0          // Slow clock
  ,input	wire clk1          // Fast clock from pll
  ,input	wire sel_clk1      // 0: select clk0  0: select clk1 
  ,input	wire clocks_enbale // 0: clocks active  1: clocks stop
                             // Note: it is on clk0 so needs to be synced to clk1
  ,input	wire test_mode
  
  ,output wire clk_out
);

wire sel_clk1_n;
wire en_clk0, en_clk0_d1, en_clk0_d2, en_clk0_d3, en_cg_clk0, clk0_gated;
wire en_clk0_d3_n;
wire en_clk1, en_clk1_d1, en_clk1_d2, en_clk1_d3, en_cg_clk1, clk1_gated;
wire en_clk1_d3_n;

//sync clocks_enbale to clk1:
wire rst_sync1 ;
m_inv m_inv_i_clk1  (.A(rst_sync1_n),  .Y(rst_sync1));
//Sync to clk1 cause the enable to stay in the pipe and release 2 clocks of clk1 when clk1 starts:
m_sync_2ff_rst sync_clocks_enbale_i (
  .rst(rst_sync1), .clk(clk1), .in(clocks_enbale), .out(clocks_enbale_clk1));
m_sync_2ff_rst sync_sel_clk1_i (
  .rst(rst_sync1), .clk(clk1), .in(sel_clk1), .out(sel_clk1_clk1));

//clk0 chain, reset to 1
m_inv    inv_i_01  (.A(sel_clk1),     .Y(sel_clk1_n) );
m_inv    inv_i_02  (.A(en_clk1_d3),   .Y(en_clk1_d3_n) );
m_and    and_i_01  (.A(sel_clk1_n),   .B(en_clk1_d3_n),  .Y(en_clk0) );
m_dff_sn dff_i_01  (.SN(rst_sync0_n), .CK(clk0),      .D(en_clk0),       .Q(en_clk0_d1) );
m_dff_sn dff_i_02  (.SN(rst_sync0_n), .CK(clk0),      .D(en_clk0_d1),    .Q(en_clk0_d2) );
m_dff_sn dff_i_03  (.SN(rst_sync0_n), .CK(clk0),      .D(en_clk0_d2),    .Q(en_clk0_d3) );
m_and    and_i_02  (.A(en_clk0_d2),   .B(clocks_enbale), .Y(en_cg_clk0) );
m_cg     cg_i_01   (.CK(clk0),        .E(en_cg_clk0),   .SE(test_mode), .Q(clk0_gated));

//clk1 chain, reset to 0
wire tie0;
m_tie0   i_tie0   (.tie0(tie0));
m_inv    inv_i_12 (.A(en_clk0_d3),   .Y(en_clk0_d3_n) );
//m_and    and_i_11 (.A(sel_clk1),     .B(en_clk0_d3_n),          .Y(en_clk1) );
m_and    and_i_11 (.A(sel_clk1_clk1),     .B(en_clk0_d3_n),          .Y(en_clk1) );
m_dff_ar dff_i_11 (.RD(rst_sync1), .CK(clk1), .D(en_clk1),    .Q(en_clk1_d1) );
m_dff_ar dff_i_12 (.RD(rst_sync1), .CK(clk1), .D(en_clk1_d1), .Q(en_clk1_d2) );
m_dff_ar dff_i_13 (.RD(rst_sync1), .CK(clk1), .D(en_clk1_d2), .Q(en_clk1_d3) );
m_and    and_i_12 (.A(en_clk1_d2),   .B(clocks_enbale_clk1),    .Y(en_cg_clk1) );
m_cg     cg_i_11  (.CK(clk1),        .E(en_cg_clk1),            .SE(tie0),        .Q(clk1_gated) );

m_or     or_i     (.A(clk0_gated),   .B(clk1_gated),            .Y(clk_out));

endmodule

