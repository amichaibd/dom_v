// Description	: Reset sample to be instantiated in each layout macro 
//                NOTE: This is *NOT* a reset synchronizer
//                When 'test_mode' is high - sample is bypassed
//                Note: There is NO aoutomatic false path on rstn_i 
//                  
//
// test--------------\
//          -----    |
//    1'b1--|   |   |\
//          |   |---| |
// clk -----|>  |   | |------>
//          --o-- /-| |
//            |   | |/
//            |   |   
// rstn_i ----+---/
// Revision	: 0.1
//
//------------------------------------------------------------------------------

module m_reset_sample (
    input wire  clk,
    input wire  rstn_i,
    input wire  test_mode,
    output wire rstn_o
);

wire 	ff;
m_dff_ar  ff_inst (.D(1'b1),   .CK(clk),      .RD(rstn_i),      .Q(ff));
m_mux     rst_mux (.D0(ff) ,   .D1(rstn_i),   .SEL(test_mode),  .X(rstn_o));
   
endmodule //m_reset_sample
