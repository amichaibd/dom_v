//`timescale 1ps/1ps
module m_or (
   input  A
  ,input  B
  ,output Y
);
`ifdef PROCESS_GF45
 OR2_X4M_A12TR m_or_i (
      .Y (Y)
     ,.A (A)
     ,.B (B)
   );
`else
  assign Y = A || B;
`endif

endmodule


  
