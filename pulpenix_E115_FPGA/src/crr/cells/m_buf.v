`include "timescale_cells.v"
module m_buf (
   input  A
  ,output Y
);
`ifdef PROCESS_GF45
BUF_X16M_A12TR m_buf_i (
	   .Y (Y), 
	   .A (A)
	  );
`else
  assign #1 Y = A ;
`endif

endmodule


  
