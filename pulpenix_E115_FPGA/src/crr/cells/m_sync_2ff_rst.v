//`timescale 1ps/1ps
module m_sync_2ff_rst (
  input  rst,
  input  clk,
  input  in,
  output out
);
   wire sync1;

   m_dff_ar sync1_ff (.D(in)   , .CK(clk),  .RD(rst), .Q(sync1));
   m_dff_ar sync2_ff (.D(sync1), .CK(clk ), .RD(rst), .Q(out));
   
 endmodule
  




