module m_dff_sn (
  input  wire CK,
  input  wire SN,
  input  wire D ,
  output wire Q
);

`ifdef PROCESS_GF45
   DFFSQ_X0P5M_A12TR m_dff_sn_inst (
      .D  (D),
      .CK (CK),
      .SN (SN),
      .Q  (Q) );
`else
   reg Q1;
   always @(posedge CK or negedge SN)
      if (!SN) Q1<=1'b1;
      else     Q1<=D;
   assign Q=Q1 ;
`endif

endmodule
