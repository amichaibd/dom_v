//`timescale 1ps/1ps
module m_dffr (
   input  CK
  ,input  D
  ,input  R
  ,output Q
);
`ifdef PROCESS_GF45
  DFFRPQ_X0P5M_A12TR m_dffr_i (
      .D (D)
     ,.R (R)
     ,.CK(CK)
     ,.Q (Q)
   );
`else
   reg Q_next;
   always @(posedge CK or posedge R) 
     if (R) Q_next <= 1'b0;
     else   Q_next <= D;
	assign Q = Q_next ;
`endif

endmodule


  
