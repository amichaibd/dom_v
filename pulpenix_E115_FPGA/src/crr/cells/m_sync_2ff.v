//`timescale 1ps/1ps

module m_sync_2ff (
  input  clk,
  input  in,
  output out
);
   wire sync1;

   m_dff sync1_ff (.D(in),    .CK(clk), .Q(sync1));
   m_dff sync2_ff (.D(sync1), .CK(clk), .Q(out)); 


endmodule
  
