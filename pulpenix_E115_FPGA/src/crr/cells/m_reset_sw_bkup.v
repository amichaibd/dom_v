// ** ----------------------------------------------------- **
// ** PURPOSE : Reset by SW reset                           **
// **                                                       **
// **             |rst_i_n                                  **
// **             |                                         **
// **             +----\   |test_mode                       **
// **             |    |   |                                **
// **             V    |   V                                **
// **          |-----| |  |\                                **
// **          | clr | \->| | rst_o_n                       **
// **  rst_sw_n|     |    | |--->                           **
// **  ------->| DFF |--->| |                               **
// **          |     |    |/                                **
// **          |     |                                      **
// **          |-----|                                      **
// **             ^                                         **
// **             |                                         **
// **             |clk                                      **
// **             |                                         **
// **                                                       **
// ** Note: NO automatic false path on this module!!        **
// ***********************************************************

module m_reset_sw
  #( parameter WIDTH = 1 ) (
  input              clk,
  input              rst_i_n,
  input  [WIDTH-1:0] rst_sw_n,
  input              test_mode,
  output [WIDTH-1:0] rst_o_n
  );
   
  wire [WIDTH-1:0] rst_smp_n;
  generate
    genvar w_ndx;
    for (w_ndx=0; w_ndx < WIDTH; w_ndx = w_ndx +1) begin : w_block
      m_dff_ar   rst_smp (.D(rst_sw_n[w_ndx])   ,.CK(clk)     ,.RD(rst_i_n)  ,.Q(rst_smp_n[w_ndx]));
      m_clk_mux  rst_mux (.D0(rst_smp_n[w_ndx]) ,.D1(rst_i_n) ,.SEL(test_mode) ,.X(rst_o_n[w_ndx]));
    end
  endgenerate

endmodule // m_reset_sw

