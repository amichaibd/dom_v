`include "timescale_cells.v"
module m_inv (
   input  A
  ,output Y
);
`ifdef PROCESS_GF45
INV_X4M_A12TR m_inv_i (
	   .Y (Y), 
	   .A (A)
	  );
`else
  assign #1 Y = !A ;
`endif

endmodule


  
