module m_reset_sw
  (
   input  clk,
   input  rst_n,
   input  rst_sw_n,
   input  test_mode,
   output rst_sync_n
   );
   
   wire rst_smp1;
   wire rst_smp3;
   wire rst ;
   
   m_inv          m_reset_inv (.A(rst_n),  .Y(rst));
   
   m_sync_2ff_rst rst_smp01(.in(rst_sw_n), .clk(clk), .rst(rst), .out(rst_smp1));
   //m_sync_2ff_rst rst_smp23(.in(rst_smp1), .clk(clk), .rst(rst), .out(rst_smp3));

   m_clk_mux      rst_mux  (.D0(rst_smp1), .D1(rst_n), .SEL(test_mode), .X(rst_sync_n) );

endmodule

