module m_dff_ar (
  input  wire CK,
  input  wire RD,
  input  wire D ,
  output wire Q
);

`ifdef PROCESS_GF45
   DFFRPQ_X4M_A12TR m_dff_ar_inst (
      .D  (D),
      .CK (CK),
      .R  (RD),
      .Q  (Q)  );
`else
   reg Q1;
   always @(posedge CK or posedge RD)
      if (RD) Q1<=0;
      else     Q1<=D;
   assign Q=Q1 ;
`endif

endmodule
