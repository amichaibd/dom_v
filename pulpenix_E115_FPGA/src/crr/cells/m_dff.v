module m_dff (
   input  CK
  ,input  D
  ,output Q
);
`ifdef PROCESS_GF45
  DFFQ_X4M_A12TR m_dff_i (
      .D (D)
     ,.CK(CK)
     ,.Q (Q)
   );
`else
   reg Q_next;
   assign Q = Q_next ;
   always @(posedge CK) Q_next <= D;
`endif

endmodule


  
