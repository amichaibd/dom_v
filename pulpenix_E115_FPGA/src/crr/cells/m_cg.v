module m_cg (
   input  CK
  ,input  E
  ,input  SE
  ,output Q
);
`ifdef PROCESS_GF45
  PREICG_X4B_A12TR m_cg_i (
      .CK  (CK)
     ,.E   (E)
     ,.SE  (SE)
     ,.ECK (Q)
   );
`else
   reg lat;
   always @(*)
      if (!CK) lat = SE || E;
   assign Q = CK && lat;
`endif

endmodule


  
