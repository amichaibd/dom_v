module m_mx2 (
   input  A
  ,input  B
  ,input  S0
  ,output Y
);
`ifdef PROCESS_GF45
MX2_X6B_A12TR m_mx2_i (
	   .Y (Y) 
	  ,.A (A)
	  ,.B (B)
	  ,.S0(S0)
	  );
`else
  assign Y = S0 ? B : A;
`endif

endmodule


  
