module m_and (
   input  A
  ,input  B
  ,output Y
);
`ifdef PROCESS_GF45
 AND2_X4M_A12TR m_and_i (
      .Y (Y)
     ,.A (A)
     ,.B (B)
   );
`else
  assign Y = A && B;
`endif

endmodule


  
