// Look for Cock MUX in the Library
module m_clk_mux (
   input  D0
  ,input  D1
  ,input  SEL
  ,output X
);
`ifdef PROCESS_GF45
MX2_X6B_A12TR m_mx2_i (
	   .Y (X) 
	  ,.A (D0)
	  ,.B (D1)
	  ,.S0(SEL)
	  );
`else
  assign X = SEL ? D1 : D0;
`endif

endmodule


  
