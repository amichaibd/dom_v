module m_tie0 (
  output tie0
);
`ifdef PROCESS_GF45
  TIELO_X1M_A12TR i_tie0 (.Y(tie0));
`else
	assign tie0 = 1'b0 ;
`endif

endmodule


  
