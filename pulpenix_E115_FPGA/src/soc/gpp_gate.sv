
// GPP  gate for accelerators

module gpp_gate
#(
    parameter APB_ADDR_WIDTH = 12  //APB slaves are 4KB by default
)
(
    input  logic                      HCLK,
    input  logic                      HRESETn,
    input  logic [APB_ADDR_WIDTH-1:0] PADDR,
    input  logic               [31:0] PWDATA,
    input  logic                      PWRITE,
    input  logic                      PSEL,
    input  logic                      PENABLE,
    output logic               [31:0] PRDATA,
    output logic                      PREADY,
    output logic                      PSLVERR
);
//parameter  SIZE_INST            = 'h800                                     ; //='h800 
//parameter  SIZE_DATA            = 'h600                                     ; //='h600 
//parameter  SIZE_STACK           = 'h100                                     ; //='h100 
//parameter  SIZE_MMIO_GENERAL    = 'hA0                                      ; //='hA0  
//parameter  SIZE_MMIO_CSR        = 'h20                                      ; //='h20  
//parameter  SIZE_MMIO_DRCT_OUT   = 'h4                                      ; //='h10  
//parameter  SIZE_MMIO_ER         = 'h4                                      ; //='h20  
//parameter  SIZE_MMIO_DRCT_IN    = 'h4                                      ; //='h10  
//
//parameter  OFFSET_INST          = 'h0                                       ; //='h0   
//parameter  OFFSET_DATA          = OFFSET_INST+SIZE_INST                     ; //='h800 
//parameter  OFFSET_STACK         = OFFSET_DATA+SIZE_DATA                     ; //='hE00 
//parameter  OFFSET_MMIO_GENERAL  = OFFSET_STACK+SIZE_STACK                   ; //='hF00 
//parameter  OFFSET_MMIO_CSR      = OFFSET_MMIO_GENERAL+SIZE_MMIO_GENERAL     ; //='hFA0 
//parameter  OFFSET_MMIO_DRCT_OUT = OFFSET_MMIO_CSR+SIZE_MMIO_CSR             ; //='hFC0 
//parameter  OFFSET_MMIO_ER       = OFFSET_MMIO_DRCT_OUT+SIZE_MMIO_DRCT_OUT   ; //='hFD0 
//parameter  OFFSET_MMIO_DRCT_IN  = OFFSET_MMIO_ER+SIZE_MMIO_ER               ; //='hFF0 

dom_v  dom_v(
              .HCLK      ( HCLK    ),
              .HRESETn   ( HRESETn ),
              .PADDR     ( PADDR   ),   
              .PWDATA    ( PWDATA  ),    
              .PWRITE    ( PWRITE  ), 
              .PSEL      ( PSEL    ),
              .PENABLE   ( PENABLE ),
              .PRDATA    ( PRDATA  ),
              .PREADY    ( PREADY  ),   
              .PSLVERR   ( PSLVERR )
             );
endmodule

