// Copyright 2015 ETH Zurich and University of Bologna.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 0.51 (the “License”); you may not use this file except in
// compliance with the License. You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-0.51. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.

`include "axi_bus.sv"
`include "debug_bus.sv"
`include "pulpenix_defines.v"

`define AXI_ADDR_WIDTH         32
`define AXI_DATA_WIDTH         32
`define AXI_ID_MASTER_WIDTH     2
`define AXI_ID_SLAVE_WIDTH      4
`define AXI_USER_WIDTH          1

//Defined above
//`define MSYSTEM_ONLY
//`define ALTERA  
//`define XILINX  
//`define NOFLASH 
//Defined locally

module msystem (
  // Clock and Reset
  input  logic               clk                    ,
  input  logic               rst_n                  ,

  input  logic               pad_testmode_i         ,
  input  logic               pad_scan_enable_i      ,
  input  logic               pad_master_slave       , //1=master 0=slave chip

  output logic               pad_spim_cs1           ,
  output logic               pad_spim_cs2           ,
  output logic               pad_spim_cs3           ,
  output logic               pad_spim_cs4           ,
  input  logic               pad_spim_din1          ,  
  input  logic               pad_spim_din2_spis_clk ,  
  input  logic               pad_spim_din3_spis_di  ,  
  input  logic               pad_spim_din4          ,  
  output logic               pad_spim_clk           ,
  output logic               pad_spim_do_spis_do    ,
  output logic               pad_spif_cs            ,
  input  logic               pad_spif_di            ,
  input  logic               pad_spis_cs            , 
  output logic               pad_uart_tx            ,
  input  logic               pad_uart_rx            ,  
  output wire [1:0]          o_qspi_mod              
);

wire        clk_int = clk;
wire        rstn_int = rst_n;


wire              fetch_enable_i    = 1'b1 ;
wire              spi_master_sdi1_i ;//= 1'b0 ;
wire              spi_master_sdi2_i ;//= 1'b0 ;
wire              spi_master_sdi3_i ;//= 1'b0 ;
wire              spi_sdi1_i        = 1'b0 ;
wire              spi_sdi2_i        = 1'b0 ;
wire              spi_sdi3_i        = 1'b0 ;
wire              scl_pad_i         = 1'b0 ;
wire              sda_pad_i         = 1'b0 ;
wire              uart_cts          = 1'b0 ;
wire              uart_dsr          = 1'b0 ;
wire    [31:0]    gpio_in           = 32'd0 ;
logic   [31:0]    gpio_out          ;
logic   [31:0]    gpio_dir          ;
logic   [31:0] [5:0] gpio_padcfg    ;
logic   [31:0] [5:0] pad_cfg_o      ;
logic   [31:0]    pad_mux_o         ;

wire              tck_i             = 1'b0 ; 
wire              trstn_i           = 1'b0 ; 
wire              tms_i             = 1'b0 ; 
wire              tdi_i             = 1'b0 ;

  
    // gpp interconnect    
logic             gpp_hclk      ;
logic             gpp_hresetn   ;
logic      [11:0] gpp_paddr     ;
logic      [31:0] gpp_pwdata    ;
logic             gpp_pwrite    ;
logic             gpp_psel      ;
logic             gpp_penable   ;     
logic      [31:0] gpp_prdata    ;
logic             gpp_pready    ;
logic             gpp_pslverr   ;   

  // mmp1 interconnect    
logic             pmm1_hclk      ;
logic             pmm1_hresetn   ;
logic      [19:0] pmm1_paddr     ;
logic      [31:0] pmm1_pwdata    ;
logic             pmm1_pwrite    ;
logic             pmm1_psel      ;
logic             pmm1_penable   ;     
logic      [31:0] pmm1_prdata    ;
logic             pmm1_pready    ;
logic             pmm1_pslverr   ;   
    
logic     [22:0] o_gpp_irq_out   ;


logic        fetch_enable_int ;
logic        core_busy_int    ;
logic        clk_gate_core_int;
logic [31:0] irq_to_core_int  ;

logic [31:0] boot_addr_int    ;


wire enable_core = 1 ;

AXI_BUS
  #(
  .AXI_ADDR_WIDTH ( `AXI_ADDR_WIDTH     ),
  .AXI_DATA_WIDTH ( `AXI_DATA_WIDTH     ),
  .AXI_ID_WIDTH   ( `AXI_ID_SLAVE_WIDTH ),
  .AXI_USER_WIDTH ( `AXI_USER_WIDTH     )
  ) slaves[2:0]();

AXI_BUS
  #(
  .AXI_ADDR_WIDTH ( `AXI_ADDR_WIDTH      ),
  .AXI_DATA_WIDTH ( `AXI_DATA_WIDTH      ),
  .AXI_ID_WIDTH   ( `AXI_ID_MASTER_WIDTH ),
  .AXI_USER_WIDTH ( `AXI_USER_WIDTH      )
  ) masters[3:0]();

DEBUG_BUS
debug();


parameter DATA_RAM_SIZE        = 32768; // in bytes
parameter INSTR_RAM_SIZE       = 32768; // in bytes
   
localparam INSTR_ADDR_WIDTH = $clog2(INSTR_RAM_SIZE)+1; // to make space for the boot rom
localparam DATA_ADDR_WIDTH  = $clog2(DATA_RAM_SIZE);

wire [21:0] init_ram_addr;
wire [31:0] init_ram_data;   
wire system_rdy          ;
wire [3:0] o_qspi_dat    ;
wire [3:0] i_qspi_dat    ;
wire o_qspi_cs_n_i       ;
wire o_qspi_sck_i        ;


wire init_ram1_wr ;
wire init_ram2_wr ;
wire init_ram3_wr ;
wire init_ram4_wr ;
wire init_ram5_wr ;
wire init_ram6_wr ;
wire init_ram7_wr ;
wire init_ram8_wr ;
wire init_ram9_wr ;

wire system_rdy_from_boot_flash ;
//wire flash_active  ;

    


`ifdef NOFLASH
 assign system_rdy = 1'b1 ;
 
 assign o_qspi_dat      = 4'b0  ; 
 assign o_qspi_cs_n_i   = 1'b0  ; 
 assign o_qspi_sck_i    = 1'b0  ; 
 assign o_qspi_mod      = 2'b0  ; 
 assign init_ram_addr   = 22'b0 ; 
 assign init_ram_data   = 32'b0 ; 
 assign init_ram1_wr    = 1'b0  ; 
 assign init_ram2_wr    = 1'b0  ; 
 assign init_ram3_wr    = 1'b0  ; 
 assign init_ram4_wr    = 1'b0  ; 
 assign init_ram5_wr    = 1'b0  ; 
 assign init_ram6_wr    = 1'b0  ; 
 assign init_ram7_wr    = 1'b0  ; 
 assign init_ram8_wr    = 1'b0  ; 
 assign init_ram9_wr    = 1'b0  ; 
 assign pmm1_prdata     = 32'b0 ; 
 assign pmm1_pready     = 1'b0  ; 
 
`else   

boot_flash_direct_intrfc 
  #(.DATA_RAM_SIZE  ( DATA_RAM_SIZE  ),
    .INSTR_RAM_SIZE ( INSTR_RAM_SIZE )) 
  boot_flash_direct_intrfc_i (
  ////////////////////////////
  .clk_int         (clk_int        ), 
  .rstn_int        (rstn_int       ),
  
  .o_qspi_dat      ( o_qspi_dat    ),
  .i_qspi_dat      ( i_qspi_dat    ),
  .o_qspi_cs_n     ( o_qspi_cs_n_i ),
  .o_qspi_sck      ( o_qspi_sck_i  ),
  .o_qspi_mod      ( o_qspi_mod    ),
  
  .init_ram_addr   ( init_ram_addr ),
  .init_ram_data   ( init_ram_data ),   
  .init_ram1_wr    ( init_ram1_wr  ),
  .init_ram2_wr    ( init_ram2_wr  ),
  .init_ram3_wr    ( init_ram3_wr  ),
  .init_ram4_wr    ( init_ram4_wr  ),
  .init_ram5_wr    ( init_ram5_wr  ),
  .init_ram6_wr    ( init_ram6_wr  ),
  .init_ram7_wr    ( init_ram7_wr  ),
  .init_ram8_wr    ( init_ram8_wr  ),
  .init_ram9_wr    ( init_ram9_wr  ),
   
  .i_wb_adr        ( {12'd0,pmm1_paddr}  ),
  .i_wb_cyc        ( pmm1_penable   ),
  .i_wb_we         ( pmm1_pwrite    ),
  .i_wb_data_in    ( pmm1_pwdata    ),
  .o_wb_data_out   ( pmm1_prdata    ),
  .o_wb_ack        ( pmm1_pready    ),
  
  .i_system_mem_rdy (1'b1           ),
  .o_system_rdy     (system_rdy_from_boot_flash  )
) ;  

                                  
assign system_rdy = system_rdy_from_boot_flash ;
`endif



//----------------------------------------------------------------------------//
// Core region
//----------------------------------------------------------------------------//
//wire  [14:0]             ram_ctrl      ;
//wire  [59:0]             ram_fix_addr_d;
//wire  [47:0]             ram_fix_data_d;
//wire  [59:0]             ram_fix_addr_i;
//wire  [47:0]             ram_fix_data_i;

   
core_region  #(
  .AXI_ADDR_WIDTH       ( `AXI_ADDR_WIDTH      ),
  .AXI_DATA_WIDTH       ( `AXI_DATA_WIDTH      ),
  .AXI_ID_MASTER_WIDTH  ( `AXI_ID_MASTER_WIDTH ),
  .AXI_ID_SLAVE_WIDTH   ( `AXI_ID_SLAVE_WIDTH  ),
  .AXI_USER_WIDTH       ( `AXI_USER_WIDTH      ),
  // Modified
  .INIT_INSTR_ADDR_W    ( INSTR_ADDR_WIDTH     ),
  .INIT_DATA_ADDR_W     ( DATA_ADDR_WIDTH      ),
  .DATA_RAM_SIZE        ( DATA_RAM_SIZE        ),
  .INSTR_RAM_SIZE       ( INSTR_RAM_SIZE       )
  //End Modified
  ) core_region_i (
////////////////////////////
  .clk                ( clk_int                             ),
  .rst_n              ( rstn_int                            ),
                                                            
  .testmode_i         ( pad_testmode_i                      ),
  .fetch_enable_i     ( fetch_enable_int & system_rdy & enable_core  ),
  .irq_i              ( irq_to_core_int                     ),
  .core_busy_o        ( core_busy_int                       ),
  .clock_gating_i     ( clk_gate_core_int ),
  .boot_addr_i        ( boot_addr_int                       ),

  // modified
  .ram_init_data_i    (init_ram2_wr &  pad_master_slave     ),
  .init_addr_data_i   (init_ram_addr[DATA_ADDR_WIDTH-1:0]   ),
  .init_wdata_data_i  (init_ram_data                        ),
    `ifndef NOFLASH                                        
  .init_we_data_i     (init_ram2_wr &  pad_master_slave     ),
    `else                                                  
   .init_we_data_i    (1'b0                                 ),
    `endif                                                 
  .ram_init_instr_i   (init_ram1_wr &  pad_master_slave     ),
  .init_addr_instr_i  (init_ram_addr[INSTR_ADDR_WIDTH-1:0]  ),
  .init_wdata_instr_i (init_ram_data                        ),
    `ifndef NOFLASH                                         
  .init_we_instr_i    (init_ram1_wr &  pad_master_slave     ),
    `else                                                   
  .init_we_instr_i    (1'b0                                 ),
    `endif                                                  
                                                 
  // End modified                                       
                                                            
  .core_master        ( masters[0]                          ),
  .dbg_master         ( masters[1]                          ),
  .data_slave         ( slaves[1]                           ),
  .instr_slave        ( slaves[0]                           ),
  .debug              ( debug                               ),
                                                            
  .tck_i              ( tck_i                               ),
  .trstn_i            ( trstn_i                             ),
  .tms_i              ( tms_i                               ),
  .tdi_i              ( tdi_i                               ),
  .tdo_o              ( tdo_o                               )
  
  //.ram_ctrl           ( ram_ctrl                            ),
  //.ram_fix_addr_i     ( ram_fix_addr_i                      ),
  //.ram_fix_data_i     ( ram_fix_data_i                      ),
  //.ram_fix_addr_d     ( ram_fix_addr_d                      ),
  //.ram_fix_data_d     ( ram_fix_data_d                      )

  );

  //----------------------------------------------------------------------------//
  // Peripherals
  //----------------------------------------------------------------------------//
  wire [1:0] spi_mode_o ;
  wire [1:0] spi_master_mode_o ;
  
  peripherals  #(
  .AXI_ADDR_WIDTH      ( `AXI_ADDR_WIDTH      ),
  .AXI_DATA_WIDTH      ( `AXI_DATA_WIDTH      ),
  .AXI_SLAVE_ID_WIDTH  ( `AXI_ID_SLAVE_WIDTH  ),
  .AXI_MASTER_ID_WIDTH ( `AXI_ID_MASTER_WIDTH ),
  .AXI_USER_WIDTH      ( `AXI_USER_WIDTH      )
  ) peripherals_i (
  /////////////////////////////
  .clk_i            ( clk_int            ),
  .rst_n            ( rstn_int           ),
                                         
  .axi_spi_master   ( masters[2]         ),  
  .axi_uart_master  ( masters[3]       ),   // Added to allow mastering axi from UART port , for debug over UART , 
                                            // consider replacing/merging adv_dbg
  
  .debug            ( debug              ),
                                         
  .spi_clk_i        ( spi_clk_i          ),
  .testmode_i       ( pad_testmode_i     ),
  .spi_cs_i         ( spi_cs_i           ),
  .spi_mode_o       ( spi_mode_o         ),
  .spi_sdo0_o       ( spi_sdo0_o         ),
  .spi_sdo1_o       ( spi_sdo1_o         ),
  .spi_sdo2_o       ( spi_sdo2_o         ),
  .spi_sdo3_o       ( spi_sdo3_o         ),
  .spi_sdi0_i       ( spi_sdi0_i         ),
  .spi_sdi1_i       ( spi_sdi1_i         ),
  .spi_sdi2_i       ( spi_sdi2_i         ),
  .spi_sdi3_i       ( spi_sdi3_i         ),
                                         
  .slave            ( slaves[2]          ),
                                         
  .uart_tx          ( pad_uart_tx        ),
  .uart_rx          ( pad_uart_rx        ),
  .uart_rts         ( uart_rts           ),
  .uart_dtr         ( uart_dtr           ),
  .uart_cts         ( uart_cts           ),
  .uart_dsr         ( uart_dsr           ),
                                         
  .spi_master_clk   ( spi_master_clk_o   ),
  .spi_master_csn0  ( spi_master_csn0_o  ),
  .spi_master_csn1  ( spi_master_csn1_o  ),
  .spi_master_csn2  ( spi_master_csn2_o  ),
  .spi_master_csn3  ( spi_master_csn3_o  ),
  .spi_master_mode  ( spi_master_mode_o  ),
  .spi_master_sdo0  ( spi_master_sdo0_o  ),
  .spi_master_sdo1  ( spi_master_sdo1_o  ),
  .spi_master_sdo2  ( spi_master_sdo2_o  ),
  .spi_master_sdo3  ( spi_master_sdo3_o  ),
  .spi_master_sdi0  ( spi_master_sdi0_i  ),
  .spi_master_sdi1  ( spi_master_sdi1_i  ),
  .spi_master_sdi2  ( spi_master_sdi2_i  ),
  .spi_master_sdi3  ( spi_master_sdi3_i  ),
                    
  .scl_pad_i        ( scl_pad_i          ),
  .scl_pad_o        ( scl_pad_o          ),
  .scl_padoen_o     ( scl_padoen_o       ),
  .sda_pad_i        ( sda_pad_i          ),
  .sda_pad_o        ( sda_pad_o          ),
  .sda_padoen_o     ( sda_padoen_o       ),
                                         
  .gpio_in          ( gpio_in            ),
  .gpio_out         ( gpio_out           ),
  .gpio_dir         ( gpio_dir           ),
  .gpio_padcfg      ( gpio_padcfg        ),
                                         
  .core_busy_i      ( core_busy_int      ),
  .irq_o            ( irq_to_core_int    ),
  .fetch_enable_i   ( fetch_enable_i     ),
  .fetch_enable_o   ( fetch_enable_int   ),
  .clk_gate_core_o  ( clk_gate_core_int  ),
                                         
  .pad_cfg_o        ( pad_cfg_o          ),
  .pad_mux_o        ( pad_mux_o          ),
  .boot_addr_o      ( boot_addr_int      ),
    
  // GPP Interface (General Purpose Peripherals)
                    
  .gpp_hclk         ( gpp_hclk           ),
  .gpp_hresetn      ( gpp_hresetn        ),
  .gpp_paddr        ( gpp_paddr          ),
  .gpp_pwdata       ( gpp_pwdata         ),
  .gpp_pwrite       ( gpp_pwrite         ),
  .gpp_psel         ( gpp_psel           ),
  .gpp_penable      ( gpp_penable        ),            
  .gpp_prdata       ( gpp_prdata         ),
  .gpp_pready       ( gpp_pready         ),
  .gpp_pslverr      ( gpp_pslverr        ),
    
  // PMM1 Interface (Peripheral Mapped Memory #1)
  .pmm1_hclk        ( pmm1_hclk          ),
  .pmm1_hresetn     ( pmm1_hresetn       ),
  .pmm1_paddr       ( pmm1_paddr         ),
  .pmm1_pwdata      ( pmm1_pwdata        ),
  .pmm1_pwrite      ( pmm1_pwrite        ),
  .pmm1_psel        ( pmm1_psel          ),
  .pmm1_penable     ( pmm1_penable       ),            
  .pmm1_prdata      ( pmm1_prdata        ),
  .pmm1_pready      ( pmm1_pready        ),
  .pmm1_pslverr     ( pmm1_pslverr       ),
  .gpp_irq_out      ( o_gpp_irq_out      )
    
  );

//Mux SPI interfaces out
  //Output pads
  
  logic spi_master_sdo ;
  assign pad_spim_cs1          = spi_master_csn0_o  ;
  assign pad_spim_cs2          = spi_master_csn1_o  ;                         
  assign pad_spim_cs3          = spi_master_csn2_o  ;                         
  assign pad_spim_cs4          = spi_master_csn3_o  ; 
  
  // assign pad_spim_clk          = !system_rdy | flash_active ?  o_qspi_sck_i     :   //Flash 
  //                                                              spi_master_clk_o ;   //Master

  assign pad_spim_clk          = !system_rdy  ?  o_qspi_sck_i     :   //Flash 
                                                 spi_master_clk_o ;   //Master
                                                               
                                                               
  assign spi_master_sdo        = !spi_master_csn0_o ? spi_master_sdo0_o : 
                                 !spi_master_csn1_o ? spi_master_sdo1_o :
                                 !spi_master_csn2_o ? spi_master_sdo2_o : spi_master_sdo3_o ;                                 
  assign pad_spim_do_spis_do   = !pad_spif_cs           ? o_qspi_dat[0]     :  //Flash
                                 pad_spis_cs            ? spi_master_sdo    :  //Master if not cs of slave
                                 spi_sdo0_o                             ;  //Slave meand cs of slave is down
  assign pad_spif_cs           = o_qspi_cs_n_i     ;
  //Ibput pads
  assign spi_master_sdi0_i =  pad_spim_din1 ; 
  assign spi_master_sdi1_i =  pad_spim_din2_spis_clk ; 
  assign spi_master_sdi2_i =  pad_spim_din3_spis_di ; 
  assign spi_master_sdi3_i =  pad_spim_din4 ;
  assign spi_cs_i          =  pad_spis_cs ; //Slave
  assign spi_clk_i         =  pad_spim_din2_spis_clk;
  assign spi_sdi0_i        =  pad_spim_din3_spis_di ;
  assign i_qspi_dat        =  {2'b11,pad_spif_di,1'b0}  ;  

  
parameter APB_ADDR_WIDTH = 12; //APB slaves are 4KB by default

//----------------------------------------------------------------------------//

// GPP General Purpose Port Gate module

gpp_gate #(
  .APB_ADDR_WIDTH(APB_ADDR_WIDTH)) 
  gpp_gate_i  (                          
  .HCLK                      ( gpp_hclk           ) ,
  .HRESETn                   ( gpp_hresetn        ) ,
  .PADDR                     ( gpp_paddr          ) ,
  .PWDATA                    ( gpp_pwdata         ) ,
  .PWRITE                    ( gpp_pwrite         ) ,
  .PSEL                      ( gpp_psel           ) ,
  .PENABLE                   ( gpp_penable        ) ,
  .PRDATA                    ( gpp_prdata         ) ,
  .PREADY                    ( gpp_pready         ) ,
  .PSLVERR                   ( gpp_pslverr        )   
);

//----------------------------------------------------------------------------//
// Axi node
//----------------------------------------------------------------------------//

axi_node_intf_wrap #(
  .NB_MASTER      ( 3                    ),
  .NB_SLAVE       ( 4                    ),
  .AXI_ADDR_WIDTH ( `AXI_ADDR_WIDTH      ),
  .AXI_DATA_WIDTH ( `AXI_DATA_WIDTH      ),
  .AXI_ID_WIDTH   ( `AXI_ID_MASTER_WIDTH ),
  .AXI_USER_WIDTH ( `AXI_USER_WIDTH      )
  ) axi_interconnect_i (
  .clk       ( clk_int          ),
  .rst_n     ( rstn_int         ),
  .test_en_i ( pad_testmode_i   ),
  
  .master    ( slaves           ),
  .slave     ( masters          ),
 
  .start_addr_i ( { 32'h1A10_0000, 32'h0010_0000, 32'h0000_0000 } ),
  // Udi: extended APB end_addr range to cover PMM (Peripheral Mapped Memory)
  //.end_addr_i   ( { 32'h1A11_FFFF, 32'h001F_FFFF, 32'h000F_FFFF } )
  .end_addr_i   ( { 32'h1A2F_FFFF, 32'h001F_FFFF, 32'h000F_FFFF } )
);
  
endmodule

