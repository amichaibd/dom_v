// Copyright 2017 ETH Zurich and University of Bologna.
// Copyright and related rights are licensed under the Solderpad Hardware
// License, Version 0.51 (the “License”); you may not use this file except in
// compliance with the License.  You may obtain a copy of the License at
// http://solderpad.org/licenses/SHL-0.51. Unless required by applicable law
// or agreed to in writing, software, hardware and materials distributed under
// this License is distributed on an “AS IS” BASIS, WITHOUT WARRANTIES OR
// CONDITIONS OF ANY KIND, either express or implied. See the License for the
// specific language governing permissions and limitations under the License.
`include "pulpenix_defines.v"

module sp_ram_wrap
  #(
    parameter RAM_SIZE   = 32768,    // in bytes 
    parameter ADDR_WIDTH = $clog2(RAM_SIZE),
    parameter DATA_WIDTH = 32,
    parameter NUM_WORDS_REG = 256,
	  parameter ADDR_WIDTH_REG = $clog2(NUM_WORDS_REG) 
  )(
    // Clock and Reset
    input  logic                    clk,
    input  logic                    rstn_i,
    input  logic                    en_i,
    input  logic [ADDR_WIDTH-1:0]   addr_i,
    input  logic [DATA_WIDTH-1:0]   wdata_i,
    output logic [DATA_WIDTH-1:0]   rdata_o,
    input  logic                    we_i,
    input  logic [DATA_WIDTH/8-1:0] be_i,
    input  logic                    bypass_en_i
  );

	reg [ADDR_WIDTH-1:0] addr_i_d1 ;
	always @( posedge clk or negedge rstn_i)
		if (!rstn_i) addr_i_d1 <=#1 {ADDR_WIDTH{1'b0}} ;
		else addr_i_d1 <=#1 addr_i ;
		
	wire [31:0]	rdata_o_reg ;
	wire [31:0]	rdata_o_ram ;
  wire [31:0] rdata_o_ram2 ;
  wire [31:0] rdata_o_ram1 ;
	wire        we_i_reg ;
	assign we_i_reg = we_i & (addr_i<NUM_WORDS_REG) ;


  wire                    en_i_d         ;
  wire [ADDR_WIDTH-1:0]   addr_i_d       ;
  wire [DATA_WIDTH-1:0]   wdata_i_d      ;
  wire                    we_i_d         ;
  wire [DATA_WIDTH/8-1:0] be_i_d         ;
  wire                    bypass_en_i_d  ;
     
    
     
`ifdef ALTERA
    assign   en_i_d         = en_i        ; 
    assign   addr_i_d       = addr_i      ; 
    assign   wdata_i_d      = wdata_i     ; 
    assign   we_i_d         = we_i        ; 
    assign   be_i_d         = be_i        ; 
    assign   bypass_en_i_d  = bypass_en_i ; 
             altera_sram_32768x32	altera_sram_32768x32_i 
               (     // TBD Change to 8K    
      	    	.address ({2'b00,addr_i[ADDR_WIDTH-1:2]} ),  // TBD CHECK
      	    	.byteena ( be_i & {4{we_i}} ),
      	    	.clken 	 ( en_i 			),
      	    	.clock   ( clk 				),
      	    	.data    ( wdata_i 			),
      	    	.wren    ( we_i 			),
      	    	.q       ( rdata_o_ram		)
      	   		);
      
              assign rdata_o = rdata_o_ram ;
      
`elsif ASIC
// Locate here your ASIC specific compiled memory instantiation.  		
`else 
// Generic simulation model
  sp_ram
  #(
    .ADDR_WIDTH ( ADDR_WIDTH ),
    .DATA_WIDTH ( DATA_WIDTH ),
    .NUM_WORDS  ( RAM_SIZE   )
  )
  sp_ram_i
  (
    .clk     ( clk       ),

    .en_i    ( en_i      ),
    .addr_i  ( addr_i    ),
    .wdata_i ( wdata_i   ),
    .rdata_o ( rdata_o   ),
    .we_i    ( we_i      ),
    .be_i    ( be_i      )
  );
`endif 
 
endmodule
