
/* GCD Accelerator

   Greatest Common Divisor Using Euclid's algorithm

	Author: Udi Kra
    
	Description:

    HW sequential design to calculate GCD .
    The GCD method applied is the euclidean simple algorithm 
    The design includes a control state machine and a data-path
    A block-diagram  chart is available and provide in order to illustrate the Machine.        
 */
 
 module accelerator_1             // accelerator_1 is an euclidean based GCD Machine
  #(parameter NUM_WIDTH = 16) (   // Parametrized bit width of arguments.
 
    input clk,          // Clock 
    input rst_n,        // Async reset (asserted low) 
    
    input [NUM_WIDTH-1:0] a,    // Input Argument "a" 
    input [NUM_WIDTH-1:0] b,    // Input Argument "b" 
    input ab_in_valid,          // when asserted gcd inputs are valid 
    
    output logic [NUM_WIDTH-1:0] gcd,   // Output gcd result
    output logic gcd_out_ready          // Done signal , will assert when calculation is done 
                                        // and deassert on a new calculation trigger                                       
  );
  

  // State-Machine State enumerated coding allocation. 
  
  enum logic [1:0] { IDLE, CALC, DONE } state, next_state; 
  
  // IDLE : Machine is idle pending for valid input data
  // CALC : Machine is busy iterating to resolve calculation
  // DONE : Calculation is done.
        
  reg [NUM_WIDTH-1:0] a_intrm ,      b_intrm ;       // Intermediate a,b sequential registers 
  reg [NUM_WIDTH-1:0] a_intrm_next , b_intrm_next ;  // a,b next intermediate value (pre-sampled)
  
  reg calc_done ; // Internal signal to indicate calculation is done
  
//---------------------------------------------------------------------------------

  // State Machine Combinational section - compute state
  
  always @*
  begin        
    next_state = state ;     // Default LHS signals to prevent any unintended latch generation.     
    case (state)             // Figure out next state
    IDLE: 
       if (ab_in_valid) begin   // Once a,b are valid start calculation
       next_state = CALC ;       
       end        
    CALC:  
       if (calc_done) next_state = DONE  ;   // Keep calculating till calculation dobe condition is indicated.      
    DONE:  
       next_state = IDLE ;                   // Upon Done, go back to the IDLE cycle.     
   default: begin                            // Non valid stated transition , error assertion.
       if (rst_n) $display("%t: State machine in ERROR state, Returning to IDLE state\n",$time); 
       next_state = IDLE ;
       end             
    endcase       
  end
//---------------------------------------------------------------------------------
     
  // State Machine Sequential section - switch state  
  
  always @(posedge clk or negedge rst_n)   // New State sampling
	if (!rst_n) 
	   state <= IDLE ;                     // Idle state entry upon reset.
    else
       begin    
       state <= next_state;                // Actual sampling.
       end    
        
 
//---------------------------------------------------------------------------------

      
  // Combinational Calculation of the next Intermediate values
  
  
  always @* 
  begin
   // Default LHS signals to prevent any unintended latch generation.  
      calc_done = 0 ;      // by default calculation is done 
      a_intrm_next = a ;   // by default intermediate next "a" gets the "a" input value  
      b_intrm_next = b ;   // by default intermediate next "a" gets the "a" input value  

      
      if (state==CALC) begin // Overwrite default values if needed
        if (a_intrm==b_intrm) begin  // Calculation completed condition
          a_intrm_next = a_intrm ;  
          b_intrm_next = b_intrm ;                   
          calc_done = 1 ;            // Indicate calculation is done
        end
        else begin
          if (a_intrm > b_intrm) begin           // Calculation not yet completed ; a>b
            a_intrm_next = a_intrm - b_intrm ;   // apply Euclidean algorithm step  
            b_intrm_next = b_intrm ;            
          end else begin
            a_intrm_next = a_intrm  ;            // a<=b
            b_intrm_next = b_intrm - a_intrm ; 
          end                                              
        end 
      end  // end of state==CALC         
  end // end of always

 
 
//---------------------------------------------------------------------------------

      
// Sequential section - update intermediate a,b
  
  always @(posedge clk or negedge rst_n)  // Sample the intermediate values
	if (!rst_n) begin
	   a_intrm <= {NUM_WIDTH{1'b0}} ;
       b_intrm <= {NUM_WIDTH{1'b0}} ; 
       gcd_out_ready <= 0 ;
       gcd <= 32'b0 ; 
    end       
    else begin
	   a_intrm <= a_intrm_next ;
       b_intrm <= b_intrm_next ; 
       if (state==DONE) begin
          gcd_out_ready <= 1'b1 ;
          gcd <= a_intrm ;  // Eventually when we are done gcd result gets the "a" intermediate value  
       end
       else if (ab_in_valid) begin
          gcd_out_ready  <= 1'b0 ; 
          gcd <= 32'b0 ; // Invalid null till DONE
       end          
    end       

//---------------------------------------------------------------------------------
       

endmodule