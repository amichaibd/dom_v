import zmq

class port :
      
    def __init__(self):
    
      self.port_name = "tcp://localhost:5555"
      self.socket = None
      self.connect()
      
    def connect(self) :
      context = zmq.Context()
      print("Connecting to pulp_pygate server…")
      self.socket = context.socket(zmq.REQ)
      self.socket.connect("tcp://localhost:5555")  

    def disconnect(self) :
      context = zmq.Context()
      print("Disconnecting from pulp_pygate")
      self.socket.disconnect(self.port_name)  
  
    def read(self,addr) :
    
       addrByteList = [None] * 4
       
       addrByteList[0] = ( addr >> 24 ) & 0xff;
       addrByteList[1] = ( addr >> 16 ) & 0xff;
       addrByteList[2] = ( addr >> 8  ) & 0xff;
       addrByteList[3] =   addr         & 0xff; 
    
       self.socket.send(b"R" + bytearray(addrByteList))

       # poll till value is returned , using Non-block to enable CTRL-C interrupts
       retMsg = None
       while (retMsg==None) :       
         try:
           retMsg = self.socket.recv(flags=zmq.NOBLOCK)
         except zmq.Again as e:
           continue  
           
       rdata = 0 
       for b in retMsg[0:4] :
          rdata = rdata*256 + b
       return rdata
       
    def write(self,addr,data) :
    
       byteList = [None] * 8
   
       byteList[0] = ( addr >> 24 ) & 0xff;
       byteList[1] = ( addr >> 16 ) & 0xff;
       byteList[2] = ( addr >> 8  ) & 0xff;
       byteList[3] =   addr         & 0xff; 

       byteList[4] = ( data >> 24 ) & 0xff;
       byteList[5] = ( data >> 16 ) & 0xff;
       byteList[6] = ( data >> 8  ) & 0xff;
       byteList[7] =   data         & 0xff; 
           
       self.socket.send(b"W" + bytearray(byteList))

       # poll till (dummy) reply is returned , using Non-block to enable CTRL-C interrupts
       retMsg = None
       while (retMsg==None) :       
         try:
           retMsg = self.socket.recv(flags=zmq.NOBLOCK)
         except zmq.Again as e:
           continue  

