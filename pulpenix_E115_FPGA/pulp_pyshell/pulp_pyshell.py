
import time
import serial
import sys
import serial_gateway as sgw
import bp_save

#================================================================================

class serGate () :

   def __init__(self):   
      self.fromPulpQue = [] 
      self.toPulpQue = []    

#---------------------------------------------------------------------------------

def configPort(portName) :

   # configure the serial connections (the parameters differs on the device you are connecting to)
   ser = serial.Serial()
   ser.port=portName
   ser.baudrate=115200
   ser.parity=serial.PARITY_NONE
   ser.stopbits=serial.STOPBITS_ONE
   ser.bytesize = serial.EIGHTBITS #number of bits per bytes
   ser.xonxoff = False             #disable software flow control
   ser.rtscts = False              #disable hardware (RTS/CTS) flow control
   ser.dsrdtr = False              #disable hardware (DSR/DTR) flow control
   ser.timeout = 1                 #non-block read    
   ser.writeTimeout = 1            #timeout for write  
   return ser   


#----------------------------------------------------------

class fileRef :
    def __init__(self):
      self.file = None
      self.type = None
      self.atEOF = False

#----------------------------------------------------------

class pyShell :
      def __init__(self):
      
        self.firstReady = True # used to indicate pyshell
        self.masterReadAccessOn = False
        self.masterWriteAccessOn = False
        self.startDebugBridge = False
        self.masterWriteAccessOn = False
        self.strFromSer = ""
        # self.dbrOn = False
        self.serReady = False
        self.masterAccessOn = False
        self.newLine = False
        self.serPyshellCmdOn = False
        self.serRead = ''
        self.accessFileIdx = -1
        self.serPyshellCmdIdx = 0 
        self.masterAccessActive = False

      
      def pyShIterate(self,numItr) :

        # print("TMP DBG pyShIterate ,  len(stdioSerGate.fromPulpQue) = %s" % len(stdioSerGate.fromPulpQue))
         
        for i in range(numItr) : # Iterate mumItr times
                                    
           # Parse serial from Pulp
           
           if not self.serReady :
                
               if not (self.newLine or self.serReady) :
                         
                   len_stdioSerGate_fromPulpQue =  len(stdioSerGate.fromPulpQue)
                   if len_stdioSerGate_fromPulpQue == 0 :
                      continue
                   else:
                      for i in range(len_stdioSerGate_fromPulpQue) : 
                         self.serRead = stdioSerGate.fromPulpQue.pop(0)                                                
                         if ord(self.serRead) <=127 :  # ignore escapes
                           c = self.serRead.decode('ascii')               
                           self.strFromSer += c  
                           self.newLine = (c=='\n')
                           if self.newLine :
                             break
                           self.serReady = (self.strFromSer=="Ready> ")
                           # print("TMP DBG self.strFromSer=%s,serReady=%s,nl=%s,ord(c)=%x" % (self.strFromSer,self.serReady,self.newLine,ord(c))) 
                           self.serPyshellCmdIdx = (self.strFromSer.find("$pyshell"))
                           self.serPyshellCmdOn = self.serPyshellCmdIdx>-1 
                         else :
                           print("WARNING: found non ascii stdioSerGate.fromPulpQue.pop(0) = %x" , ord(self.serRead))
                         
               else :  
                   # print("TMP DBG1 self.strFromSer  = %s" % self.strFromSer)  
                   if (self.masterAccessActive and self.newLine) : # return control to menu upon master access response return                
                         stdioSerGate.toPulpQue.extend("\n".encode())
                         self.masterAccessActive = False                                  

               
                   if self.serPyshellCmdOn :                   
                        if  (self.serPyshellCmdIdx>0) : 
                          self.pyShellCmdStr = self.strFromSer[self.serPyshellCmdIdx:]              
                          self.strFromSer =  self.strFromSer[:self.serPyshellCmdIdx] 
                        else :
                          self.pyShellCmdStr = self.strFromSer                       
                          
                   if (not  self.serPyshellCmdOn) or (self.serPyshellCmdOn and (self.serPyshellCmdIdx>0)) :
                   
                       if (self.accessFileIdx<0) : 
                         print("%s" % self.strFromSer ,end='') 
                       else :             
                         fileIdxList[self.accessFileIdx].file.write("%s" %(self.strFromSer))
                                
                   if self.serPyshellCmdOn :
                       exPyshell(self.pyShellCmdStr)
                       self.serPyshellCmdOn = False
                                                     

                   self.newLine = False
                   self.serPyshellCmdOn = False
                   self.serRead = '' 
                   #print("TMP DBG  empty strFromSer")                 
                   self.strFromSer=""
                   #print("TMP DBG strFromSer=%sXXX self.newLine=%s; " % (self.strFromSer,str(self.newLine))) 
                 
                   
           else : # @serReady ("Ready> " detected) get keyboard menu responses input to be sent over serial
             #print("TMP DBG READY")
             if not (self.masterAccessActive  and self.serReady) :         
                print("Ready> ",end='')
             
             if self.firstReady :
                stdioSerGate.toPulpQue.extend(("$p\n").encode()) # agreed to indicate python shell mode to DUT                
                self.firstReady = False  
             else :  
                self.serPyshellCmd = False       
                self.strToSer = input("") 
                if self.strToSer.find("#q") >= 0 :
                  print("#q detected, QUITTING") 
                  if gdbEnabled :
                    dbgBr.callback.recoverBP()
                  exit()
                 
                self.masterReadAccessOn = self.strToSer.find("#r") >= 0
                self.masterWriteAccessOn = self.strToSer.find("#w") >= 0
                
                if (self.masterReadAccessOn or self.masterWriteAccessOn) :
                  self.masterAccessActive = True    
 
                if  self.strToSer.find("#a0") >= 0 :  # load image starting address 0 , data=0x00101000 and jump to reset entry 0x80
                  load_app_by_uart_and_jmp_reset(self.strToSer) 
                  
                elif  self.strToSer.find("#a") >= 0 : # load app starting address instr=4000 , data=0x00101000
                                    
                  load_app_by_uart_and_run(self.strToSer)
                  self.strToSer = ''
           
                stdioSerGate.toPulpQue.extend((self.strToSer + '\n').encode())

             self.serReady = False
 
              
# --------------------------------------------------------------------------------

def quitApp() :
 print("\npyshell: Quitting Application upon quitApp() message received over UART\n") 
 quit()


# --------------------------------------------------------------------------------




def openFile(file_name,type) :
  f = open(file_name,type)
  fileEntry = fileRef()
  fileEntry.file = f
  fileEntry.type = type
  fileEntry.atEOF = False   
  fileIdxList.append(fileEntry)
  fileIdx = len(fileIdxList)-1
  print("pyshell: Opening file %s of type %c , file Index is %d\n" % (file_name,type,fileIdx))
  return (fileIdx)


#--------------------------------------------------------

def closeFile(fileIdx) :
  fileName = fileIdxList[fileIdx].file.name
  fileIdxList[fileIdx].file.close()
  print("pyshell: Closed file of idx = %d , %s" % (fileIdx,fileName))
  return (1)


#--------------------------------------------------------

def accessFile(fileIdx) :
  # global accessFileIdx  
  pySh.accessFileIdx = fileIdx ;
  return 1

#--------------------------------------------------------
def isStrDelimiter(c) :
        # cval = ord(c)
        # return (((cval>=0x09) and (cval<=0x0D)) or (cval==0x20))
        return (ord(c) <= 0x20) or (ord(c)==127) # any thing other than visible charterers
        
#--------------------------------------------------------

def fgets(fileIdx) :
    # Check if an active program is accessing a file
    if (fileIdx>=0) :
     if (fileIdxList[fileIdx].type=='r') :
        str_on = False
        str_buf = "" # prevent uninitialized debug prints
        isEOF = fileIdxList[fileIdx].atEOF
        if isEOF :
          stdioSerGate.toPulpQue.extend(bytearray([0xFF]))
          return 1
		        
        while not (isEOF) : 
            c =  fileIdxList[fileIdx].file.read(1)             
            isEOF = (c=='')
            fileIdxList[fileIdx].atEOF = isEOF
            if isEOF :
              stdioSerGate.toPulpQue.extend(bytearray([0xFF]))
              return 1
            if isStrDelimiter(c) :	 # white char
              if (str_on) :
                 # str_buf += c
                 str_buf += ""   # close the string               
                 break
            else :           # Non 'white' char.          
              str_on = True
              str_buf += c	
        stdioSerGate.toPulpQue.extend(str_buf.encode())
        return 0

#------------------------------------------------------------------

def fgetln(fileIdx) :

    if (fileIdx>=0) :
     if (fileIdxList[fileIdx].type=='r') :
        isEOF = fileIdxList[fileIdx].atEOF
        if isEOF :       
          stdioSerGate.toPulpQue.extend(bytearray([0xFF]))
          return 1
		                                               
        line  = fileIdxList[fileIdx].file.readline()      
        isEOF = (len(line)==0)
        
        if isEOF :               
          fileIdxList[fileIdx].atEOF = isEOF
          stdioSerGate.toPulpQue.extend(bytearray([0xFF]))            
          stdioSerGate.toPulpQue.extend(bytearray([0])) # ??? Patch to match with simulation extra byte

        else :
          stdioSerGate.toPulpQue.extend(line.encode())                 
          stdioSerGate.toPulpQue.extend(bytearray([0]))
 
        return 0

#---------------------------------------------------------------------------------------

def fgetHexF(fileIdx) :
       
    for line in fileIdxList[fileIdx].file :
         # print ("line=%s ; len_line=%d" % (line,len(line)))
         if (len(line)!=0)  :  
            hexStr = line.split()[0]     
            stdioSerGate.toPulpQue.extend(hexStr[0].encode())
            stdioSerGate.toPulpQue.extend(hexStr[1].encode())
   

    #  TMP Dummy patch for uart_tb simulation compatibility duplicating for some reason last char  
    stdioSerGate.toPulpQue.extend(hexStr[0].encode())
    stdioSerGate.toPulpQue.extend(hexStr[1].encode())
    #  TMP Dummy patch for uart_tb simulation compatibility duplicating for some reason last char  
    
    
    stdioSerGate.toPulpQue.extend(bytearray([0xFF]))   # indicate EOF
    
    return -1

#----------------------------------------------------------------------------------------

def get_appMainAddr(path_str) :

  elfFile = open(path_str+"_loadable_elf.txt",'r') 
  for line in elfFile :
    lineTokens = line.split() ;
    if "<main>:" in lineTokens :
       elfFile.close()
       main_addr_str = lineTokens[lineTokens.index("<main>:")-1]
       main_addr = int(main_addr_str,16)
       print("pyshell: "+path_str+" <main> address = " + main_addr_str)          
       stdioSerGate.toPulpQue.extend(main_addr.to_bytes(4, byteorder='little'))
       break       

  if not (gdbEnabled and gdbAppEnabled) :       
    return 1    
    
  else :  # enter gdb for application
    gdbAppActive = True
    dbgBr = debugBridge()
    serGW.wr_mem_by_uart(0x1a110000,1)    # Enter debug mode    
    return -1 # indicate no returned charterer    
#------------------------------------------------------------------

def exPyshell(strFromSer) :
  global cmdRetVal
  i = 0
  while strFromSer[i]==' ' : # go to first non blank char
     i=i+1
  while strFromSer[i]!=' ' : # go to first blank char
     i=i+1 
  while strFromSer[i]==' ' : # go to first non-blank char
     i=i+1  
  cmdStr = strFromSer[i:]
  fullCmdStr = "global cmdRetVal ; cmdRetVal="+cmdStr
  
  # print ("DBG pyshell cmd: %s" % fullCmdStr)
  
  exec(fullCmdStr)
  if (cmdRetVal!=-1) :
   stdioSerGate.toPulpQue.extend(bytearray([cmdRetVal]))
 
#---------------------------------------------------------------------

def loadh_by_uart(fileName,offset) :
  print("Loading app/image to address space over uart from file %s with offset of %x" % (fileName,offset))
  f = open(fileName,'r')
  for line in f :
    lineTokens = line.split()
    addr = int(lineTokens[0][1:],16) + offset
    data = int(lineTokens[1],16)
    # print("TMP DBG: loading addr=%x , data=%x" %(addr,data))    
    serGW.wr_mem_by_uart(addr,data)
    while (ser.inWaiting()>0) :
       ignoreRetChar = self.serPort.read(1)
      
#---------------------------------------------------------------------

def load_app_by_uart_and_jmp_reset(cmd_line) :


  cmdLineTokens = cmd_line.split()
  app_instr_file_name = cmdLineTokens[1] + "_instr_loadh.txt"
  app_data_file_name  = cmdLineTokens[1] + "_data_loadh.txt"
  serGW.wr_mem_by_uart(0x1a110000,1)    # Enter debug mode
  loadh_by_uart(app_instr_file_name,0) 
  loadh_by_uart(app_data_file_name,0) 
  print("Done Loading app instr and data files")
  while (ser.inWaiting()>0) : # Clear pending UART communication from device
     ignoreChar = ser.read(1)  
  print("Jumping to reset address 0x80")
  serGW.wr_mem_by_uart(0x1a112000,0x80) # Set PC to 80
  serGW.wr_mem_by_uart(0x1a110000,0)    # Exit debug mode
 

#---------------------------------------------------------------------



def load_app_by_uart_and_run(cmd_line) :

  cmdLineTokens = cmd_line.split()
  app_instr_file_name = cmdLineTokens[1] + "_instr_loadh.txt"
  app_data_file_name  = cmdLineTokens[1] + "_data_loadh.txt"
  loadh_by_uart(app_instr_file_name,0) 
  loadh_by_uart(app_data_file_name,0) 
  print("Done Loading app instr and data files")
  
  elfFileName = cmdLineTokens[1]+"_loadable_elf.txt"
  print("Getting main address from %s" % elfFileName)
  elfFile = open(elfFileName,'r') 
  for line in elfFile :
    lineTokens = line.split() ;
    if "<main>:" in lineTokens :
       elfFile.close()
       main_addr_str = lineTokens[lineTokens.index("<main>:")-1]
       main_addr = int(main_addr_str,16)
       break
       
  print("Calling elf detected main address = %x" % main_addr)
  callStr = "c " + hex(main_addr)[2:] 
  stdioSerGate.toPulpQue.extend(callStr.encode())
 
 
#==============================================================================
         
         
# MAIN

gdbEnabled = False
gdbAppEnabled = False
gdbAppActive = False
dbgBr = None   

#global accessFileIdx    
fileIdxList = []
#accessFileIdx = -1 ;

stdioSerGate = serGate()
escSerGate = serGate()

 
if(len(sys.argv) < 2):
    print("Missing Port Argument, please provide one of COM1,COM2,COM3, .... etc.")
    quit() 

ser = configPort(sys.argv[1])

if len(sys.argv)>=3:
 if (sys.argv[2]=="gdb") and not sys.platform.startswith('win') :
   gdbEnabled = True
   print ("GDB Enabled")
   
if len(sys.argv)==4 :
 if (sys.argv[3]=="app") and gdbEnabled :
   gdbAppEnabled = True
   print ("GDB App Enabled")

serGW = sgw.serGateWay(ser,stdioSerGate,escSerGate) 
serGW.activate()
pySh = pyShell()

#-------------------------------------------------------------------------------------

if gdbEnabled :  # Currently Debug Bridge supported only in Linux
    sys.path.insert(0, '../debug_bridge_py_swig')
    import debug_bridge_py_swig as dbr
    import bp_save

    #-------------------------------------------------------------------------------------
    
    class PyCallback(dbr.Python_Callback):      
       def __init__(self):
          dbr.Python_Callback.__init__(self)
          #self.pollingIsActive = False
          #self.pollingCnt = 0
          if not gdbAppActive :
             bpsDoReset = False # don't reset before recovering  
             self.bps = bp_save.bpSave(serGW,bpsDoReset)  
             self.recoverBP()
       
       #-------------------------------------------------------------------------------------
    
       def recoverBP(self) :    
          # print("TMP DBG self.bps.bpValidFlagAddr = %x" % self.bps.bpValidFlagAddr)
          if (self.uart_mem_read(self.bps.bpValidFlagAddr)==self.bps.bpValidFlag) :
             print("Recovering leftover breakpoints original instructions")                    
             numBPs = self.uart_mem_read(self.bps.bpStoreIdxAddr)
             # print("TMP DBG numBPs = %d", numBPs)
             for i in range(numBPs) :
               recoverBPaddr = self.uart_mem_read(self.bps.bpStoreBaseAddr+(i*8))
               recoverBPinstr = self.uart_mem_read(self.bps.bpStoreBaseAddr+(i*8)+4)
               serGW.wr_mem_by_uart(recoverBPaddr,recoverBPinstr)
               print("Recovered BreakPoint orig instr (potentially destroyed by recent run) : addr = %x , instr = %x" % (recoverBPaddr,serGW.rd_mem_by_uart(recoverBPaddr)))
               
             self.bps.resetBPsave() # reset bp flag and index
             
          else :
             print("No Breakpoints to recover")  
                             
      #-------------------------------------------------------------------------------------
               
          
       def uart_mem_write(self, addr, be, wdata):  # call back function
          #print("TMP DBGW uart_mem_write addr %x , data %x" % (addr,wdata)) 
          if (be!=0xF) :
            print ("TMP ERROR, QUITING: Currently supporting only: be==0xF")
            quit()

          else :

             # Handle breakpoints memorizing
             if (wdata==0x00100073) :             
                 bpReplacedInstr = self.uart_mem_read(addr)
                 self.bps.checkBP(addr,bpReplacedInstr)
                 
             serGW.wr_mem_by_uart(addr,wdata)
                                  

    #-------------------------------------------------------------------------------------
      
       def uart_mem_read(self, addr): # call back function
       
              # dilute polling in order not to starve other AXI/APB traffic
              #if (addr==0x1a110000) and self.pollingIsActive :
              #  self.pollingCnt = self.pollingCnt + 1
              #  if self.pollingCnt % 1 != 0 :
              #     pySh.pyShIterate(numItr=1)
              #     serGW.gwIterate(numItr=1)
              #     return 0
              
              # print("TMP DBGR @uart_mem_read addr=%x"% addr)
              rdata = 0        
              serGW.escSerGate.toPulpQue.append(sgw.SU_CMD_RD_WORD)            
              serGW.escSerGate.toPulpQue.append(addr) 
              
              while len(serGW.escSerGate.fromPulpQue)==0 : # iterate serGW till read data is back
                 serGW.gwIterate(numItr=1)   
      
                 
              rdata = serGW.escSerGate.fromPulpQue.pop(0) 
              
              #if not ((addr==0x1a110000) and (rdata==0)) : # avoid polling debug prints        
              #   print("TMP DBGR uart_mem_read  addr %x , data %x" % (addr,rdata))   
              if ((addr==0x1a110000) and (rdata==0)) :  # activate other threads while gdb bridge is polling
                 #self.pollingIsActive = True
                 pySh.pyShIterate(numItr=1)
                 serGW.gwIterate(numItr=1)
              # else :
                 #self.pollingIsActive = False
              # print("TMP DBGR DONE! @uart_mem_read addr=%x"% addr)             
              return rdata
 
   #-------------------------------------------------------------------------------------

    class debugBridge() :
      def __init__(self): 
         dut_portNumber = 1234
         gdb_portNumber = 5678  
         
         print ("Creating Bridge")     
         self.active = False   
         
         print ("Creating UART Interface")
         mem = dbr.uartIF() ;
    
         print ("Binding Callback to python")
         
         self.callback = PyCallback().__disown__()
         mem.setCallback(self.callback)
         
         self.bridge = dbr.Bridge(dbr.unknown, mem, dut_portNumber, gdb_portNumber);
                   
         # self.bridge.log_print_enable() # TMP LOG ENABLED
         
         # main loop      # replacing bridge.mainLoop() to avoid threading issues ;
         print ("Setting up gdb connection")
         self.bridge.setup_gdb_connection()
         
         self.active = True
         
      #------------------------------------------------------------------         
      def dbgBrIterate(self,numItr) :
  
       if not(self.active) :
         return

       gdbPacketOk = True
     
       for i in range (numItr) :
     
          gdbPacketOk = self.bridge.get_and_decode_gdb_packet()
       
          if (not gdbPacketOk) :
            print ("GDB Packet access/decode fail, Quitting") 
            return False
            
       return True
         
#==============================================================================
         

# print("Please Reset Board to Connect and boot menu") # Not needed any more

# clear leftover communication from device
while (ser.inWaiting()>0) :
   ignoreChar = ser.read(1)

if gdbEnabled and not gdbAppEnabled :
     dbgBr = debugBridge()
     serGW.wr_mem_by_uart(0x1a110000,1)    # Enter debug mode 
     
else :  # Force PC to jump to reset address
  # print("TMP DBG *0x43c = %x" % rd_mem_by_uart(0x43c))
  print("Jumping to reset address 0x80")
  serGW.wr_mem_by_uart(0x1a110000,1)    # Enter debug mode
  serGW.wr_mem_by_uart(0x1a112000,0x80) # Set PC to 80
  serGW.wr_mem_by_uart(0x1a110000,0)    # Exit debug mode
  
 
#---------------------------------------------------------------------------------------

# Pseudo Multi Threading
#loop_cnt=0
dbgBrIterateOK = True
dbgBrReEstablshCnt = 0
dbgBrReEstablshLimit = 5

while (True) :
   pySh.pyShIterate(1)
   serGW.gwIterate(numItr=1) 
   if (gdbEnabled and not gdbAppEnabled) or gdbAppActive :  # Currently Debug Bridge supported only in Linux 
      dbgBrIterateOK = dbgBr.dbgBrIterate(1)
      if not dbgBrIterateOK :
          print ("pulp_pydhell.py Message: Debug bridge iteration fail , attempting to re-establish connection")
          dbgBr.bridge.close_gdb_connection()
          dbgBr.bridge.setup_gdb_connection()
          dbgBrReEstablshCnt = dbgBrReEstablshCnt+1
          if dbgBrReEstablshCnt==dbgBrReEstablshLimit :
            print ("Quitting after %d re-establish connection attempts" % dbgBrReEstablshCnt)
   # loop_cnt=loop_cnt+1
   #print("TMP DBG: loop_cnt = %d" % loop_cnt)
      
      
   
 
#==============================================================================
      
   

