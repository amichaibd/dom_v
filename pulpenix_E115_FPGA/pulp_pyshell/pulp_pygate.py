
import time
import serial
import sys
import serial_gateway as sgw
import bp_save
import time
import zmq

#================================================================================

class serGate () :

   def __init__(self):   
      self.fromPulpQue = [] 
      self.toPulpQue = []    

#---------------------------------------------------------------------------------

def configPort(portName) :

   # configure the serial connections (the parameters differs on the device you are connecting to)
   ser = serial.Serial()
   ser.port=portName
   ser.baudrate=115200
   ser.parity=serial.PARITY_NONE
   ser.stopbits=serial.STOPBITS_ONE
   ser.bytesize = serial.EIGHTBITS #number of bits per bytes
   ser.xonxoff = False             #disable software flow control
   ser.rtscts = False              #disable hardware (RTS/CTS) flow control
   ser.dsrdtr = False              #disable hardware (DSR/DTR) flow control
   ser.timeout = 1                 #non-block read    
   ser.writeTimeout = 1            #timeout for write  
   return ser   

#----------------------------------------------------------

class fileRef :
    def __init__(self):
      self.file = None
      self.type = None
      self.atEOF = False

#----------------------------------------------------------

class pyShell :
      def __init__(self):
      
        self.firstReady = True # used to indicate pyshell
        self.masterReadAccessOn = False
        self.masterWriteAccessOn = False
        self.startDebugBridge = False
        self.masterWriteAccessOn = False
        self.strFromSer = ""
        # self.dbrOn = False
        self.serReady = False
        self.masterAccessOn = False
        self.newLine = False
        self.serPyshellCmdOn = False
        self.serRead = ''
        self.accessFileIdx = -1
        self.serPyshellCmdIdx = 0 
        self.masterAccessActive = False
      
      def pyShIterate(self,numItr) :

        for i in range(numItr) : # Iterate mumItr times
                                    
           # Parse serial from Pulp
           
           if not self.serReady :
                
               if not (self.newLine or self.serReady) :
                         
                   len_stdioSerGate_fromPulpQue =  len(stdioSerGate.fromPulpQue)
                   if len_stdioSerGate_fromPulpQue == 0 :
                      continue
                   else:
                      for i in range(len_stdioSerGate_fromPulpQue) : 
                         self.serRead = stdioSerGate.fromPulpQue.pop(0)                                                
                         if ord(self.serRead) <=127 :  # ignore escapes
                           c = self.serRead.decode('ascii')               
                           self.strFromSer += c  
                           self.newLine = (c=='\n')
                           if self.newLine :
                             break
                           self.serReady = (self.strFromSer=="Ready> ")
                           self.serPyshellCmdIdx = (self.strFromSer.find("$pyshell"))
                           self.serPyshellCmdOn = self.serPyshellCmdIdx>-1 
                         else :
                           print("WARNING: found non ascii stdioSerGate.fromPulpQue.pop(0) = %x" , ord(self.serRead))
                         
               else :  
                   if (self.masterAccessActive and self.newLine) : # return control to menu upon master access response return                
                         stdioSerGate.toPulpQue.extend("\n".encode())
                         self.masterAccessActive = False                                  
               
                   if self.serPyshellCmdOn :                   
                        if  (self.serPyshellCmdIdx>0) : 
                          self.pyShellCmdStr = self.strFromSer[self.serPyshellCmdIdx:]              
                          self.strFromSer =  self.strFromSer[:self.serPyshellCmdIdx] 
                        else :
                          self.pyShellCmdStr = self.strFromSer                       
                          
                   if (not  self.serPyshellCmdOn) or (self.serPyshellCmdOn and (self.serPyshellCmdIdx>0)) :
                   
                       if (self.accessFileIdx<0) : 
                         print("%s" % self.strFromSer ,end='') 
                       else :             
                         fileIdxList[self.accessFileIdx].file.write("%s" %(self.strFromSer))
                                
                   if self.serPyshellCmdOn :
                       exPyshell(self.pyShellCmdStr)
                       self.serPyshellCmdOn = False
                                                     
                   self.newLine = False
                   self.serPyshellCmdOn = False
                   self.serRead = ''   
                   self.strFromSer=""
                                   
           else : # @serReady ("Ready> " detected) get keyboard menu responses input to be sent over serial
             if not (self.masterAccessActive  and self.serReady) :         
                print("Ready> ",end='')
             
             if self.firstReady :
                stdioSerGate.toPulpQue.extend(("$p\n").encode()) # agreed to indicate python shell mode to DUT                
                self.firstReady = False  
             else :  
                self.serPyshellCmd = False       
                self.strToSer = input("") 
                if self.strToSer.find("#q") >= 0 :
                  print("#q detected, QUITTING") 
                  if gdbEnabled :
                    dbgBr.callback.recoverBP()
                  exit()
                 
                self.masterReadAccessOn = self.strToSer.find("#r") >= 0
                self.masterWriteAccessOn = self.strToSer.find("#w") >= 0
                
                if (self.masterReadAccessOn or self.masterWriteAccessOn) :
                  self.masterAccessActive = True    
 
                if  self.strToSer.find("#a0") >= 0 :  # load image starting address 0 , data=0x00101000 and jump to reset entry 0x80
                  load_app_by_uart_and_jmp_reset(self.strToSer) 
                  
                elif  self.strToSer.find("#a") >= 0 : # load app starting address instr=4000 , data=0x00101000
                                    
                  load_app_by_uart_and_run(self.strToSer)
                  self.strToSer = ''
           
                stdioSerGate.toPulpQue.extend((self.strToSer + '\n').encode())

             self.serReady = False
              
# --------------------------------------------------------------------------------

def quitApp() :
 print("\npyshell: Quitting Application upon quitApp() message received over UART\n") 
 quit()

# --------------------------------------------------------------------------------

def openFile(file_name,type) :
  f = open(file_name,type)
  fileEntry = fileRef()
  fileEntry.file = f
  fileEntry.type = type
  fileEntry.atEOF = False   
  fileIdxList.append(fileEntry)
  fileIdx = len(fileIdxList)-1
  print("pyshell: Opening file %s of type %c , file Index is %d\n" % (file_name,type,fileIdx))
  return (fileIdx)

#--------------------------------------------------------

def closeFile(fileIdx) :
  fileName = fileIdxList[fileIdx].file.name
  fileIdxList[fileIdx].file.close()
  print("pyshell: Closed file of idx = %d , %s" % (fileIdx,fileName))
  return (1)


#--------------------------------------------------------

def accessFile(fileIdx) :
  # global accessFileIdx  
  pySh.accessFileIdx = fileIdx ;
  return 1

#--------------------------------------------------------
def isStrDelimiter(c) :
        # cval = ord(c)
        # return (((cval>=0x09) and (cval<=0x0D)) or (cval==0x20))
        return (ord(c) <= 0x20) or (ord(c)==127) # any thing other than visible charterers
        
#--------------------------------------------------------

def fgets(fileIdx) :
    # Check if an active program is accessing a file
    if (fileIdx>=0) :
     if (fileIdxList[fileIdx].type=='r') :
        str_on = False
        si = 0
        str_buf = "" # prevent uninitialized debug prints
        isEOF = fileIdxList[fileIdx].atEOF
        if isEOF :
          stdioSerGate.toPulpQue.extend(bytearray([0xFF]))
          return 1
		        
        while not (isEOF) : 
            c =  fileIdxList[fileIdx].file.read(1)             
            isEOF = (c=='')
            fileIdxList[fileIdx].atEOF = isEOF
            if isEOF :
              stdioSerGate.toPulpQue.extend(bytearray([0xFF]))
              return 1
            if isStrDelimiter(c) :	 # white char
              if (str_on) :
                 str_buf += c            
                 break
            else :           # Non 'white' char.          
              str_on = True
              str_buf += c	
        stdioSerGate.toPulpQue.extend(str_buf.encode())
        return 1

#------------------------------------------------------------------

def exPyshell(strFromSer) :
  global cmdRetVal
  i = 0
  while strFromSer[i]==' ' : # go to first non blank char
     i=i+1
  while strFromSer[i]!=' ' : # go to first blank char
     i=i+1 
  while strFromSer[i]==' ' : # go to first non-blank char
     i=i+1  
  cmdStr = strFromSer[i:]
  fullCmdStr = "global cmdRetVal ; cmdRetVal="+cmdStr
  exec(fullCmdStr)
  if (cmdRetVal!=-1) :
   stdioSerGate.toPulpQue.extend(bytearray([cmdRetVal]))
 
#---------------------------------------------------------------------
            
# MAIN

gdbEnabled = False
gdbAppEnabled = False
gdbAppActive = False
dbgBr = None   

#global accessFileIdx    
fileIdxList = []
#accessFileIdx = -1 ;

stdioSerGate = serGate()
escSerGate = serGate()

if(len(sys.argv) < 2):
    print("Missing Port Argument, please provide one of COM1,COM2,COM3, .... etc.")
    quit() 

ser = configPort(sys.argv[1])

if len(sys.argv)>=3:
 if (sys.argv[2]=="gdb") and not sys.platform.startswith('win') :
   gdbEnabled = True
   print ("GDB Enabled")
   
if len(sys.argv)==4 :
 if (sys.argv[3]=="app") and gdbEnabled :
   gdbAppEnabled = True
   print ("GDB App Enabled")

# setup ZMQ socket access to remote python
zmq_context = zmq.Context()
zmq_socket = zmq_context.socket(zmq.REP)
zmq_socket.bind("tcp://*:5555")

serGW = sgw.serGateWay(ser,stdioSerGate,escSerGate) 
serGW.activate()
pySh = pyShell()

#-------------------------------------------------------------------------------------

if gdbEnabled :  # Currently Debug Bridge supported only in Linux
    sys.path.insert(0, '../debug_bridge_py_swig')
    import debug_bridge_py_swig as dbr
    import bp_save

    #-------------------------------------------------------------------------------------
    
    class PyCallback(dbr.Python_Callback):      
       def __init__(self):
          dbr.Python_Callback.__init__(self)
          if not gdbAppActive :
             bpsDoReset = False # don't reset before recovering  
             self.bps = bp_save.bpSave(serGW,bpsDoReset)  
             self.recoverBP()
       
       #-------------------------------------------------------------------------------------
    
       def recoverBP(self) :    
          # print("TMP DBG self.bps.bpValidFlagAddr = %x" % self.bps.bpValidFlagAddr)
          if (self.uart_mem_read(self.bps.bpValidFlagAddr)==self.bps.bpValidFlag) :
             print("Recovering leftover breakpoints original instructions")                    
             numBPs = self.uart_mem_read(self.bps.bpStoreIdxAddr)
             # print("TMP DBG numBPs = %d", numBPs)
             for i in range(numBPs) :
               recoverBPaddr = self.uart_mem_read(self.bps.bpStoreBaseAddr+(i*8))
               recoverBPinstr = self.uart_mem_read(self.bps.bpStoreBaseAddr+(i*8)+4)
               serGW.wr_mem_by_uart(recoverBPaddr,recoverBPinstr)
               print("Recovered BreakPoint orig instr (potentially destroyed by recent run) : addr = %x , instr = %x" % (recoverBPaddr,serGW.rd_mem_by_uart(recoverBPaddr)))
               
             self.bps.resetBPsave() # reset bp flag and index
             
          else :
             print("No Breakpoints to recover")  
                             
      #-------------------------------------------------------------------------------------
                         
       def uart_mem_write(self, addr, be, wdata):  # call back function
          if (be!=0xF) :
            print ("TMP ERROR, QUITING: Currently supporting only: be==0xF")
            quit()

          else :

             # Handle breakpoints memorizing
             if (wdata==0x00100073) :             
                 bpReplacedInstr = self.uart_mem_read(addr)
                 self.bps.checkBP(addr,bpReplacedInstr)
                 
             serGW.wr_mem_by_uart(addr,wdata)
                                  
    #-------------------------------------------------------------------------------------
      
       def uart_mem_read(self, addr): # call back function
       
              rdata = 0        
              serGW.escSerGate.toPulpQue.append(sgw.SU_CMD_RD_WORD)            
              serGW.escSerGate.toPulpQue.append(addr) 
              
              while len(serGW.escSerGate.fromPulpQue)==0 : # iterate serGW till read data is back
                 serGW.gwIterate(numItr=1)   
      
                 
              rdata = serGW.escSerGate.fromPulpQue.pop(0) 
              
              if ((addr==0x1a110000) and (rdata==0)) :  # activate other threads while gdb bridge is polling
                 #self.pollingIsActive = True
                 pySh.pyShIterate(numItr=1)
                 serGW.gwIterate(numItr=1)

              return rdata
 
   #-------------------------------------------------------------------------------------

    class debugBridge() :
      def __init__(self): 
         dut_portNumber = 1234
         gdb_portNumber = 5678  
         
         print ("Creating Bridge")     
         self.active = False   
         
         print ("Creating UART Interface")
         mem = dbr.uartIF() ;
    
         print ("Binding Callback to python")
         
         self.callback = PyCallback().__disown__()
         mem.setCallback(self.callback)
         
         self.bridge = dbr.Bridge(dbr.unknown, mem, dut_portNumber, gdb_portNumber);
                   
         print ("Setting up gdb connection")
         self.bridge.setup_gdb_connection()
         
         self.active = True
         
      #------------------------------------------------------------------         
      def dbgBrIterate(self,numItr) :
  
       if not(self.active) :
         return

       gdbPacketOk = True
     
       for i in range (numItr) :
     
          gdbPacketOk = self.bridge.get_and_decode_gdb_packet()
       
          if (not gdbPacketOk) :
            print ("GDB Packet access/decode fail, Quitting") 
            return False
            
       return True
         
#==============================================================================

# clear leftover communication from device
while (ser.inWaiting()>0) :
   ignoreChar = ser.read(1)

if gdbEnabled and not gdbAppEnabled :
     dbgBr = debugBridge()
     serGW.wr_mem_by_uart(0x1a110000,1)    # Enter debug mode 
     
else :  # Force PC to jump to reset address

  print("Jumping to reset address 0x80")
  serGW.wr_mem_by_uart(0x1a110000,1)    # Enter debug mode
  serGW.wr_mem_by_uart(0x1a112000,0x80) # Set PC to 80
  serGW.wr_mem_by_uart(0x1a110000,0)    # Exit debug mode
   
#---------------------------------------------------------------------------------------

dbgBrIterateOK = True
dbgBrReEstablshCnt = 0
dbgBrReEstablshLimit = 5

while (True) :
   pySh.pyShIterate(1)
   serGW.gwIterate(numItr=1) 
   if (gdbEnabled and not gdbAppEnabled) or gdbAppActive :  # Currently Debug Bridge supported only in Linux 
      dbgBrIterateOK = dbgBr.dbgBrIterate(1)
      if not dbgBrIterateOK :
          print ("pulp_pydhell.py Message: Debug bridge iteration fail , attempting to re-establish connection")
          dbgBr.bridge.close_gdb_connection()
          dbgBr.bridge.setup_gdb_connection()
          dbgBrReEstablshCnt = dbgBrReEstablshCnt+1
          if dbgBrReEstablshCnt==dbgBrReEstablshLimit :
            print ("Quitting after %d re-establish connection attempts" % dbgBrReEstablshCnt)

   # Check remote python messages
   try:
        #check for a message, this will not block   
        message = zmq_socket.recv(flags=zmq.NOBLOCK) 

   except zmq.Again as e:
        continue
        
   # print("Received request: %s" % str(message)) # --- uncomment for debug only
                        
   if (message[0]==ord('R')) :  # READ access
     addr = 0          
     for b in message[1:5] :
       addr = addr*256 + b         
     # print("Received request to read from addr: %04x" % addr) # --- uncomment for debug only
     
     zmqRetData = serGW.rd_mem_by_uart(addr)
               
     retByteList = [None] * 4
     
     retByteList[0] = ( zmqRetData >> 24 ) & 0xff;
     retByteList[1] = ( zmqRetData >> 16 ) & 0xff;
     retByteList[2] = ( zmqRetData >> 8  ) & 0xff;
     retByteList[3] =   zmqRetData         & 0xff; 
     
     zmq_socket.send(bytearray(retByteList))   #  Send reply back to client
     
   elif (message[0]==ord('W')) :  # WRITE access
     addr = 0          
     for b in message[1:5] :
       addr = addr*256 + b         
     print("Received request to write to addr: %04x" % addr) # --- uncomment for debug only

     data = 0          
     for b in message[5:9] :
       data = data*256 + b         
     print("Received request to write data: %04x" % data) # --- uncomment for debug only
     
     serGW.wr_mem_by_uart(addr,data)
                            
     zmq_socket.send(bytearray([0]))   #  Send dummy reply back to client 

#==============================================================================
 