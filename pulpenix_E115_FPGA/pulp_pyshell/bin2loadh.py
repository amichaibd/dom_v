import os
import sys


# **********************************************************************
# MAIN
# **********************************************************************
if "__main__" == __name__:
    if(len(sys.argv) < 3):
        print("Usage %s input_filename, destination start address." % (sys.argv[0]))
        quit()
    input_filename = sys.argv[1]
    start_address = sys.argv[2]
    fnameBase = os.path.splitext(input_filename)[0]
    output_filename = os.path.join('', fnameBase+"_instr.txt")
    with open(input_filename, "rb") as file:
        with open(output_filename, 'w') as p:
            filesize = os.path.getsize(input_filename)
            if filesize == 0:
                print("File %s is null size" % (input_filename))
                quit()
            address = int(start_address)
            while filesize > 0:
                byte = file.read(4)
                if byte:
                    if filesize >= 4:
                        out = "@0000" + "%04X %02X%02X%02X%02X" % (address, byte[3], byte[2], byte[1], byte[0])
                        filesize -= 4
                    else:
                        out = "@0000" + "%04X " % address
                        for i in range(4):
                            try:
                                out += "%02X" % byte[i]
                            except:
                                out += "00"
                        filesize = 0
                    out += '\n'
                    address += 4
                    p.write(out)
