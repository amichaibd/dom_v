
import time
import serial
import sys
import os
import serial_gateway as sgw
import bp_save

#================================================================================
 
 

def loadh_by_uart(fileName,offset,ser_gw) :
  print("Loading app/image to address space over uart from file %s with offset of %x" % (fileName,offset))
  f = open(fileName,'r')
  for line in f :
    lineTokens = line.split()
    addr = int(lineTokens[0][1:],16) + offset
    data = int(lineTokens[1],16)
    # print("TMP DBG: loading addr=%x , data=%x" %(addr,data))    
    ser_gw.wr_mem_by_uart(addr,data)
    while (ser.inWaiting()>0) :
       ignoreRetChar = ser_gw.serPort.read(1)
      
#================================================================================

      
# MAIN

if(len(sys.argv) < 2):
    print("Missing Port Argument, please provide one of COM1,COM2,COM3, .... etc.")
    quit() 
ser = sgw.configPort(sys.argv[1])

if(len(sys.argv) < 3):
    print("Missing path to app")
    quit() 
appPath  = sys.argv[2]

# print ("TMP DBG len(sys.argv) = %d" % len(sys.argv))

if(len(sys.argv) >= 4):
   instr_offset  = int(sys.argv[3],16)
   print ("App Instruction code pulpenix addres space Offset = %x" % instr_offset)
else :
   instr_offset  = 0

if(len(sys.argv) == 5):
   data_offset  = int(sys.argv[4],16)
   print ("App Data code pulpenix addres space Offset = %x" % data_offset)
else :
    data_offset = 0


stdioSerGate = sgw.serGate()
escSerGate = sgw.serGate()
serGW = sgw.serGateWay(ser,stdioSerGate,escSerGate)  

bpsDoReset = True
bps = bp_save.bpSave(serGW,bpsDoReset)

serGW.activate()

app_instr_file_name = appPath + "_instr_loadh.txt"
app_data_file_name  = appPath + "_data_loadh.txt"
serGW.wr_mem_by_uart(0x1a110000,1)    # Enter debug mode
loadh_by_uart(app_instr_file_name,instr_offset,serGW) 
loadh_by_uart(app_data_file_name,data_offset,serGW) 

serGW.gwIterate(numItr=1)

while (ser.inWaiting()>0) : # Clear pending UART communication from device
   ignoreChar = ser.read(1) 

bps.resetBPsave()

print("Done Loading app instr and data files")   

is_windows = sys.platform.startswith('win')
if is_windows :
  copy_cmd_str = "copy"
else :
  copy_cmd_str = "cp"



# Copy application elf for eclipse/gdb local use

  os.system("%s %s app.elf\n" % (copy_cmd_str , appPath+".elf"))

if is_windows :
   print("")
   print("NOTICE GDB/ECLIPSE debug is NOT available in Windows")
   print("All other pyShell features are available both in Windows and in Linux")
else : 
   # generate local initial cmd files foreclipse
   os.system("echo file $MY_PULP_ENV/pulp_pyshell/app.elf > eclipse_cmd.txt")
   os.system("echo target remote :5678                   >> eclipse_cmd.txt")
   os.system("echo set remotetimeout 10                  >> eclipse_cmd.txt")
   os.system("echo set tcp connect-timeout 10            >> eclipse_cmd.txt")
   os.system("echo set \$pc=0x80                          >> eclipse_cmd.txt")           
   
   # generate local initial cmd files for gdb
   os.system(copy_cmd_str + " eclipse_cmd.txt gdb_cmd.txt\n")
   os.system("echo b main >> gdb_cmd.txt")
   os.system("echo c      >> gdb_cmd.txt") 

print("")
print("Done setting up environment") 
print("")

quit()




 