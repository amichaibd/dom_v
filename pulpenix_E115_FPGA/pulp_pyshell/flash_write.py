import os
import time
import serial
import sys
from optparse import OptionParser

options = []
parser = OptionParser()
parser.add_option("-w", "--write",  help="Execute write cickle",    action="store_true",    dest="write_cickle")
parser.add_option("-e", "--erase",  help="Execute erase cickle",    action="store_true",    dest="erase_cickle")
parser.add_option("-v", "--verify", help="Execute verify cickle",    action="store_true",    dest="verify_cickle")
parser.add_option("-d", "--debug",  help="Enable debug messages",   action="store_true",    dest="debug_mode")
parser.add_option("-f", "--file",   help="File name",           type="string",          dest="file")
parser.add_option("-s", "--serial", help="Serial device name",  type="string",          dest="serial_device")

TIMEOUT_MS = 100

def INFO(string):
    print("INFO: " + string)

def DEBUG(string):
    if options.debug_mode:
        print("DEBUG: " + string)

def WARN(string):
    if options.debug_mode:
        print("WARNING: " + string)

def ERROR(string):
    print("ERROR: " + string)

def configPort(portName) :
    # configure the serial connections (the parameters differs on the device you are connecting to)
    ser = serial.Serial()
    ser.port=portName
    ser.baudrate=115200
    ser.parity=serial.PARITY_NONE
    ser.stopbits=serial.STOPBITS_ONE
    ser.bytesize = serial.EIGHTBITS #number of bits per bytes
    ser.xonxoff = False             #disable software flow control
    ser.rtscts = False              #disable hardware (RTS/CTS) flow control
    ser.dsrdtr = False              #disable hardware (DSR/DTR) flow control
    ser.timeout = 0                 #non-block read
    ser.writeTimeout = 2            #timeout for write
    return ser
# ----------------------------------------------------------

def waitReady() :
   serRead = ''
   strFromSer = ''
   timeout = TIMEOUT_MS
   while timeout > 0:
       serRead = ser.read(1)
       if serRead != b'' and ord(serRead) <= 127:
           c = serRead.decode('ascii')
           if c == '\r' or c == '\n':
               strFromSer = ''
               continue
           strFromSer += c
           if "Ready> " in strFromSer:
               DEBUG("recv: " + strFromSer)
               DEBUG("Wait time: %d ms" % (TIMEOUT_MS - timeout))
               return 1
       time.sleep(.001)
       timeout -= 1
   return 0

def waitAnswer(needToWaitReady) :
    serRead = ''
    strFromSer = ''
    timeout = TIMEOUT_MS
    if needToWaitReady == False or waitReady() == 1:
        while timeout > 0:
            serRead = ser.read(1)
            if serRead != b'' and ord(serRead) <= 127:
                c = serRead.decode('ascii')
                if "mem " in strFromSer:
                    if c == '\r' or c == '\n':
                        DEBUG("recv: " + strFromSer)
                        DEBUG("Wait time: %d ms" % (TIMEOUT_MS - timeout))
                        lst = strFromSer.split()
                        if len(lst) > 3:
                            return lst[3][2:]
                elif c == '\r' or c == '\n':
                    strFromSer = ''
                    continue
                strFromSer += c
            time.sleep(.001)
            timeout -= 1
    WARN("Timeout")
    return 'Timeout'


def waitReadiness() :
    timeout = TIMEOUT_MS
    if waitReady() == 1:
        while timeout > 0:
            DEBUG("r 1a280000")
            ser.write(("r 1a280000\r\n").encode())
            str_answer = waitAnswer(True)
            if str_answer == 'Timeout':
                break
            answer = int(str_answer, 16)
            DEBUG("Answer: %X" %  answer)
            mask = 0x80000000
            if mask & answer == 0:
                DEBUG("Wait time: %d ms" % (TIMEOUT_MS - timeout))
                return 1
            time.sleep(.001)
            timeout -= 1
    WARN("Timeout")
    return 0

def enableWrite() :
    timeout = TIMEOUT_MS
    attempts = 3
    while timeout > 0:
        DEBUG("w 1a280000 10000000")
        ser.write(("w 1a280000 10000000\r\n").encode())
        if waitReady() == 0:
            if attempts > 1:
                attempts -= 1
                continue
            else:
                ERROR("Impossible to enable write mode, exceeded attempts to: w 1a280000 10000000")
                break
        DEBUG("r 1a280000")
        ser.write(("r 1a280000\r\n").encode())
        str_answer = waitAnswer(True)
        if str_answer == 'Timeout':
            if attempts > 1:
                attempts -= 1
                continue
            else:
                ERROR("Impossible to enable write mode, exceeded attempts to: r 1a280000")
                break
        answer = int(str_answer, 16)
        mask = 0x10000000
        if mask & answer == mask:
            DEBUG("Wait time: %d ms" % (TIMEOUT_MS - timeout))
            return 1
        time.sleep(.001)
        timeout -= 1
    WARN("Timeout")
    return 0

def disableWrite() :
    DEBUG("w 1a280000 00000000")
    ser.write(("w 1a280000 00000000\r\n").encode())
    waitReady()


# "Ready> "
# mem 0x00000000 = 0x00000013
# mem 0x1A280000 = 0x10000000
# mem 0x00000000 = 0xC0000000
# --------------------------------------------------------

# --- Erase -----------------------------------------------------------
def ProduceErase(filename):
    print("\n======== Erase ===")
    error = 0
    with open(filename, 'r') as file:
        if os.path.getsize(filename) == 0:
            ERROR("Input file have null size")
            error = 1
        elif enableWrite() == 0:
            error = 1
        else:
            filedata = file.readlines()
            address = 0xFFFF
            string = ''
            amount = 0
            for i in filedata:
                if i[0] == '@':
                    if string == '':
                        address = int(i[5:9], 16)
                    amount = amount + 4
            DEBUG("last address detected = %X" %  address)        
            if address != 0xFFFF and amount > 0:
                attempts = 3
                address = address - address % (8 * 1024)
                DEBUG("erase address = address - address mod (8 * 1024) = %X" %  address)
                while amount > 0:
                    attempts -= 1
                    string = "w 1a280000 8000%04X\r\n" % address
                    DEBUG(string[:-2])
                    ser.write(string.encode())
                    if waitReadiness() == 0:
                        if attempts > 0:
                            continue
                        else:
                            ERROR("Impossible to erase flash, exceeded attempts to: %s " % string)
                            error = 1
                            break
                    INFO("Flash sector at address %04X erased success." % address)
                    address = address + (8 * 1024)
                    if amount % (8 * 1024) > 0:
                        amount = amount - amount % (8 * 1024)
                    else:
                        amount = amount - (8 * 1024)
            disableWrite()
        file.close()

    if error == 1:
        ERROR("Erase failed")
    else:
        INFO("Erase completed successfully")

# --- Write -----------------------------------------------------------
def ProduceWrite(filename):
    print("\n======== Write ===")
    error = 0
    with open(filename, 'r') as file:
        if os.path.getsize(filename) == 0:
            ERROR("Input file have null size")
            error = 1
        elif enableWrite() == 0:
            error = 1
        else:
            filedata = file.readlines()
            for i in filedata:
                if i[0] == '@':
                    attempts = 3
                    while attempts > 0:
                        attempts -= 1
                        string = "w 1A20" + i[5:]
                        DEBUG(string[:-1])
                        ser.write(string.encode())
                        if waitReady() == 0:
#                        if waitReadiness() == 0:
                            if attempts > 0:
                                continue
                            else:
                                ERROR("Impossible to write data to flash")
                                error = 1
                                break
                        break
                    if error == 1:
                        break
                    print("_", end='', flush=True)
                    DEBUG("OK")
            disableWrite()
        if error == 0:
            print("\n")
        file.close()

    if error == 1:
        ERROR("Write failed")
    else:
        INFO("Write completed successfully")

# --- Verify -----------------------------------------------------------
# >> d 1a200000 10
# d 1a200000 10
#
# mem 0x1a200000 = 0x0000008c
# mem 0x1a200004 = 0x00000013
# mem 0x1a200008 = 0x00000013
# mem 0x1a20000c = 0x00000013
# Ready>
def ProduceVerify(filename):
    print("\n======== Verify ===")
    error = 0
    with open(filename, 'r') as file:
        if os.path.getsize(filename) == 0:
            ERROR("Input file have null size")
            error = 1
        else:
            filedata = file.readlines()
            attempts = 3
            while attempts > 0:
                attempts -= 1
                start_address = 0xFFFF
                amount = 0
                for i in filedata:
                    if i[0] == '@':
                        if start_address == 0xFFFF:
                            start_address = int(i[5:9], 16)
                        amount = amount + 4

                if start_address == 0xFFFF or amount == 0:
                    INFO("Nothing to verify.")
                    break
                string = "d 1A20%04X %X\r\n" % (start_address, amount)
                DEBUG(string[:-2])
                ser.write(string.encode())

                for i in filedata:
                    if i[0] == '@':
                        lst = i.split()
                        if len(lst) != 2:
                            continue
                        addr = lst[0][5:]
                        data = lst[1]
                        str_answer = waitAnswer(False)
#                        str_answer = data.upper()
                        if str_answer == 'Timeout':
                            error = 1
                            if attempts > 0:
                                break
                            else:
                                ERROR("Impossible to read data for verify at address %s" % addr)
                                break
                        if str_answer.upper() != data.upper():
                            error = 1
                            ERROR("[1A20%s] chip: %s; file: %s" % (addr, str_answer, data))
                            break
                        print(".", end='', flush=True)
                        DEBUG("OK")
                waitReady()
                if error == 0:
                    print("\n")
                    break
        file.close()
    if error == 1:
        ERROR("Verify failed")
    else:
        INFO("Verify completed successfully")

# **********************************************************************
# MAIN
# **********************************************************************
if "__main__" == __name__:
    (options, args) = parser.parse_args()
    if options.serial_device == None:
        ERROR("Specify device name, please provide one of COM1..n for Windows or /dev/ttyUSB0..n for Linux.")
        exit()

    filename = options.file
    ser = configPort(options.serial_device)
    try:
        ser.open()
    except IOError:
        ERROR("%s does not appear to exist." % options.serial_device)
        exit()
    else:
        print("\n**********************\nflash_write.py started\n**********************\n")
        INFO("Opened %s" % options.serial_device)

        ser.write(("\r\n").encode())
        waitReady()

        if options.erase_cickle:
            ProduceErase(filename)
        if options.write_cickle:
            ProduceWrite(filename)
        if options.verify_cickle:
            ProduceVerify(filename)
    finally:
        ser.close()


