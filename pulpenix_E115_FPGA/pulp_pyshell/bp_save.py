

class bpSave :
    
   def __init__(self,ser_gw,doReset):
   
      self.bpDict = {}              
      self.instrMemAddrLimit = 0x8000
      self.muxNumBPsave = 20
      self.bpStoreIdx = 0          
      self.bpStoreBaseAddr = self.instrMemAddrLimit - (8*self.muxNumBPsave)
      self.bpStoreIdxAddr = self.bpStoreBaseAddr - 4
      self.bpValidFlagAddr = self.bpStoreIdxAddr - 4
      self.bpValidFlag = 0xABCDEF11  
      self.sgw = ser_gw
      if doReset : 
         self.resetBPsave()
      else :
         print ("NOTICE: Top part of Instr memory starting at address %x is used by pyShell in Debug mode to Store breakpoints information" %self.bpValidFlagAddr)
         print ("Make Sure no code reside there !!!")
      
   def checkBP(self,addr,bpReplacedInstr) :
      if not (addr in self.bpDict.keys()) :        
           storeBPaddr = self.bpStoreBaseAddr+(8*self.bpStoreIdx)              
           self.sgw.wr_mem_by_uart(storeBPaddr,addr)       
           self.sgw.wr_mem_by_uart(storeBPaddr+4,bpReplacedInstr)                 
           self.bpDict[addr] = bpReplacedInstr
           # print("TMP DBG: pyShell memorizing BP: EBREAK CODE=0x00100073 written to address %x replacing instr %x" %(addr,bpReplacedInstr))                              
           if self.bpStoreIdx==0 : # is First 
             self.sgw.wr_mem_by_uart(self.bpValidFlagAddr,self.bpValidFlag) 
           self.bpStoreIdx += 1
           self.sgw.wr_mem_by_uart(self.bpStoreIdxAddr,self.bpStoreIdx)                                 

   def resetBPsave(self) :
       self.sgw.wr_mem_by_uart(self.bpStoreIdxAddr,0) 
       self.sgw.wr_mem_by_uart(self.bpValidFlagAddr,0)            
                       
