

Adding Support for file Interface from the C program under the Pulpenix environment.
====================================================================================

You may find this capability very useful, as it enables modifying the tested input data without the need to recompile your program
as well as print outputs either to a terminal or into a file. This feature is also available in the FPGA environment to come. 

Once you follow setup instructions at the bottom of this document, you will be able to use following services from your C program as demonstrated in 

pulpenix_ddp_env\pulpenix_E115_FPGA\apps\hello_file_access_example\hello_file_access_example.c (available upon below setup)

[By the way this program also provide a time stamp usage example]

Pulpenix file access services:
==============================

int bm_fopen_r(char * file_name) ;                    // Open file for read , return integer is the file index reference

int bm_fopen_w(char * file_name) ;                    // Open file for write , return integer is the file index reference

int  bm_fclose(int file_idx) ;                        // Close the file

char bm_fgets (int file_id, char * str_buf) ;         // reads the next string from a file (till next space/tab/new-line)
                                                      // Equivalent to C standard fscanf("%s",str_buf)

int bm_fprintf(int file_id , const char *fmt, ...);   // Equivalent to C standard fprintf , use the file_id returned from bm_fopen_w

unsigned int dec_str_to_uint (char * s) ;             // Converts a string represented a decimal value to its represented value 

unsigned int hex_str_to_uint (char * s) ;             // Converts a string represented a hex value (without the 0x prefix) to its represented value         


Usage Example:
==============

See hello_file_access_example.c for full source example

// Get value from file

unsigned int get_val_from_file (int file_idx) {

  char op_str[10] ;  // operand string read from file (up to 10 decimal digits)

  bm_fgets (file_idx,op_str) ;         // Get the next string from the file (String are separated by white characters)
  return dec_str_to_uint (op_str) ;  
}

main () {

  ...

  unsigned int gcd_inF  = bm_fopen_r("gcd_in.txt") ;  // Open GCD input file with values to calculate
  unsigned int gcd_outF = bm_fopen_w("gcd_out.txt") ; // Open GCD output file to print results into it. 
    	          
  int op_a, op_b ;  // GCD operands  
  
  op_a = get_val_from_file (gcd_inF) ;
  op_b = get_val_from_file (gcd_inF) ; 
        
  boolean done_parsing  ;
  done_parsing = FALSE  ;
  
  while (!(done_parsing)) {
     	          
        int op_a, op_b ;   // GCD operands
        
        op_a = get_val_from_file (gcd_inF) ;
        op_b = get_val_from_file (gcd_inF) ; 
        
        if ((op_a==0)&&(op_b==0)) done_parsing = TRUE ;  // Agreed indication of end-of-data in input file
        
        else   {
            int gcd_result = get_gcd( op_a, op_b) ;
            bm_fprintf(gcd_outF,"GCD ( %8d , %8d ) = %8d\n",op_a, op_b, gcd_result) ; 
        }       
    } // while done parsing
 
 
    // finish
    bm_fclose(gcd_inF) ;  // Close Input File
    bm_fclose(gcd_outF) ; // Close Output File       


Input file gcd_in.txt :  (Edit to add more GCD calculations

1024    2048
3078    3717
2112     154
1563     102
   0       0  # STOP HERE

Printed Output file gcd_out.txt (after execution)

GCD (     1024 ,     2048 ) =     1024
GCD (     3078 ,     3717 ) =        9
GCD (     2112 ,      154 ) =       22
GCD (     1563 ,      102 ) =        3

----------------------------------------------------------------------------------------

File Access feature Setup:
===========================

IMPORTANT! 
We are going to replace pulpenix_ddp_env -> pulpenix_E115_FPGA with an updated version
If you have already done any work in the environment then backup your elsewhere the filed you have already changed
which are : 
Your application C source  at            ... pulpenix_E115_FPGA/my_app_name/my_app_name.c
Your accelerator Verilog code at         ... pulpenix_E115_FPGA/src/gpp_accelerators/accelerator_*.v
Your accelerator specific interface at:  ... pulpenix_E115_FPGA/src/soc/gpp_gate.sv
Once you update pulpenix_E115_FPGA copy back your modified files to their locations.

WINDOWS MODELSIM

Assuming you already accomplished the base setup
Take care of above IMPORTANT comment if needed
Download from Moodle pulpenix_E115_FPGA_V1A.7z
Extract ans replace  pulpenix_ddp_env -> pulpenix_E115_FPGA  with the updated version.
All other base release guidance apply

VLSI SERVER IRUN

Assuming you already accomplished the base setup
Take care of above IMPORTANT comment if needed
cd ... pulpenix_ddp_env
tar -xzvf /projects/83612/pulpenix_E115_FPGA_V1A.tgz
All other base release guidance apply


