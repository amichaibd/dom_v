onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /tb/pulpenix_i1/msystem_i/gpp_gate_i/HCLK
add wave -noupdate -radix decimal {/tb/pulpenix_i1/msystem_i/gpp_gate_i/gpp_regs_in[1]}
add wave -noupdate -radix decimal {/tb/pulpenix_i1/msystem_i/gpp_gate_i/gpp_regs_in[0]}
add wave -noupdate /tb/pulpenix_i1/msystem_i/gpp_gate_i/PSEL
add wave -noupdate -radix decimal /tb/pulpenix_i1/msystem_i/gpp_gate_i/PRDATA
add wave -noupdate /tb/pulpenix_i1/msystem_i/gpp_gate_i/PADDR
add wave -noupdate -radix decimal {/tb/pulpenix_i1/msystem_i/gpp_gate_i/gpp_regs_out[3]}
add wave -noupdate -radix decimal {/tb/pulpenix_i1/msystem_i/gpp_gate_i/gpp_regs_out[2]}
add wave -noupdate -radix decimal {/tb/pulpenix_i1/msystem_i/core_region_i/CORE/RISCV_CORE/id_stage_i/registers_i/mem[14]}
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2992715998 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 214
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2990268887 ps} {2992831113 ps}
