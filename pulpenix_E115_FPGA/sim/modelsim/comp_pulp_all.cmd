@echo off
powershell -command "if (Test-Path ./work) { Remove-Item work -Recurse ; }"
powershell -command "../../../\modeltech\win32acoem\vlib work"
powershell -command "../../../\modeltech\win32acoem\vlog.exe +define+SIM_UART_SPEED_FACTOR=8   -f .\pulpenix_E115_FPGA.f"
powershell -command "vlog -work work -refresh -force_refresh"