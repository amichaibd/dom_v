#!/bin/bash
# ------------------------------------------------------------------------------------


# Modify this only if GCC toolchain is located elsewhere


# ENICS
setenv RISCV_PULP_TOOLCHAIN_ROOT /project/generic/users/udik/ws/pulp/toolchain/


# gcc binaries name prefix
setenv RISCV_XX "riscv32-unknown-elf"
setenv RISCV_XX "riscv-none-embed"

# Modify this only if GCC toolchain is located elsewhere
setenv ECLIPSE_ROOT "$RISCV_PULP_TOOLCHAIN_ROOT/eclipse"
# setenv RISCV_GCC_BIN "$RISCV_PULP_TOOLCHAIN_ROOT/pulp_toolchain/bin"
setenv RISCV_GCC_BIN "$RISCV_PULP_TOOLCHAIN_ROOT/gnu-mcu-eclipse/7.2.0-2-20180111-2230/bin"
setenv RISCV_PULP_SW_UTILS "../sw_utils"
setenv RISCV_PULP_LIBS "../libs"
setenv RISCV_PULP_SW_APPS_REF "../ref"

alias pulp_comp_app_noopt "$RISCV_PULP_SW_UTILS/comp_app_noopt_uart.sh"
alias pulp_get_app "$RISCV_PULP_SW_UTILS/pulp_get_app.sh" 

#irun simulation aliases

alias pulp_irun "irun -timescale 1ns/1ns -sv -v93  -access rw  -f pulpenix_E115_FPGA.f -delay_trigger"
alias pulp_qirun "qirun -timescale 1ns/1ns -sv -v93 -access rw -f pulpenix_E115_FPGA.f -delay_trigger"

