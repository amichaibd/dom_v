#!/bin/tcsh
# ------------------------------------------------------------------------------------

# if you personalize this file make your own personal copy with different file name and DONT push back to repository

# Modify only this if working a a different space
setenv MY_PULP_ENV "/project/sansa/users/udik/ws/ddp_course/pulpenix_ddp_env/pulpenix_E115_FPGA"

# Modify this only if GCC toolchain is located elsewhere
# setenv RISCV_PULP_TOOLCHAIN_ROOT="/vagrant/vshare/riscv"
setenv RISCV_PULP_TOOLCHAIN_ROOT "/project/generic/users/udik/ws/pulp/toolchain"

# gcc binaries name prefix
# setenv RISCV_XX "riscv32-unknown-elf"
setenv RISCV_XX "riscv-none-embed"

# Modify this only if GCC toolchain is located elsewhere
setenv ECLIPSE_ROOT "$RISCV_PULP_TOOLCHAIN_ROOT/eclipse"
# setenv RISCV_GCC_BIN "$RISCV_PULP_TOOLCHAIN_ROOT/pulp_toolchain/bin"
setenv RISCV_GCC_BIN "$RISCV_PULP_TOOLCHAIN_ROOT/gnu-mcu-eclipse/7.2.0-2-20180111-2230/bin"

# ------------------------------------------------------------------------------------

# DEBUG BRIDGE UNIQUE KEY

# Just to be safe from previous leftovers and set/setenv priorities
unset BRIDGE_UID
unset BRIDGE_DUT_PORT
unset BRIDGE_DBG_PORT

# User Specific Debugger Bridge unique Port numbers
# Use your your ID last 4 digits !!!!!!!!!!!!!!!

setenv BRIDGE_UID 6290
# Must start with 1 and be unique per user
setenv BRIDGE_DUT_PORT "1$BRIDGE_UID"
# Must start with 2 and be unique per user
setenv BRIDGE_DBG_PORT "2$BRIDGE_UID"


# ------------------------------------------------------------------------------------

# NEVER MODIFY !

  
setenv MY_PULP_APPS "$MY_PULP_ENV/apps"  
setenv RISCV_PULP_LIBS "$MY_PULP_APPS/libs"
setenv RISCV_PULP_SW_UTILS "$MY_PULP_APPS/sw_utils"
setenv RISCV_PULP_SW_APPS_REF "$MY_PULP_APPS/ref"
setenv RISCV_PULP_GDB_UTIL "$MY_PULP_APPS/gdb"
setenv MY_PULP_IRUN "$MY_PULP_ENV/sim/irun"
setenv MY_PULP_MODELSIM "$MY_PULP_ENV/sim/modelsim"

# ------------------------------------------------------------------------------------

# toolchain aliases 

alias pulp_terminal_gdb "$RISCV_GCC_BIN/$RISCV_XX-gdb -x gdb_cmd.txt"
alias pulp_eclipse "$ECLIPSE_ROOT/eclipse -data $MY_PULP_ENV/apps/eclipse_ws &"

alias pulp_comp_app_noopt "$RISCV_PULP_SW_UTILS/comp_app_noopt_uart.sh"
alias pulp_get_app "$RISCV_PULP_SW_UTILS/pulp_get_app.sh" 

#irun simulation aliases

alias pulp_irun "irun -timescale 1ns/1ns -sv -v93  -access rw  -f pulpenix_E115_FPGA.f -delay_trigger"
alias pulp_qirun "qirun -timescale 1ns/1ns -sv -v93 -access rw -f pulpenix_E115_FPGA.f -delay_trigger"

# Modelsim (Questa) simulation aliases

alias pulp_vlib "vlib work"
alias pulp_vlog "vlog -dpiheader dpiheader.h -f pulpino_tb.f"
alias pulp_vsim "vsim +MEMLOAD "PRELOAD" -sv_lib mem_dpi -dpilib mem_dpi tb -c"
alias pulp_vsim_probe "pulp_vsim -input probe_all.tcl"
alias pulp_vsim_trace "pulp_vsim +DO_TRACE "" -input run.tcl"
alias pulp_vsim_gdb "pulp_vsim -gTEST=MEM_DPI -do vsim_run_gdb.tcl" 
alias pulp_vsim_gdb_with_probe "pulp_vsim -defparam tb.TEST=MEM_DPI -input run_gdb_with_probe.tcl"

