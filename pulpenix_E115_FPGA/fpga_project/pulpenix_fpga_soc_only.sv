`include "pulpenix_defines.v"


//    define ALTERA_SYN  => Synthesize only pulpilium multi system on Altera    
//    not define ALTERA_SYN  => Synthesize all system with clock resets and reg file    
//     define ALTERA_EMU_ASIC_SYN => On Altera    
//     not define ALTERA_EMU_ASIC_SYN => On ASIC    


module pulpenix  (
  input  logic               pad_test_mode    ,// for DFT 
  input  logic               pad_scan_enable  ,// for DFT
  input  logic               pad_master_slave , //1=master chip    0=slave chip
    // UART bus   
  output logic               pad_uart_tx      ,
  input  logic               pad_uart_rx      ,
       
  //SPI Interfaces combined
  output logic               pad_spim_cs1     ,
  output logic               pad_spim_cs2     ,
  output logic               pad_spim_cs3     ,
  output logic               pad_spim_cs4     ,
  input  logic               pad_spim_din1  ,  
  input  logic               pad_spim_din2_spis_clk ,  
  input  logic               pad_spim_din3_spis_di  ,  
  input  logic               pad_spim_din4    ,  
  output logic               pad_spim_clk     ,
  output logic               pad_spim_do_spis_do    ,
  output logic               pad_spif_cs      ,
  input  logic               pad_spif_di      ,
  input  logic               pad_spis_cs      ,
  input  wire                pad_rst_n     ,  
  output wire  [1:0]         o_qspi_mod       ,
  
  //Clock reset regs signals added
  //**************************************************************************8

  input  wire                clk_slow      ,// 40MHz from outside
  input  wire                clk_midl      ,// 20Mhz-80Mhz  from synthesizer for modem, ADC & DAC 
  input  wire                clk_fast      ,//100Mhz~400Mhz from synthesizer for msystem 
  input  wire                spi_nativ_n    //SPI slave select 0=native 1=host_based 
 
);
  
//****************************************************************************************  
//****************************************************************************************  
//
//                 SIGNAL DEFINITION  
//  
//****************************************************************************************  
//****************************************************************************************  

wire                clk_msystem  = clk_slow  ;//Clock to msystem
wire                rst_msystem_n = pad_rst_n ;//Reset to msystem synch. to clk_msystem


// TEMPORARY CONSTANTS AND UNCONNECTES OUTPUTS TO ALL UNUSED INTERFACES

// UART Control
logic              uart_rts;                // output 
logic              uart_dtr;                // output 
logic              uart_cts = 0;            // input  
logic              uart_dsr = 0;            // input  
                                                  
// Misc                                            
logic              clk_sel_i = 0;           // input  
logic              clk_standalone_i = 0;    // input  
logic              testmode_i = 0;          // input  
//logic              scan_enable_i = 0;       // input  
                                                   
                                                   
logic              scl_pad_i = 0;           // input  
logic              scl_pad_o ;              // output 
logic              scl_padoen_o ;           // output 
logic              sda_pad_i = 0;           // input  
logic              sda_pad_o;               // output 
logic              sda_padoen_o;            // output 
                                            
logic       [31:0] gpio_in = 0;             // input  
logic       [31:0] gpio_out;                // output 
logic       [31:0] gpio_dir;                // output 
logic [31:0] [5:0] gpio_padcfg;             // output 
                                               

//*****************************************************************
//*****************************************************************
//*****************************************************************
//                 
//          MSYSTEM   
//                 
//*****************************************************************
//*****************************************************************
//*****************************************************************

msystem msystem_i (
  /////////////////////////////////////
  .clk                       ( clk_msystem           ),
  .rst_n                     ( rst_msystem_n         ),
  .pad_testmode_i            ( pad_test_mode         ),
  .pad_scan_enable_i         ( pad_scan_enable       ),
  .pad_master_slave          ( pad_master_slave      ),

  .pad_spim_cs1              ( pad_spim_cs1          ),
  .pad_spim_cs2              ( pad_spim_cs2          ),
  .pad_spim_cs3              ( pad_spim_cs3          ),
  .pad_spim_cs4              ( pad_spim_cs4          ),
  .pad_spim_din1             ( pad_spim_din1         ),  
  .pad_spim_din2_spis_clk    ( pad_spim_din2_spis_clk),  
  .pad_spim_din3_spis_di     ( pad_spim_din3_spis_di ),  
  .pad_spim_din4             ( pad_spim_din4         ),  
  .pad_spim_clk              ( pad_spim_clk          ),
  .pad_spim_do_spis_do       ( pad_spim_do_spis_do   ),
  .pad_spif_cs               ( pad_spif_cs           ),
  .pad_spif_di               ( pad_spif_di           ),
  .pad_spis_cs               ( pad_spis_cs           ), 
  
  .pad_uart_rx               ( pad_uart_rx           ),
  .pad_uart_tx               ( pad_uart_tx           ),
  .o_qspi_mod                ( o_qspi_mod            )//tri-state of flash used by altera

);



endmodule

