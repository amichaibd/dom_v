# -------------------------------------------------------------------------- #
#
# Copyright (C) 2018  Intel Corporation. All rights reserved.
# Your use of Intel Corporation's design tools, logic functions 
# and other software and tools, and its AMPP partner logic 
# functions, and any output files from any of the foregoing 
# (including device programming or simulation files), and any 
# associated documentation or information are expressly subject 
# to the terms and conditions of the Intel Program License 
# Subscription Agreement, the Intel Quartus Prime License Agreement,
# the Intel FPGA IP License Agreement, or other applicable license
# agreement, including, without limitation, that your use is for
# the sole purpose of programming logic devices manufactured by
# Intel and sold by Intel or its authorized distributors.  Please
# refer to the applicable agreement for further details.
#
# -------------------------------------------------------------------------- #
#
# Quartus Prime
# Version 18.1.0 Build 625 09/12/2018 SJ Lite Edition
# Date created = 16:59:32  March 21, 2019
#
# -------------------------------------------------------------------------- #
#
# Notes:
#
# 1) The default values for assignments are stored in the file:
#		pulpenix_assignment_defaults.qdf
#    If this file doesn't exist, see file:
#		assignment_defaults.qdf
#
# 2) Altera recommends that you do not modify this file. This
#    file is updated automatically by the Quartus Prime software
#    and any changes you make may be lost or overwritten.
#
# -------------------------------------------------------------------------- #

# NOTICE THAT DEFINE to "=0" YET YIELDS TRUE FOR `ifdef

set_global_assignment -name FAMILY "Cyclone IV E"
set_global_assignment -name DEVICE EP4CE115F23I7
set_global_assignment -name TOP_LEVEL_ENTITY altera
set_global_assignment -name ORIGINAL_QUARTUS_VERSION 18.1.0
set_global_assignment -name PROJECT_CREATION_TIME_DATE "16:59:32  MARCH 21, 2019"
set_global_assignment -name LAST_QUARTUS_VERSION "18.1.0 Lite Edition"
set_global_assignment -name PROJECT_OUTPUT_DIRECTORY output_files

set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/includes/smart_uart_defines.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/includes/remote_access_intrfc.sv
set_global_assignment -name VERILOG_MACRO "PULP_FPGA_EMUL=1"
set_global_assignment -name VERILOG_MACRO "ALTERA=1"
set_global_assignment -name VERILOG_MACRO "BLOCK_FLASH_LOAD=1"
set_global_assignment -name VERILOG_MACRO "INTERNAL_BOOT=1"
set_global_assignment -name VERILOG_MACRO "NOFLASH=1"
set_global_assignment -name VERILOG_MACRO "MSYSTEM_ONLY=1"
set_global_assignment -name SEARCH_PATH ../src/
set_global_assignment -name SEARCH_PATH ../src/soc/includes/
set_global_assignment -name SEARCH_PATH ../src/ips/riscv/include/
set_global_assignment -name SEARCH_PATH ../src/ips/adv_dbg_if/rtl/
set_global_assignment -name SEARCH_PATH ../src/ips/apb/apb_event_unit/include/
set_global_assignment -name SEARCH_PATH ../src/ips/apb/apb_i2c/
set_global_assignment -name SEARCH_PATH ../src/ips/axi/axi_node/
set_global_assignment -name SEARCH_PATH ../src/gpp_accelerators/

set_global_assignment -name VERILOG_INPUT_VERSION SYSTEMVERILOG_2005
set_global_assignment -name VERILOG_SHOW_LMF_MAPPING_MESSAGES OFF
set_global_assignment -name MIN_CORE_JUNCTION_TEMP "-40"
set_global_assignment -name MAX_CORE_JUNCTION_TEMP 100
set_global_assignment -name POWER_PRESET_COOLING_SOLUTION "23 MM HEAT SINK WITH 200 LFPM AIRFLOW"
set_global_assignment -name POWER_BOARD_THERMAL_MODEL "NONE (CONSERVATIVE)"
set_global_assignment -name SMART_RECOMPILE ON
set_global_assignment -name PARTITION_NETLIST_TYPE SOURCE -section_id Top
set_global_assignment -name PARTITION_FITTER_PRESERVATION_LEVEL PLACEMENT_AND_ROUTING -section_id Top
set_global_assignment -name PARTITION_COLOR 16764057 -section_id Top
#set_global_assignment -name TIMING_ANALYZER_MULTICORNER_ANALYSIS ON

set_location_assignment PIN_D18 -to o_qspi_sck
set_location_assignment PIN_D19 -to o_qspi_cs_n
set_location_assignment PIN_C3 -to led[3]
set_location_assignment PIN_C4 -to led[2]
set_location_assignment PIN_B5 -to led[1]
set_location_assignment PIN_A5 -to led[0]
set_location_assignment PIN_AB11 -to altera_clk25mhz

set_location_assignment PIN_T1 -to sys_rst
set_location_assignment PIN_C19 -to pad_spif_di
set_location_assignment PIN_F11 -to pad_spim_do_spis_do

# Other hand-made cable connection setup
# set_location_assignment PIN_B16 -to i_uart_tx
# set_location_assignment PIN_A16 -to o_uart_rx

# FTDI Cable connection setup
set_location_assignment PIN_R4 -to i_uart_tx
set_location_assignment PIN_T4 -to o_uart_rx

set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to led[3]
set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to led[2]
set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to led[1]
set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to led[0]
set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to pad_spif_di
set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to o_qspi_cs_n
set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to i_uart_tx
set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to o_uart_rx
set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to o_qspi_sck
set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to sys_rst
set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to pad_spim_do_spis_do
set_instance_assignment -name IO_STANDARD "3.3-V LVCMOS" -to altera_clk25mhz

set_global_assignment -name SYSTEMVERILOG_FILE ./altera_soc_only.v
set_global_assignment -name SYSTEMVERILOG_FILE ./pulpenix_fpga_soc_only.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/crr/cells/m_cg.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/sp_ram_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/sp_ram_wrap16K.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/sp_ram_wrap8K.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice/axi_ar_buffer.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice/axi_aw_buffer.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice/axi_b_buffer.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice/axi_r_buffer.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice/axi_slice.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice/axi_w_buffer.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice_dc/dc_data_buffer.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice_dc/dc_full_detector.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice_dc/dc_synchronizer.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice_dc/dc_token_ring_fifo_din.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice_dc/dc_token_ring_fifo_dout.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_slice_dc/dc_token_ring.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_slave/axi_spi_slave.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_uart_slave/axi_uart_slave.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_slave/spi_slave_axi_plug.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_slave/spi_slave_cmd_parser.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_slave/spi_slave_controller.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_slave/spi_slave_dc_fifo.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_slave/spi_slave_regs.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_slave/spi_slave_rx.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_slave/spi_slave_syncro.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_slave/spi_slave_tx.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_master/spi_master_clkgen.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_master/spi_master_controller.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_master/spi_master_fifo.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_master/spi_master_rx.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_spi_master/spi_master_tx.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_address_decoder_AR.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_address_decoder_AW.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_address_decoder_BR.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_address_decoder_BW.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_address_decoder_DW.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_AR_allocator.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_ArbitrationTree.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_AW_allocator.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_BR_allocator.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_BW_allocator.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_DW_allocator.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_FanInPrimitive_Req.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_multiplexer.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_node.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_request_block.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_response_block.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_node/axi_RR_Flag_Req.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_mem_if_DP/axi_mem_if_SP.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_mem_if_DP/axi_read_only_ctrl.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi_mem_if_DP/axi_write_only_ctrl.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi2apb/axi2apb32.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/axi2apb/axi2apb.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_node/apb_node.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_node/apb_node_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_uart_sv/apb_uart.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_uart_sv/uart_interrupt.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_uart_sv/uart_rx.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_uart_sv/uart_tx.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_uart_sv/io_generic_fifo.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_gpio/apb_gpio.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_timer/apb_timer.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_timer/timer.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_i2c/apb_i2c.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_i2c/i2c_master_bit_ctrl.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_i2c/i2c_master_byte_ctrl.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_event_unit/apb_event_unit.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_event_unit/generic_service_unit.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_event_unit/sleep_unit.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_spi_master/apb_spi_master.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_spi_master/spi_master_apb_if.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_fll_if/apb_fll_if.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb_pulpino/apb_pulpino.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/apb/apb2per/apb2per.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/axi/core2axi/core2axi.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/axi2apb_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/axi_mem_if_SP_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/axi_node_intf_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/axi_slice_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/axi_spi_slave_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/boot_code.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/boot_rom_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/clk_rst_gen.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/core2axi_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/core_region.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/instr_ram_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/periph_bus_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/peripherals.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/ram_mux.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/components/cluster_clock_gating.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/components/cluster_clock_inverter.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/components/cluster_clock_mux2.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/components/generic_fifo.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/components/pulp_clock_inverter.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/components/pulp_clock_mux2.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/components/rstgen.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/components/sp_ram.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/msystem_soc_only.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/include/riscv_defines.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/include/apu_core_package.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/include/apu_macros.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_alu_div.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_alu.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_compressed_decoder.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_controller.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_cs_registers.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_debug_unit.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_decoder.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_int_controller.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_ex_stage.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_hwloop_controller.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_hwloop_regs.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_id_stage.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_if_stage.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_load_store_unit.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_mult.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_prefetch_buffer.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_prefetch_L0_buffer.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_register_file.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_core.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/riscv/riscv_fetch_fifo.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/adv_dbg_if/rtl/adbg_or1k_biu.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/adv_dbg_if/rtl/adbg_or1k_defines.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/adv_dbg_if/rtl/adbg_or1k_module.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/adv_dbg_if/rtl/adbg_or1k_status_reg.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/adv_dbg_if/rtl/adbg_axi_biu.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/adv_dbg_if/rtl/adbg_axi_defines.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/adv_dbg_if/rtl/adbg_axi_module.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/adv_dbg_if/rtl/adbg_defines.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/adv_dbg_if/rtl/adbg_tap_top.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/adv_dbg_if/rtl/adbg_top.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/ips/adv_dbg_if/rtl/adv_dbg_if.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/boot_flash_direct_intrfc.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/flash/flash_boot.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/flash/llqspi.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/flash/wbqspiflash.v
set_global_assignment -name SYSTEMVERILOG_FILE ../src/crr/spi_slave_native.v

#----------------------------------------------------------------------------------------------- 
# ADD HERE DOM5 SOURCE FILES
set_global_assignment -name SYSTEMVERILOG_FILE ../src/soc/gpp_gate.sv

##===========DOM_V=============
## Includes
set_global_assignment -name SEARCH_PATH ../../verif/inputs/
set_global_assignment -name SEARCH_PATH ../../source/rtl/includes/


set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/includes/dom_pkg.sv

set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/data_mem_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/data_mem.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/dom_v.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/inst_mem.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/inst_mem_wrap.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/io_ctrl.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/core/alu.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/core/control.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/core/core.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/core/pc.sv
set_global_assignment -name SYSTEMVERILOG_FILE ../../source/rtl/core/registers.sv
#...
#...
#----------------------------------------------------------------------------------------------- 

set_global_assignment -name QIP_FILE ALTPLL1.qip
set_global_assignment -name QIP_FILE altera_sram_32768x32.qip
set_global_assignment -name SIGNALTAP_FILE output_files/top_level.stp




set_global_assignment -name QIP_FILE altera_sram_512x32.qip

set_global_assignment -name QIP_FILE altera_sram_512x32_take2.qip
set_global_assignment -name QIP_FILE altera_sram_512x32_take3.qip
set_instance_assignment -name PARTITION_HIERARCHY root_partition -to | -section_id Top