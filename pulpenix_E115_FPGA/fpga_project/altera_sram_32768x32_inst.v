altera_sram_32768x32	altera_sram_32768x32_inst (
	.address ( address_sig ),
	.byteena ( byteena_sig ),
	.clken ( clken_sig ),
	.clock ( clock_sig ),
	.data ( data_sig ),
	.wren ( wren_sig ),
	.q ( q_sig )
	);
