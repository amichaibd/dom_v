
module altera (
input                  sys_rst          ,
input				  altera_clk25mhz  ,

//Common UART 
output wire            o_uart_rx        ,
input  wire            i_uart_tx        ,

// Flash Interface

input   wire           pad_spif_di      ,
output  wire           pad_spim_do_spis_do ,
output	wire	       o_qspi_cs_n      ,
output	wire 	       o_qspi_sck	      ,

//leds
output  [3:0]          led             

);


`ifdef NOFLASH
  defparam   pulpenix_i1.msystem_i.core_region_i.instr_mem.sp_ram_wrap_i.altera_sram_32768x32_i.init_file = "./app_instr.mif" ;  
  defparam   pulpenix_i1.msystem_i.core_region_i.data_mem.altera_sram_32768x32_i.init_file = "./app_data.mif"  ; 
`endif 
  
  //initial force msystem_i.peripherals_i.apb_uart_sv_i.cfg_div_val=21 ; 
  //*****************************************************************
  //                Clocks and Resets Module
  //*****************************************************************
  
  wire clk_slow         ; 
  wire clk_midl         ;
  wire clk_fast          ;
  wire clk_ultra        ;
  wire pll_locked       ;

  ALTPLL1 u_clocks_resets (
      ///////////////////////////
      .areset    (!sys_rst           ),
      .inclk0    (altera_clk25mhz    ),
      .c0        (clk_slow           ),
      .c1        (clk_midl           ),
      .c2        (clk_fast           ),
      .c3        (clk_ultra          ),
      .locked    (pll_locked         )
    );
  
  //------------------------------------------------------------------------------

  // TMP Play leds, just to see board is alive

  wire sec_pulse ;

  logic [31:0] cnt ;
  always @(posedge clk_slow or negedge sys_rst)
    if (~sys_rst) cnt<=0 ; 
    else cnt <= (sec_pulse ? 0 : cnt+1) ;

  assign sec_pulse = (cnt==40000000) ; // pulse per second at 25 MHz

  logic [3:0] sec_cnt ;
  always @(posedge clk_slow or negedge sys_rst)
    if (~sys_rst) sec_cnt<=0 ; 
    else if (sec_pulse) sec_cnt <= sec_cnt+1;

  assign led[3:0] = ~sec_cnt ; // 4 bit Binary counter of seconds // TMP 4'b0101

  
  pulpenix  pulpenix_i1 (
    .pad_test_mode          ( 1'b0             ), // for DFT 
    .pad_scan_enable        ( 1'b0             ), // for DFT 
    .pad_master_slave       ( 1'b1             ), //1=master 0=slave
    
    .pad_uart_tx            ( o_uart_rx        ), // output 
    .pad_uart_rx            ( i_uart_tx        ), // input 
    .o_qspi_mod 		    ( o_qspi_mod       ),
                                                 
    .pad_spim_cs1           (                  ),
    .pad_spim_cs2           (                  ),
    .pad_spim_cs3           (                  ),
    .pad_spim_cs4           (                  ),
    .pad_spim_din1          ( 1'b0             ),  
    .pad_spim_din2_spis_clk ( 1'b0             ),  
    .pad_spim_din3_spis_di  ( 1'b0             ),  
    .pad_spim_din4          ( 1'b0             ),  
    .pad_spim_clk           ( o_qspi_sck       ),
    .pad_spim_do_spis_do    ( pad_spim_do_spis_do       ),
    .pad_spif_cs            ( o_qspi_cs_n      ),
    .pad_spif_di            ( pad_spif_di      ),
    .pad_spis_cs            ( 1'b1             ),

 
    .pad_rst_n              (sys_rst                ),//was reset_n       ,// Reset from crystal
    .clk_slow               (clk_slow               ),// 40MHz from outside
    .clk_midl               (clk_midl               ),// 20Mhz-80Mhz  from synthesizer for modem, ADC & DAC 
    .clk_fast               (clk_fast               ),//100Mhz~400Mhz from synthesizer for msystem
    .spi_nativ_n            (1'b1                   ) //SPI slave select 0=native 1=host_based 

    );

endmodule
