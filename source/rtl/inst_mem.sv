//-----------------------------------------------------------------------------
// Title         : instruction  memory - Behavioral
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : inst_mem.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// Behavioral duel read dueal write memory
//------------------------------------------------------------------------------
// Modification history :
//------------------------------------------------------------------------------
`include "dom_defines.sv"
//---------------------MEMORY------------------------
//                start   size    end     # of words
//  Inst memory   0x000  0x800  0x7FF    512
//  Data memory   0x800  0x600  0xDFF    384
//  Stack         0xE00  0x100  0xEFF    64 
//  MMIO_general  0xF00  0xA0   0xF9F    40 
//  MMIO_CSR      0xFA0  0x20   0xFBF    8  
//  MMIO_drct_out 0xFC0  0x10   0xFCF    4  
//  MMIO_ER       0xFD0  0x20   0xFEF    8  
//  MMIO_drct_in  0xFF0  0x10   0xFFF    4  
//---------------------------------------------------
module inst_mem import dom_pkg::*;
                (
                input  logic        clock    ,
                input  logic        enable   ,//glb_en   ,
                //core access
                input  logic [31:0] address_a,//curr_pc    ,
                input  logic [3:0]  byteena_a,
                input  logic [31:0] data_a   ,//core_wr_data ,
                input  logic        rden_a   ,//core_rd_en   ,
                input  logic        wren_a   ,//core_wr_en ,
                output logic [31:0] q_a      ,//instruction,
                //SOC access
                input  logic [31:0] address_b,//io_address ,
                input  logic [3:0]  byteena_b,
                input  logic [31:0] data_b   ,//io_wr_data ,
                input  logic        rden_b   ,//io_rd_en   ,
                input  logic        wren_b   ,//io_wr_en   ,
                output logic [31:0] q_b       //rd_inst_io  
                );
localparam LOCAL_MEM_SIZE = SIZE_INST;
logic [7:0] mem     [LOCAL_MEM_SIZE-1:0];
logic [7:0] next_mem[LOCAL_MEM_SIZE-1:0];

always_comb begin
    next_mem = mem;
    if(wren_a) begin
        if(byteena_a[0]) next_mem[address_b+0]= data_a[7:0];
        if(byteena_a[1]) next_mem[address_b+1]= data_a[15:8];
        if(byteena_a[2]) next_mem[address_b+2]= data_a[23:16];
        if(byteena_a[3]) next_mem[address_b+3]= data_a[31:24]; 
    end
    if(wren_b) begin
        if(byteena_b[0]) next_mem[address_b+0]= data_b[7:0];
        if(byteena_b[1]) next_mem[address_b+1]= data_b[15:8];
        if(byteena_b[2]) next_mem[address_b+2]= data_b[23:16];
        if(byteena_b[3]) next_mem[address_b+3]= data_b[31:24]; 
    end
end 
genvar i;
generate // the memory flipflops
    for (i = 0; i<LOCAL_MEM_SIZE; i++) begin : inst_mem
        `DOM_EN_MSFF(mem[i], next_mem[i], clock, enable)
    end
endgenerate
logic [31:0] pre_q_a;  
logic [31:0] pre_q_b;  
assign pre_q_a = {mem[address_a+3],mem[address_a+2],mem[address_a+1],mem[address_a+0]};
assign pre_q_b = {mem[address_b+3],mem[address_b+2],mem[address_b+1],mem[address_b+0]};
`DOM_EN_MSFF(q_a, pre_q_a, clock, rden_a&&enable)
`DOM_EN_MSFF(q_b, pre_q_b, clock, rden_b&&enable)
endmodule
