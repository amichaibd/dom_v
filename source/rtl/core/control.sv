//-----------------------------------------------------------------------------
// Title         : Controler
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : control.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// decodes the instruction into control bits of the data path
//------------------------------------------------------------------------------
// Modification history :
//------------------------------------------------------------------------------
`include "dom_defines.sv"
module control import dom_pkg::*;  
               (input   logic [6:0]  opcode,          
                input   logic [2:0]  funct3,          
                input   logic [6:0]  funct7,          
                output  logic        ctl_pc_to_reg,        
                output  logic        ctl_branch,      
                output  logic        ctl_mem_rd,      
                output  logic        ctl_mem_to_reg,  
                output  logic [3:0]  ctl_ALU_op,      
                output  logic        ctl_mem_wr,      
                output  logic        ctl_ALU_imm,     
                output  logic        ctl_ALU_pc,     
                output  logic        ctl_s_format,    
                output  logic        ctl_jal,    
                output  logic        ctl_jalr,    
                output  logic        ctl_LUI,    
                output  logic        ctl_U_imm,    
                output  logic        ctl_reg_wr       
                );
logic [4:0] funct5;
assign funct5 = funct7[6:2];

always_comb begin : decode_opcode
    ctl_pc_to_reg   = 1'b0;
    ctl_branch      = 1'b0;
    ctl_mem_rd      = 1'b0;
    ctl_mem_to_reg  = 1'b0;
    ctl_mem_wr      = 1'b0;
    ctl_ALU_imm     = 1'b0;
    ctl_ALU_pc      = 1'b0;
    ctl_reg_wr      = 1'b0;
    ctl_s_format    = 1'b0;
    ctl_jal         = 1'b0;
    ctl_jalr        = 1'b0;
    ctl_LUI         = 1'b0;
    ctl_U_imm       = 1'b0;
    ctl_ALU_op      = 4'b0;

    unique casez (opcode) 
    7'b0110011 : begin                  //R_type - Aritmetics
            ctl_reg_wr      = 1'b1;     //enable Write to the destination register
            ctl_ALU_op      = {funct7[5],funct3};
    end
    7'b0010011: begin                   //I_type - Aritmetics
            ctl_ALU_imm     = 1'b1;     //take the immediate and not register 2.
            ctl_reg_wr      = 1'b1;     //enable Write to the destination register
            ctl_ALU_op[2:0] = funct3;
            if(funct3[1:0]==2'b01) begin
                ctl_ALU_op      = {funct7[5],funct3}; //shift in case of SLLI/SRLI/SRAI
            end
    end
    7'b0000011: begin                   //I_type - Load from memory
            ctl_ALU_imm     = 1'b1;     //take the immediate and not register 2.
            ctl_mem_rd      = 1'b1;     //indicate the memory to return data (and not '0)
            ctl_mem_to_reg  = 1'b1;     //the "write-back" register source is the memory and not the ALU_out
            ctl_reg_wr      = 1'b1;     //enable Write to the destination register
    end
    7'b0100011: begin                   //S_type - write to memory
            ctl_ALU_imm     = 1'b1;     //take the immediate and not register 2.
            ctl_s_format    = 1'b1;     //make immediate bits {[31:25],[11:7]}
            ctl_mem_wr      = 1'b1;
    end
    7'b1100111: begin                   //I_type - immidate jump
            ctl_reg_wr      = 1'b1;     //enable Write to the destination register
            ctl_pc_to_reg   = 1'b1;
            ctl_jalr        = 1'b1;
            ctl_ALU_imm     = 1'b1;     //take the immediate and not register 2.
    end
    7'b1101111: begin                   //j_type - immidate jump
            ctl_reg_wr      = 1'b1;     //enable Write to the destination register
            ctl_pc_to_reg   = 1'b1;
            ctl_jal         = 1'b1;
            ctl_ALU_op      = {1'b0,funct3};
    end
    7'b1100011: begin                   //B-type
            ctl_branch      = 1'b1;
            ctl_ALU_op      = {1'b0,funct3};
    end
    7'b0110111: begin                   //LUI
            ctl_reg_wr      = 1'b1;
            ctl_U_imm       = 1'b1;
            ctl_LUI         = 1'b1;
    end
    7'b0010111: begin                   //AUIPC
            ctl_reg_wr      = 1'b1;
            ctl_U_imm       = 1'b1;
            ctl_ALU_pc      = 1'b1;
    end
    default : ctl_ALU_op =4'b0;
endcase
end


endmodule
