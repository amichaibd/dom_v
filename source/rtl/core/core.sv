//-----------------------------------------------------------------------------
// Title         : CORE 
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : core.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// Top level of the DOM-V Core. will connect all the cre modules.
//------------------------------------------------------------------------------
// Modification history :
//------------------------------------------------------------------------------

// single cycle CORE.
`include "dom_defines.sv"
module core import dom_pkg::*;  (
    input  logic        clk,                 
    input  logic        rst,                 
    input  logic        glb_en,                 
    //int_memory                             
    output logic [31:0] next_pc,    
    input  logic [31:0] instruction,
    //data_memory                            
    output logic [31:0] mem_address,
    output logic [31:0] mem_wr_data,
    output logic        ctl_mem_wr, 
    output logic        ctl_mem_rd, 
    output logic [2:0]  funct3,     
    input  logic [31:0] mem_rd_data,         
    //MMIO
    output t_expose_reg expose_reg,          
    input  t_csr csr                  
    );

//    program counter
logic [31:0]  pc_plus4; 
logic [31:0]  sample_pc_plus4; 
logic [31:0]  curr_pc;

//    registers
logic [4:0] reg_rd_ptr_1;
logic [4:0] reg_rd_ptr_2;
logic [4:0] reg_rd_ptr_3;
logic [31:0]   reg_rd_1;
logic [31:0]   reg_rd_2;
logic [31:0]   reg_rd_3;
logic [4:0] reg_wr_ptr;
logic [31:0]   reg_wr_data;
//    ALU
logic [4:0] pre_immediate;
logic [31:0]   ALU_out;
logic [31:0]   samp_ALU_out;
logic [31:0]   ALU_in_1;
logic [31:0]   ALU_in_2;
logic [31:0]   sign_extend_imm; 
logic [31:0]   U_imm; 

//    control signals
logic [3:0]             ctl_ALU_op;
logic                   ctl_pc_to_reg;
logic                   samp_ctl_pc_to_reg;
logic                   ctl_branch;
logic                   ctl_mem_to_reg;
logic                   samp_ctl_mem_to_reg;
logic                   ctl_ALU_imm;
logic                   ctl_reg_wr;
logic                   ctl_s_format;
logic                   ctl_jal ;
logic                   ctl_jalr ;
logic                   ctl_LUI ;
logic                   ctl_U_imm ;
logic                   ctl_ALU_pc ;

//PC
logic                   EnPC  ;
logic                   rstPC ;
////////////////////////////////////////////////////////////////////////////////////////
//          control
////////////////////////////////////////////////////////////////////////////////////////
assign funct3 = instruction[14:12];
control control(.opcode         (instruction[6:0]),
                .funct3         (instruction[14:12]),
                .funct7         (instruction[31:25]),
                .ctl_pc_to_reg  (ctl_pc_to_reg),       //out
                .ctl_branch     (ctl_branch),          //out
                .ctl_mem_rd     (ctl_mem_rd),          //out
                .ctl_mem_to_reg (ctl_mem_to_reg),      //out
                .ctl_ALU_op     (ctl_ALU_op),          //out
                .ctl_mem_wr     (ctl_mem_wr),          //out
                .ctl_ALU_imm    (ctl_ALU_imm),         //out
                .ctl_ALU_pc     (ctl_ALU_pc),          //out
                .ctl_s_format   (ctl_s_format),        //out
                .ctl_jal        (ctl_jal),             //out
                .ctl_jalr       (ctl_jalr),            //out
                .ctl_U_imm      (ctl_U_imm),           //out
                .ctl_LUI        (ctl_LUI),             //out
                .ctl_reg_wr     (ctl_reg_wr)           //out
                );

////////////////////////////////////////////////////////////////////////////////////////
//          Data Memory
////////////////////////////////////////////////////////////////////////////////////////
assign mem_wr_data  =   reg_rd_2;
assign mem_address  =   ALU_out;

////////////////////////////////////////////////////////////////////////////////////////
//          ALU
////////////////////////////////////////////////////////////////////////////////////////

assign pre_immediate   = ctl_s_format ? instruction[11:7] : instruction[24:20];    //the sign_extend_imm defaulet is [31:20] in case of s_forma1 its {[31:25],[11:7]}
assign sign_extend_imm = {{20{instruction[31]}},instruction[31:25],pre_immediate}; //I_type instructions
assign U_imm           = {instruction[31:12],12'b0};

`DOM_EN_MSFF(curr_pc, next_pc, clk, glb_en)
assign ALU_in_1        = ctl_LUI     ? 32'b0   :// LUI   -> 0  + U_imm
                         ctl_ALU_pc  ? curr_pc :// AUIPC -> pc + U_imm
                                       reg_rd_1;// commen case

assign ALU_in_2        = ctl_ALU_imm ? sign_extend_imm :// I_type-> rs1 + imm
                         ctl_U_imm   ? U_imm           :// AUIPC -> pc  + U_imm
                                       reg_rd_2        ;// commen case

alu alu (.ALU_in_1          (ALU_in_1),         //in  32'b
         .ALU_in_2          (ALU_in_2),         //in  32'b
         .ctl_ALU_op        (ctl_ALU_op),       //in  
         .ctl_branch        (ctl_branch),       //in  
         .ALU_out           (ALU_out),          //out 32'b
         .branch_cond_met  (branch_cond_met));  //out 1'b


////////////////////////////////////////////////////////////////////////////////////////
//          Program Counter
////////////////////////////////////////////////////////////////////////////////////////

logic [31:7] immediate_31to7;
assign EnPC     = csr.en_pc;
assign rstPC    = csr.rst_pc;
assign immediate_31to7  = instruction[31:7];
pc pc (.clk                (clk),               //in
       .rst                (rst),               //in
       .EnPC               (EnPC),              //in
       .glb_en             (glb_en),            //in
       .rstPC              (rstPC),             //in
       .ctl_jal            (ctl_jal),           //in
       .ctl_jalr           (ctl_jalr),          //in
       .branch_cond_met    (branch_cond_met),   //in
       .jalr_ALU_out       (ALU_out),           //in
       .immediate_31to7    (immediate_31to7),   //in
       .pc_plus4           (pc_plus4),          //out
       .pc                 (next_pc));          //out
                                                     

////////////////////////////////////////////////////////////////////////////////////////
//          Registers
////////////////////////////////////////////////////////////////////////////////////////
assign reg_rd_ptr_1 = instruction[19:15];
assign reg_rd_ptr_2 = instruction[24:20];
assign reg_rd_ptr_3 = csr.rd_ptr;
assign reg_wr_ptr   = instruction[11:7];
`DOM_EN_MSFF(samp_ALU_out, ALU_out, clk, glb_en)//to align latency with data_mem latency (register file will foword data if needed)
`DOM_EN_MSFF(sample_pc_plus4,       pc_plus4,       clk, glb_en)//to align latency with data_mem latency (register file will foword data if needed)
`DOM_EN_MSFF(samp_ctl_mem_to_reg,   ctl_mem_to_reg, clk, glb_en)//to align latency with data_mem latency (register file will foword data if needed)
`DOM_EN_MSFF(samp_ctl_pc_to_reg,    ctl_pc_to_reg,  clk, glb_en)//to align latency with data_mem latency (register file will foword data if needed)
assign reg_wr_data  = samp_ctl_mem_to_reg ? mem_rd_data :
                      samp_ctl_pc_to_reg  ? sample_pc_plus4    : 
                                            samp_ALU_out; //The write data can come from memory or from ALU or from the pc.
registers registers (.clk          (clk),          
                     .rst          (rst),
                     .glb_en       (glb_en),       //in
                     .reg_rd_ptr_1 (reg_rd_ptr_1), //input
                     .reg_rd_ptr_2 (reg_rd_ptr_2), //input
                     .reg_rd_ptr_3 (reg_rd_ptr_3), //input
                     .reg_wr_ptr   (reg_wr_ptr),   //input
                     .reg_wr_data  (reg_wr_data),  //input
                     .ctl_reg_wr   (ctl_reg_wr),   //input
                     .reg_rd_1     (reg_rd_1),     //output
                     .reg_rd_2     (reg_rd_2),     //output
                     .reg_rd_3     (reg_rd_3)      //output
                    );
assign expose_reg.register = reg_rd_3;//read register acording to csr.rd_ptr;
assign expose_reg.pc = curr_pc;//expose the pc

endmodule
