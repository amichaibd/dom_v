//-----------------------------------------------------------------------------
// Title         : Register File 
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : registers.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// holds the 32 registres for the DOM-V ISA.
// has an extra read port for CSR feature
//------------------------------------------------------------------------------
// Modification history :
//------------------------------------------------------------------------------
`include "dom_defines.sv"

module registers import dom_pkg::*;
                 (input logic           clk          ,
                  input logic           rst          ,
                  input logic           glb_en       ,
                  input logic [4:0]     reg_rd_ptr_1 ,//input
                  input logic [4:0]     reg_rd_ptr_2 ,//input
                  input logic [4:0]     reg_rd_ptr_3 ,//input
                  input logic [4:0]     reg_wr_ptr   ,//input
                  input logic [31:0]    reg_wr_data  ,//input
                  input logic           ctl_reg_wr   ,//input
                  output logic [31:0]   reg_rd_1     ,//output
                  output logic [31:0]   reg_rd_2     ,//output
                  output logic [31:0]   reg_rd_3      //output
                 );
logic [MSB_REGISTER:0][31:0]   register;
logic [MSB_REGISTER:0][31:0]   next_register;
logic [4:0]          samp_wr_ptr;
logic                samp_ctl_reg_wr;
//write to register
always_comb begin
    next_register = register;                       //defualt -> keep old value
    if (samp_ctl_reg_wr) begin
        next_register[samp_wr_ptr] = reg_wr_data;    //overide the defualt value in the "write register"
    end                                             //note - no need for else due to defualt value
    next_register[0] = '0;
end
//the registers
`DOM_EN_MSFF(register, next_register, clk, glb_en) 
//Read from register
`DOM_EN_MSFF(samp_wr_ptr, reg_wr_ptr, clk, glb_en) 
`DOM_EN_MSFF(samp_ctl_reg_wr, ctl_reg_wr, clk, glb_en) 
always_comb begin 
    reg_rd_1 = register[reg_rd_ptr_1];
    reg_rd_2 = register[reg_rd_ptr_2];
    reg_rd_3 = register[reg_rd_ptr_3];
    // fowording unit - due to synchronic read from data_mem the data will arive a cycle late. 
    // need to use the delay write data 
    if((reg_rd_ptr_1 == samp_wr_ptr) && samp_ctl_reg_wr && (samp_wr_ptr!=5'b0)) begin
        reg_rd_1 = reg_wr_data; 
    end
    if((reg_rd_ptr_2 == samp_wr_ptr) && samp_ctl_reg_wr && (samp_wr_ptr!=5'b0)) begin
        reg_rd_2 = reg_wr_data; 
    end
end



endmodule
