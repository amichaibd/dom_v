//-----------------------------------------------------------------------------
// Title         : arithmetic-logic unit 
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : alu.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// the ALU supports the DOM-V opcodes
//------------------------------------------------------------------------------
// Modification history :
//------------------------------------------------------------------------------
`include "dom_defines.sv"

module alu import dom_pkg::*;  (
    input   logic [32-1:0] ALU_in_1,   //in  32'b
    input   logic [32-1:0] ALU_in_2,   //in  32'b
    input   logic [3:0]    ctl_ALU_op, //in  4'b
    input   logic          ctl_branch, //in  4'b
    output  logic [32-1:0] ALU_out,    //out 32'b
    output  logic          branch_cond_met   //out 1'b
    );
logic [4:0]     shamt;
logic [32-1:0]  new_ALU_in_2;
logic pre_branch_cond_met;
always_comb begin
    shamt = ALU_in_2[4:0];
    new_ALU_in_2 = ctl_ALU_op[3] ? ~ALU_in_2 : ALU_in_2;
    unique casez (ctl_ALU_op) 
//use adder
       4'b0000  : ALU_out = ALU_in_1 + new_ALU_in_2 + ctl_ALU_op[3]        ;//ADD
       4'b1000  : ALU_out = ALU_in_1 + new_ALU_in_2 + ctl_ALU_op[3]        ;//SUB
       4'b0010  : ALU_out = {31'b0, $signed(ALU_in_1) < $signed(ALU_in_2)} ;//SLT
       4'b0011  : ALU_out = {31'b0 , ALU_in_1 < ALU_in_2}                  ;//SLTU
//shift
       4'b0001  : ALU_out = ALU_in_1 << shamt                              ;//SLL
       4'b0101  : ALU_out = ALU_in_1 >> shamt                              ;//SRL
       4'b1101  : ALU_out = $signed(ALU_in_1) >>> shamt                    ;//SRA
//bit wise opirations
       4'b0100  : ALU_out = ALU_in_1 ^ ALU_in_2                            ;//XOR
       4'b0110  : ALU_out = ALU_in_1 | ALU_in_2                            ;//OR
       4'b0111  : ALU_out = ALU_in_1 & ALU_in_2                            ;//AND
       default  : ALU_out = 32'b0                                          ;
    endcase

    //for branch condition.
    pre_branch_cond_met = 1'b0;// should be implementaed using the first case (better synthesis)
    unique casez (ctl_ALU_op[2:1])//bits [2:1] is the HW check and bit[0] is compliment check. 
       2'b00   : pre_branch_cond_met = (ALU_in_1==ALU_in_2)                  ;// BEQ/BNE
       2'b10   : pre_branch_cond_met = ($signed(ALU_in_1)<$signed(ALU_in_2)) ;// BLT/BGE
       2'b11   : pre_branch_cond_met = (ALU_in_1<ALU_in_2)                   ;// BLTU/BGEU
       default : pre_branch_cond_met = 1'b0                                  ;
    endcase
    branch_cond_met = ctl_branch ? pre_branch_cond_met^ctl_ALU_op[0] : 1'b0; //^ctl_ALU_op[0] will compliment the checker (== will turn into !=), (BLT will turn into BGE),

end

endmodule

