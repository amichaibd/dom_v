//-----------------------------------------------------------------------------
// Title         : Program counter 
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : pc.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// the program counter will choose the instruction address.
//------------------------------------------------------------------------------
// Modification history :
//------------------------------------------------------------------------------
`include "dom_defines.sv"

module pc import dom_pkg::*;
          ( input  logic         clk,
            input  logic         rst,
            input  logic         glb_en,                 
            input  logic         EnPC,
            input  logic         rstPC,
            input  logic         ctl_jal,
            input  logic         ctl_jalr,
            input  logic         branch_cond_met,
            input  logic [31:0]  jalr_ALU_out,
            input  logic [31:7]  immediate_31to7,
            output logic [31:0]  pc_plus4,
            output logic [31:0]  pc);
logic [31:0]   next_pc;
logic [31:0]   prev_pc;
logic [31:0]   imm_branch;
logic [31:0]   imm_jal;
logic [31:0]   immediate;
logic [31:0]   jump_adrs;
assign imm_branch = {{20{immediate_31to7[31]}},immediate_31to7[7],    immediate_31to7[30:25],immediate_31to7[11:8], 1'b0};
assign imm_jal    = {{12{immediate_31to7[31]}},immediate_31to7[19:12],immediate_31to7[20],   immediate_31to7[30:21],1'b0};//FIXME - added the 1'b0 at the end to get resolts in TB - IDK why this is needed..
assign immediate  = ctl_jal ? imm_jal  : imm_branch;
assign jump_adrs  = prev_pc + immediate[31:0];
assign pc_plus4   = prev_pc + 32'd4;
assign next_pc    = ctl_jalr                 ? jalr_ALU_out : //in case of jalr = rd_1+imm
                    ctl_jal||branch_cond_met ? jump_adrs    : //in case of branch met or jal.
                                               pc_plus4     ; //common case - next line in program

//`DOM_EN_RST_MSFF(pc, next_pc, clk, EnPC && glb_en ,rstPC || rst) 
`DOM_EN_RST_MSFF(prev_pc, pc, clk, glb_en ,rstPC || rst) 
assign pc = EnPC ? next_pc : prev_pc;
endmodule

