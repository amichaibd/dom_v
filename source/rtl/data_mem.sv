//-----------------------------------------------------------------------------
// Title         : data memory - Behavioral
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : data_mem.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// Behavioral duel read dueal write memory
//------------------------------------------------------------------------------
// Modification history :
//------------------------------------------------------------------------------
`include "dom_defines.sv"
//---------------------MEMORY------------------------
//                start   size    end     # of words
//  Inst memory   0x000  0x800  0x7FF    512
//  Data memory   0x800  0x600  0xDFF    384
//  Stack         0xE00  0x100  0xEFF    64 
//  MMIO_general  0xF00  0xA0   0xF9F    40 
//  MMIO_CSR      0xFA0  0x20   0xFBF    8  
//  MMIO_drct_out 0xFC0  0x10   0xFCF    4  
//  MMIO_ER       0xFD0  0x20   0xFEF    8  
//  MMIO_drct_in  0xFF0  0x10   0xFFF    4  
//---------------------------------------------------
module data_mem import dom_pkg::*; 
               (input  logic        clock    ,
                input  logic        enable   ,
                //core access
                input  logic [31:0] address_a,
                input  logic [3:0]  byteena_a,
                input  logic [31:0] data_a   ,
                input  logic        rden_a   ,
                input  logic        wren_a   ,
                output logic [31:0] q_a      ,
                //SOC access
                input  logic [31:0] address_b,
                input  logic [3:0]  byteena_b,
                input  logic [31:0] data_b   ,
                input  logic        rden_b   ,
                input  logic        wren_b   ,
                output logic [31:0] q_b      
               );
localparam LOCAL_MEM_SIZE             = SIZE_DATA+SIZE_STACK+SIZE_MMIO_GENERAL;
localparam LOCAL_OFFSET_MMIO_GENERAL  = SIZE_DATA+SIZE_STACK;
localparam LOCAL_OFFSET_STACK_POINTER = SIZE_DATA;
logic [7:0] mem     [LOCAL_MEM_SIZE-1:0];
logic [7:0] next_mem[LOCAL_MEM_SIZE-1:0];

always_comb begin
    next_mem = mem;
    if(wren_a) begin
        if(byteena_a[0]) next_mem[address_a+0]= data_a[7:0];
        if(byteena_a[1]) next_mem[address_a+1]= data_a[15:8];
        if(byteena_a[2]) next_mem[address_a+2]= data_a[23:16];
        if(byteena_a[3]) next_mem[address_a+3]= data_a[31:24]; 
    end
    if(wren_b) begin
        if(byteena_b[0]) next_mem[address_b+0]= data_b[7:0];
        if(byteena_b[1]) next_mem[address_b+1]= data_b[15:8];
        if(byteena_b[2]) next_mem[address_b+2]= data_b[23:16];
        if(byteena_b[3]) next_mem[address_b+3]= data_b[31:24]; 
    end
end 
genvar i;
generate // the memory flipflops
    for (i = 0; i<LOCAL_MEM_SIZE; i++) begin : data_mem_gen
        `DOM_EN_MSFF(mem[i], next_mem[i], clock, enable)
    end
endgenerate
logic [31:0] pre_q_a;  
logic [31:0] pre_q_b;  
assign pre_q_a = {mem[address_a+3],mem[address_a+2],mem[address_a+1],mem[address_a+0]};
assign pre_q_b = {mem[address_b+3],mem[address_b+2],mem[address_b+1],mem[address_b+0]};
`DOM_EN_MSFF(q_a, pre_q_a, clock, rden_a&&enable)
`DOM_EN_MSFF(q_b, pre_q_b, clock, rden_b&&enable)

//  MMIO_GENERAL      0xF00  0xA0   0xF9F    8  
//  This is just for simulation signals. (wont effect logic or syntethis)
logic [7:0] mmio_general [LOCAL_MEM_SIZE-1:LOCAL_OFFSET_MMIO_GENERAL];
assign mmio_general = mem[LOCAL_MEM_SIZE-1:LOCAL_OFFSET_MMIO_GENERAL];

logic [7:0] stack_pointer [LOCAL_OFFSET_MMIO_GENERAL-1:LOCAL_OFFSET_STACK_POINTER];
assign stack_pointer = mem[LOCAL_OFFSET_MMIO_GENERAL-1:LOCAL_OFFSET_STACK_POINTER];



endmodule
