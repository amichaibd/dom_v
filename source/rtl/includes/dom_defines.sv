//-----------------------------------------------------------------------------
// Title         : DOM-V defines 
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : dom_defines.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// DOM-V defines - mainly FlipFlops variations.
//-----------------------------------------------------------------------------


`define  DOM_MSFF(q,i,clk)              \
         always_ff @(posedge clk)       \
            q<=i;

`define  DOM_EN_MSFF(q,i,clk,en)        \
         always_ff @(posedge clk)       \
            if(en) q<=i;

`define  DOM_RST_MSFF(q,i,clk,rst)      \
         always_ff @(posedge clk) begin \
            if (rst) q <='0;            \
            else     q <= i;            \
         end
        
`define  DOM_EN_RST_MSFF(q,i,clk,en,rst)\
         always_ff @(posedge clk)       \
            if (rst)    q <='0;         \
            else if(en) q <= i;

`define  DOM_EN_RST_VAL_MSFF(q,i,clk,en,rst,val)\
         always_ff @(posedge clk)       \
            if (rst)    q <=val;         \
            else if(en) q <= i;
