//-----------------------------------------------------------------------------
// Title         : DOM-V
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : dom_v.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// This is the top of the IP.
// instantiates 4 module 
// 1) core
// 2) Data memory
// 3) instruction memory
// 4) IO control
// will due all the interconect and the TOP level interface.
//------------------------------------------------------------------------------
// Modification history :
//------------------------------------------------------------------------------

`include "dom_defines.sv"

module dom_v import dom_pkg::*;
    (
    input  logic        HCLK,
    input  logic        HRESETn,
    input  logic [11:0] PADDR,
    input  logic [31:0] PWDATA,
    input  logic        PWRITE,
    input  logic        PSEL,
    input  logic        PENABLE,
    output logic [31:0] PRDATA,
    output logic        PREADY,
    output logic        PSLVERR
    );

logic [31:0] next_pc;
logic [31:0] instruction;
logic [31:0] core_address;
logic [31:0] core_wr_data;
logic        ctl_mem_wr;
logic        ctl_mem_rd;
logic [2:0]  funct3;
logic [31:0] pre_rd_data_core;
logic [31:0] rd_data_core;
logic [31:0] io_address;
logic [31:0] io_wr_data;
logic        io_wr_en;
logic        io_rd_en;
logic [31:0] rd_inst_io;
logic [31:0] pre_rd_inst_io;
logic [31:0] rd_data_io;
t_expose_reg expose_reg;
logic        glb_en;
logic        rstPC;
t_drct_out   drct_out;
t_drct_in    drct_in;
t_csr        csr;
assign PSLVERR =1'b0;

io_ctrl io_ctrl (
            //SOC
            .HCLK       (HCLK),    //input
            .rst        (~HRESETn), //input
            .PADDR      (PADDR),   //input
            .PWDATA     (PWDATA),  //input
            .PWRITE     (PWRITE),  //input
            .PSEL       (PSEL),    //input
            .PENABLE    (PENABLE), //input
            .PRDATA     (PRDATA),  //output
            .PREADY     (PREADY),  //output
            //MEM
            .wr_data    (io_wr_data), //output
            .address    (io_address), //output
            .wr_en      (io_wr_en),   //output
            .rd_en      (io_rd_en),   //output
            .rd_inst    (rd_inst_io), //input from inst_mem
            .rd_data    (rd_data_io), //input from data_mem
            .gear       (csr.gear),        //input  
            .glb_en     (glb_en),     //output to all
            .drct_out   (drct_out),   //input
            .drct_in    (drct_in)     //output
            );


core core (
            .clk             (HCLK),         //input
            .rst             (~HRESETn),     //input
            .glb_en          (glb_en),     //input
            //inst_mem
            .next_pc         (next_pc),      //output
            .instruction     (instruction),  //input
            //data_mem
            .mem_address     (core_address),  //output
            .mem_wr_data     (core_wr_data),  //output
            .ctl_mem_wr      (ctl_mem_wr),   //output
            .ctl_mem_rd      (ctl_mem_rd),   //output
            .funct3          (funct3),       //output
            .mem_rd_data     (rd_data_core),  //input
            //MMIO
            .expose_reg      (expose_reg),   //output
            .csr             (csr)           //input  
            );

//---------------------MEMORY------------------------
//                start   size    end     # of words
//  Inst memory   0x000  0x800  0x7FF    512
//  Data memory   0x800  0x600  0xDFF    384
//  Stack         0xE00  0x100  0xEFF    64 
//  MMIO_general  0xF00  0xA0   0xF9F    40 
//  MMIO_CSR      0xFA0  0x20   0xFBF    8  
//  MMIO_drct_out 0xFC0  0x10   0xFCF    4  
//  MMIO_ER       0xFD0  0x20   0xFEF    8  
//  MMIO_drct_in  0xFF0  0x10   0xFFF    4  
//---------------------------------------------------

inst_mem_wrap  inst_mem_wrap(
            .clock      (HCLK),
            .rst        (~HRESETn),
            .address_a  (next_pc),
            .address_b  (io_address),
            .byteena_a  (4'b1111),
            .byteena_b  (4'b1111),
            .data_a     (32'b0),
            .data_b     (io_wr_data),
            .enable     (glb_en),
            .rden_a     (csr.en_pc),
            .rden_b     (io_rd_en),
            .wren_a     (1'b0),
            .wren_b     (io_wr_en),
            .q_a        (instruction),
            .q_b        (rd_inst_io)
            );        

//assign byte enable with funct3
//
logic [3:0] core_byteena;
assign core_byteena = (funct3==3'b000)  ? 4'b0001 : //SB
                      (funct3==3'b001)  ? 4'b0011 : //SH
                    /*(funct3==3'b011)*/  4'b1111 ; //SW
data_mem_wrap  data_mem_wrap(
            .clock      (HCLK),
            .rst        (~HRESETn),
            .enable     (glb_en),
            //core_access
            .address_a  (core_address),
            .byteena_a  (core_byteena),
            .data_a     (core_wr_data),
            .rden_a     (ctl_mem_rd),
            .wren_a     (ctl_mem_wr),
            .q_a        (pre_rd_data_core),
            //soc_access
            .address_b  (io_address),
            .byteena_b  (4'b1111),
            .data_b     (io_wr_data),
            .rden_b     (io_rd_en),
            .wren_b     (io_wr_en),
            .q_b        (rd_data_io),
            //other memory regions
            .expose_reg   (expose_reg),  //input 
            .csr          (csr),         //output 
            .drct_out     (drct_out),    //output
            .drct_in      (drct_in)      //input
            );   
logic [3:0] sample_funct3;
`DOM_EN_MSFF(sample_funct3, funct3, HCLK, glb_en) 
assign rd_data_core = (sample_funct3==3'b000)  ? {{24{pre_rd_data_core[7 ]}},pre_rd_data_core[7 :0]} : //LB
                      (sample_funct3==3'b001)  ? {{16{pre_rd_data_core[15]}},pre_rd_data_core[15:0]} : //LH
                      (sample_funct3==3'b010)  ? {                           pre_rd_data_core[31:0]} : //LW
                      (sample_funct3==3'b100)  ? {24'b0,                     pre_rd_data_core[7 :0]} : //LBU
                    /*(sample_funct3==3'b101)*/  {16'b0,                     pre_rd_data_core[15:0]} ; //LHU
    
endmodule
