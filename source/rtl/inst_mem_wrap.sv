//-----------------------------------------------------------------------------
// Title         : instruction memory wrap
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : inst_mem_wrap.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// will `ifdef the SRAM memory vs behavrial memory
//------------------------------------------------------------------------------
// Modification history :
//------------------------------------------------------------------------------
`include "dom_defines.sv"
//---------------------MEMORY------------------------
//                start   size    end     # of words
//  Inst memory   0x000  0x800  0x7FF    512
//  Data memory   0x800  0x600  0xDFF    384
//  Stack         0xE00  0x100  0xEFF    64 
//  MMIO_general  0xF00  0xA0   0xF9F    40 
//  MMIO_CSR      0xFA0  0x20   0xFBF    8  
//  MMIO_drct_out 0xFC0  0x10   0xFCF    4  
//  MMIO_ER       0xFD0  0x20   0xFEF    8  
//  MMIO_drct_in  0xFF0  0x10   0xFFF    4  
//---------------------------------------------------
module inst_mem_wrap import dom_pkg::*;
                (
                input  logic        clock    ,
                input  logic        rst      ,
                input  logic [31:0] address_a,//curr_pc    ,
                input  logic [31:0] address_b,//io_address ,
                input  logic [3:0]  byteena_a,//,
                input  logic [3:0]  byteena_b,//,
                input  logic [31:0] data_a   ,//core_wr_data ,
                input  logic [31:0] data_b   ,//io_wr_data ,
                input  logic        enable   ,//glb_en   ,
                input  logic        rden_a   ,//core_rd_en   ,
                input  logic        rden_b   ,//io_rd_en   ,
                input  logic        wren_a   ,//core_wr_en ,
                input  logic        wren_b   ,//io_wr_en   ,
                output logic [31:0] q_a      ,//instruction,
                output logic [31:0] q_b      //rd_inst_io  
                );
logic [31:0] pre_q_b;
`ifdef ALTERA
altera_sram_512x32_take3	altera_sram_512x32_inst_mem (
	.clock      (clock),
	.address_a  (address_a[10:2]),
	.address_b  (address_b[10:2]),
	.byteena_a  (byteena_a),
	.byteena_b  (byteena_b),
	.data_a     (data_a),
	.data_b     (data_b),
	.enable     (enable),
	.rden_a     (rden_a),
	.rden_b     (rden_b),
	.wren_a     (wren_a),
	.wren_b     (wren_b),
	.q_a        (q_a),
	.q_b        (pre_q_b)
	);
`else
inst_mem inst_mem(
    .clock      (clock),
    .address_a  (address_a),
    .address_b  (address_b),
    .byteena_a  (byteena_a),
    .byteena_b  (byteena_b),
    .data_a     (data_a),
    .data_b     (data_b),
    .enable     (enable),
    .rden_a     (rden_a),
    .rden_b     (rden_b),
    .wren_a     (wren_a),
    .wren_b     (wren_b),
    .q_a        (q_a),
    .q_b        (pre_q_b)
    );

`endif

logic range_rden_b;
logic sample_range_rden_b;
assign range_rden_b =  (address_b < SIZE_INST) ? rden_b : '0;
`DOM_EN_MSFF(sample_range_rden_b, range_rden_b, clock, enable)
assign q_b = sample_range_rden_b ? pre_q_b : '0;

endmodule
