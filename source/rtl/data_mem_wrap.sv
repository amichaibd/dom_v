//-----------------------------------------------------------------------------
// Title         : data memory wrap
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : data_mem_wrap.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// will `ifdef the SRAM memory vs behavrial memory
// the data memry wrap cople of memory regions:
//                 start  size   end      # of words
//  Inst memory    0x000  0x800  0x7FF    512       -> SRAM
//  Data memory    0x800  0x600  0xDFF    384       -> SRAM
//  Stack          0xE00  0x100  0xEFF    64        -> SRAM
//  MMIO_general   0xF00  0xA0   0xF9F    40        -> SRAM
//  MMIO_CSR       0xFA0  0x20   0xFBF    8         -> FF
//  MMIO_drct_out  0xFC0  0x10   0xFCF    1         -> FF
//  MMIO_ER        0xFD0  0x20   0xFEF    1         -> FF
//  MMIO_drct_in   0xFF0  0x10   0xFFF    1         -> FF
//------------------------------------------------------------------------------
// Modification history :
//------------------------------------------------------------------------------

`include "dom_defines.sv"
module data_mem_wrap import dom_pkg::*;  
                (input   logic             clock,          
                 input   logic             rst,          
                 input   logic             enable,        
                 //core
                 input   logic [31:0]      address_a,
                 input   logic [3:0]       byteena_a,
                 input   logic [31:0]      data_a,
                 input   logic             rden_a,  
                 input   logic             wren_a,  
                 output  logic [31:0]      q_a,     
                 //io_ctrl                              
                 input   logic [31:0]      address_b,       
                 input   logic [3:0]       byteena_b,
                 input   logic [31:0]      data_b,       
                 input   logic             rden_b,         
                 input   logic             wren_b,         
                 output  logic [31:0]      q_b,       
                 //MMIO memory regions
                 input   t_expose_reg      expose_reg,
                 output  t_csr             csr,
                 output  t_drct_out        drct_out,
                 input   t_drct_in         drct_in
                );

t_mmio mmio_mem;
t_mmio next_mmio_mem;
logic [31:0] offset_address_a;
logic [31:0] offset_address_b;
logic [31:0] mmio_q_a;
logic [31:0] mmio_q_b;
logic [31:0] mmio_data_core;
logic [31:0] mmio_data_io; 
logic [31:0] data_q_a;
logic [31:0] data_q_b;
logic in_range_data_a, in_range_mmio_data_a, in_range_data_b, in_range_mmio_data_b;
logic sample_in_range_data_a, sample_in_range_mmio_data_a, sample_in_range_data_b, sample_in_range_mmio_data_b;

//---------------------MEMORY------------------------
//                 start  size   end      # of words
//  Inst memory    0x000  0x800  0x7FF    512
//   Data memory   0x800  0x600  0xDFF    384
//   Stack         0xE00  0x100  0xEFF    64 
//   MMIO_general  0xF00  0xA0   0xF9F    40 
//  MMIO_CSR       0xFA0  0x20   0xFBF    8  
//  MMIO_drct_out  0xFC0  0x10   0xFCF    1  
//  MMIO_ER        0xFD0  0x20   0xFEF    1  
//  MMIO_drct_in   0xFF0  0x10   0xFFF    1  
//---------------------------------------------------
always_comb begin
    in_range_data_a      = (address_a<(MSB_DATA_MEM)&&address_a>(LSB_DATA_MEM-1));
    in_range_data_b      = (address_b<(MSB_DATA_MEM)&&address_b>(LSB_DATA_MEM-1));
    in_range_mmio_data_a = (address_a<(MSB_MMIO_MEM)&&address_a>(LSB_MMIO_MEM-1));
    in_range_mmio_data_b = (address_b<(MSB_MMIO_MEM)&&address_b>(LSB_MMIO_MEM-1));
    offset_address_a     = address_a;
    offset_address_b     = address_b;
    if (in_range_data_a) offset_address_a[11]=1'b0;
    if (in_range_data_b) offset_address_b[11]=1'b0;
end

`ifdef ALTERA
altera_sram_512x32_take3	altera_sram_512x32_data_mem (
	.clock      (clock),
	.address_a  (offset_address_a[10:2]),
	.address_b  (offset_address_b[10:2]),
	.byteena_a  (byteena_a),
	.byteena_b  (byteena_b),
	.data_a     (data_a),
	.data_b     (data_b),
	.enable     (enable),
	.rden_a     (rden_a&&in_range_data_a),
	.rden_b     (rden_b&&in_range_data_b),
	.wren_a     (wren_a&&in_range_data_a),
	.wren_b     (wren_b&&in_range_data_b),
	.q_a        (data_q_a),
	.q_b        (data_q_b)
	);
`else

data_mem        
data_mem (                                                             
    .clock      (clock),
    .address_a  (offset_address_a[10:0]),
    .address_b  (offset_address_b[10:0]),
    .byteena_a  (byteena_a),
    .byteena_b  (byteena_b),
    .data_a     (data_a),
    .data_b     (data_b),
    .enable     (enable),
    .rden_a     (rden_a&&in_range_data_a),
    .rden_b     (rden_b&&in_range_data_b),
    .wren_a     (wren_a&&in_range_data_a),
    .wren_b     (wren_b&&in_range_data_b),
    .q_a        (data_q_a),
    .q_b        (data_q_b)
    );
`endif

//=======================================================
//================MMIO memory write======================
//=======================================================
always_comb begin
    next_mmio_mem = mmio_mem; //defualt value
    if (rst) begin //rst value for CSR
        next_mmio_mem.csr.en_pc  = '0;// en_pc
        next_mmio_mem.csr.rst_pc = '0;// rst+pc
        next_mmio_mem.csr.rd_ptr = '0;// rd_ptr
        next_mmio_mem.csr.gear   = '0;// gear
        next_mmio_mem.csr.start  = '0;// start -> after SOC sets this bit dom should reset it.
        next_mmio_mem.csr.done   = '0;// done  -> After DOM sets this SOC should reset it.
    end
    if(wren_a) begin//TODO add byte enable
        unique case (address_a[12:0])
            OFFSET_MMIO_CSR   + 'h0  : next_mmio_mem.csr.en_pc   = data_a[0]  ;
            OFFSET_MMIO_CSR   + 'h4  : next_mmio_mem.csr.rst_pc  = data_a[0]  ;
            OFFSET_MMIO_CSR   + 'h8  : next_mmio_mem.csr.rd_ptr  = data_a[4:0];
            OFFSET_MMIO_CSR   + 'hC  : next_mmio_mem.csr.gear    = data_a[3:0];
            OFFSET_MMIO_CSR   + 'h10 : next_mmio_mem.csr.start   = data_a[0]  ;
            OFFSET_MMIO_CSR   + 'h14 : next_mmio_mem.csr.done    = data_a[0]  ;
            OFFSET_MMIO_DRCT_OUT     : next_mmio_mem.drct_out.out= data_a     ;
            default                  : /*do nothing*/                        ;
        endcase
    end
    if(wren_b) begin   //mem_wr_2 has prioroty over mem_wr_1
        unique case (address_b[12:0])
            OFFSET_MMIO_CSR   + 'h0  : next_mmio_mem.csr.en_pc   = data_b[0]  ;
            OFFSET_MMIO_CSR   + 'h4  : next_mmio_mem.csr.rst_pc  = data_b[0]  ;
            OFFSET_MMIO_CSR   + 'h8  : next_mmio_mem.csr.rd_ptr  = data_b[4:0];
            OFFSET_MMIO_CSR   + 'hC  : next_mmio_mem.csr.gear    = data_b[3:0];
            OFFSET_MMIO_CSR   + 'h10 : next_mmio_mem.csr.start   = data_b[0]  ;
            OFFSET_MMIO_CSR   + 'h14 : next_mmio_mem.csr.done    = data_b[0]  ;
            OFFSET_MMIO_DRCT_OUT     : next_mmio_mem.drct_out.out= data_b     ;
            default                  : /*do nothing*/                        ;
        endcase
    end
//these dont need "decoding" - always stay connected
        next_mmio_mem.expose_reg    = expose_reg;
        next_mmio_mem.drct_in       = drct_in;
end 

//=======================================================
//================MMIO memory flops======================
//=======================================================
`DOM_EN_MSFF(mmio_mem, next_mmio_mem, clock, enable) 

//=======================================================
//================MMIO memory read======================
//=======================================================
always_comb begin
    mmio_data_core =32'b0;
    if(rden_a) begin//TODO add byte enable
        unique case (address_a[12:0])
            OFFSET_MMIO_CSR   + 'h0  : mmio_data_core  = {31'b0,mmio_mem.csr.en_pc} ;
            OFFSET_MMIO_CSR   + 'h4  : mmio_data_core  = {31'b0,mmio_mem.csr.rst_pc};
            OFFSET_MMIO_CSR   + 'h8  : mmio_data_core  = {27'b0,mmio_mem.csr.rd_ptr};
            OFFSET_MMIO_CSR   + 'hC  : mmio_data_core  = {28'b0,mmio_mem.csr.gear}  ;
            OFFSET_MMIO_CSR   + 'h10 : mmio_data_core  = {31'b0,mmio_mem.csr.start} ;
            OFFSET_MMIO_CSR   + 'h14 : mmio_data_core  = {31'b0,mmio_mem.csr.done}  ;
            OFFSET_MMIO_DRCT_OUT     : mmio_data_core  =        mmio_mem.drct_out.out;
            OFFSET_MMIO_ER    + 'h0  : mmio_data_core  =        mmio_mem.expose_reg.pc ;
            OFFSET_MMIO_ER    + 'h4  : mmio_data_core  =        mmio_mem.expose_reg.register;
            OFFSET_MMIO_DRCT_IN      : mmio_data_core  =        mmio_mem.drct_in.in;
            default                  : mmio_data_core  = 32'b0                      ;
        endcase
    end
    mmio_data_io =32'b0;
    if(rden_b) begin   
        unique case (address_b[12:0])
            OFFSET_MMIO_CSR   + 'h0  : mmio_data_io  = {31'b0,mmio_mem.csr.en_pc} ;
            OFFSET_MMIO_CSR   + 'h4  : mmio_data_io  = {31'b0,mmio_mem.csr.rst_pc};
            OFFSET_MMIO_CSR   + 'h8  : mmio_data_io  = {27'b0,mmio_mem.csr.rd_ptr};
            OFFSET_MMIO_CSR   + 'hC  : mmio_data_io  = {28'b0,mmio_mem.csr.gear}  ;
            OFFSET_MMIO_CSR   + 'h10 : mmio_data_io  = {31'b0,mmio_mem.csr.start} ;
            OFFSET_MMIO_CSR   + 'h14 : mmio_data_io  = {31'b0,mmio_mem.csr.done}  ;
            OFFSET_MMIO_DRCT_OUT     : mmio_data_io  =        mmio_mem.drct_out.out;
            OFFSET_MMIO_ER    + 'h0  : mmio_data_io  =        mmio_mem.expose_reg.pc ;
            OFFSET_MMIO_ER    + 'h4  : mmio_data_io  =        mmio_mem.expose_reg.register ;
            OFFSET_MMIO_DRCT_IN      : mmio_data_io  =        mmio_mem.drct_in.in    ;
            default                  : mmio_data_io  = 32'b0                      ;
        endcase
    end
end
//sample the read (synchornic read)
`DOM_EN_MSFF(mmio_q_a, mmio_data_core, clock, enable && in_range_mmio_data_a && rden_a)
`DOM_EN_MSFF(mmio_q_b, mmio_data_io,   clock, enable && in_range_mmio_data_b && rden_b)

`DOM_EN_MSFF(sample_in_range_data_a      , in_range_data_a      && rden_a, clock, enable)
`DOM_EN_MSFF(sample_in_range_mmio_data_a , in_range_mmio_data_a && rden_a, clock, enable)
`DOM_EN_MSFF(sample_in_range_data_b      , in_range_data_b      && rden_b, clock, enable)
`DOM_EN_MSFF(sample_in_range_mmio_data_b , in_range_mmio_data_b && rden_b, clock, enable)

//mux between the MMIO and the DATA
always_comb begin
    q_a = sample_in_range_data_a       ? data_q_a:
          sample_in_range_mmio_data_a  ? mmio_q_a:
                                         32'b0;
    q_b = sample_in_range_data_b       ? data_q_b:
          sample_in_range_mmio_data_b  ? mmio_q_b:
                                         32'b0;
    csr      = mmio_mem.csr;
    drct_out = mmio_mem.drct_out.out;
end

endmodule
