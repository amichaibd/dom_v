//-----------------------------------------------------------------------------
// Title         : input output interface contol
// Project       : DOM-V
//-----------------------------------------------------------------------------
// File          : io_ctrl.sv
// Author        : Amichai Ben-David
// Created       : 1/2020
//-----------------------------------------------------------------------------
// Description :
// all TOP level interface with the IP will go thrue the io_cntrl.
// will generate a pulse rd/wr request from the APB rd/wr
// has a global EN for gear feature.
//------------------------------------------------------------------------------
// Modification history :
//------------------------------------------------------------------------------

`include "dom_defines.sv"

module io_ctrl import dom_pkg::*;
               (input  logic [11:0]   PADDR ,  //from SOC 
                input  logic          HCLK  ,  //from SOC 
                input  logic          rst,     //from SOC 
                input  logic [31:0]   PWDATA , //from SOC 
                input  logic          PWRITE , //from SOC 
                input  logic          PSEL   , //from SOC
                input  logic          PENABLE, //from SOC 
                output logic [31:0]   PRDATA , //to   SOC 
                output logic          PREADY , //to   SOC 
                output logic [31:0]   wr_data, //to   MEM
                output logic [31:0]   address, //to   MEM
                output logic          wr_en  , //to   MEM
                output logic          rd_en  , //to   MEM
                input  logic [31:0]   rd_inst, //from inst_mem
                input  logic [31:0]   rd_data, //from data_mem
                input  logic [3:0]    gear   , //from data_mem
                output logic          glb_en , //to   all
                input  t_drct_out     drct_out,//to SOC
                output t_drct_in      drct_in  //from SOC
                );
//request
//logic PWRITE_2;
//logic PSEL_2;
//logic PENABLE_2;
//logic old_write_en;
//logic pre_write_en;
//logic old_read_en;
//logic pre_read_en;
//logic set_PREADY;
//logic rst_PREADY;
//logic pre_PREADY;

logic [3:0] count;
logic [3:0] next_count;
`DOM_RST_MSFF(count, next_count, HCLK, glb_en) //glb_en will rst
assign next_count = count + 4'b01;
assign glb_en = (count == gear) |  rst;


//request
//Sample input interface to SOC
////TODO - TEMP fixed latency
//`DOM_MSFF(      wr_data,       PWDATA,     HCLK )
//`DOM_MSFF(      address,      '0|PADDR,    HCLK )
//`DOM_MSFF(      PWRITE_2,      PWRITE,     HCLK )
//`DOM_MSFF(      PSEL_2,        PSEL,       HCLK )
//`DOM_EN_RST_MSFF(   PENABLE_2,     PENABLE,    HCLK ,   glb_en, rst | ~PENABLE)
////sample again to generate a pulse
//assign pre_write_en =  PWRITE_2 && PSEL_2 && PENABLE_2;
//assign pre_read_en  = ~PWRITE_2 && PSEL_2 && PENABLE_2;
//`DOM_EN_MSFF(old_write_en, pre_write_en,  HCLK,   glb_en)
//`DOM_EN_MSFF(old_read_en,  pre_read_en,   HCLK,   glb_en)
//assign wr_en = ~old_write_en &&  pre_write_en;
//assign rd_en = ~old_read_en  &&  pre_read_en;

//response
////TODO - TEMP fixed latency
//`DOM_EN_MSFF(pre_PREADY,  (wr_en||rd_en),   HCLK,   glb_en)
//logic old_pre_PREADY;
//`DOM_MSFF (old_pre_PREADY, pre_PREADY, HCLK)
//assign set_PREADY = rst || (pre_PREADY&(~old_pre_PREADY)) ;
//assign rst_PREADY = ~PENABLE && PSEL && PREADY;
//`DOM_EN_RST_MSFF (PREADY, 1'b1, HCLK, set_PREADY, rst_PREADY) //PREADY
////`DOM_MSFF(PRDATA, rd_data | rd_inst , HCLK )                  //PDATA

//request
logic pre_wr_en;
logic post_wr_en;
logic pre_rd_en;
logic post_rd_en;
logic [31:0] pre_address;
logic [31:0] pre_wr_data;

`DOM_MSFF(pre_wr_en,    PSEL&&PENABLE&&PWRITE,  HCLK)
`DOM_EN_MSFF(post_wr_en,    pre_wr_en,          HCLK,   glb_en)
`DOM_MSFF(pre_rd_en,  PSEL&&PENABLE&&(~PWRITE),   HCLK)
`DOM_EN_MSFF(post_rd_en, pre_rd_en,   HCLK,   glb_en)
assign wr_en = pre_wr_en&&(~post_wr_en);//Sample rise in dom clk
assign rd_en = pre_rd_en&&(~post_rd_en);//Sample rise in dom clk
`DOM_MSFF(pre_address,    '0|PADDR,  HCLK)
`DOM_MSFF(pre_wr_data,    PWDATA,  HCLK)
assign address = pre_address;
assign wr_data = pre_wr_data;
//response
logic pre_PREADY;
logic sample_rd_en;
logic post_PREADY;
logic [31:0] pre_PRDATA;

`DOM_EN_MSFF(sample_rd_en, rd_en, HCLK, glb_en)
`DOM_EN_MSFF(pre_PREADY, wr_en|sample_rd_en, HCLK, glb_en)//rd latency has extra cycle
`DOM_MSFF (post_PREADY, pre_PREADY  , HCLK)
assign PREADY = pre_PREADY&&(~post_PREADY);//Sample rise in SOC clk
`DOM_EN_MSFF(pre_PRDATA, rd_data|rd_inst, HCLK, glb_en)
assign PRDATA = pre_PRDATA;

endmodule
