// Includes
+incdir+../source/rtl/includes/

../source/rtl/data_mem.sv
../source/rtl/data_mem_wrap.sv
../source/rtl/dom_v.sv
../source/rtl/inst_mem_wrap.sv
../source/rtl/io_ctrl.sv
../source/rtl/core/alu.sv
../source/rtl/core/control.sv
../source/rtl/core/core.sv
../source/rtl/core/pc.sv
../source/rtl/core/registers.sv

../verif/dom_v_tb.sv
