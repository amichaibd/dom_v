//---------------------------------------------------------

#define vec (*(volatile unsigned int (*)[100])(0x10000000))
 
//---------------------------------------------------------

int main(void) { 
 
  int n  = 9;
  int swap;
  int a=0;
  int b=1;
  int c;



  for (int e=0; e<20; e++){
    vec[e] = 0;
  }
  for (int k=0; k<10; k++){
    vec[k] = 20-k;
  }

  for (int i=0;i<n;i++) {
    for (int j=0;j<n;j++) { 		
      if (vec[j]>vec[j+1]) {
        swap     = vec[j];
        vec[j]   = vec[j+1];
        vec[j+1] = swap;
      }
    }
  }

    for (int j=15;j<100;j++){
        c=a+b;
        a=b;
        b=c;
        vec[j] = c;
    }
return 0; 

}
