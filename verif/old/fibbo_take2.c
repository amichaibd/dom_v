//---------------------------------------------------------

#define vec (*(volatile unsigned int (*)[100])(0x00000F00))
// MEMORY
//    instrram     : ORIGIN = 0x00000000, LENGTH = 0x0800
//    dataram      : ORIGIN = 0x00000800, LENGTH = 0x0600
//    stack        : ORIGIN = 0x00000E00, LENGTH = 0x0100
//    MMIO         : ORIGIN = 0x00000F00, LENGTH = 0x0012
//    MMIO_MapOut  : ORIGIN = 0x00000F12, LENGTH = 0x0004
//    MMIO_PC      : ORIGIN = 0x00000F16, LENGTH = 0x0004
//    MMIO_REGSITER: ORIGIN = 0x00000F20, LENGTH = 0x0080
//---------------------------------------------------------

int main(void) { 
    int a = 0;
    int b = 1;
    int fibbo;
    int i =0;
    while (i<10){
        fibbo = a+b;
        a = b;
        b = fibbo;
        i++;
    }
    vec[0] = a;
    vec[1] = b;
    vec[2] = fibbo;
    vec[3] = 1;

return 0; 

}
