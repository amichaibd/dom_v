//---------------------------------------------------------

#define vec (*(volatile unsigned int (*)[100])(0x10000000))
 
//---------------------------------------------------------

int main(void) { 
    int a = 0;
    int b = 1;
    int c;
    int i =0;
    
    while (i<100){
        c = a+b;
        vec[i] = c;
        a = b;
        b = c;
        i++;
    }
    
return 0; 

}
